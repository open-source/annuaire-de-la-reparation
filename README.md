# Bienvenue dans le projet annuaire des réparateurs

Pour rappel les [Règles de gestion de l'activité](doc/dev/tasks-rules.md)

## Démarrer
- [Récupération du code source](doc/dev/checkout.md)

### Développement backend
- [Démarrer les conteneurs docker de développement](doc/dev/startbackendcontainers.md)
- [Gérer l'environnement symfony](doc/dev/backend-commands.md)

### Développement frontend admin
- [Pré-requis pour développer le front end](doc/dev/frontend-prerequisite.md)
- [Gérer l'environnement angular js](doc/dev/admin-frontend-commands.md)

### Développement frontend public
- [Pré-requis pour développer le front end](doc/dev/frontend-prerequisite.md)
- [Gérer l'environnement reactjs](doc/dev/public-frontend-commands.md)

### Utiliser l'application de base
- [Accéder à la version actuelle de l'application](doc/use/gettingstart.md)

## Structure du projet
- [admin-frontend](admin-frontend/README.md) : application angular.js pour la partie administration
- [backend](backend/README.md) : Bundles Symfony (API, admin, public) et Application Web de base
- [doc](doc/README.md) : documentation
- [docker-backend](docker-backend/README.md) : conteneur docker pour servir l'application Web de base du backend
- [public-frontend](public-frontend/README.md) : Composant ReactJS utilisé par le frontend public ainsi que les pages html de test

### Utiliser l'application de base
- [Accéder à la version actuelle de l'application](doc/use/gettingstart.md)

### Livraison de l'application
- [Procédure de livraison de l'application](doc/liv/livraison.md)

### Licence 
