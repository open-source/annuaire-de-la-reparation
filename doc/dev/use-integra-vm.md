# Utiliser la VM INTEGRA dans l'environnement IPSIS

## Démarrer la VM
La VM intégra se trouve dans le répertoire du projet Fournitures Externes\De Client\20170411 - VM de référence\2017-04-10_VM_REF_ADEME_CTOS7.1-PHP_7-6-18
Copier le fichier VM-REF-ADEME-PHP5.6.19-CTOS7 (dhcp active).zip

>>> la configuration réseau est à refaire dans la VM d'origine et pose des problèmes.
VM-REF-ADEME-PHP5.6.19-CTOS7.ova

## Connexion à la VM 
Le login est root /ademeref

## En cas de problème d'adresse MAC
Supprimer le cache de la mac adresse de la carte : 
rm /etc/udev/rules.d/70-persistent-net.rule 
Puis rebooter le serveur (shutdown –r 0)
