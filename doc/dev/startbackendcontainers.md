# Démarrage des conteneurs docker
## Pré-requis
### Docker
Docker doit être installé.

Remarques : 
- L'environnement classique est : PC Hôte sous windows et VM Linux avec docker.
- Il doit être possible d'utiliser docker avec Windows 10 mais celà n'a pas été testé.


### Windows + VM Linux
** Si le checkout a été fait sous Windows, il faut créer un lien symbolique vers le répertoire partagé, exemple : 

> ln -s /mnt/hgfs/dvp/projets/are/sources/are/ dvp/are

## Pour démarrer le backend
Se placer dans le répertoire racine du projet (il contient le fichier 'docker-compose.yml')

> cd dvp/are

Démarrer les conteneurs dockers
> docker-compose up -d

## Quelques fonctions utiles
### Exécuter une commande
Obtenir la version de php
> docker-compose exec site php --version

ou

> docker exec are_site_1 php --version

### Se mettre dans un bash en mode interactif
> docker exec -it are_site_1 bash

### Pour reconstruire l'image
> cd dvp/are

> docker-compose stop

> docker-compose build

> docker-compose rm 