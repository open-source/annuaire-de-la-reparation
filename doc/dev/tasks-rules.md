# Règles de gestion de son activité
Les règles de travail ci-dessous sont à appliquer en bonne intelligence afin d’atteindre les objectifs suivants :
- Assurer la visibilité sur les travaux réalisés et à mener,
- Assurer la fiabilité de la version livrée au client,
- Gagner du temps.


## Règle n°1
Les mails (y compris les mails provenant de JIRA) doivent être consultés au moins toutes les deux heures,

## Règle n°2
Toute réception d'une nouvelle tâche JIRA **avec estimation originale à 0** doit faire l'objet d'un chiffrage rapide (en 5, 10 minutes) sur le temps nécessaire à son traitement (temps estimé) en le justifiant a minima (description des travaux à réaliser). Ce chiffrage doit se faire avant de commencer toute nouvelle tâche ou le soir. Ce chiffrage ne doit jamais être corrigé par la suite.

## Règle n°3
Toute tâche jugée trop longue, trop complexe, hors scope, inutile ou imprécise doit être revalidée par le Scrum Master et/ou le Responsable de projet -> réaffecter la tâche avec mention "travaux à valider".

## Règle n°4
Suite à des développements tâche (et a minima le soir), il faut :
- tester en local ses développements, et créer éventuellement des tests unitaires ou d'intégration permettant de s'assurer de la complète résolution de la tâche,
- lancer les tests unitaires impactés par la modification,
- commiter le code impacté sans chercher à updater l’ensemble du code,
- un merge peut être à réaliser si d’autres personnes ont modifié les fichiers entre temps (en cas de merge, recommencer ces étapes à la première),
- clore la tâche dans la version logicielle adéquate en notant le temps consacré à son traitement.

## Règle n°5
Si votre tâche n'est pas présente dans JIRA :
- créer une nouvelle tâche en mettant les bonnes références (Tâche planning, SP, Mantis, Pmot, ...)
- appliquer la règle n°2

## Règle n°6
Votre travail de la semaine ne consiste qu'à traiter des tâches JIRA, la somme du temps consacré à ces tâches sur les 7 derniers jours glissants doit être d'une semaine. Tout écart devra être justifié. Une journée de travail = 8h et une semaine = 5 jours pour une personne à 100%.
Le temps impacté sur le projet doit correspondre au temps déclaré sur l'Intranet.

## Règle n°7
Une tâche JIRA ne doit pas contenir des travaux trop disparates. Créer plusieurs tâches dans ce cas.

## Règle n°8
Les tâches démarrées doivent être passées en état "En cours" (bouton « Démarrer la progression »). Mettre à jour tous les soirs le **temps passé** et le **temps restant** pour ces tâches. 

## Règle n°9
Avant le démarrage de chaque tâche, il est nécessaire de s'assurer auprès des encadrants de la bonne compréhension fonctionnelle et technique des éléments à réaliser.

## Règle n°10
Regarder JENKINS tous les matins pour s'assurer qu'aucun build n'est bloqué par une de ses modifications de la veille.

## Règle n° 11
Dans JIRA, citer les gens en utilisant la fonctionnalité associée (@xxx)