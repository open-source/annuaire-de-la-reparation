# Pré-requis pour le développement du frontend admin ou public
## Liste des pré-requis
- NodeJS 6.x ou supérieur
- NPM


## NodeJS 6.x+ et NPM
### Sous Windows
#### Installation de NodeJS 6.x 
Installer nodejs à partir du msi proposé à l'adresse https://nodejs.org/

exemple : https://nodejs.org/dist/v6.10.1/node-v6.10.1-x64.msi

#### Mise à jour de npm
En ligne de commande, exécuter la commande suivante : 
> npm install npm@latest -g


### Sous Ubuntu 16.04
Modifier le fichier `/etc/apt/sources.list.d/nodesource.list` pour y ajouter les dépots NodeJS :
> deb https://deb.nodesource.com/node_6.x xenial main

> deb-src https://deb.nodesource.com/node_6.x xenial main

Importer la clé GPG du nouveau dépot :
> curl -s https://deb.nodesource.com/gpgkey/nodesource.gpg.key | sudo apt-key add -

Installer la mise à jour de node :
> sudo apt-get update

> sudo apt-get install nodejs

La commande `node -v` doit maintenant indiquer que la version 6.X est utilisée.
