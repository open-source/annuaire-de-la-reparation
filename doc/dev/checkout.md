# Récupération du code source en ligne de commande
Se placer dans le répertoire destiné à accueillir le code source : 
- exemple pour windows : v:\projets\are\sources\
- exemple sous linux : /home/etude/dvp/are/sources/

Executer la commande suivante
> git clone ssh://git@gitlab.ext-itlink.fr:2033/ademe/are.git

# Configuration de git sous Windows
**Sous windows, il faut Désactiver l'"autorcrlf"** : 

Se mettre dans le répertoire ou se trouve la racine du repo git local et faire :
> git config --local core.autocrlf false

Et/ou le faire de facon globale : 
> git config --global core.autocrlf false