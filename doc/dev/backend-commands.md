# Gérer son environnement symfony

** Attention les conteneurs doivent être [démarrés](startbackendcontainers.md) **

## Corriger les permissions

Sous Linux, donner l'accès en écriture à Symfony au dossiers de cache et logs et au répertoire d'uploads
> chmod -R 777 backend/app/cache backend/app/logs backend/web/uploads

## Installer / Mettre à jour les modules Symfony

Executer la commande
> docker exec are_site_1 composer install

## Installer les ressources

Executer la commande
> docker-compose exec site php app/console assets:install web

## Générer la clé permettant la signature des jetons d'authentification JWT

Executer les commandes
> docker-compose exec site mkdir -p var/jwt

> docker-compose exec site openssl genrsa -out var/jwt/private.pem 4096

> docker-compose exec site openssl rsa -pubout -in var/jwt/private.pem -out var/jwt/public.pem


Un problème de droits peut exister sur le répertoire backend/var/*. Après avoir générer la clé pour la signature JWT, les commandes suivantes 
sont à exécuter depuis le répertoire are: 

> sudo chown etude:etude backend/var/

> sudo chmod 755 backend/var/jwt/*


## Initialiser la base de données

Créer la structure de la base de données en exécutant le script SQL `sql/v1_X_X/000-init.sql`. Ajouter les données de référence et un utilisateur pour le développement en exécutant les scripts `sql/v1_X_X/001-ref-data.sql` et `sql/v1_X_X/002-dev-user.sql`.

## Initialiser une base de données avec des réparateurs de tests

Lancer la commande :

> docker exec -i are_mysql_1 mysql --host=localhost --protocol=tcp --user=are --password=are are < sql/v1_0_0/are_dev.sql

Un dump plus récent de la base est disponible. Il comprend le script 001-ajout-cfe-nouvelles-regions.sql de la version v1.3.0: 

> docker exec -i are_mysql_1 mysql --host=localhost --protocol=tcp --user=are --password=are are < sql/v1_3_0/are_dev_1.3.0.sql

## Exécuter les tests unitaires

Exécuter le script SQL `sql/v1_X_X/00X-test-db.sql` pour créer la base de données de test puis l'initialiser avec le script `000-init.sql`. Les test unitaires s'exécutent avec la commande
> docker exec are_site_1 vendor/phpunit/phpunit/phpunit -c app/

Les données nécessaires aux tests sont automatiquement ajoutées lors du lancement des tests et nettoyées ensuite.

## Documentation de l'API

En environnement de développement, une version en ligne est disponible à l'adresse `http://localhost/app_dev.php/api/doc`. Cette version est automatiquement mise à jour. Il est possible d'exporter la documentation au format html avec la commande
> docker exec are_site_1 sh -c 'php app/console api:doc:dump --format=html --no-sandbox > api-doc/index.html'

## Exécuter l'import périodique

### Configuration du serveur FTP

Sur la machine hôte :
> apt install vsftpd

Activer l'accès en écriture en ajoutant dans le fichier `/etc/vsftpd.conf`
> write_enable=YES

Démarrer le serveur
> sudo systemctl start vsftpd

Par défaut les utilisateurs locaux peuvent se connecter au FTP.

## Configuration Symfony

Modifier le fichier `parameters.yml` avec l'adresse de la machine hote vu du conteneur et les informations de connexion d'un compte local. Modifier également les paramètres relatifs aux dossiers pour pointer vers des dossier accessibles sur le FTP.

L'import périodique est lancé de cette façon
> docker exec are_site_1 php app/console import:ftp

## Configuration des mails en dev
 
Modifications à ne pas commit :


Fichier backend/app/config/parameters.yml :
> Modifier le paramètre mailer_transport (mettre sntp au lieu de gmail)

Fichier backend/app/config/config.yml :
> Ajouter  `encryption: tls`  à la configuration de swiftmailer

*Note : Après avoir réalisé les modifications, **ne pas oublier de supprimer le cache** *



