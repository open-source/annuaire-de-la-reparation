# Charger un dump
Vider la base
> docker-compose stop\
> sudo rm -Rf mysql_data\
> docker-compose rm\
> docker-compose up -d\

Charger le dump
> mysql --host=localhost --protocol=tcp --user=are --password=are are < ./db-annuaire_rep.sql

# Accèder en mode admin
Via phpMyAdmin changer le mot de passe de l'utilisateur avec **Admin01!** soit la chaine suivante :  
$2y$13$f71izJ1jECMqltPywncGfOzNU5D0PEAhk6cQNZEUsgqRx8iyFi0j.



