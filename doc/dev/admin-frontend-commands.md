# Gérer l'environnement angular js

Les commandes présentées ici sont des commandes qui peuvent s'exécuter sous Windows ou sous Linux

## Se placer dans le bon répertoire
Toutes les commandes présentées ici sont à exécuter dans le répertoire du frontend administration

Exécuter la commande
> cd admin-frontend

## Développer et tester l'application
Installer les modules NPM requis par l'application
> npm install

Lancer l'application en mode de développement
> npm start

Si Webpack ne détecte pas automatiquement les changements sur les fichiers, augmenter le nombre de fichiers qu'il peut surveiller avec la commande :
> echo 524288 | sudo tee /proc/sys/fs/inotify/max_user_watches

## Installer l'application dans le bundle Symfony ARAdminBundle
En mode dévelopement:
> npm run build

En mode production:
> npm run build-prod

Pour le mode production, il faut ensuite adapter le nom des fichiers javascript dans le fichier `backend/src/ARAdminBundle/Resources/views/index.html.twig` pour correspondre au nom des fichiers générés lors de la compilation.

Mettre à jour les ressources dans Symfony
> docker exec are\_site\_1 php app/console assets:install web

Vider le cache Symfony
>sudo rm -rf ../backend/app/cache/*


### NOTE : 
Afin de simplifier l'installation en production, dans le répertoire admin-frontend exécuter le script install.sh
