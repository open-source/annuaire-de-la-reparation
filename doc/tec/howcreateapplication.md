# Mise en place de l'environnement logiciel

## Backend Symfony

### Création de l'application

L'application a été créée en suivant le [guide officiel](http://symfony.com/doc/current/setup.html#creating-symfony-applications-with-composer). Symfony 2.8 est utilisé.

> cd are/backend
> composer create-project symfony/framework-standard-edition are "2.8.*"

### Création des bundle

Les bundle ARAdminBundle, ARApiV1Bundle, ARCommonBundle et ARPublicBundle ont été créés via la console Symfony

> php app/console generate:bundle

### Bundles utilisés dans l'application

* FOSUserBundle : Gestion des comptes utilisateurs
* FOSRestBundle : Structure REST pour l'API
* NelmioApiDocBundle : Génération de la documentation de l'API à partir des annotations dans le code
* JWTAuthenticationBundle : Gestion des jetons JWT pour l'authentification à l'API
* JMSSerializerBundle : Sérialiseur JSON pour les entités dans les résultats des appels à l'API
* NelmioCorsBundle : Gestion des requêtes CORS pour l'API
* Assetic : Gestion des ressources
* Doctrine : ORM
* Twig : Moteur de template
* PHPUnit : Tests unitaires

## Application React pour le frontend public

### Modules utilisés dans l'application React

* react-linkify : Rendre les liens cliquable dans une portion texte
* react-url-query : Gestion des paramètres d'url
* react-share : Partage sur les réseaux sociaux

## Application Angular pour le frontend admin

### Création de l'application Angular

Le [guide officiel Angular CLI](https://angular.io/docs/ts/latest/cli-quickstart.html) a été suivi pour la création de l'application.
Ce guide nécessite Node 6.9 et NPM 3 qui ne sont pas disponibles par défaut sur Ubuntu 16.04


Le nouveau projet Angular a ensuite pu être créé. Il y a [un bug npm](https://github.com/npm/npm/issues/15034) lors de la création du projet sur un dossier partagé VMWare, il faut donc le créer en dehors puis le déplacer ensuite :

> cd ~

> ng new admin-frontend

> cd dvp/are

> mv ~/admin-frontend .

Pour vérifier le fonctionnement de l'application :

> cd admin-frontend

> ng serve --open

Angular est utilisé en version 4.

### Modules utilisés dans l'application Angular

* Angular 4
* angular2-jwt : Authentification à l'API via token JWT
* angular2-modal : Affichage de modales
* bootstrap-sass & ng-bootstrap : CSS Bootstrap et intégration angular
* rxjs : Implémentation JS/TypeScript des Reactive Extension utilisées pour la gestion des événements
* ngx-datatable : Pour les tableaux de données
* angular-linky : Rendre les liens cliquable dans une portion texte
* moment : Opérations sur les dates
* ngx-mydatepicker : Calendrier de sélection de dates pour les formulaires
* angular-2-dropdown-multiselect : Champ de sélection multiple pour les formulaires

## Intégration de l'application Angular avec Symfony

Il faut modifier le répertoire de compilation de l'application Angular pour la placer dans les ressources Symfony. La propriété `outDir` du fichier `admin-frontend/.angular-cli.json` est modifiée pour pointer vers les ressources Symfony :
> "outDir": "../backend/src/ARAdminBundle/Resources/public/js"

Le template Symfony `index.html.twig` du bundle `ARAdminBundle` a été modifié pour inclure les scripts de l'application Angular. Il faut ensuite compiler l'application Angular et installer les ressources :
> cd admin-frontend

> ng build

> cd ../backend

> php app/console assets:install web

Lors de l'ouverture de `http://localhost/admin`, l'application Angular est servie par Symfony.
