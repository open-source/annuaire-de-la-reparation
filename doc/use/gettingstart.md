# Démarrer avec l'annuaire des réparateurs

## Version de développement 

Le front public est accessible à l'adresse : 

http://localhost:10180

Le back office est accessible selon le type de développement à l'adresse:

http://localhost:10180/admin ou http://localhost:4200

## Connexion aux versions de développement et de démonstration
Le login est `test@test.fr` et le mot de passe `Passw0$!`