# Livraison de l'application

## Avant la génération de l'archive

### Compiler et générer les ressources Angular (partie Admin) dans Symfony

Lancer les commandes suivantes : 

> cd ~/dvp/are/sources/are/admin-frontend  
> ng build --base-href=/admin --target=development --environment=prod

### Compiler et générer les ressources React (partie Publique) dans Symfony

Lancer les commandes suivantes : 

> cd ~/dvp/are/sources/are/public-frontend  
> ./build.sh

Ajouter les changements sur git s'il y en a. 
Il faut également s'assurer que le fichier de configuration /are/backend/app/config/parameters.yml.dist est à jour (tous les paramètres ont bien été ajoutés dans ce fichier).

## Génération de l'archive

*Il faut s'assurer au préalable que la version sur git est à jour et que c'est bien celle que l'on souhaite livrer.*

Dans le répertoire ~/dvp/are/sources/are, ouvrir avec un éditeur de texte le fichier "package.sh" et mettre à jour le numéro de version : variable NUM_VERSION. Ensuite exécuter les commandes suivantes:

> cd ~/dvp/are/sources/are
> ./package.sh

*Il faudra entrer son identifiant et un mot de passe pour les accès à Git Lab*.  

Vérifier que chaque étape s'est bien passée et appuyer sur une touche quelconque pour continuer l'exécution du script, jusqu'à la fin du script.  

Une archive ARE-LIV01-{NUM_VERSION}.tar.gz est générée dans le répertoire ~/dvp/are/sources/are/backend/target.  
Cette archive contient le livrable qui servira à l'installation avec l'application Symfony et toutes ses ressources compilés.

Une archive ARE-LIV01-SRC-{NUM_VERSION}.tar.gz est générée dans le répertoire ~/dvp/are/release/.  
Cette archive contient le code source de l'application Symfony (les dépendances et ressources ne sont pas compilées) avec les codes sources pour Angular et React.

Ces deux archives sont à livrer au client.

## Mettre à jour la version de l'application

Pour mettre à jour le numéro de version dans l'application, il faut modifier le fichier "parameters.yml.dist" et mettre à jour le paramètre *app\_version*

## Taguer la version dans GIT

Pour tagguer la version courante, il faut exécuter la commande suivante: 

> git tag v{NUM_VERSION}

Par exemple: 

> git tag v1_0_0

## Mettre à jour Mantis

Sur Mantis, il faut mettre à jour l'historique des changements et créer une nouvelle version. 

Pour cela:

1 - Se connecter à Mantis  
2 - Aller dans le menu *Administration* et créer la version courante si elle n'existe pas déjà  
3 - Fermer les Mantis traités durant cette version en y indiquant la version associée.  
4 - Dans le menu *Administration* marquer la version comme publiée  
5 - Vérifier dans le menu *Historique des changements* vérifier que l'historique des changements est à jour.

## Mettre le livrable dans le répertoire de livraison

1 - Créer un nouveau répertoire dans le répertoire "*Livraison\Vers Client*" et y ajouter les livrables.  
2 - Créer un PV si besoin et l'envoyer.
