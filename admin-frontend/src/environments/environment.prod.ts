export const environment = {
  production: true,
  deployUrl: "/bundles/aradmin/js/",
  apiRoot: "/api/",
  version: "1.3.1"
};
