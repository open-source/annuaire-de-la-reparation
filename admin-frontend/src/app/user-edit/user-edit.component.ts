import { Component, OnInit, ViewChild } from '@angular/core';
import { Location } from '@angular/common';
import { ActivatedRoute, Router, Params } from "@angular/router";

import { UserFormComponent } from "app/user-form/user-form.component";
import { User } from "app/_models/user";
import { UserService } from "app/_services/user.service";
import { ServerErrorService } from "app/_services/server-error.service";

/** Composant de modification d'un utilisateur. */
@Component({
  selector: 'app-user-edit',
  templateUrl: './user-edit.component.html',
  styleUrls: ['./user-edit.component.scss']
})
export class UserEditComponent implements OnInit {
  /** Utilisateur à modifier */
  user: User;
  /** Formulaire */
  @ViewChild(UserFormComponent)
  private formComponent: UserFormComponent;

  /**
   * Constructeur.
   * 
   * @param route
   * @param router
   * @param userService
   * @param modalService
   */
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private location: Location,
    private userService: UserService,
    private serverErrorService: ServerErrorService
  ) { }

  /**
   * Récupération de l'utilisateur à modifier.
   */
  ngOnInit() {
    this.route.params
      .switchMap((params: Params) => this.userService.getUser(params['id']))
      .subscribe(u => this.user = u as User);
  }

  /**
   * Soumission du formulaire : modification de l'utilisateur.
   * 
   * @param event 
   */
  onSubmitUser(event: any) {
    let user: User = event.user as User;
    let departments: string[] = event.departments as string[];

    this.userService.updateUser(user, departments).subscribe(
      response => this.router.navigate(['users', this.user.id]),
      err => {
        if (err.status == 400) {
          this.formComponent.showFormErrors(err);
        }
        else {
          this.serverErrorService.show(err);
        }
      }
    );
  }

  /**
   * Retour à la page précédente.
   */
  onBack() {
    this.location.back();
  }
}
