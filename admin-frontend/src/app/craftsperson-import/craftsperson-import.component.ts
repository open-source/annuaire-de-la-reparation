import { Component, ViewChild } from '@angular/core';
import { RequestOptions } from '@angular/http';
import { Modal } from 'angular2-modal/plugins/bootstrap';

import { CraftspersonService } from '../_services/craftsperson.service';
import { CraftspersonImport } from '../_models/craftsperson-import';
import { ImportLog } from '../_models/import-log';
import { ImportStatus } from '../_enums/import-status';
import { Asset } from '../_utils/asset';

/** Composant d'import d'un fichier de réparateurs. */
@Component({
    templateUrl: 'craftsperson-import.component.html',
    styleUrls: ['craftsperson-import.component.css']
})
export class CraftspersonImportComponent {
    /** Champ de sélection de fichier à importer */
    @ViewChild('fileInput') fileInput: any;
    /** Référence vers la classe de statut de l'import pour utilisation dans la vue */
    importStatus = ImportStatus;
    /** Référence vers la classe de gestion des assets */
    asset = Asset;
    /** Statut d'import courant */
    currentImportStatus = ImportStatus.None;
    /** Logs d'import */
    importLogs: ImportLog[];
    /** Nombre de lignes importées (ajoutées, modifiées ou supprimées) */
    nbImportedLines: number;
    /**Nombre de lignes totales */
    nbTotalLines: number;

    /**
     * Constructeur.
     * 
     * @param craftspersonService 
     * @param modalService
     */
    constructor(
        private craftspersonService: CraftspersonService,
        private modalService: Modal) { }

    /**
     * Modification du fichier sélectionné.
     * 
     * @param event 
     */
    fileChangeEvent(event) {
        let fileList: FileList = event.target.files;
        if (fileList.length > 0) {
            let file: File = fileList[0];
            this.importLogs = null;
            this.currentImportStatus = ImportStatus.Checking;
            this.craftspersonService.importCraftspersons(file, true).subscribe(
                result => this.showConfirmModal(result, file),
                err => this.showErrors(err)
            );
        }
    }

    /**
     * Affichage des erreurs d'import.
     * 
     * @param logs 
     */
    private showErrors(logs: ImportLog[]) {
        this.currentImportStatus = ImportStatus.Error;
        this.importLogs = logs;
    }

    /**
     * Affichage de la fenêtre de confirmation d'import.
     * 
     * @param checkResult 
     * @param file 
     */
    private showConfirmModal(checkResult: CraftspersonImport, file: File) {
        this.modalService.confirm()
            .size('lg')
            .isBlocking(true)
            .showClose(true)
            .title('Confirmation d\'import')
            .body(`
                <p>L'import va apporter les modifications suivantes :</p>
                <ul>
                    <li>` + checkResult.created + ` fiches créées</li>
                    <li>` + checkResult.updated + ` fiches mises à jour</li>
                    <li>` + checkResult.deleted + ` fiches supprimées</li>
                </ul>
                <p>Souhaitez-vous effectuer l'import ?</p>
            `)
            .okBtn('Importer')
            .cancelBtn('Annuler')
            .open()
            .then(dialog => dialog.result)
            .then(result => this.doImport(file))
            .catch(err => {
                this.currentImportStatus = ImportStatus.Canceled;
                this.importLogs = checkResult.logs;
            });
    }

    /**
     * Lancement de l'import.
     * 
     * @param file 
     */
    private doImport(file: File) {
        this.currentImportStatus = ImportStatus.Importing;
        this.craftspersonService.importCraftspersons(file, false).subscribe(
            result => {
                this.currentImportStatus = ImportStatus.Done;
                if(result.logs && result.logs.length) {
                    this.currentImportStatus = ImportStatus.Incomplete;
                    this.importLogs = result.logs;
                    this.nbImportedLines = result.created + result.updated + result.deleted;
                    this.nbTotalLines = result.created + result.updated + result.deleted + result.ignored + result.invalid;
                }
            },
            err => this.showErrors(err)
        );
    }
}