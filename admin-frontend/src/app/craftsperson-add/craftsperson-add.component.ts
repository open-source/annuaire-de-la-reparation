import { Component, ViewChild } from '@angular/core';
import { Router } from '@angular/router';

import { Craftsperson } from 'app/_models/craftsperson';
import { CraftspersonService } from 'app/_services/craftsperson.service';
import { CraftspersonFormComponent } from 'app/craftsperson-form/craftsperson-form.component';
import { ServerErrorService } from "app/_services/server-error.service";

/** Composant de saisie d'un nouveau réparateur */
@Component({
  selector: 'app-craftsperson-add',
  templateUrl: './craftsperson-add.component.html',
  styleUrls: ['./craftsperson-add.component.scss']
})
export class CraftspersonAddComponent {
  /** Statut d'upload du logo */
  private logoUploaded: boolean;
  /** Nouveau réparateur */
  craftsperson : Craftsperson;
  /** Formulaire */
  @ViewChild(CraftspersonFormComponent)
  private formComponent: CraftspersonFormComponent;

  /**
   * Constructeur.
   */
  constructor(
    private craftspersonService: CraftspersonService,
    private router: Router,
    private serverErrorService: ServerErrorService
  ) {
    // Nouveau réparateur
    this.craftsperson =  {
      id: 0,
      siret: undefined,
      name: '',
      nafCode: undefined,
      cmaCode: undefined,
      zipCode: '',
      zipCodeLabel: '',
      address1: '',
      address2: '',
      phone: '',
      email: '',
      website: '',
      isEnabled: true,
      isError: false,
      isReparactor: false,
      isUpdated: true,
      reparactorCertificates: '',
      reparactorDescription: '',
      reparactorHours: '',
      reparactorServices: '',
      updateDate: '',
      creationDate: '',
      logoFile: '',
      otherInfo: ''
    };
  }

  /**
   * Soumission du formulaire : ajout d'un réparateur.
   * 
   * @param event 
   */
  onSubmitCraftsperson(event: any) {
    let saveCraftsperson: Craftsperson = event.craftsperson;
    let nafCodes: string = event.nafCodes;
    let logo: File = event.logo;
    
    let addedCraftsperson;
    this.craftspersonService.addCraftsperson(saveCraftsperson, nafCodes).subscribe(
      craftsperson => {
        addedCraftsperson = craftsperson;
        if(logo) {
          this.craftspersonService.updateCraftspersonLogo(craftsperson, logo).subscribe(
            success => this.router.navigate(['craftspersons', addedCraftsperson.id]),
            error => this.serverErrorService.show(error)
          );
        }
        else {
          this.router.navigate(['craftspersons', addedCraftsperson.id]);
        }
      },
      err => {
        if (err.status == 400) {
          this.formComponent.showFormErrors(err);
        }
        else {
          this.serverErrorService.show(err);
        }
      }
    );
  }
}
