import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { ActivatedRoute, Params, Router } from "@angular/router";
import { Modal } from "angular2-modal/plugins/bootstrap";

import { UserService } from "app/_services/user.service";
import { User } from "app/_models/user";
import { AuthenticationService } from "app/_services/auth.service";
import { Role } from "app/_enums/role";
import { ServerErrorService } from "app/_services/server-error.service";

/** Composant d'affichage d'un utilisateur */
@Component({
  selector: 'app-user-view',
  templateUrl: './user-view.component.html',
  styleUrls: ['./user-view.component.scss']
})
export class UserViewComponent implements OnInit {
  /** Utilisateur affiché */
  public user: User;

  /**
   * Constructeur.
   */
  constructor(
    private router: Router,
    private userService: UserService,
    private authService: AuthenticationService,
    private modalService: Modal,
    private route: ActivatedRoute,
    private location: Location,
    private serverErrorService: ServerErrorService
  ) { }

  /**
   * Récupération de l'utilisateur à afficher.
   */
  ngOnInit() {
    // Utilisateur à afficher
    this.route.params
      .switchMap((params: Params) => this.userService.getUser(params['id']))
      .subscribe(
      u => this.user = u,
      err => this.serverErrorService.show(err));
  }

  /**
   * Formattage des départements pour affichage.
   * 
   * @return string
   */
  public getDepartments() {
    return this.user.codeDepartment.map(d => d.code).join(', ');
  }

  /**
   * Formattage du rôle pour affichage.
   * 
   * @return string
   */
  public getRole() {
    let roleString = '';
    if(this.user.roles.length > 0) {
      switch(this.user.roles[0]) {
        case Role[Role.ROLE_CMA] :
        roleString = 'CMA';
        break;
        case Role[Role.ROLE_ADMIN] :
        roleString = 'Administrateur'
        break;
        default:
        roleString = this.user.roles[0];
        break;
      }
    }
    return roleString;
  }

  /**
   * Indique si l'utilisateur affiché est l'utilisateur connecté.
   * 
   * @return vrai si l'utilisateur connecté est celui affiché
   */
  public isCurrentUser() {
    return this.authService.getLoggedInUser().id == this.user.id;
  }

  /**
   * Suppression de l'utilisateur.
   * 
   * @param id 
   */
  onDelete() {
    // Modale de confirmation
    this.modalService.confirm()
      .title('Confirmation de suppression')
      .body('Supprimer cet utilisateur ?')
      .okBtn('Supprimer')
      .cancelBtn('Annuler')
      .open()
      .then(dialog => dialog.result)
      // Suppression effective en cas de confirmation
      .then(result => this.userService.deleteUser(this.user.id).subscribe(
        response => {
          this.router.navigate(['/users']);
        },
        err => this.serverErrorService.show(err)
      ));
  }

  /**
   * Retour à la page précédente.
   */
  onBack() {
    this.location.back();
  }
}
