import { Component } from '@angular/core';
import { environment } from "environments/environment";

/** Composant pied de page */
@Component({
  selector: 'footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent {
  /** Version de l'application */
  version = environment.version;

  /**
   * Constructeur.
   */
  constructor() { }

}
