import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { Modal, OneButtonPreset } from 'angular2-modal/plugins/bootstrap';
import { DialogRef } from "angular2-modal/esm";

import { Subject } from "rxjs";

import { DepartmentService } from 'app/_services/department.service';
import { UserService } from 'app/_services/user.service';
import { User } from 'app/_models/user';
import { Department } from 'app/_models/department';
import { Observable } from "rxjs/Observable";
import { AuthenticationService } from "app/_services/auth.service";
import { AppConstants } from "app/app.constants";
import { ServerErrorService } from "app/_services/server-error.service";

/** Composant de liste des utilisateurs. */
@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {
  /** Template pour une en-tête avec filtre texte */
  @ViewChild('filterTmpl') filterTmpl: TemplateRef<any>;
  /** Template pour une en-tête avec filtre booléen */
  @ViewChild('boolFilterTmpl') boolFilterTmpl: TemplateRef<any>;
  /** Template pour une en-tête avec filtre sur le département */
  @ViewChild('depFilterTmpl') depFilterTmpl: TemplateRef<any>;
  /** Template pour une colonne affichant des codes NAF */
  @ViewChild('arrayTmpl') arrayTmpl: TemplateRef<any>;
  /** Template pour une colonne affichant un booléen */
  @ViewChild('boolTmpl') boolTmpl: TemplateRef<any>;
  /** Template pour une colonne affichant des actions */
  @ViewChild('actionTmpl') actionTmpl: TemplateRef<any>;
  /** Description des colonnes de la table */
  columns = [];
  /** Utilisateurs affichés */
  users: User[];
  /** Nombre total d'utilisateurs correspondants au filtre courant */
  count = 0;
  /** Offset lié à la page courante */
  offset = 0;
  /** Nombre de réparateurs par page */
  limit = 50;
  /** Colonne de tri */
  sortColumn: string = null;
  /** Direction du tri */
  sortDir: string = null;
  /** Statut de chargement des données de la table */
  loading = false;
  /** Utilisateurs sélectionnés */
  selected: User[] = [];
  /** Départements */
  departments: Department[];
  /** Filtres de recherche sur les colonnes, il faut initialiser les propriétés des listes déroulantes */
  searchFilters: any = { codeDepartment: '', isMailContact: '' };
  /** Sujet d'écoute pour les événements de filtrage */
  keyUpSubject = new Subject<string>();
  /** Champ de sélection de page */
  pagerInput: any;

  /**
   * Constructeur.
   * 
   * @param userService
   * @param modalService
   */
  constructor(
    private authService: AuthenticationService,
    private userService: UserService,
    private departmentService: DepartmentService,
    private modalService: Modal,
    private serverErrorService: ServerErrorService
  ) { }

  /**
   * Initialisation des colonnes de la table et chargement des données de la table.
   */
  ngOnInit() {
    // Ajout d'un délai avant prise en compte du filtre
    this.keyUpSubject
      .debounceTime(AppConstants.FILTER_TYPE_DELAY_MS)
      .distinctUntilChanged()
      .subscribe((event) => this.onFilter(event));

    // Chargement des départements pour le filtre de la table
    this.departmentService.getDepartments().subscribe(response => this.departments = response);

    // Colonnes de la table
    this.columns = [
      { checkboxable: true, sortable: false, width: 30, canAutoResize: false, draggable: false, resizeable: false },
      { prop: 'lastname', name: 'Nom', headerTemplate: this.filterTmpl },
      { prop: 'firstname', name: 'Prénom', headerTemplate: this.filterTmpl },
      { prop: 'email', name: 'Adresse mail', headerTemplate: this.filterTmpl },
      { prop: 'phone', name: 'Téléphone', headerTemplate: this.filterTmpl },
      { prop: 'codeDepartment', name: 'Département(s)', sortable: false, headerTemplate: this.depFilterTmpl, cellTemplate: this.arrayTmpl },
      { prop: 'isMailContact', name: 'Contact par mail', headerTemplate: this.boolFilterTmpl, cellTemplate: this.boolTmpl },
      { prop: 'id', name: 'Actions', sortable: false, cellTemplate: this.actionTmpl }
    ];

    // Chargement des données de la table
    this.loadTableData();
  }

  /**
   * Retourne les départements sous forme de chaine de caractères pour une ligne de la table.
   * 
   * @param departments 
   */
  getDepartmentsString(departments: Department[]): string {
    if (departments.length > 5) {
      return departments.length + ' départements';
    }
    return departments.map(d => d.name).join(', ');
  }

  /**
   * Chargement des données à afficher dans la table en fonction des paramètres de pagination, tri et filtrage.
   */
  private loadTableData() {
    // Vidage de la sélection
    this.selected = [];
    // Indicateur de chargement
    this.loading = true;

    const start = this.offset * this.limit;
    this.userService.getUsers(start, this.limit, this.sortColumn, this.sortDir, this.searchFilters['lastname'], this.searchFilters['firstname'], this.searchFilters['email'], this.searchFilters['phone'], this.searchFilters['codeDepartment'], this.searchFilters['isMailContact']).subscribe(response => {
      this.users = response.users as User[];
      this.count = response.total;

      const end = start + this.limit;
      const users: User[] = [...this.users];

      for (let i = start; i < end; i++) {
        users[i] = response.users[i - start];
      }

      this.users = users;
      this.loading = false;
    },
      err => this.serverErrorService.show(err));
  }

  /**
   * Changement de page affichée.
   * 
   * @param event 
   */
  onPage(event: any) {
    // Mise à jour des paramètres de pagination
    this.offset = event.offset;
    this.limit = event.limit;

    // Rechargement des données affichées
    this.loadTableData();
  }

  /**
   * Changement de colonne ou direction de tri.
   * 
   * @param event 
   */
  onSort(event: any) {
    // Mise à jour des paramètres de tri
    const sort = event.sorts[0];
    this.sortColumn = sort.prop;
    this.sortDir = sort.dir;

    // Retour en 1er page
    this.offset = 0;

    // Rechargement des données affichées
    this.loadTableData();
  }

  /**
   * Filtrage d'une colonne.
   * 
   * @param event 
   */
  onFilter(event: any) {
    this.offset = 0;
    this.loadTableData();
  }

  /**
   * Nettoyage des filtres et rechargement de la grille.
   */
  onClearFilters() {
    Object.keys(this.searchFilters).forEach(key => {
      this.searchFilters[key] = '';
    });
    this.loadTableData();
  }

  /**
   * Sélection d'utilisateurs.
   * 
   * @param selected 
   */
  onSelect({ selected }) {
    this.selected.splice(0, this.selected.length);
    this.selected.push(...selected);
  }

  /**
   * Suppression de la sélection.
   */
  onDeleteSelected() {
    // Empêcher la suppression de son propre compte
    if (this.selected.some(u => u.id == this.authService.getLoggedInUser().id)) {
      this.modalService.alert()
        .title('Suppression impossible')
        .body('La supression de votre compte n\'est pas autorisée. Veuillez déselectionner votre compte avant de poursuivre.')
        .open();
    }
    else {
      this.deleteSelected();
    }
  }

  /**
   * Suppression effective de la sélection.
   */
  private deleteSelected() {
    this.modalService.confirm()
      .title('Confirmation de suppression')
      .body('Supprimer le(s) utilisateur(s) sélectionné(s) ?')
      .okBtn('Supprimer')
      .cancelBtn('Annuler')
      .open()
      .then(dialog => dialog.result)
      // Suppression effective en cas de confirmation
      .then(result => {
        const requests: any = Observable.from(this.selected.map(user => this.userService.deleteUser(user.id)));

        let waitModal: DialogRef<OneButtonPreset>;
        const subscription = requests.concatAll().subscribe(
          response => { },
          err => {
            // Erreur
            this.loadTableData();
            if (waitModal) {
              waitModal.close();
            }
            this.serverErrorService.show(err);
          },
          () => {
            // Fin des requêtes
            this.loadTableData();
            this.modalService.alert().title('Utilisateur(s) supprimé(s)').body('Utilisateur(s) supprimé(s) avec succès.').open();
            if (waitModal) {
              waitModal.close();
            }
          });

        // Afficher une modale en attendant la fin du traitement, avec possibilité d'annulation
        this.modalService.alert().title('Traitement en cours').body('Suppression en cours, merci de patienter...').okBtn('Annuler')
          .isBlocking(true)
          .open()
          .then(dialog => {
            waitModal = dialog;
            return dialog.result;
          })
          .then(result => {
            // Clic sur Annuler
            subscription.unsubscribe()
            this.loadTableData();
          });
      });
  }

  /**
   * Nombre de pages.
   */
  get pageCount() {
    return Math.ceil(this.count / this.limit);
  }

  /**
   * Page courante.
   */
  get currentPage() {
    return this.offset + 1;
  }

  /**
   * Numéro de page entrée dans le champ de sélection.
   */
  get pageValue() {
    if (typeof (this.pagerInput) === 'number' && (this.pagerInput % 1) === 0 && this.pagerInput > 0 && this.pagerInput <= this.pageCount) {
      let result = this.pagerInput;
      this.pagerInput = null;
      return result;
    }
    else {
      return this.currentPage;
    }
  }
}
