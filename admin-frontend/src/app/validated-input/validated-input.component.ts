import { Component, OnChanges, Input, ElementRef, ViewChild } from "@angular/core";
import { FormControl, AbstractControl } from "@angular/forms";

export interface FormError {
  error: string;
  params: any;
}

/** Champ de formulaire avec messages de validation */
@Component({
  selector: 'validated-input',
  templateUrl: './validated-input.component.html'
})
export class ValidatedInputComponent implements OnChanges {
  /** Label */
  @Input()
  labelText: string = '';
  /** Erreurs à afficher */
  @Input()
  inputErrors: any;
  /** Messages d'erreur */
  @Input()
  errorDefs: any;
  /** Statut de modification */
  @Input()
  isPristine: boolean;
  /** Message d'erreur affiché */
  errorMessage: string = '';

  /**
   * Affichage des erreurs sur modification du champ.
   * 
   * @param changes 
   */
  ngOnChanges(changes: any) {
    this.errorMessage = '';
    if (this.inputErrors && !this.isPristine) {
      Object.keys(this.errorDefs).some(key => {
        if (this.inputErrors[key]) {
          this.errorMessage = this.errorDefs[key];
          return true;
        }
      });
    }
  }
}