import { Component, OnInit, ViewChild } from '@angular/core';
import { Location } from '@angular/common';
import { ActivatedRoute, Params, Router } from '@angular/router';

import { Craftsperson } from 'app/_models/craftsperson';
import { CraftspersonService } from "app/_services/craftsperson.service";
import { CraftspersonFormComponent } from "app/craftsperson-form/craftsperson-form.component";
import { ServerErrorService } from "app/_services/server-error.service";

/** Edition d'un réparateur existant */
@Component({
  selector: 'app-craftsperson-edit',
  templateUrl: './craftsperson-edit.component.html',
  styleUrls: ['./craftsperson-edit.component.scss']
})
export class CraftspersonEditComponent implements OnInit {
  /** Statut d'upload du logo */
  private logoUploaded: boolean;
  /** Réparateur à modifier */
  craftsperson: Craftsperson;
  /** Formulaire */
  @ViewChild(CraftspersonFormComponent)
  private formComponent: CraftspersonFormComponent;

  /**
   * Constructeur.
   * 
   * @param route 
   * @param craftspersonService 
   */
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private location: Location,
    private craftspersonService: CraftspersonService,
    private serverErrorService: ServerErrorService
  ) { }

  /**
   * Initialisation du composant.
   */
  ngOnInit() {
    this.route.params
      .switchMap((params: Params) => this.craftspersonService.getCraftsperson(params['id']))
      .subscribe(craftsperson => {
        this.craftsperson = craftsperson as Craftsperson;
      });
  }

  /**
   * Soumission du formulaire : mise à jour du réparateur.
   * 
   * @param event 
   */
  onSubmitCraftsperson(event: any) {
    let saveCraftsperson: Craftsperson = event.craftsperson;
    let nafCodes: string = event.nafCodes;
    let logo: File = event.logo;

    this.craftspersonService.updateCraftsperson(saveCraftsperson, nafCodes).subscribe(
      response => this.router.navigate(['craftspersons', saveCraftsperson.id]),
      err => {
        if (err.status == 400) {
          this.formComponent.showFormErrors(err);
        }
        else {
          this.serverErrorService.show(err);
        }
      }
    );

    if(logo) {
      this.craftspersonService.updateCraftspersonLogo(saveCraftsperson, logo).subscribe(
        response => this.logoUploaded = true
      );
    }
  }

  /**
   * Retour à la page précédente.
   */
  onBack() {
    this.location.back();
  }

}
