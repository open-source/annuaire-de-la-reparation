/**
 * L'énumération ExportStatus définit les différents états de l'export de réparateurs.
 * 
 * @author vdouillet
 */
export enum ExportStatus {
    Working,
    Success,
    Error,
    None
}