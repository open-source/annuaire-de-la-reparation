/**
 * L'énumération ImportStatus définit les différents états d'import de réparateurs.
 * 
 * @author vdouillet
 */
export enum ImportStatus {
    Checking,
    Importing,
    Done,
    Canceled,
    Error,
    Incomplete,
    None
}