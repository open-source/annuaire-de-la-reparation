/**
 * L'énumération Role définit les différents rôles utilisateurs.
 * 
 * @author vdouillet
 */
export enum Role {
    ROLE_CMA,
    ROLE_ADMIN
}