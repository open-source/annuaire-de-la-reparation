import { Component } from '@angular/core';
import { Asset } from "app/_utils/asset";

/** Composant en-tête. */
@Component({
    selector: 'header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.scss']
})
export class HeaderComponent {
    /** Référence vers la gestion des ressources */
    asset = Asset;

    /** 
     * Constructeur. 
     */
    constructor() {
    }
}