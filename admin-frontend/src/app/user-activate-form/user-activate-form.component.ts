import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from "@angular/forms";
import { AppConstants } from "app/app.constants";

/** Composant formulaire d'activation d'un utilisateur */
@Component({
  selector: 'app-user-activate-form',
  templateUrl: './user-activate-form.component.html',
  styleUrls: ['./user-activate-form.component.scss']
})
export class UserActivateFormComponent {
  /** Champs du formulaire */
  activateForm: FormGroup;
  /** Utilisateur en cours d'édition */
  @Input() submitLabel: string;
  /** Soumission du formulaire */
  @Output() submitActivate: EventEmitter<any> = new EventEmitter<any>();

  /**
   * Constructeur.
   */
  constructor(
    private formBuilder: FormBuilder
  ) {
    // Initialisation des champs du formulaire
    this.activateForm = this.formBuilder.group({
      password: ['', [Validators.required, Validators.pattern(AppConstants.PASSWORD_REGEX)]],
      confirmPassword: ['', [Validators.required, this.matchOtherValidator('password')]]
    });
  }

  /**
   * Validateur d'égalité entre deux champs de formulaire.
   * 
   * @param otherControlName
   */
  matchOtherValidator(otherControlName: string) {

    let thisControl: FormControl;
    let otherControl: FormControl;

    return function matchOtherValidate(control: FormControl) {

      if (!control.parent) {
        return null;
      }

      // Initialisation des contrôles à valider.
      if (!thisControl) {
        thisControl = control;
        otherControl = control.parent.get(otherControlName) as FormControl;
        if (!otherControl) {
          throw new Error('matchOtherValidator(): other control is not found in parent group');
        }
        otherControl.valueChanges.subscribe(() => {
          thisControl.updateValueAndValidity();
        });
      }

      if (!otherControl) {
        return null;
      }

      // Vérification d'égalité
      if (otherControl.value !== thisControl.value) {
        return {
          matchOther: true
        };
      }

      return null;
    }
  }

  /**
   * Soumission du formulaire.
   */
  onSubmit(form, event: Event) {
    const password = this.activateForm.value.password;

    this.submitActivate.emit({
      password: password
    });
  }

  /**
   * Affichage des erreurs de formulaire.
   * 
   * @param error 
   */
  showFormErrors(error: any) {
    if (error.status == 400) {
      const data = error.json().errors.children;
      const fields = Object.keys(data || {});
      fields.filter(f => data[f].errors && data[f].errors.length).forEach((field) => {
        const control = this.activateForm.get(field);
        const errors = {};
        data[field].errors.forEach(error => {
          errors[error] = true;
        })
        control.setErrors(errors);
      });
    }
  }
}
