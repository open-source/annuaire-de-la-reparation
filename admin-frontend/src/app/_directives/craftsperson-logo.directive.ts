import { Directive, OnInit, Input } from '@angular/core';
import { Http } from '@angular/http';
import { DomSanitizer } from '@angular/platform-browser';

import { CraftspersonService } from "app/_services/craftsperson.service";

/** Directive pour l'affichage du logo d'un réparateur */
@Directive({
  selector: '[craftspersonLogo]',
  host: {
    '[src]': 'this.sanitizer.bypassSecurityTrustUrl(imgSrc)'
  }
})
export class CraftspersonLogoDirective implements OnInit {
  /** URL vers le logo */
  imgSrc: string;
  /** Id du réparateur */
  @Input('craftspersonLogoId') craftspersonLogoId: number;

  /**
   * Constructeur.
   * 
   * @param http 
   * @param sanitizer 
   * @param craftspersonService 
   */
  constructor(private http: Http,
    private sanitizer: DomSanitizer,
    private craftspersonService: CraftspersonService) { }

  /**
   * Chargement du logo.
   */
  ngOnInit() {
    this.craftspersonService.getCraftspersonLogo(this.craftspersonLogoId)
      .map(data => data.json())
      .subscribe(
        data => {
          let urlCreator = window.URL;
          this.imgSrc = urlCreator.createObjectURL(data);
        }
      );
  }
}