import { Component, OnInit, ViewChild, TemplateRef } from '@angular/core';
import { IMultiSelectOption, IMultiSelectSettings, IMultiSelectTexts } from "angular-2-dropdown-multiselect";
import { Modal } from "angular2-modal/plugins/bootstrap";
import { Subscription } from "rxjs";

import { AppConstants } from "app/app.constants";
import { ExportStatus } from "app/_enums/export-status";
import { CraftspersonService } from "app/_services/craftsperson.service";
import { Craftsperson } from "app/_models/craftsperson";
import { AuthenticationService } from "app/_services/auth.service";
import { ServerErrorService } from "app/_services/server-error.service";

/** Composant d'export de réparateurs. */
@Component({
  selector: 'app-craftspersons-export',
  templateUrl: './craftspersons-export.component.html',
  styleUrls: ['./craftspersons-export.component.scss']
})
export class CraftspersonsExportComponent implements OnInit {
  /** Nom du fichier exporté */
  private static readonly CSV_FILENAME = "export-reparateurs.csv";
  /** Liste de départements */
  departments: IMultiSelectOption[] = [];
  /** Configuration des dropdown */
  readonly dropdownSettings: IMultiSelectSettings = AppConstants.DROPDOWN_SETTINGS;
  /** Textes des dropdown */
  readonly dropdownTexts: IMultiSelectTexts = AppConstants.DROPDOWN_TEXTS;
  /** Départements sélectionnés */
  selectedDepartments: string[] = [];
  /** Statut de l'export */
  currentStatus = ExportStatus.None;
  /** Référence aux statut pour utilisation dans la vue */
  exportStatus = ExportStatus;
  /** Souscription à la demande du fichier à l'api */
  exportSubscription: Subscription;

  /**
   * Constructeur.
   */
  constructor(
    private authService: AuthenticationService,
    private craftspersonService: CraftspersonService,
    private serverErrorService: ServerErrorService
  ) { }

  /**
   * Chargement de la liste des départements pour la dropdown.
   */
  ngOnInit() {
    if (this.authService.loggedIn()) {
      this.departments = this.authService.getLoggedInUser().codeDepartment.map(dpt => {
        return { id: dpt.code, name: dpt.name }
      });
    }
  }

  /**
   * Soumission de la demande du fichier.
   */
  onSubmit() {
    // Export en cours
    this.currentStatus = ExportStatus.Working;

    // Sélection de tous les départements si aucun sélectionné
    if (this.selectedDepartments.length == 0) {
      this.selectedDepartments = this.departments.map(d => d.id);
    }

    this.exportSubscription = this.craftspersonService.export(this.selectedDepartments).subscribe(
      f => this.downloadFile(f),
      error => {
        this.currentStatus = ExportStatus.Error;
        this.serverErrorService.show(error);
      }
    );
  }

  /**
   * Annulation de la demande du fichier.
   */
  onCancel() {
    this.currentStatus = ExportStatus.None;
    if (this.exportSubscription) {
      this.exportSubscription.unsubscribe();
    }
  }

  /**
   * Télécharger un fichier csv avec le contenu fourni.
   * 
   * @param string fileContent
   */
  private downloadFile(fileContent) {
    const csvBlob = new Blob([fileContent], { type: 'text/csv' });
    if (window.navigator && window.navigator.msSaveOrOpenBlob) {
      // IE 11
      window.navigator.msSaveOrOpenBlob(csvBlob, CraftspersonsExportComponent.CSV_FILENAME);
    }
    else {
      // Autres navigateurs
      const csvURL = window.URL.createObjectURL(csvBlob);
      const dlLink = document.createElement('a');
      dlLink.href = csvURL;
      dlLink.setAttribute('download', CraftspersonsExportComponent.CSV_FILENAME);
      document.body.appendChild(dlLink);
      dlLink.click();
      document.body.removeChild(dlLink);
    }

    this.currentStatus = ExportStatus.Success;
  }
}
