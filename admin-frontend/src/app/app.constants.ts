import { AbstractControl } from "@angular/forms";
import { IMultiSelectSettings, IMultiSelectTexts } from "angular-2-dropdown-multiselect";
import { INgxMyDpOptions } from "ngx-mydatepicker";

/** Constantes. */
export class AppConstants {
    /** Délai avant prise en compte d'un filtre de colonne plein texte (pour les tables) */
    public static readonly FILTER_TYPE_DELAY_MS = 500;

    /** Format des dates */
    public static readonly DATE_FORMAT = 'DD/MM/YYYY';

    /** Expression régulière pour la validation d'adresses mail */
    public static readonly EMAIL_REGEX: RegExp = /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)$/;
    /** Expression régulière pour la validation de numéros de téléphones (plusieurs numéros acceptés) */
    public static readonly PHONE_REGEX: string = '([0-9 ,])+';
    /** Expression régulière pour la validation de dates au format dd-mm-yyyy */
    public static readonly DATE_REGEX: RegExp = /^\d{2}\/\d{2}\/\d{4}$/;
    /** Expression régulière pour les mots de passe : 8 caractères avec 1 minuscule, 1 majuscule, 1 chiffre et 1 caractère spécial */
    public static readonly PASSWORD_REGEX: RegExp = /^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$/;

    /** Configuration des dropdowns */
    public static readonly DROPDOWN_SETTINGS: IMultiSelectSettings = {
        showCheckAll: true,
        showUncheckAll: true
    };
    /** Textes des dropdowns */
    public static readonly DROPDOWN_TEXTS: IMultiSelectTexts = {
        checkAll: 'Cocher tout',
        uncheckAll: 'Décocher tout',
        checked: 'sélectionné',
        checkedPlural: 'sélectionnés',
        searchPlaceholder: 'Chercher',
        defaultTitle: 'Sélectionner',
        allSelected: 'Tous sélectionnés',
    }

    /** Configuration des datepickers */
    public static readonly DATEPICKER_SETTINGS: INgxMyDpOptions = {
        dateFormat: 'dd/mm/yyyy',
        showTodayBtn: false,
        dayLabels: {su: 'Dim', mo: 'Lun', tu: 'Mar', we: 'Mer', th: 'Jeu', fr: 'Ven', sa: 'Sam'},
        monthLabels: { 1: 'Jan', 2: 'Fév', 3: 'Mars', 4: 'Avril', 5: 'Mai', 6: 'Juin', 7: 'Juil', 8: 'Août', 9: 'Sept', 10: 'Oct', 11: 'Nov', 12: 'Déc' },
        ariaLabelNextMonth: 'Mois suivant',
        ariaLabelNextYear: 'Année suivante',
        ariaLabelPrevMonth: 'Mois précédent',
        ariaLabelPrevYear: 'Année précédente'
    };

    /** Textes de validation */
    public static readonly VALIDATION_LABELS = {
        invalidDateRange: 'La date de début doit être inférieure ou égale à la date de fin'
    }
}