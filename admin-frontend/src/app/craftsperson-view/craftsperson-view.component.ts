import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Modal } from "angular2-modal/plugins/bootstrap";

import 'rxjs/add/operator/switchMap';

import { Craftsperson } from "app/_models/craftsperson";
import { CraftspersonService } from "app/_services/craftsperson.service";
import { ServerErrorService } from "app/_services/server-error.service";

/** Composant d'affichage d'un réparateur */
@Component({
  selector: 'app-craftsperson-view',
  templateUrl: './craftsperson-view.component.html',
  styleUrls: ['./craftsperson-view.component.scss']
})
export class CraftspersonViewComponent implements OnInit {
  /** Réparateur à afficher */
  craftsperson: Craftsperson;

  /**
   * Constructeur.
   * 
   * @param craftspersonService 
   * @param route 
   */
  constructor(
    private router: Router,
    private craftspersonService: CraftspersonService,
    private route: ActivatedRoute,
    private location: Location,
    private modalService: Modal,
    private serverErrorService: ServerErrorService
  ) { }

  /**
   * Initialisation du composant.
   */
  ngOnInit() {
    this.route.params
      .switchMap((params: Params) => this.craftspersonService.getCraftsperson(params['id']))
      .subscribe(craftsperson => this.craftsperson = craftsperson);
  }

  /**
   * Suppression du réparateur.
   * 
   * @param id 
   */
  onDelete() {
    // Modale de confirmation
    this.modalService.confirm()
        .title('Confirmation de suppression')
        .body('Supprimer ce réparateur ?')
    .okBtn('Supprimer')
    .cancelBtn('Annuler')
    .open()
    .then(dialog => dialog.result)
    // Suppression effective en cas de confirmation
    .then(result => this.craftspersonService.deleteCraftsperson(this.craftsperson.id).subscribe(
        response => {
            this.router.navigate(['/craftspersons']);
        },
        err => this.serverErrorService.show(err)
    ));
  }

  /**
   * Retour à la page précédente.
   */
  onBack() {
    this.location.back();
  }
}
