import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { AuthenticationService } from '../_services/auth.service';

/** Composant de connexion. */
@Component({
    templateUrl: 'login.component.html'
})
export class LoginComponent implements OnInit {
    /** Informations de connexion */
    model: any = {};
    /** Statut de chargement */
    loading = false;
    /** Erreur affichée */
    error = '';

    /** 
     * Constructeur. 
     */
    constructor(
        private router: Router,
        private authenticationService: AuthenticationService
    ) { }

    /** 
     * Initialisation du composant.
     */
    ngOnInit() {
        this.authenticationService.logout();
    }

    /** 
     * Connexion.
     */
    login() {
        this.loading = true;
        this.authenticationService.login(this.model.username, this.model.password)
            .subscribe(
            result => this.router.navigate(['/']),
            err => this.handleLoginError('Adresse mail ou mot de passe incorrect')
            );
    }

    /**
     * Gestion d'une erreur de login.
     * 
     * @param error description de l'erreur
     */
    private handleLoginError(error: string) {
        this.error = error;
        this.loading = false;
    }
}