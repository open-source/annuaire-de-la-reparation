import { Component } from '@angular/core';
import { AuthenticationService } from './_services/auth.service';
import { User } from "app/_models/user";

/** Composant principal. */
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  /**
   * Constructeur.
   * 
   * @param authService 
   */
  constructor(private authService: AuthenticationService) {}

  /**
   * Indique si l'utilisateur est connecté.
   * 
   * @return vrai si l'utilisateur est connecté, faux sinon
   */
  public isLoggedIn(): boolean {
    return this.authService.loggedIn();
  }

  /**
   * Retourne l'utilisateur connecté.
   * 
   * @return User
   */
  public getCurrentUser(): User {
    if(this.isLoggedIn()) {
      return this.authService.getLoggedInUser();
    }
    return null;
  }
}
