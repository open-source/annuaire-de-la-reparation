import { Injectable } from '@angular/core';
import { RequestOptions, URLSearchParams, Response, Http } from '@angular/http';
import { AuthHttp } from "angular2-jwt";

import { environment } from "app/../environments/environment";
import { User } from "app/_models/user";
import { Department } from "app/_models/department";
import { Observable } from "rxjs/Observable";

/** Service d'accès aux utilisateurs */
@Injectable()
export class UserService {
  /** Url d'accès aux utilisateurs */
  private usersUrl = environment.apiRoot.concat('users');
  /** Url d'accès à un utilisateur */
  private userUrl = environment.apiRoot.concat('users/{id}');
  /** Url d'accès à l'utilisateur courant */
  private currentUrl = environment.apiRoot.concat('users/current');
  /** Url d'activation d'un utilisateur */
  private activateUrl = environment.apiRoot.concat('users/activate');
  /** Url de demande de réinitialisation de mot de passe */
  private passwordResetRequestUrl = environment.apiRoot.concat('users/ask-password-reset');

  /**
   * Constructeur.
   * 
   * @param authHttp
   */
  constructor(
    private authHttp: AuthHttp,
    private http: Http
  ) { }

  /**
   * Retourne l'utilisateur courant.
   */
  public getCurrent(): Observable<User> {
    return this.authHttp.get(this.currentUrl).map(response => response.json() as User);
  }

  /**
   * Retourne la liste des utilisateurs.
   * 
   * @param offset
   * @param limit
   * @param sortColumn
   * @param sortDir
   * @param lastnameSearch
   * @param firstnameSearch
   * @param emailSearch
   * @param phoneSearch
   * @param departmentCodeSearch
   * @param isMailConcact
   */
  public getUsers(offset: number, limit: number, sortColumn: string, sortDir: string, lastnameSearch: string, firstnameSearch: string, emailSearch: string, phoneSearch: string, departmentCodeSearch: string, isMailConcact: string): Observable<any> {
    let filters = new URLSearchParams();
    filters.set('offset', offset as any as string);
    filters.set('limit', limit as any as string);

    if (sortColumn) {
      filters.set('sort', sortColumn);
    }
    if (sortDir) {
      filters.set('dir', sortDir.toUpperCase());
    }
    if (lastnameSearch && lastnameSearch.length) {
      filters.set('lastname', lastnameSearch);
    }
    if (firstnameSearch && firstnameSearch.length) {
      filters.set('firstname', firstnameSearch);
    }
    if (emailSearch && emailSearch.length) {
      filters.set('email', emailSearch);
    }
    if (phoneSearch && phoneSearch.length) {
      filters.set('phone', phoneSearch);
    }
    if (departmentCodeSearch && departmentCodeSearch.length) {
      filters.set('departmentCode', departmentCodeSearch);
    }
    if (isMailConcact) {
      filters.set('isMailContact', isMailConcact);
    }

    let requestOptions = new RequestOptions();
    requestOptions.search = filters;

    return this.authHttp.get(this.usersUrl, requestOptions).map(response => response.json());
  }

  /**
   * Suppression d'un utilisateur.
   * 
   * @param id
   */
  public deleteUser(id: number): Observable<Response> {
    let url = this.userUrl.replace(/\{id\}/g, id.toString());

    return this.authHttp.delete(url);
  }

  /**
   * Ajout d'un utilisateur.
   * 
   * @param user
   * @param departments
   * @return utilisateur créé
   */
  public addUser(user: User, departments: string[]): Observable<User> {
    return this.authHttp.post(this.usersUrl, this.getUserRequestBody(user, departments)).flatMap(
      response => {
        let location = response.headers.get('Location');
        return this.authHttp.get(location);
      }).map(
      response => response.json() as User);
  }

  /**
   * Modification d'un utilisateur.
   * 
   * @param user
   * @param departments
   */
  public updateUser(user: User, departments: string[]): Observable<Response> {
    let url = this.userUrl.replace(/\{id\}/g, user.id.toString());
    return this.authHttp.put(url, this.getUserRequestBody(user, departments));
  }

  /**
   * Création du corps de requête pour ajout/modification d'un utilisateur.
   * 
   * @param user
   * @param departments 
   */
  private getUserRequestBody(user: User, departments: string[]) {
    return {
      'user': {
        lastname: user.lastname,
        firstname: user.firstname,
        email: user.email,
        phone: user.phone,
        departments: departments.join(','),
        isMailContact: user.isMailContact,
        role: user.roles.length > 0 ? user.roles[0] : ''
      }
    };
  }

  /**
   * Récupération d'un utilisateur.
   * 
   * @param id
   */
  public getUser(id: number): Observable<User> {
    let url = this.userUrl.replace(/\{id\}/g, id.toString());

    return this.authHttp.get(url).map(response => response.json() as User);
  }

  /**
   * Activation d'un utilisateur.
   * 
   * @param token
   * @param password
   */
  public activateUser(token: string, password: string): Observable<Response> {
    return this.http.post(this.activateUrl, this.getUserActivationBody(token, password));
  }

  /**
   * Corps d'une requête d'activation d'un utilisateur.
   * 
   * @param token
   * @param password
   */
  private getUserActivationBody(token: string, password: string) {
    return {
      'activate': {
        token: token,
        password: password
      }
    }
  }

  /**
   * Demande de réinitialisation pour le compte associé à l'adresse email fournie.
   * 
   * @param email 
   */
  public passwordResetRequest(email: string) {
    return this.http.post(this.passwordResetRequestUrl, {
      'passwordResetRequest': {
        email: email
      }
    });
  }
}
