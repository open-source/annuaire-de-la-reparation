import { Injectable } from '@angular/core';

import { Observable } from "rxjs/Observable";

import { environment } from "app/../environments/environment";
import { AuthHttp } from "angular2-jwt";

import { Department } from '../_models/department';

/** Service d'accès aux départements. */
@Injectable()
export class DepartmentService {
    /** Url d'api de récupération des départements */
    private departmentsUrl = environment.apiRoot.concat('departments');
    
    /**
     * Constructeur.
     * 
     * @param authHttp 
     */
    constructor(private authHttp: AuthHttp) { }

    /**
     * Récupération des départements.
     */
    getDepartments(): Observable<Department[]> {
        return this.authHttp.get(this.departmentsUrl)
            .map(response => response.json().departments as Department[])
    }
}