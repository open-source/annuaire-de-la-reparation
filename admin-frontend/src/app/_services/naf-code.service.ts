import { Injectable } from '@angular/core';

import { Observable } from "rxjs/Observable";

import { environment } from "app/../environments/environment";
import { AuthHttp } from "angular2-jwt";

import { NafCode } from "app/_models/naf-code";

/** Service d'accès aux codes NAF. */
@Injectable()
export class NafCodeService {
    /** Url d'api de récupération des codes NAF */
    private nafCodesUrl = environment.apiRoot.concat('naf-codes');

    /**
     * Constructeur.
     * 
     * @param authHttp 
     */
    constructor(private authHttp: AuthHttp) { }

    /**
     * Récupération des codes NAF.
     */
    getNafCodes(): Observable<NafCode[]> {
        return this.authHttp.get(this.nafCodesUrl).map(response => response.json().nafCodes as NafCode[]);
    }
}