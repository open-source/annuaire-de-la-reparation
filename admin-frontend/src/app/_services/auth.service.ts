import { Injectable } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';

import { tokenNotExpired, AuthConfigConsts } from 'angular2-jwt';

import { environment } from "app/../environments/environment";
import { UserService } from "app/_services/user.service";
import { User } from "app/_models/user";
import { Role } from "app/_enums/role";

/** Service d'authentification. */
@Injectable()
export class AuthenticationService {
    /** Clé des données utilisateur dans le local storage */
    private readonly USER_NAME = 'user';
    /** Url de l'api d'authentification */
    private authUrl = environment.apiRoot.concat('login_check');

    /**
     * Constructeur.
     * 
     * @param http 
     */
    constructor(
        private http: Http,
        private userService: UserService
    ) { }

    /**
     * Soumission d'une requête de connexion.
     * 
     * @param username 
     * @param password 
     */
    public login(username: string, password: string): Observable<User> {
        return this.http.post(this.authUrl, { username: username, password: password })
            .flatMap((response: Response) => {
                // handle token from the response
                let token = response.json() && response.json().token;
                if (token) {
                    // store jwt token in local storage to keep user logged in between page refreshes
                    localStorage.setItem(AuthConfigConsts.DEFAULT_TOKEN_NAME, token);

                    // get current user info
                    return this.userService.getCurrent();
                }
                else {
                    Observable.throw(new Error('missing token'));
                }
            })
            // store user profile in local storage
            .do(u => localStorage.setItem(this.USER_NAME, JSON.stringify(u)));
    }

    /**
     * Déconnexion.
     */
    public logout(): void {
        // clear token & user from local storage
        localStorage.removeItem(AuthConfigConsts.DEFAULT_TOKEN_NAME);
        localStorage.removeItem(this.USER_NAME);
    }

    /**
     * Statut d'authentification.
     */
    public loggedIn(): boolean {
        return tokenNotExpired() && localStorage.getItem(this.USER_NAME) != null;
    }

    /**
     * Utilisateur connecté.
     * 
     * @return User
     */
    public getLoggedInUser(): User {
        return JSON.parse(localStorage.getItem(this.USER_NAME));
    }

    /**
     * Indique si l'utilisateur connecté est administrateur.
     * 
     * @return boolean
     */
    public isAdmin(): boolean {
        return this.loggedIn() &&
            this.getLoggedInUser().roles.length > 0 &&
            this.getLoggedInUser().roles[0] === Role[Role.ROLE_ADMIN];
    }
}