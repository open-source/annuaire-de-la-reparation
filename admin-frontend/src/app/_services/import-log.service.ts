import { Injectable } from '@angular/core';
import { RequestOptions, URLSearchParams, Response } from "@angular/http";
import { AuthHttp } from "angular2-jwt";
import { Observable } from "rxjs/Observable";

import * as moment from 'moment/moment';

import { environment } from "app/../environments/environment";
import { User } from "app/_models/user";
import { ImportLog } from "app/_models/import-log";
import { AppConstants } from "app/app.constants";

/** Service d'accès aux logs d'import */
@Injectable()
export class ImportLogService {
  /** Url d'accès aux logs */
  private importLogsUrl = environment.apiRoot.concat('import-logs');

  /**
   * Constructeur.
   * 
   * @param authHttp
   */
  constructor(
    private authHttp: AuthHttp
  ) { }

  /**
   * Retourne les logs correspondants aux filtres.
   * 
   * @param startDate
   * @param endDate
   * @param regions
   * @return Observable<ImportLog[]>
   */
  public get(startDate: Date, endDate: Date, regions: string[]): Observable<ImportLog[]> {
    let startDateFilter = startDate ? moment(startDate).format(AppConstants.DATE_FORMAT) : '';
    let endDateFilter = endDate ? moment(endDate).format(AppConstants.DATE_FORMAT) : '';
    let filters = new URLSearchParams();
    filters.set('startDate', startDateFilter);
    filters.set('endDate', endDateFilter);
    filters.set('regions', regions.join(','));

    let requestOptions = new RequestOptions();
    requestOptions.search = filters;

    return this.authHttp.get(this.importLogsUrl, requestOptions).map(response => response.json().logs as ImportLog[]);
  }
}
