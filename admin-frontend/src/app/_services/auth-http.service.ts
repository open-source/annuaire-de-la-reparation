import { Injectable } from '@angular/core';
import { Router } from "@angular/router";
import { Request, XHRBackend, RequestOptions, Response, Http, RequestOptionsArgs } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

/** Service HTTP avec redirection vers la page de login sur erreur 401. */
@Injectable()
export class AuthHttpService extends Http {

    /**
     * Constructeur.
     */
    constructor(
        backend: XHRBackend,
        defaultOptions: RequestOptions,
        private router: Router) {
        super(backend, defaultOptions);
    }

    /**
     * Traitement d'une requête.
     * 
     * @param url 
     * @param options 
     */
    request(url: string | Request, options?: RequestOptionsArgs): Observable<Response> {
        return super.request(url, options).catch((error: Response) => {
            // Erreur 401 : redirection vers le login (sauf pour la page de login)
            if (error.status === 401 && window.location.href.indexOf('login') == -1) {
                this.router.navigate(['login']);
            }
            return Observable.throw(error);
        });
    }
}