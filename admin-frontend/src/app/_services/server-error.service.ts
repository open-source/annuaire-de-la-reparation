import { Injectable } from '@angular/core';
import { Modal } from "angular2-modal/plugins/bootstrap";

/** Service HTTP avec redirection vers la page de login sur erreur 401. */
@Injectable()
export class ServerErrorService {

    /**
     * Constructeur.
     */
    constructor(
        private modalService: Modal
    ) { }

    /**
     * Affichage d'une erreur.
     * 
     * @param error 
     */
    show(error: Response) {
        let bodyContent = "Le serveur a répondu avec le code d'erreur {errorCode} ({errorStatus}). Veuillez contacter l'administrateur si l'erreur persiste.";
        this.modalService.alert()
            .title('Erreur de communication avec le serveur')
            .body(bodyContent.replace(/\{errorCode\}/g, error.status.toString()).replace(/\{errorStatus\}/g, error.statusText))
            .open();
    }
}