import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions, URLSearchParams, Response, ResponseContentType } from '@angular/http';
import { AuthHttp } from 'angular2-jwt';

import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { Observable } from "rxjs/Observable";

import { environment } from "app/../environments/environment";
import { Cfe } from '../_models/cfe';

/** Service d'accès aux réparateurs. */
@Injectable()
export class CfeService {
    private cfeUrl = environment.apiRoot.concat('cfe/{id}');
    private cfesUrl = environment.apiRoot.concat('cfe');

    /**
     * Constructeur.
     * 
     * @param authHttp 
     */
    constructor(private authHttp: AuthHttp) { }

    /**
     * Récupération des cfes
     */
    getAllCfes(): Observable<Cfe[]> {
        let requestOptions = new RequestOptions();
        return this.authHttp.get(this.cfesUrl, requestOptions).map(response => response.json() as Cfe[]);
    }

    /**
     * Données d'un cfe.
     * 
     * @param id 
     */
    getCfe(id: number): Observable<Cfe> {
        let url = this.cfeUrl.replace(/\{id\}/g, id.toString());
        return this.authHttp.get(url).map(response => response.json() as Cfe);
    }

    /**
     * Mise à jour d'un cfe.
     * 
     * @param cfe
     */
    public updateCfe(cfe: Cfe): Observable<Cfe[]> {
        let url = this.cfeUrl.replace(/\{id\}/g, cfe.id.toString());
        return this.authHttp.put(url, this.getCfeBody(cfe)).map(response => response.json() as Cfe[]);
    }

    /**
     * Création du corps de requête pour modification de cfe.
     * 
     * @param cfe
     */
    private getCfeBody(cfe: Cfe) {
        return {
            'cfe': {
                id: cfe.id,
                url: cfe.url
            }
        };
    }
}