import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions, URLSearchParams, Response, ResponseContentType } from '@angular/http';
import { AuthHttp } from 'angular2-jwt';

import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { Observable } from "rxjs/Observable";

import { environment } from "app/../environments/environment";
import { Craftsperson } from '../_models/craftsperson';
import { GetCraftspersons } from '../_models/get-craftspersons';
import { CraftspersonImport } from '../_models/craftsperson-import';
import { ImportLog } from "app/_models/import-log";

/** Service d'accès aux réparateurs. */
@Injectable()
export class CraftspersonService {
    /** Url d'api pour accès à un réparateur */
    private craftspersonUrl = environment.apiRoot.concat('craftspersons/{id}');
    /** Url d'api pour accès à un réparateur */
    private craftspersonUpdateLogoUrl = environment.apiRoot.concat('craftspersons/{id}/logo');
    /** Url d'api pour accès à un réparateur */
    private craftspersonLogoUrl = environment.apiRoot.concat('craftspersons/logo/{id}');
    /** Url d'api pour accès aux réparateurs */
    private craftspersonsUrl = environment.apiRoot.concat('craftspersons');
    /** Url d'api pour la vérification avant import d'un fichier */
    private craftspersonsImportCheckUrl = environment.apiRoot.concat('craftspersons/check-file');
    /** Url d'api pour l'import d'un fichier */
    private craftspersonsImportUrl = environment.apiRoot.concat('craftspersons/import-file');
    /** Url d'api pour l'export de réparateurs */
    private craftspersonsExportUrl = environment.apiRoot.concat('craftspersons/csv');

    /**
     * Constructeur.
     * 
     * @param authHttp 
     */
    constructor(private authHttp: AuthHttp) { }

    /**
     * Récupération des réparateurs avec pagination, tri et filtres.
     * 
     * @param offset 
     * @param limit 
     * @param sortColumn 
     * @param sortDir 
     * @param updateDate 
     * @param siretSearch 
     * @param nameSearch 
     * @param zipCodeSearch 
     * @param addressSearch 
     * @param phoneSearch 
     * @param error 
     * @param reparactor 
     * @param enabled 
     * @param updated 
     * @param cmaCode 
     * @param nafCode 
     */
    getCraftspersons(offset: number, limit: number, sortColumn: string, sortDir: string, updateDate: string, siretSearch: string, nameSearch: string, zipCodeSearch: string, addressSearch: string, phoneSearch: string, error: string, reparactor: string, enabled: string, updated: string, cmaCode: string, nafCode: string): Observable<GetCraftspersons> {
        let filters = new URLSearchParams();
        filters.set('offset', offset as any as string);
        filters.set('limit', limit as any as string);

        if (sortColumn) {
            filters.set('sort', sortColumn);
        }
        if (sortDir) {
            filters.set('dir', sortDir.toUpperCase());
        }
        if (siretSearch && siretSearch.length) {
            filters.set('siret', siretSearch);
        }
        if (nameSearch && nameSearch.length) {
            filters.set('name', nameSearch);
        }
        if (zipCodeSearch && zipCodeSearch.length) {
            filters.set('zipCode', zipCodeSearch);
        }
        if (addressSearch && addressSearch.length) {
            filters.set('address', addressSearch);
        }
        if (phoneSearch && phoneSearch.length) {
            filters.set('phone', phoneSearch);
        }
        if (error) {
            filters.set('isError', error);
        }
        if (reparactor) {
            filters.set('isReparactor', reparactor);
        }
        if (enabled) {
            filters.set('isEnabled', enabled);
        }
        if (updated) {
            filters.set('isUpdated', updated);
        }
        if (updateDate) {
            filters.set('updateDate', updateDate);
        }
        if (cmaCode) {
            filters.set('cmaCode', cmaCode);
        }
        if (nafCode) {
            filters.set('nafCode', nafCode);
        }

        let requestOptions = new RequestOptions();
        requestOptions.search = filters;

        return this.authHttp.get(this.craftspersonsUrl, requestOptions).map(response => response.json() as GetCraftspersons);
    }

    /**
     * Import d'un fichier de réparateurs.
     * 
     * @param file 
     * @param isCheck 
     */
    importCraftspersons(file: File, isCheck: boolean) {
        let formData: FormData = new FormData();
        formData.append('uploadFile', file, file.name);
        let url = isCheck ? this.craftspersonsImportCheckUrl : this.craftspersonsImportUrl;
        return this.authHttp.post(url, formData)
            .map(response => response.json().result as CraftspersonImport)
            .catch(error => Observable.throw(error.json().errors as ImportLog[]));
    }

    /**
     * Données d'un réparateur.
     * 
     * @param id 
     */
    getCraftsperson(id: number): Observable<Craftsperson> {
        let url = this.craftspersonUrl.replace(/\{id\}/g, id.toString());
        return this.authHttp.get(url).map(response => response.json() as Craftsperson);
    }

    /**
     * Mise à jour d'un réparateur.
     * 
     * @param craftsperson 
     * @param nafCodes
     */
    public updateCraftsperson(craftsperson: Craftsperson, nafCodes: string): Observable<Response> {
        let url = this.craftspersonUrl.replace(/\{id\}/g, craftsperson.id.toString());
        return this.authHttp.put(url, this.getCraftspersonBody(craftsperson, nafCodes));
    }

    /**
     * Ajout d'un réparateur.
     * 
     * @param craftsperson 
     * @param nafCodes 
     */
    public addCraftsperson(craftsperson: Craftsperson, nafCodes: string): Observable<Craftsperson> {
        return this.authHttp.post(this.craftspersonsUrl, this.getCraftspersonBody(craftsperson, nafCodes)).flatMap(
            response => {
                  let location = response.headers.get('Location');
                       return this.authHttp.get(location);
                    }).map(
             response => response.json() as Craftsperson);
    }

    /**
     * Création du corps de requête pour ajout/modification de réparateur.
     * 
     * @param craftsperson 
     * @param nafCodes 
     */
    private getCraftspersonBody(craftsperson: Craftsperson, nafCodes: string) {
        return {
            'craftsperson': {
                siret: craftsperson.siret,
                name: craftsperson.name,
                zipCode: craftsperson.zipCode,
                zipCodeLabel: craftsperson.zipCodeLabel,
                address1: craftsperson.address1,
                address2: craftsperson.address2,
                phone: craftsperson.phone,
                email: craftsperson.email,
                website: craftsperson.website,
                isReparactor: craftsperson.isReparactor,
                isEnabled: craftsperson.isEnabled,
                isError: craftsperson.isError,
                reparactorDescription: craftsperson.reparactorDescription,
                reparactorHours: craftsperson.reparactorHours,
                reparactorServices: craftsperson.reparactorServices,
                reparactorCertificates: craftsperson.reparactorCertificates,
                cmaCode: craftsperson.cmaCode.code,
                nafCodes: nafCodes,
                otherInfo: craftsperson.otherInfo
            }
        };
    }

    /**
     * Récupération du logo d'un réparateur.
     * 
     * @param logoId 
     */
    public getCraftspersonLogo(logoId: number) : Observable<Response> {
        let url = this.craftspersonLogoUrl.replace(/\{id\}/g, logoId.toString());

        return this.authHttp.get(url, {responseType: ResponseContentType.Blob});
    }

    /**
     * Mise à jour du logo d'un réparateur.
     * 
     * @param craftsperson
     * @param logo
     */
    public updateCraftspersonLogo(craftsperson: Craftsperson, logo: File) : Observable<Response> {
        let url = this.craftspersonUpdateLogoUrl.replace(/\{id\}/g, craftsperson.id.toString());
        let formData: FormData = new FormData();
        formData.append('logo', logo, logo.name);

        return this.authHttp.post(url, formData);
    }

    /**
     * Suppression d'un réparateur.
     * 
     * @param id
     */
    public deleteCraftsperson(id: number): Observable<Response> {
        let url = this.craftspersonUrl.replace(/\{id\}/g, id.toString());

        return this.authHttp.delete(url);
    }

    /**
     * Export des réparateurs.
     * 
     * @param departements
     */
    public export(departments: string[]) {
        let filters = new URLSearchParams();
        filters.set('departments', departments.join(','));

        let requestOptions = new RequestOptions();
        requestOptions.search = filters;
        requestOptions.responseType = ResponseContentType.Blob;

        return this.authHttp.get(this.craftspersonsExportUrl, requestOptions).map(data => data.json());
    }
}