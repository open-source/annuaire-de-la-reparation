import { Craftsperson } from "./craftsperson";

/** Résultat de requête de récupération de réparateurs. */
export class GetCraftspersons {
    /** Nombre total de réparateurs correspondant aux filtres de la requête */
    total: number;
    /** Réparateurs avec prise en compte des filtres de pagination */
    craftspersons: Craftsperson[];
}