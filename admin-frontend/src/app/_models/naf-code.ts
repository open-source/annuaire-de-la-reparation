/** Code NAFA. */
export class NafCode {
    /** Code */
    code: string;
    /** Label */
    label: string;
}