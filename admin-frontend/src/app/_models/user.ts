/** Utilisateur. */
import { Department } from "app/_models/department";

export class User {
    /** Identifiant */
    id: number;
    /** Prénom */
    firstname: string;
    /** Nom */
    lastname: string;
    /** Email */
    email: string;
    /** Téléphone */
    phone: string;
    /** CMA associées */
    codeDepartment: Department[];
    /** Contact par mail */
    isMailContact: boolean;
    /** Privilèges */
    roles: string[];
}