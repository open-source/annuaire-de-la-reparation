import { Department } from "app/_models/department";
import { NafCode } from "app/_models/naf-code";

/** Réparateur */
export class Craftsperson {
    /** Identifiant */
    id: number;
    /** Siret */
    siret: number;
    /** Nom commercial */
    name: string;
    /** Code NAFA */
    nafCode: NafCode[];
    /** Code CMA */
    cmaCode: Department;
    /** Code postal */
    zipCode: string;
    /** Libellé postal */
    zipCodeLabel: string;
    /** Adresse (ligne 1) */
    address1: string;
    /** Adresse (ligne 2) */
    address2: string;
    /** Numéro de téléphone */
    phone: string;
    /** Email */
    email: string;
    /** Site internet */
    website: string;
    /** Statut réparacteur */
    isReparactor: boolean;
    /** Date de création */
    creationDate: string;
    /** Date de dernière mise à jour */
    updateDate: string;
    /** Statut de mise à jour manuelle */
    isUpdated: boolean;
    /** Statut d'activation */
    isEnabled: boolean;
    /** Statut de signalement d'erreur */
    isError: boolean;
    /** Présentation réparacteur */
    reparactorDescription: string;
    /** Horaires d'ouverture réparacteur */
    reparactorHours: string;
    /** Services réparacteur */
    reparactorServices: string;
    /** Certifications réparacteur */
    reparactorCertificates: string;
    /** Nom du fichier logo */
    logoFile: string;
    /** Autres informations */
    otherInfo: string;
}