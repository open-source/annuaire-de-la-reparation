/** Résultat de vérification avant import de réparateurs. */
import { ImportLog } from "app/_models/import-log";

export class CraftspersonImport {
    /** Nombre de réparateurs créés */
    created: number;
    /** Nombre de réparateurs mis à jour */
    updated: number;
    /** Nombre de réparateurs supprimés */
    deleted: number;
    /** Nombre de réparateurs invalides */
    invalid: number;
    /** Logs d'import */
    logs: ImportLog[];
}