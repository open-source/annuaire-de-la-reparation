
/** Log d'import d'un fichier de réparateurs. */
export class ImportLog {
    /** Date du log */
    date: string;
    /** Région concernée */
    region: string;
    /** SIRET de l'entreprise */
    siret: string;
    /** Fichier importé */
    filename: string;
    /** Ligne d'erreur dans le fichier */
    line: number;
    /** Message de log */
    message: string;
}