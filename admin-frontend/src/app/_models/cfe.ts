/** Cfe */
export class Cfe {
    /** Identifiant */
    id: number;
    /** Region */
    region: number;
    /** Adresse Cfe */
    url: string;
}