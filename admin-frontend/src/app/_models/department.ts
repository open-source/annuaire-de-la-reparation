/** Département */
export class Department {
    /** Code */
    code: string;
    /** Nom */
    name: string;
}