import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Location } from '@angular/common';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { IMultiSelectOption, IMultiSelectSettings, IMultiSelectTexts } from 'angular-2-dropdown-multiselect';

import { Department } from "app/_models/department";
import { User } from 'app/_models/user';
import { AppConstants } from "app/app.constants";
import { Role } from "app/_enums/role";
import { DepartmentService } from "app/_services/department.service";

/** Formulaire d'édition d'utilisateur */
@Component({
  selector: 'app-user-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.scss']
})
export class UserFormComponent implements OnInit {
  /** Utilisateur en cours d'édition */
  @Input() user: User;
  /** Soumission du formulaire */
  @Output() submitUser: EventEmitter<any> = new EventEmitter<any>();
  /** Champs du formulaire */
  userForm: FormGroup;
  /** Liste de départements */
  departments: IMultiSelectOption[] = [];
  /** Enumération des rôles */
  readonly role = Role;
  /** Configuration des dropdown */
  readonly dropdownSettings: IMultiSelectSettings = AppConstants.DROPDOWN_SETTINGS;
  /** Textes des dropdown */
  readonly dropdownTexts: IMultiSelectTexts = AppConstants.DROPDOWN_TEXTS;

  /**
   * Constructeur.
   */
  constructor(
    private formBuilder: FormBuilder,
    private departmentService: DepartmentService,
    private location: Location
  ) {
    // Initialisation des champs du formulaire
    this.userForm = this.formBuilder.group({
      firstname: ['', Validators.required],
      lastname: ['', Validators.required],
      email: ['', [Validators.required, Validators.pattern(AppConstants.EMAIL_REGEX)]],
      phone: ['', [Validators.minLength(10), Validators.pattern(AppConstants.PHONE_REGEX)]],
      departments: [[]],
      isMailContact: '',
      role: ['', Validators.required],
    });
  }

  /**
   * Chargement des données nécessaires au composant.
   */
  ngOnInit() {
    // Chargement de la liste des départements pour la dropdown
    this.departmentService.getDepartments().subscribe(dpts => {
      this.departments = dpts.map(dpt => {
        let dptName = dpt.code + ' - ' + dpt.name;
        return { id: dpt.code, name: dptName }
      });
      // Chargement dans le formulaire des informations de l'utilisateur
      this.initFormValues();
    });
  }

  /**
   * Chargement des valeurs du formulaire à partir des données de l'utilisateur en cours d'édition.
   */
  private initFormValues() {
    this.userForm.setValue({
      firstname: this.user.firstname,
      lastname: this.user.lastname,
      email: this.user.email,
      phone: this.user.phone,
      departments: this.user.codeDepartment.map(d => d.code),
      isMailContact: this.user.isMailContact,
      role: this.user.roles.length > 0 ? this.user.roles[0] : ''
    });
  }

  /**
   * Soumission du formulaire.
   */
  onSubmit(form, event: Event) {
    const formModel = this.userForm.value;

    const saveUser: User = {
      id: this.user.id,
      firstname: formModel.firstname,
      lastname: formModel.lastname,
      email: formModel.email,
      phone: formModel.phone,
      codeDepartment: this.user.codeDepartment,
      isMailContact: formModel.isMailContact as boolean,
      roles: [formModel.role]
    };

    this.submitUser.emit({
      user: saveUser,
      departments: formModel.departments
    })
  }

  /**
   * Retour à la page précédente.
   */
  onBack() {
    this.location.back();
  }

  /**
   * Affichage des erreurs de formulaire.
   * 
   * @param error 
   */
  public showFormErrors(error: any) {
    if (error.status == 400) {
      const data = error.json().errors.children;
      const fields = Object.keys(data || {});
      fields.filter(f => data[f].errors && data[f].errors.length).forEach((field) => {
        const control = this.userForm.get(field);
        const errors = {};
        data[field].errors.forEach(error => {
          errors[error] = true;
        })
        control.setErrors(errors);
      });
    }
  }
}
