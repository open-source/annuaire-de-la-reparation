import {
    Component, Directive, ElementRef, Input, OnInit, Renderer, TemplateRef, ViewChild,
    ViewEncapsulation
} from '@angular/core';

import { Subject } from "rxjs";
import { Observable } from "rxjs/Observable";

import "rxjs/add/operator/zip";
import { CfeService } from "../_services/cfe.service";
import {BSModalContext, Modal, OneButtonPreset} from "angular2-modal/plugins/bootstrap";


/** Composant de liste des réparateurs. */
@Component({
    templateUrl: 'cfe.component.html',
    styleUrls: ['cfe.component.css'],
    encapsulation: ViewEncapsulation.None
})
export class CfeComponent{

    /** Template pour une colonne affichant des actions */
    @ViewChild('actionTmpl') actionTmpl: TemplateRef<any>;

    @ViewChild('urlTmpl') urlTmpl: TemplateRef<any>;

    rows = [];
    loadingIndicator: boolean = true;
    reorderable: boolean = true;
    columns = [];
    editedCfeId = null;

    constructor(private modalService: Modal,
                private cfeService: CfeService) {  }


    ngOnInit(){
        this.columns = [
            { prop: 'region', name: 'Region'},
            { prop: 'url', name: 'Adresse CFE', cellTemplate: this.urlTmpl},
            { prop: 'id', name: 'Actions', cellTemplate: this.actionTmpl }
        ];

        // Chargement des données de la table
        this.loadTableData();
    }

    private loadTableData(){
        this.cfeService.getAllCfes().subscribe(
            response =>{
                this.rows = response;
                this.loadingIndicator = false;
            }
        );
    }



    private onEdit(cfe){
        this.editedCfeId = cfe.id;
        this.showEditModal(cfe);
        //let urlText = document.getElementById("url"+cfe.id);
    }

    /**
     * Affichage de la fenêtre d'édition de CFE.
     *
     */
    private showEditModal(cfe) {
        this.modalService.prompt()
            .defaultValue(cfe.url)
            .size('lg')
            .isBlocking(true)
            .showClose(true)
            .title('Adresse de CFE')
            .body('<label>Region :  </label>  ' +cfe.region+
                '<br><label>Url du CFE :</label>')
            .okBtn('Enregistrer')
            .cancelBtn('Annuler')
            .open()
            .then(dialog => dialog.result)
            .then(result => {
                cfe.url = result;
                this.updateCFE(cfe);
            })
            .catch(err => {
                console.log(err);
            });
    }


    private updateCFE(cfe){
        this.loadingIndicator = true;
        this.cfeService.updateCfe(cfe).subscribe(
            response => {
                this.rows = response;
                this.loadingIndicator = false;
            }
        );
    }
}