import { environment } from "environments/environment";

/** Gestion des url des ressources statiques */
export class Asset {
    /**
     * Génération d'une url vers une ressource avec prise en compte de l'environnement.
     * @param url chemin de la ressource relatif au dossier "src" de l'application
     * @return chemin de la ressource
     */
    public static url(url: string) {
        return environment.deployUrl + url;
    }
}