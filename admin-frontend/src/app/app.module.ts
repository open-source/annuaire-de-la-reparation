import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule, Http } from '@angular/http';
import { ModalModule } from 'angular2-modal';
import { BootstrapModalModule } from 'angular2-modal/plugins/bootstrap';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { MultiselectDropdownModule } from 'angular-2-dropdown-multiselect';
import { NgxMyDatePickerModule } from 'ngx-mydatepicker';
import { LinkyModule } from 'angular-linky';

import { AppRoutingModule } from './app-routing.module';
import { AuthModule } from './auth.module';

import { AuthGuard } from './_guards/auth.guard';
import { AuthenticationService } from './_services/auth.service';
import { CfeService } from './_services/cfe.service';
import { CraftspersonService } from './_services/craftsperson.service';
import { DepartmentService } from './_services/department.service';
import { NafCodeService } from "app/_services/naf-code.service";
import { AuthHttpService } from "app/_services/auth-http.service";
import { UserService } from 'app/_services/user.service';

import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { LoginComponent } from './login/login.component';
import { CfeComponent } from './cfe/cfe.component';
import { CraftspersonsComponent } from './craftspersons/craftspersons.component';
import { CraftspersonImportComponent } from './craftsperson-import/craftsperson-import.component';
import { CraftspersonViewComponent } from './craftsperson-view/craftsperson-view.component';
import { CraftspersonFormComponent } from './craftsperson-form/craftsperson-form.component';
import { ValidatedInputComponent } from './validated-input/validated-input.component';
import { CraftspersonAddComponent } from './craftsperson-add/craftsperson-add.component';
import { CraftspersonEditComponent } from './craftsperson-edit/craftsperson-edit.component';
import { UsersComponent } from './users/users.component';
import { UserAddComponent } from './user-add/user-add.component';
import { UserFormComponent } from './user-form/user-form.component';
import { UserViewComponent } from './user-view/user-view.component';
import { UserActivateComponent } from './user-activate/user-activate.component';
import { UserEditComponent } from './user-edit/user-edit.component';
import { CraftspersonImportLogComponent } from './craftsperson-import-log/craftsperson-import-log.component';
import { ImportLogService } from "app/_services/import-log.service";
import { PasswordResetComponent } from './password-reset/password-reset.component';
import { UserActivateFormComponent } from './user-activate-form/user-activate-form.component';
import { FooterComponent } from './footer/footer.component';
import { CraftspersonLogoDirective } from "app/_directives/craftsperson-logo.directive";
import { CraftspersonsExportComponent } from './craftspersons-export/craftspersons-export.component';
import { ServerErrorService } from "app/_services/server-error.service";

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    LoginComponent,
    CfeComponent,
    CraftspersonsComponent,
    CraftspersonImportComponent,
    CraftspersonViewComponent,
    CraftspersonFormComponent,
    ValidatedInputComponent,
    CraftspersonAddComponent,
    CraftspersonEditComponent,
    UsersComponent,
    UserAddComponent,
    UserFormComponent,
    UserViewComponent,
    UserActivateComponent,
    UserEditComponent,
    CraftspersonImportLogComponent,
    PasswordResetComponent,
    UserActivateFormComponent,
    FooterComponent,
    CraftspersonLogoDirective,
    CraftspersonsExportComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    AppRoutingModule,
    AuthModule,
    ModalModule.forRoot(),
    BootstrapModalModule,
    NgxDatatableModule,
    MultiselectDropdownModule,
    NgxMyDatePickerModule,
    LinkyModule
  ],
  providers: [
    { provide: Http, useClass: AuthHttpService },
    AuthGuard,
    AuthenticationService,
    CfeService,
    CraftspersonService,
    DepartmentService,
    NafCodeService,
    UserService,
    ImportLogService,
    ServerErrorService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
