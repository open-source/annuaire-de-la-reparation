import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import { AuthenticationService } from '../_services/auth.service';

/** Guarde d'authentification */
@Injectable()
export class AuthGuard implements CanActivate {

    /**
     * Constructeur.
     * 
     * @param autService 
     * @param router 
     */
    constructor(
        private autService: AuthenticationService,
        private router: Router
    ) { }

    /**
     * Vérification d'accès à une route.
     */
    canActivate() {
        if (this.autService.loggedIn()) {
            // logged in so return true
            return true;
        }

        // not logged in so redirect to login page
        this.router.navigate(['/login']);
        return false;
    }
}