import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from "@angular/router";

import { UserService } from "app/_services/user.service";
import { UserActivateFormComponent } from "app/user-activate-form/user-activate-form.component";
import { ServerErrorService } from "app/_services/server-error.service";

enum RequestStatus {
    Success,
    InvalidToken,
    None
}

/** Composant d'activation d'un utilisateur */
@Component({
  selector: 'app-user-activate',
  templateUrl: './user-activate.component.html',
  styleUrls: ['./user-activate.component.scss']
})
export class UserActivateComponent implements OnInit {
  /** Token de l'utilisateur */
  token: string;
  /** Référence vers la classe de statut de la requête pour utilisation dans la vue */
  requestStatus = RequestStatus;
  /** Statut de la demande d'activation */
  currentStatus = RequestStatus.None;
  /** Formulaire de choix de mot de passe */
  @ViewChild(UserActivateFormComponent)
  private formComponent: UserActivateFormComponent;

  /**
   * Constructeur.
   * 
   * @param formBuilder
   */
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private userService: UserService,
    private serverErrorService: ServerErrorService
  ) { }

  /**
   * Récupération du token d'activation depuis l'url.
   */
  ngOnInit(): void {
    this.route.params.subscribe(params =>
      this.token = params['token']
    );
  }

  /**
   * Soumission du formulaire.
   * 
   * @param event 
   */
  onSubmitActivate(event: any) {
    let password: string = event.password as string;

    this.userService.activateUser(this.token, password).subscribe(
      response => this.currentStatus = RequestStatus.Success,
      err => {
        if (err.status == 404) {
          this.currentStatus = RequestStatus.InvalidToken;
        }
        else if (err.status == 400) {
          this.formComponent.showFormErrors(err);
        }
        else {
          this.serverErrorService.show(err);
        }
      }
    );
  }
}
