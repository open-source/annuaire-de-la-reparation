import { Component, OnInit, TemplateRef, ViewChild, ViewEncapsulation } from '@angular/core';
import { Modal, OneButtonPreset } from "angular2-modal/plugins/bootstrap";
import { DialogRef } from "angular2-modal/esm";

import { Subject } from "rxjs";
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/zip";

import { Craftsperson } from '../_models/craftsperson';
import { Department } from '../_models/department';
import { NafCode } from '../_models/naf-code';
import { GetCraftspersons } from '../_models/get-craftspersons';
import { CraftspersonService } from '../_services/craftsperson.service';
import { DepartmentService } from '../_services/department.service';
import { NafCodeService } from "app/_services/naf-code.service";
import { AppConstants } from "app/app.constants";
import { ServerErrorService } from "app/_services/server-error.service";

/** Composant de liste des réparateurs. */
@Component({
    templateUrl: 'craftspersons.component.html',
    styleUrls: ['craftspersons.component.css'],
    encapsulation: ViewEncapsulation.None
})
export class CraftspersonsComponent implements OnInit {
    /** Template pour une colonne affichant un booléen */
    @ViewChild('boolTmpl') boolTmpl: TemplateRef<any>;
    /** Template pour une colonne affichant une date */
    @ViewChild('dateTmpl') dateTmpl: TemplateRef<any>;
    /** Template pour une colonne affichant des actions */
    @ViewChild('actionTmpl') actionTmpl: TemplateRef<any>;
    /** Template pour une colonne affichant des codes NAF */
    @ViewChild('arrayTmpl') arrayTmpl: TemplateRef<any>;
    /** Template pour une en-tête avec filtre texte */
    @ViewChild('filterTmpl') filterTmpl: TemplateRef<any>;
    /** Template pour une en-tête avec filtre booléen */
    @ViewChild('boolFilterTmpl') boolFilterTmpl: TemplateRef<any>;
    /** Template pour une en-tête avec filtre sur le département */
    @ViewChild('depFilterTmpl') depFilterTmpl: TemplateRef<any>;
    /** Template pour une en-tête avec filtre sur la catégorie */
    @ViewChild('catFilterTmpl') catFilterTmpl: TemplateRef<any>;

    /** Description des colonnes de la table */
    columns = [];
    /** Réparateurs affichés */
    craftspersons: Craftsperson[];
    /** Nombre total de réparateurs correspondants au filtre courant */
    count = 0;
    /** Offset lié à la page courante */
    offset = 0;
    /** Nombre de réparateurs par page */
    limit = 50;
    /** Statut de chargement des données de la table */
    loading = false;
    /** Colonne de tri */
    sortColumn: string = null;
    /** Direction du tri */
    sortDir: string = null;
    /** Filtres de recherche sur les colonnes, il faut initialiser les propriétés des listes déroulantes */
    searchFilters: any = { cmaCode: '', nafCode: '', isReparactor: '', isUpdated: '', isEnabled: '', isError: '' };
    /** Départements */
    departments: Department[];
    /** Codes NAF */
    nafCodes: NafCode[];
    /** Sujet d'écoute pour les événements de filtrage */
    keyUpSubject = new Subject<string>();
    /** Réparateurs sélectionnés */
    selected: Craftsperson[] = [];
    /** Champ de sélection de page */
    pagerInput: any;

    /**
     * Constructeur.
     * 
     * @param craftspersonService 
     * @param departmentService 
     * @param nafCodeService 
     */
    constructor(
        private craftspersonService: CraftspersonService,
        private departmentService: DepartmentService,
        private nafCodeService: NafCodeService,
        private modalService: Modal,
        private serverErrorService: ServerErrorService
    ) { }

    /**
     * Initialisation du composant.
     */
    ngOnInit(): void {
        // Ajout d'un délai avant prise en compte du filtre
        this.keyUpSubject
            .debounceTime(AppConstants.FILTER_TYPE_DELAY_MS)
            .distinctUntilChanged()
            .subscribe((event) => this.onFilter(event));

        // Chargement des départements et catégories pour le filtre de la table
        this.departmentService.getDepartments().subscribe(response => this.departments = response);
        this.nafCodeService.getNafCodes().subscribe(response => this.nafCodes = response);

        // Colonnes de la table
        this.columns = [
            { checkboxable: true, sortable: false, width: 30, canAutoResize: false, draggable: false, resizeable: false },
            { prop: 'siret', headerTemplate: this.filterTmpl },
            { prop: 'name', name: 'Nom commercial', headerTemplate: this.filterTmpl },
            { prop: 'zipCode', name: 'Code postal', headerTemplate: this.filterTmpl },
            { prop: 'address1', name: 'Adresse - Ligne 1', headerTemplate: this.filterTmpl },
            { prop: 'cmaCode.name', name: 'Département', headerTemplate: this.depFilterTmpl },
            { prop: 'nafCode', name: 'NAFA', sortable: false, headerTemplate: this.catFilterTmpl, cellTemplate: this.arrayTmpl },
            { prop: 'phone', name: 'Téléphone', headerTemplate: this.filterTmpl },
            { prop: 'isReparactor', name: 'Répar\'acteur', headerTemplate: this.boolFilterTmpl, cellTemplate: this.boolTmpl },
            { prop: 'updateDate', name: 'Date de mise à jour', headerTemplate: this.filterTmpl, cellTemplate: this.dateTmpl },
            { prop: 'isUpdated', name: 'Modifié sur le site', headerTemplate: this.boolFilterTmpl, cellTemplate: this.boolTmpl },
            { prop: 'isEnabled', name: 'Affiché', headerTemplate: this.boolFilterTmpl, cellTemplate: this.boolTmpl },
            { prop: 'isError', name: 'Erreur signalée', headerTemplate: this.boolFilterTmpl, cellTemplate: this.boolTmpl },
            { prop: 'id', name: 'Actions', sortable: false, cellTemplate: this.actionTmpl }
        ]

        // Chargement des données de la table
        this.loadTableData();
    }

    /**
     * Retourne les codes NAF sous forme de chaine de caractères pour une ligne de la table.
     * 
     * @param codes 
     */
    getNafCodesString(codes: NafCode[]): string {
        return codes.map(n => n.code).join(', ');
    }

    /**
     * Retourne les classes CSS pour une ligne de la classe.
     * 
     * @param craftsperson 
     */
    getRowClass(craftsperson: Craftsperson) {
        return {
            'craftsperson-is-error': craftsperson.isError
        };
    }

    /**
     * Changement de page affichée.
     * 
     * @param event 
     */
    onPage(event: any) {
        // Mise à jour des paramètres de pagination
        this.offset = event.offset;
        this.limit = event.limit;

        // Rechargement des données affichées
        this.loadTableData();
    }

    /**
     * Changement de colonne ou direction de tri.
     * 
     * @param event 
     */
    onSort(event: any) {
        // Mise à jour des paramètres de tri
        const sort = event.sorts[0];
        this.sortColumn = sort.prop;
        this.sortDir = sort.dir;

        // Retour en 1er page
        this.offset = 0;

        // Rechargement des données affichées
        this.loadTableData();
    }

    /**
     * Filtrage d'une colonne.
     * 
     * @param event 
     */
    onFilter(event: any) {
        this.offset = 0;
        this.loadTableData();
    }

    /**
     * Nettoyage des filtres et rechargement de la grille.
     */
    onClearFilters() {
        Object.keys(this.searchFilters).forEach(key => {
            this.searchFilters[key] = '';
        });
        this.loadTableData();
    }

    /**
     * Suppression des réparateurs sélectionnés.
     */
    onDeleteSelected() {
        this.modalService.confirm()
            .title('Confirmation de suppression')
            .body('Supprimer ' + this.selected.length + ' réparateur(s) sélectionné(s) ?')
            .okBtn('Supprimer')
            .cancelBtn('Annuler')
            .open()
            .then(dialog => dialog.result)
            // Suppression effective en cas de confirmation
            .then(result => {
                const requests: any = Observable.from(this.selected.map(craftsperson => this.craftspersonService.deleteCraftsperson(craftsperson.id)));

                let waitModal: DialogRef<OneButtonPreset>;
                const deleteSubscription = requests.concatAll().subscribe(
                    response => { },
                    err => {
                        // Erreur
                        this.loadTableData();
                        if (waitModal) {
                            waitModal.close();
                        }
                        this.serverErrorService.show(err);
                    },
                    () => {
                        // Fin des requêtes
                        this.loadTableData();
                        this.modalService.alert().title('Réparateur(s) supprimé(s)').body('Réparateur(s) supprimé(s) avec succès.').open();
                        if (waitModal) {
                            waitModal.close();
                        }
                    });

                // Afficher une modale en attendant la fin du traitement, avec possibilité d'annulation
                this.modalService.alert().title('Traitement en cours').body('Suppression en cours, merci de patienter...').okBtn('Annuler')
                    .isBlocking(true)
                    .open()
                    .then(dialog => {
                        waitModal = dialog;
                        return dialog.result;
                    })
                    .then(result => {
                        // Clic sur Annuler
                        deleteSubscription.unsubscribe()
                        this.loadTableData();
                    });
            },
            () => {/** Ne rien faire si clic sur "Annuler" */ });
    }

    /**
     * Masquer/afficher les fiches sélectionnées.
     */
    onToggleHiddenSelected() {
        this.modalService.confirm()
            .title('Confirmation de modification du statut d\'affichage')
            .body('Modifier le statut d\'affichage (masqué/affiché) de(s) ' + this.selected.length + ' réparateur(s) sélectionné(s) ?')
            .okBtn('Masquer/Afficher')
            .cancelBtn('Annuler')
            .open()
            .then(dialog => dialog.result)
            .then(result => {
                const requests: any = Observable.from(this.selected.map(craftsperson => {
                    craftsperson.isEnabled = !craftsperson.isEnabled;
                    return this.craftspersonService.updateCraftsperson(craftsperson, craftsperson.nafCode.map(n => n.code).join(','));
                }));

                let waitModal: DialogRef<OneButtonPreset>;
                const toggleSubscription = requests.concatAll().subscribe(
                    response => { },
                    err => {
                        this.loadTableData();
                        if (waitModal) {
                            waitModal.close();
                        }
                        this.serverErrorService.show(err);
                    },
                    () => {
                        // Fin des requêtes
                        this.loadTableData();
                        this.modalService.alert().title('Statut d\'affichage modifié').body('Statut d\'affichage modifié avec succès.').open();
                        if (waitModal) {
                            waitModal.close();
                        }
                    }
                );

                // Afficher une modale en attendant la fin du traitement, avec possibilité d'annulation
                this.modalService.alert().title('Traitement en cours').body('Masquage/affichage en cours, merci de patienter...').okBtn('Annuler')
                    .isBlocking(true)
                    .open()
                    .then(dialog => {
                        waitModal = dialog;
                        return dialog.result;
                    })
                    .then(result => {
                        // Clic sur Annuler
                        toggleSubscription.unsubscribe()
                        this.loadTableData();
                    });
            },
            () => {/** Ne rien faire si clic sur "Annuler" */ });
    }

    /**
     * Sélection de réparateurs.
     * 
     * @param selected 
     */
    onSelect({ selected }) {
        this.selected.splice(0, this.selected.length);
        this.selected.push(...selected);
    }

    /**
     * Chargement des données à afficher dans la table en fonction des paramètres de pagination, tri et filtrage.
     */
    private loadTableData() {
        // Vidage de la sélection
        this.selected = [];
        // Indicateur de chargement
        this.loading = true;

        const start = this.offset * this.limit;
        this.craftspersonService.getCraftspersons(start, this.limit, this.sortColumn, this.sortDir, this.searchFilters['updateDate'], this.searchFilters['siret'], this.searchFilters['name'], this.searchFilters['zipCode'], this.searchFilters['address1'], this.searchFilters['phone'], this.searchFilters['isError'], this.searchFilters['isReparactor'], this.searchFilters['isEnabled'], this.searchFilters['isUpdated'], this.searchFilters['cmaCode'], this.searchFilters['nafCode'])
            .subscribe(response => {
                this.craftspersons = response.craftspersons;
                this.count = response.total;

                const end = start + this.limit;
                const craftspersons = [...this.craftspersons];

                for (let i = start; i < end; i++) {
                    craftspersons[i] = response.craftspersons[i - start];
                }

                this.craftspersons = craftspersons;
                this.loading = false;
            },
            err => this.serverErrorService.show(err));
    }

    /**
     * Nombre de pages.
     */
    get pageCount() {
        return Math.ceil(this.count / this.limit);
    }

    /**
     * Page courante.
     */
    get currentPage() {
        return this.offset + 1;
    }

    /**
     * Numéro de page entrée dans le champ de sélection.
     */
    get pageValue() {
        if (typeof (this.pagerInput) === 'number' && (this.pagerInput % 1) === 0 && this.pagerInput > 0 && this.pagerInput <= this.pageCount) {
            let result = this.pagerInput;
            this.pagerInput = null;
            return result;
        }
        else {
            return this.currentPage;
        }
    }
}