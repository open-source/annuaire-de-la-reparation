import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from "@angular/router";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";

import { AppConstants } from "app/app.constants";
import { UserService } from "app/_services/user.service";
import { UserActivateFormComponent } from "app/user-activate-form/user-activate-form.component";
import { ServerErrorService } from "app/_services/server-error.service";

enum RequestStatus {
    Success,
    EmailSent,
    Token,
    InvalidToken,
    InvalidEmail,
    None
}

/** Composant de réinitialisation de mot de passe */
@Component({
  selector: 'app-password-reset',
  templateUrl: './password-reset.component.html',
  styleUrls: ['./password-reset.component.scss']
})
export class PasswordResetComponent implements OnInit {
  /** Champs du formulaire */
  emailForm: FormGroup;
  /** Token de réinitialisation */
  token: string;
  /** Statut du composant */
  currentStatus: RequestStatus = RequestStatus.None;
  /** Référence vers la classe de statut de la requête pour utilisation dans la vue */
  requestStatus = RequestStatus;
  /** Formulaire de choix de mot de passe */
  @ViewChild(UserActivateFormComponent)
  private formComponent: UserActivateFormComponent;

  /**
   * Constructeur.
   */
  constructor(
    private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    private userService: UserService,
    private serverErrorService: ServerErrorService
  ) {
    // Initialisation des champs du formulaire
    this.emailForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.pattern(AppConstants.EMAIL_REGEX)]],
    });
  }

  /**
   * Récupération du token de changement de mot de passe depuis l'url.
   */
  ngOnInit() {
    this.route.params.subscribe(params => {
        this.token = params['token'];
        if(this.token) {
          this.currentStatus = RequestStatus.Token;
        }
      }
    );
  }

  /**
   * Soumission de la demande de réinitialisation.
   */
  onSubmitEmail() {
    const email = this.emailForm.value.email;

    this.userService.passwordResetRequest(email).subscribe(
      response => this.currentStatus = RequestStatus.EmailSent,
      err => {
        if (err.status == 400) {
          this.showFormErrors(err);
        }
        else {
          this.serverErrorService.show(err);
        }
      }
    )
  }

  /**
   * Soumission du nouveau mot de passe.
   * 
   * @param event 
   */
  onSubmitActivate(event: any) {
    let password: string = event.password as string;

    this.userService.activateUser(this.token, password).subscribe(
      response => this.currentStatus = RequestStatus.Success,
      err => {
        if (err.status == 404) {
          this.currentStatus = RequestStatus.InvalidToken;
        }
        else if (err.status == 400) {
          this.formComponent.showFormErrors(err);
        }
        else {
          this.serverErrorService.show(err);
        }
      }
    );
  }

  /**
   * Affichage des erreurs de formulaire.
   * 
   * @param error 
   */
  private showFormErrors(error: any) {
    if (error.status == 400) {
      const data = error.json().errors.children;
      const fields = Object.keys(data || {});
      fields.filter(f => data[f].errors && data[f].errors.length).forEach((field) => {
        const control = this.emailForm.get(field);
        const errors = {};
        data[field].errors.forEach(error => {
          errors[error] = true;
        })
        control.setErrors(errors);
      });
    }
  }
}
