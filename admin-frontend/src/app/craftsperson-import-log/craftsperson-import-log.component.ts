import { Component, ViewChild, TemplateRef, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { IMultiSelectSettings, IMultiSelectTexts, IMultiSelectOption } from "angular-2-dropdown-multiselect";
import { INgxMyDpOptions } from "ngx-mydatepicker";

import { AppConstants } from "app/app.constants";
import { ImportLogService } from "app/_services/import-log.service";
import { ImportLog } from "app/_models/import-log";
import { ServerErrorService } from "app/_services/server-error.service";

/** Composant de recherche de logs d'import de réparateur. */
@Component({
  selector: 'app-craftsperson-import-log',
  templateUrl: './craftsperson-import-log.component.html',
  styleUrls: ['./craftsperson-import-log.component.scss']
})
export class CraftspersonImportLogComponent implements OnInit {
  /** Template pour une colonne affichant une date */
  @ViewChild('dateTmpl')
  dateTmpl: TemplateRef<any>;
  /** Champs du formulaire */
  importLogForm: FormGroup;
  /** Erreurs de validation globales du formulaire */
  importLogFormGlobalErrors: string[] = [];
  /** Résultats de la recherche */
  importLogs: ImportLog[] = null;
  /** Indicateur de chargement de la table */
  loading = false;
  /** Régions */
  readonly regions: IMultiSelectOption[] = [
    { id: 0, name: 'Régions actuelles', isLabel: true },
    { id: 'auvergne-rhone-alpes', name: 'Auvergne-Rhone-Alpes' },
    { id: 'bourgogne-franche-comte', name: 'Bourgogne-Franche-Comté' },
    { id: 'bretagne', name: 'Bretagne' },
    { id: 'centre-val de loire', name: 'Centre-Val de Loire' },
    { id: 'corse', name: 'Corse' },
    { id: 'grand est', name: 'Grand Est' },
    { id: 'hauts-de-france', name: 'Hauts-de-France' },
    { id: 'ile-de-france', name: 'Île-de-France' },
    { id: 'normandie', name: 'Normandie' },
    { id: 'nouvelle-aquitaine', name: 'Nouvelle-Aquitaine' },
    { id: 'occitanie', name: 'Occitanie' },
    { id: 'pays de la loire', name: 'Pays de la Loire' },
    { id: 'provence-alpes-cote d\'azur', name: 'Provence-Alpes-Côte d\'Azur' },
    { id: 'guadeloupe', name: 'Guadeloupe' },
    { id: 'martinique', name: 'Martinique' },
    { id: 'guyane', name: 'Guyane' },
    { id: 'la reunion', name: 'La Réunion' },
    { id: 'mayotte', name: 'Mayotte' },
    { id: 1, name: 'Anciennes régions', isLabel: true },
    { id: 'alsace', name: 'Alsace' },
    { id: 'aquitaine', name: 'Aquitaine' },
    { id: 'auvergne', name: 'Auvergne' },
    { id: 'bourgogne', name: 'Bourgogne' },
    { id: 'centre', name: 'Centre' },
    { id: 'champagne-ardenne', name: 'Champagne-Ardenne' },
    { id: 'franche-comte', name: 'Franche-Comté' },
    { id: 'languedoc-roussillon', name: 'Languedoc-Roussillon' },
    { id: 'limousin', name: 'Limousin' },
    { id: 'lorraine', name: 'Lorraine' },
    { id: 'midi-pyrenees', name: 'Midi-Pyrénées' },
    { id: 'nord-pas-de-calais', name: 'Nord-Pas-de-Calais' },
    { id: 'basse-normandie', name: 'Basse-Normandie' },
    { id: 'haute-normandie', name: 'Haute-Normandie' },
    { id: 'picardie', name: 'Picardie' },
    { id: 'poitou-charentes', name: 'Poitou-Charentes' },
    { id: 'rhone-alpes', name: 'Rhône-Alpes' }
  ];
  /** Configuration des dropdown */
  readonly dropdownSettings: IMultiSelectSettings = AppConstants.DROPDOWN_SETTINGS;
  /** Textes des dropdown */
  readonly dropdownTexts: IMultiSelectTexts = AppConstants.DROPDOWN_TEXTS;
  /** Configuration des datepicker */
  readonly datepickerSettings: INgxMyDpOptions = AppConstants.DATEPICKER_SETTINGS;
  /** Colonnes de la table */
  columns = [];

  /**
   * Constructeur.
   * 
   * @param formBuilder
   */
  constructor(
    private importLogService: ImportLogService,
    private formBuilder: FormBuilder,
    private serverErrorService: ServerErrorService
  ) {
    // Initialisation des champs du formulaire
    this.importLogForm = this.formBuilder.group({
      startDate: [''],
      endDate: [''],
      regions: [[]]
    });
  }

  /**
   * Initialisation des colonnes de la table résultat.
   */
  ngOnInit() {
    this.columns = [
      { prop: 'date', name: 'Date', cellTemplate: this.dateTmpl },
      { prop: 'region', name: 'Région' },
      { prop: 'siret', name: 'SIRET' },
      { prop: 'filename', name: 'Fichier' },
      { prop: 'line', name: 'Ligne' },
      { prop: 'message', name: 'Message' }
    ];
  }

  /**
   * Soumission du formulaire.
   */
  onSubmit(event: Event) {
    const formModel = this.importLogForm.value;

    let startDate: Date = formModel.startDate ? formModel.startDate.jsdate : null;
    let endDate: Date = formModel.endDate ? formModel.endDate.jsdate : null;

    this.importLogService.get(startDate, endDate, formModel.regions).subscribe(
      response => {
        this.importLogFormGlobalErrors = [];
        this.importLogs = response;
      },
      err => {
        if (err.status == 400) {
          this.importLogs = null;
          this.showFormErrors(err);
        }
        else {
          this.serverErrorService.show(err);
        }
      }
    );
  }

  /**
   * Affichage des erreurs de validation serveur.
   * 
   * @param error 
   */
  public showFormErrors(error: any) {
    if (error.status == 400) {
      // Erreurs globales
      this.importLogFormGlobalErrors = [];
      if ('errors' in error.json().errors) {
        const globalErrors = error.json().errors.errors;
        globalErrors.forEach(error => {
          if (AppConstants.VALIDATION_LABELS.hasOwnProperty(error) === true) {
            this.importLogFormGlobalErrors.push(AppConstants.VALIDATION_LABELS[error]);
          }
        });
      }

      // Erreurs spécifiques aux champs
      const data = error.json().errors.children;
      const fields = Object.keys(data || {});
      fields.filter(f => data[f].errors && data[f].errors.length).forEach((field) => {
        const control = this.importLogForm.get(field);
        const errors = {};
        data[field].errors.forEach(error => {
          errors[error] = true;
        })
        control.setErrors(errors);
      });
    }
  }
}
