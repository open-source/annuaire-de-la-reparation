import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AuthGuard } from './_guards/auth.guard';

import { LoginComponent } from './login/login.component';
import { CfeComponent } from './cfe/cfe.component';
import { CraftspersonsComponent } from './craftspersons/craftspersons.component';
import { CraftspersonImportComponent } from './craftsperson-import/craftsperson-import.component';
import { CraftspersonViewComponent } from './craftsperson-view/craftsperson-view.component';
import { CraftspersonFormComponent } from './craftsperson-form/craftsperson-form.component';
import { CraftspersonAddComponent } from './craftsperson-add/craftsperson-add.component';
import { CraftspersonEditComponent } from "app/craftsperson-edit/craftsperson-edit.component";
import { UsersComponent } from "app/users/users.component";
import { UserAddComponent } from "app/user-add/user-add.component";
import { UserViewComponent } from "app/user-view/user-view.component";
import { UserActivateComponent } from "app/user-activate/user-activate.component";
import { UserEditComponent } from "app/user-edit/user-edit.component";
import { CraftspersonImportLogComponent } from "app/craftsperson-import-log/craftsperson-import-log.component";
import { PasswordResetComponent } from "app/password-reset/password-reset.component";
import { CraftspersonsExportComponent } from "app/craftspersons-export/craftspersons-export.component";

const routes: Routes = [
    { path: 'login', component: LoginComponent },
    { path: 'users/activate/:token', component: UserActivateComponent },
    { path: 'users/reset-password/:token', component: PasswordResetComponent },
    { path: 'users/reset-password', component: PasswordResetComponent },
    { path: 'cfe', component: CfeComponent, canActivate: [AuthGuard] },
    { path: 'craftspersons', component: CraftspersonsComponent, canActivate: [AuthGuard] },
    { path: 'craftspersons/import', component: CraftspersonImportComponent, canActivate: [AuthGuard] },
    { path: 'craftspersons/import/log', component: CraftspersonImportLogComponent, canActivate: [AuthGuard] },
    { path: 'craftspersons/export', component: CraftspersonsExportComponent, canActivate: [AuthGuard] },
    { path: 'craftspersons/add', component: CraftspersonAddComponent, canActivate: [AuthGuard] },
    { path: 'craftspersons/:id', component: CraftspersonViewComponent, canActivate: [AuthGuard] },
    { path: 'craftspersons/edit/:id', component: CraftspersonEditComponent, canActivate: [AuthGuard] },
    { path: 'users', component: UsersComponent, canActivate: [AuthGuard] },
    { path: 'users/add', component: UserAddComponent, canActivate: [AuthGuard] },
    { path: 'users/:id', component: UserViewComponent, canActivate: [AuthGuard] },
    { path: 'users/edit/:id', component: UserEditComponent, canActivate: [AuthGuard] },
    // redirection vers l'accueil
    { path: '**', redirectTo: 'craftspersons' }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }