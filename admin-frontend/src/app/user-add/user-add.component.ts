import { Component, ViewChild } from '@angular/core';
import { Location } from '@angular/common';
import { Router } from '@angular/router';

import { User } from 'app/_models/user';
import { UserService } from 'app/_services/user.service';
import { UserFormComponent } from "app/user-form/user-form.component";
import { Role } from "app/_enums/role";
import { ServerErrorService } from "app/_services/server-error.service";

/** Composant d'ajout d'utilisateur. */
@Component({
  selector: 'app-user-add',
  templateUrl: './user-add.component.html',
  styleUrls: ['./user-add.component.scss']
})
export class UserAddComponent {
  /** Formulaire */
  @ViewChild(UserFormComponent)
  private formComponent: UserFormComponent;
  /** Nouvel utilisateur */
  user: User;

  /**
   * Constructeur.
   */
  constructor(
    private userService: UserService,
    private router: Router,
    private location: Location,
    private serverErrorService: ServerErrorService
  ) {
    this.user = {
      id: 0,
      firstname: '',
      lastname: '',
      email: '',
      phone: '',
      codeDepartment: [],
      isMailContact: true,
      roles: [Role[Role.ROLE_CMA]]
    }
  }

  /**
   * Soumission du formulaire : ajout d'un utilisateur.
   * 
   * @param event 
   */
  onSubmitUser(event: any) {
    let user: User = event.user as User;
    let departments: string[] = event.departments as string[];

    this.userService.addUser(user, departments).subscribe(
      user => this.router.navigate(['users']),
      err => {
        if (err.status == 400) {
          this.formComponent.showFormErrors(err);
        }
        else {
          this.serverErrorService.show(err);
        }
      }
    );
  }

  /**
   * Retourne à la page précédente.
   */
  onBack() {
    this.location.back();
  }
}
