import { Component, OnInit, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { Location } from '@angular/common';
import { FormBuilder, FormGroup, FormControl, Validators, AsyncValidatorFn, AbstractControl } from "@angular/forms";

import { Craftsperson } from "app/_models/craftsperson";
import { NafCodeService } from "app/_services/naf-code.service";
import { NafCode } from "app/_models/naf-code";
import { Department } from "app/_models/department";
import { UserService } from "app/_services/user.service";
import { AppConstants } from "app/app.constants";
import { AuthenticationService } from "app/_services/auth.service";

/** Formulaire d'édition de réparateur */
@Component({
  selector: 'app-craftsperson-form',
  templateUrl: './craftsperson-form.component.html',
  styleUrls: ['./craftsperson-form.component.css']
})
export class CraftspersonFormComponent implements OnInit {
  /** Champ de sélection de logo */
  @ViewChild('logoInput') logoInput: any;
  /** Réparateur en cours d'édition */
  @Input() craftsperson: Craftsperson;
  /** Soumission du formulaire */
  @Output() submitCraftsperson: EventEmitter<any> = new EventEmitter<any>();
  /** Champs du formulaire */
  craftspersonForm: FormGroup;
  /** Liste de départements */
  departments: Department[];
  /** Fichier de logo */
  private logo: File;

  /**
   * Constructeur.
   * 
   * @param nafCodeService
   * @param userService
   * @param formBuilder
   */
  constructor(
    private authService: AuthenticationService,
    private nafCodeService: NafCodeService,
    private userService: UserService,
    private formBuilder: FormBuilder,
    private location: Location) {

    // Initialisation des champs du formulaire
    this.craftspersonForm = this.formBuilder.group({
      siret: ['', [Validators.required, Validators.minLength(14), Validators.maxLength(14)]],
      name: ['', Validators.required],
      isMasked: '',
      isError: '',
      address1: ['', Validators.required],
      address2: '',
      zipCode: ['', [Validators.required, Validators.minLength(5), Validators.maxLength(5), Validators.pattern('[0-9]+')]],
      zipCodeLabel: ['', Validators.required],
      phone: ['', [Validators.minLength(10), Validators.pattern(AppConstants.PHONE_REGEX)]],
      email: ['', Validators.pattern(AppConstants.EMAIL_REGEX)],
      website: '',
      isReparactor: '',
      reparactorDescription: '',
      reparactorHours: '',
      reparactorServices: '',
      reparactorCertificates: '',
      nafCodes: ['', Validators.required],
      cmaCode: '',
      otherInfo: ''
    });
  }

  /** 
   * Initialisation du composant.
   */
  ngOnInit() {
    // CMA sélectionnables
    this.departments = this.authService.getLoggedInUser().codeDepartment;

    // Récupération des informations du réparateur
    this.initFormValues();
  }

  /**
   * Chargement des valeurs du formulaire à partir des données du réparateur en cours d'édition.
   */
  private initFormValues() {
    this.craftspersonForm.setValue({
      siret: this.craftsperson.siret ? this.craftsperson.siret : '',
      name: this.craftsperson.name,
      isMasked: !this.craftsperson.isEnabled,
      isError: this.craftsperson.isError,
      address1: this.craftsperson.address1,
      address2: this.craftsperson.address2,
      zipCode: this.craftsperson.zipCode,
      zipCodeLabel: this.craftsperson.zipCodeLabel,
      phone: this.craftsperson.phone,
      email: this.craftsperson.email,
      website: this.craftsperson.website,
      isReparactor: this.craftsperson.isReparactor,
      reparactorDescription: this.craftsperson.reparactorDescription,
      reparactorHours: this.craftsperson.reparactorHours,
      reparactorServices: this.craftsperson.reparactorServices,
      reparactorCertificates: this.craftsperson.reparactorCertificates,
      nafCodes: this.craftsperson.nafCode ? this.craftsperson.nafCode.map(n => n.code).join(',') : '',
      cmaCode: this.craftsperson.cmaCode ? this.craftsperson.cmaCode.code : '',
      otherInfo: this.craftsperson.otherInfo,
    });
  }

  /**
   * Soumission du formulaire.
   */
  onSubmit(form, event: Event) {
    const formModel = this.craftspersonForm.value;

    const saveCraftsperson: Craftsperson = {
      id: this.craftsperson.id,
      siret: formModel.siret as number,
      name: formModel.name,
      nafCode: this.craftsperson.nafCode,
      zipCode: formModel.zipCode,
      zipCodeLabel: formModel.zipCodeLabel,
      address1: formModel.address1,
      address2: formModel.address2,
      phone: formModel.phone,
      email: formModel.email,
      website: formModel.website,
      isReparactor: formModel.isReparactor as boolean,
      updateDate: this.craftsperson.updateDate,
      creationDate: this.craftsperson.creationDate,
      isUpdated: this.craftsperson.isUpdated,
      isEnabled: !(formModel.isMasked as boolean),
      isError: formModel.isError as boolean,
      reparactorDescription: formModel.reparactorDescription,
      reparactorHours: formModel.reparactorHours,
      reparactorServices: formModel.reparactorServices,
      reparactorCertificates: formModel.reparactorCertificates,
      cmaCode: this.departments.find(department => department.code == formModel.cmaCode),
      logoFile: '',
      otherInfo: formModel.otherInfo,
    };

    this.submitCraftsperson.emit({
      craftsperson: saveCraftsperson,
      nafCodes: formModel.nafCodes,
      logo: this.logo
    })
  }

  /**
   * Statut d'activation du bouton de soumission.
   */
  isSubmitDisabled() {
    return this.craftspersonForm.invalid;
  }

  /**
   * Modification du logo sélectionné.
   * 
   * @param event 
   */
  onLogoChange(event) {
      let fileList: FileList = event.target.files;
      if (fileList.length > 0) {
          this.logo = fileList[0];
      }
  }

  /**
   * Retour à la page précédente.
   */
  onBack() {
    this.location.back();
  }

  /**
   * Affichage des erreurs de formulaire.
   * 
   * @param error 
   */
  public showFormErrors(error: any) {
    if (error.status == 400) {
      const data = error.json().errors.children;
      const fields = Object.keys(data || {});
      fields.filter(f => data[f].errors && data[f].errors.length).forEach((field) => {
        const control = this.craftspersonForm.get(field);
        const errors = {};
        data[field].errors.forEach(error => {
          errors[error] = true;
        })
        control.setErrors(errors);
      });
    }
  }
}
