
#remove ARAdminBundle previous assets
rm -Rf ../backend/src/ARAdminBundle/Resources/public/*

#create assets subfolders in ARAdminBundle asset folder
mkdir ../backend/src/ARAdminBundle/Resources/public/js
npm run build-prod

cd ../backend/src/ARAdminBundle/Resources/public/js
mv inline.[0-9a-z]*.bundle.js inline.bundle.js
mv main.[0-9a-z]*.bundle.js main.bundle.js
mv polyfills.[0-9a-z]*.bundle.js polyfills.bundle.js
mv vendor.[0-9a-z]*.bundle.js vendor.bundle.js
mv styles.[0-9a-z]*.bundle.css styles.bundle.css
rm *.txt
