# sh

RELEASE_FOLDER=~/dvp/are/release/are
NUM_VERSION=1.4.0

#Démarrage du container docker
docker-compose up -d

# Création d'un répertoire "target" et ajout du code source de la dernière version de l'application (backend seulement) sur git
echo "Ajout du code source dans le répertoire target sur docker"
docker exec are_site_1 rm -rf target
docker exec are_site_1 mkdir target
rm -rf $RELEASE_FOLDER
git clone https://gitlab.ext-itlink.fr/ademe/are.git $RELEASE_FOLDER
DOCKER_CONTAINER_ID="$(docker ps -aqf 'name=are_site_1')"
docker cp $RELEASE_FOLDER/backend $DOCKER_CONTAINER_ID:/u/target

read -p "Appuyer sur une touche pour continuer ..."

# Suppression vendors
echo "Suppression des anciennes ressources si elles existent sur docker puis regénération des ressources"
docker exec are_site_1 bash -c 'cd target/backend && rm -rf vendor/ composer.lock .composer/cache'
echo "Création du fichier parameters.yml depuis parameters.yml.dist"
docker exec are_site_1 bash -c 'cd target/backend && cp app/config/parameters.yml.dist app/config/parameters.yml'

read -p "Appuyer sur une touche pour continuer ..."


# Installer / Mettre à jour les modules Symfony
echo "Installation / Mise à jour des modules Symfony"
docker exec are_site_1 bash -c 'cd target/backend && composer install'

read -p "Appuyer sur une touche pour continuer ..."

# Génération des assets de Symfony
echo "Génération des assets de Symfony"
docker exec are_site_1 bash -c 'cd target/backend && php app/console assets:install web'

read -p "Appuyer sur une touche pour continuer ..."

# Création des tar.gz
echo "Création des archives"

echo "Création de l'archive contenant l'application Symfony et toutes les sources compilées"
docker exec are_site_1 bash -c 'cd target && tar czvf ARE-LIV01-'$NUM_VERSION'.tar.gz backend'
echo "Création de l'archive contenant le code source"
cd $RELEASE_FOLDER
cd ..
tar czvf ARE-LIV01-SRC-$NUM_VERSION.tar.gz are

# Remove backend in target folder
docker exec are_site_1 bash -c 'cd target && rm -rf backend'
