#!/bin/bash

# Pour fonctionner le script doit être utilisé avec sudo

# Nécessite d'installer la clé publique RSA : 
# Avec l'une des commandes suivantes
# - ssh-copy-id deb@87.98.189.143 -p 2032
# ou 
# - cat ~/.ssh/id_rsa.pub | ssh deb@87.98.189.143 -p 2032 "mkdir -p ~/.ssh && cat >> ~/.ssh/authorized_keys"
### Demander le mot de passe à JGY

# Nécessite de donner accès au répertoire backend/cache
# - sudo 

DOCKER_IP=87.98.189.143
DOCKER_PORT=2032

echo --- Stop local containers
docker-compose stop

echo --- Give correct rights to cache, logs and upload files
sudo chmod a+w -R backend/app/cache backend/app/logs backend/web/uploads
echo --- Removing existing symfony cache
rm -Rf backend/app/cache/*

echo --- Removing existing symfony bundle
sudo chmod a+w -R backend/web/bundles
rm -Rf backend/web/bundles/*

echo -- stop existing containers
ssh deb@$DOCKER_IP -p $DOCKER_PORT 'docker-compose -f /home/deb/docker-are/docker-compose.yml stop'

echo -- stop and remove existing containers
ssh deb@$DOCKER_IP -p $DOCKER_PORT 'docker-compose -f /home/deb/docker-are/docker-compose.yml rm -f'

echo -- copy new files
rsync -avz -e "ssh -p $DOCKER_PORT" --progress --partial ./docker-compose.yml deb@$DOCKER_IP:/home/deb/docker-are/
rsync -avz -e "ssh -p $DOCKER_PORT" --progress --partial ./backend deb@$DOCKER_IP:/home/deb/docker-are
rsync -avz -e "ssh -p $DOCKER_PORT" --progress --partial ./sql deb@$DOCKER_IP:/home/deb/docker-aree

echo -- start new containers
ssh deb@$DOCKER_IP -p $DOCKER_PORT 'docker-compose -f /home/deb/docker-are/docker-compose.yml build'
ssh deb@$DOCKER_IP -p $DOCKER_PORT 'docker-compose -f /home/deb/docker-are/docker-compose.yml up -d'


