# Installation de l'application
echo --- Removing existing symfony cache
php app/console cache:clear
php app/console cache:clear --env=prod

echo --- Running composer
composer install

echo --- Install symfony resources
php app/console assets:install web


chmod -R 777 app/cache app/logs

echo --- Build SSL infor for JWT
mkdir -p var/jwt
openssl genrsa -out var/jwt/private.pem 4096
openssl rsa -pubout -in var/jwt/private.pem -out var/jwt/public.pem

echo --- Install cron tasks
cat crontasks | crontab -u www-data -