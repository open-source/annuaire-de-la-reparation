<?php

namespace ARApiV1Bundle\Form;

use ARCommonBundle\Enum\Entity;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * La classe CraftspersonType définit un formulaire pour les entités {@link Craftsperson}.
 *
 * @author vdouillet
 *
 */
class CraftspersonType extends AbstractType {

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('siret');
        $builder->add('name');
        $builder->add('address1');
        $builder->add('address2');
        $builder->add('zipCode');
        $builder->add('zipCodeLabel');
        $builder->add('phone');
        $builder->add('email');
        $builder->add('website');
        $builder->add('isEnabled', CheckboxType::class);
        $builder->add('isReparactor', CheckboxType::class);
        $builder->add('isError', CheckboxType::class);
        $builder->add('reparactorDescription');
        $builder->add('reparactorHours');
        $builder->add('reparactorServices');
        $builder->add('reparactorCertificates');
        $builder->add('otherInfo');
        $builder->add('cmaCode', EntityType::class, array(
            'class' => Entity::DEPARTMENT,
        ));
        $builder->add('nafCodes', TextType::class, array(
            'mapped' => false,
        ));
        $builder->add('submit', SubmitType::class);
    }

    /**
     * {@inheritdoc}
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class'        => 'ARCommonBundle\Entity\Craftsperson',
            'csrf_protection'   => false,
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'craftsperson';
    }
}