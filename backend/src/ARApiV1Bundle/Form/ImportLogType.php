<?php

namespace ARApiV1Bundle\Form;

use ARCommonBundle\Enum\Entity;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Regex;

/**
 * La classe ImportLogType définit un formulaire pour la recherche d'entités {@link ImportLog}.
 *
 * @author vdouillet
 *
 */
class ImportLogType extends AbstractType {

    const DATE_REGEX = '/^\d{2}\/\d{2}\/\d{4}$/';

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('startDate', TextType::class, array(
            'required' => true,
            'constraints' => array(
                new Regex(array(
                    'pattern' => $this::DATE_REGEX,
                    'message' => 'pattern'
                ))
            )
        ));
        $builder->add('endDate', TextType::class, array(
            'required' => true,
            'constraints' => array(
                new Regex(array(
                    'pattern' => $this::DATE_REGEX,
                    'message' => 'pattern'
                ))
            )
        ));
        $builder->add('regions');
    }

    /**
     * {@inheritdoc}
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'csrf_protection'   => false,
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'import_log';
    }
}