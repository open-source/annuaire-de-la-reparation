<?php

namespace ARApiV1Bundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * La classe UserType définit un formulaire pour les entités {@link User}.
 *
 * @author vdouillet
 *
 */
class UserType extends AbstractType {

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('lastname');
        $builder->add('firstname');
        $builder->add('phone');
        $builder->add('email');
        $builder->add('isMailContact', CheckboxType::class);
        $builder->add('departments', TextType::class, array(
            'mapped' => false,
        ));
        $builder->add('role', TextType::class, array(
            'mapped' => false,
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class'        => 'ARCommonBundle\Entity\User',
            'csrf_protection'   => false,
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'user';
    }
}