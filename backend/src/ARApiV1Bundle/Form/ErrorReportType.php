<?php

namespace ARApiV1Bundle\Form;

use ARCommonBundle\Entity\CraftspersonErrorMessage;
use ARCommonBundle\Enum\Entity;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

/**
 * La classe ErrorReportType définit un formulaire pour le signalement d'erreur sur les entités {@link Craftsperson}.
 *
 * @author vdouillet
 *
 */
class ErrorReportType extends AbstractType {

    // Longueur de la description
    const DESCRIPTION_MIN_LENGTH = 50;
    const DESCRIPTION_MAX_LENGTH = 500;

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('description', EntityType::class, array(
            'class' => CraftspersonErrorMessage::class,
            'choice_label' => 'message',
            'expanded' => true,
            'multiple' => true
        ));
        /*$builder->add('description', TextType::class, array(
            'constraints' => array(
                new NotBlank(array(
                    'message' => 'required'
                )),
                new Length(array(
                    'min' => $this::DESCRIPTION_MIN_LENGTH,
                    'max' => $this::DESCRIPTION_MAX_LENGTH,
                    'minMessage' => 'length',
                    'maxMessage' => 'length'
                ))
            )
        ));*/
    }

    /**
     * {@inheritdoc}
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'csrf_protection'   => false,
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'error_report';
    }
}