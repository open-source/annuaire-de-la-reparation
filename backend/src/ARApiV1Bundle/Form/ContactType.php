<?php

namespace ARApiV1Bundle\Form;

use ARCommonBundle\Entity\CraftspersonErrorMessage;
use ARCommonBundle\Enum\Entity;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Email;

/**
 * La classe ContactType définit un formulaire pour l'envoi de mail à un {@link Craftsperson}.
 *
 */
class ContactType extends AbstractType {

    // Longueur de la description
    const MESSAGE_MIN_LENGTH = 50;
    const MESSAGE_MAX_LENGTH = 1000;

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('email', TextType::class, array(
            'constraints' => array(
                new NotBlank(array(
                    'message' => 'required'
                )),
                new Email(array(
                    'message' => 'invalid mail'
                ))
            )
        ));

        $builder->add('message', TextType::class, array(
            'constraints' => array(
                new NotBlank(array(
                    'message' => 'required'
                )),
                new Length(array(
                    'min' => $this::MESSAGE_MIN_LENGTH,
                    'max' => $this::MESSAGE_MAX_LENGTH,
                    'minMessage' => 'length',
                    'maxMessage' => 'length'
                ))
            )
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'csrf_protection'   => false,
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'contact_craftsperson';
    }
}