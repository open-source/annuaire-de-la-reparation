<?php

namespace ARApiV1Bundle\Exception;

use Symfony\Component\HttpKernel\Exception\HttpException;

/**
 * RetryWithHttpException.
 *
 * @author vdouillet
 */
class RetryWithHttpException extends HttpException
{
    /**
     * Constructeur.
     *
     * @param string $message
     * @param \Exception $previous
     * @param int $code
     */
    public function __construct($message = null, \Exception $previous = null, $code = 0)
    {
        parent::__construct(449, $message, $previous, array(), $code);
    }
}
