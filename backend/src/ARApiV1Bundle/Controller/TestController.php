<?php

namespace ARApiV1Bundle\Controller;

use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;

class TestController extends FOSRestController {

  /**
   * @Rest\Get("/test")
   * @Rest\View
   */
  public function indexAction() {
      /* @var \ARCommonBundle\Service\GeocodeService $geocoder */
      $geocoder = $this->get('common.geocode_service');

      return array('result' => $geocoder->geocodeAddress('Strasbourg'));
  }
}