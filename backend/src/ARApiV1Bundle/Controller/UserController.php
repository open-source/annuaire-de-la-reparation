<?php

namespace ARApiV1Bundle\Controller;

use ARApiV1Bundle\Form\PasswordResetRequestType;
use ARApiV1Bundle\Form\UserActivateType;
use ARApiV1Bundle\Form\UserType;
use ARCommonBundle\Entity\User;
use ARCommonBundle\Enum\Entity;
use ARCommonBundle\Enum\Role;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Request\ParamFetcherInterface;
use FOS\RestBundle\View\View;
use Nelmio\ApiDocBundle\Annotation as Doc;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * La classe UserController définit le contrôleur des requêtes liées aux entités {@link User}.
 *
 * @author vdouillet
 *        
 */
class UserController extends FOSRestController {

    /**
     * Vérification d'accès aux fonctions protégées du contrôleur.
     */
    private function checkAccess() {
        // Accessible seulement aux administrateurs
        if(!$this->isGranted(Role::ADMIN)) {
            throw new AccessDeniedHttpException('Administrator rights required');
        }
    }

    /**
     * Récupère tous les utilisateurs
     *
     * N'est accessibles qu'aux comptes administrateurs.
     * 
     * @Doc\ApiDoc(
     *  resource=true,
     *  section="Utilisateurs",
     *  headers={
     *    { "name"="Authorization", "description"="Jeton d'authentification JWT précédé de la mention 'Bearer '", "required"=true }
     *  },
     *  output={"class"=User::class, "collection"=true, "collectionName"="users"},
     *  statusCodes={
     *    401="Jeton d'authentification JWT manquant ou invalide",
     *    403="Droits d'accès insuffisants (non administrateur)"
     *  }
     * )
     * @Rest\Get("/users")
     * @Rest\QueryParam(name="offset", requirements="[\d]+", description="Facultatif. Décalage")
     * @Rest\QueryParam(name="limit", requirements="[\d]+", description="Facultatif. Nombre de résultats")
     * @Rest\QueryParam(name="sort", requirements="[\w]+", description="Facultatif. Colonne de tri")
     * @Rest\QueryParam(name="dir", requirements="ASC|DESC", description="Facultatif. Sens de tri")
     * @Rest\QueryParam(name="lastname", requirements="[\w]+", description="Facultatif. Recherche par nom")
     * @Rest\QueryParam(name="firstname", requirements="[\w]+", description="Facultatif. Recherche par prénom")
     * @Rest\QueryParam(name="email", requirements="[\w]+", description="Facultatif. Recherche par email")
     * @Rest\QueryParam(name="phone", requirements="[\w]+", description="Facultatif. Recherche par téléphone")
     * @Rest\QueryParam(name="isMailContact", requirements="[\w]+", description="Facultatif. Recherche par statut de contact mail")
     * @Rest\QueryParam(name="departmentCode", requirements="[\w]+", description="Facultatif. Recherche par département")
     * @Rest\View
     */
    public function getAllAction(ParamFetcherInterface $paramFetcher) {
        // Droits d'accès
        $this->checkAccess();

        // Pagination et filtrage
        $offset = $paramFetcher->get('offset');
        $limit = $paramFetcher->get('limit');
        $sortColumn = $paramFetcher->get('sort');
        $sortDir = $paramFetcher->get('dir');
        $lastname = $paramFetcher->get('lastname');
        $firstname = $paramFetcher->get('firstname');
        $email = $paramFetcher->get('email');
        $phone = $paramFetcher->get('phone');
        $isMailContact = $paramFetcher->get('isMailContact');
        $departmentCode = $paramFetcher->get('departmentCode');
        // Tri et paginationpar défaut
        if(empty($sortColumn)) {
            $sortColumn = 'id';
        }
        if(empty($sortDir)) {
            $sortDir = 'ASC';
        }
        if(empty($limit)) {
            $limit = 50;
        }

        /** @var \ARCommonBundle\Entity\UserRepository $repo */
        $repo = $this->getDoctrine()->getRepository(Entity::USER);
        return $repo->getWithFilters($offset, $limit, $sortColumn, $sortDir, $lastname, $firstname, $email, $phone, $isMailContact, $departmentCode);
    }

    /**
     * Récupère l'utilisateur connecté
     * 
     * @Doc\ApiDoc(
     *  resource=true,
     *  section="Utilisateurs",
     *  headers={
     *    { "name"="Authorization", "description"="Jeton d'authentification JWT précédé de la mention 'Bearer '", "required"=true }
     *  },
     *  output={ "class"=User::class, "collection"=false },
     *  statusCodes={
     *    401="Jeton d'authentification JWT manquant ou invalide",
     *  },
     * )
     * @Rest\Get("/users/current")
     * @Rest\View
     */
    public function getCurrentAction() {
        $user = $this->getUser();
        return $user;
    }

    /**
     * Supprime un utilisateur
     *
     * N'est accessibles qu'aux comptes administrateurs.
     * 
     * @Doc\ApiDoc(
     *  resource=true,
     *  section="Utilisateurs",
     *  headers={
     *    { "name"="Authorization", "description"="Jeton d'authentification JWT précédé de la mention 'Bearer '", "required"=true }
     *  },
     *  statusCodes={
     *    204="Utilisateur supprimé avec succès",
     *    403="Droits d'accès insuffisants (non administrateur)",
     *    404="Utilisateur introuvable"
     *  },
     * )
     *
     * @Rest\Delete("/users/{id}")
     * @Rest\View(statusCode=204)
     */
    public function removeAction(User $user)
    {
        $this->checkAccess();
        // Suppression de son propre compte interdite
        if($this->getUser()->getId() == $user->getId()) {
            throw new AccessDeniedHttpException('Deleting your own account is not authorized');
        }

        $em = $this->getDoctrine()->getEntityManager();
        $em->remove($user);
        $em->flush();
    }

    /**
     * Ajout d'un utilisateur.
     *
     * N'est accessibles qu'aux comptes administrateurs.
     *
     * #### Réponse en cas de code 400 ####
     * {
     *  "code" : 400,
     *  "message" : "Validation failed",
     *  "errors" : {
     *    "errors" : [
     *      "Erreur générale 1",
     *      "Erreur générale 2"
     *    ],
     *    "children" : {
     *      "lastname" : {
     *        "errors" : [
     *          "Erreur sur le nom de famille 1",
     *          "Erreur sur le nom de famille 2"
     *        ]
     *      }
     *      "phone" : {
     *        "errors" : [
     *          "Erreur sur le téléphone 1",
     *          "Erreur sur le téléphone 2"
     *        ]
     *      }
     *    }
     *  }
     * }
     *
     * @Doc\ApiDoc(
     *  resource=true,
     *  section="Utilisateurs",
     *  headers={
     *    { "name"="Authorization", "description"="Jeton d'authentification JWT précédé de la mention 'Bearer '", "required"=true }
     *  },
     *  statusCodes={
     *    201="Utilisateur créé avec succès",
     *    400="Données manquantes ou invalide",
     *    403="Droits d'accès insuffisants (non administrateur)",
     *  },
     * )
     *
     * @Rest\Post("/users")
     * @Rest\RequestParam(name="user[lastname]", description="Nom")
     * @Rest\RequestParam(name="user[firstname]", description="Prénom")
     * @Rest\RequestParam(name="user[phone]", requirements="/(?:\+?\d{10,11},?)+/", description="Numéro de téléphone")
     * @Rest\RequestParam(name="user[email]", requirements="email", description="Adresse mail")
     * @Rest\RequestParam(name="user[departments]", description="Code(s) des départements, séparés par une virgule")
     * @Rest\RequestParam(name="user[isMailContact]", description="Statut d'acceptation des contacts par mail")
     * @Rest\RequestParam(name="user[role]", requirements="ROLE_CMA|ROLE_ADMIN", description="Niveau de privilèges")
     * @Rest\View
     */
    public function addAction(Request $request) {
        // Droits d'accès
        $this->checkAccess();

        /** @var $userManager \FOS\UserBundle\Model\UserManagerInterface */
        $userManager = $this->get('fos_user.user_manager');
        $user = $userManager->createUser();

        // Traitement des valeurs envoyées
        return $this->processForm($user, $request, 'POST');
    }

    /**
     * Modification d'un utilisateur.
     *
     * N'est accessibles qu'aux comptes administrateurs.
     *
     * #### Réponse en cas de code 400 ####
     * {
     *  "code" : 400,
     *  "message" : "Validation failed",
     *  "errors" : {
     *    "errors" : [
     *      "Erreur générale 1",
     *      "Erreur générale 2"
     *    ],
     *    "children" : {
     *      "lastname" : {
     *        "errors" : [
     *          "Erreur sur le nom de famille 1",
     *          "Erreur sur le nom de famille 2"
     *        ]
     *      }
     *      "phone" : {
     *        "errors" : [
     *          "Erreur sur le téléphone 1",
     *          "Erreur sur le téléphone 2"
     *        ]
     *      }
     *    }
     *  }
     * }
     *
     * @Doc\ApiDoc(
     *  resource=true,
     *  section="Utilisateurs",
     *  headers={
     *    { "name"="Authorization", "description"="Jeton d'authentification JWT précédé de la mention 'Bearer '", "required"=true }
     *  },
     *  statusCodes={
     *    204="Utilisateur mis à jour avec succès",
     *    400="Données manquantes ou invalide",
     *    403="Droits d'accès insuffisants (non administrateur)",
     *    404="Utilisateur introuvable avec l'identifiant fourni"
     *  },
     * )
     *
     * @Rest\Put("/users/{id}")
     * @Rest\RequestParam(name="user[lastname]", description="Nom")
     * @Rest\RequestParam(name="user[firstname]", description="Prénom")
     * @Rest\RequestParam(name="user[phone]", requirements="/(?:\+?\d{10,11},?)+/", description="Numéro de téléphone")
     * @Rest\RequestParam(name="user[email]", requirements="email", description="Adresse mail")
     * @Rest\RequestParam(name="user[departments]", description="Code(s) des départements, séparés par une virgule")
     * @Rest\RequestParam(name="user[isMailContact]", description="Statut d'acceptation des contacts par mail")
     * @Rest\RequestParam(name="user[role]", requirements="ROLE_CMA|ROLE_ADMIN", description="Niveau de privilèges")
     * @Rest\View
     */
    public function editAction(User $user, Request $request) {
        // Droits d'accès
        $this->checkAccess();
        // Traitement du formulaire
        return $this->processForm($user, $request, 'PUT');
    }

    /**
     * Traitement du formulaire d'ajout/mise à jour d'un utilisateur.
     *
     * @param User $formData
     * @param Request $request
     * @param string $method
     * @return Response
     */
    private function processForm(User $formData, Request $request, $method) {
        $form = $this->createForm(new UserType(), $formData, array(
            'method' => $method,
        ));
        $form->handleRequest($request);
        $toPersist = $form->getData();

        // Gestion des départements
        $toPersist->getCodeDepartment()->clear();
        $dptRepo = $this->getDoctrine()->getRepository(Entity::DEPARTMENT);
        if($form->get('departments')->getData()) {
            foreach (split(',', $form->get('departments')->getData()) as $codeDepartment) {
                $entity = $dptRepo->find($codeDepartment);
                if($entity) {
                    $toPersist->addCodeDepartment($entity);
                }
                else {
                    $form->get('departments')->addError(new FormError('unknownDepartmentCode'));
                }
            }
        }
        else {
            $form->get('departments')->addError(new FormError('required'));
        }

        // Gestion des rôles
        $toPersist->setRoles(array());
        $role = $form->get('role')->getData();
        if($role) {
            if($role === Role::CMA || $role === Role::ADMIN) {
                $toPersist->addRole($role);
            }
            else {
                $form->get('role')->addError(new FormError('unknownRole'));
            }
        }
        else {
            $form->get('role')->addError(new FormError('required'));
        }

        if($form->isValid()) {
            /** @var $userService \ARCommonBundle\Service\UserService */
            $userService = $this->get('common.user_service');

            $response = new Response();
            if($method == 'POST') {
                // Création
                $userService->create($toPersist, $this->getParameter('account_activate_link'));
                // Code 201 et header 'Location' dans la réponse
                $response->setStatusCode(201);
                $response->headers->set('Location',
                    $this->generateUrl('arapiv1_user_get', array('id' => $toPersist->getId()), true)
                );
            }
            else {
                // Modification
                $this->getDoctrine()->getEntityManager()->flush();
                $response->setStatusCode(204);
            }

            return $response;
        }

        return View::create($form, 400);
    }

    /**
     * Récupère un utilisateur
     *
     * N'est accessibles qu'aux comptes administrateurs.
     *
     * @Doc\ApiDoc(
     *  resource=true,
     *  section="Utilisateurs",
     *  headers={
     *    { "name"="Authorization", "description"="Jeton d'authentification JWT précédé de la mention 'Bearer '", "required"=true }
     *  },
     *  output={"class"=User::class, "collection"=false},
     *  statusCodes={
     *    403="Droits d'accès insuffisants (non administrateur)",
     *    404="Utilisateur introuvable avec l'identifiant fourni"
     *  },
     * )
     *
     * @Rest\Get("/users/{id}")
     * @Rest\View
     */
    public function getAction(User $user) {
        // Droits d'accès administrateur
        $this->checkAccess();

        return $user;
    }

    /**
     * Activation d'un utilisateur.
     *
     * #### Réponse en cas de code 400 ####
     * {
     *  "code" : 400,
     *  "message" : "Validation failed",
     *  "errors" : {
     *    "errors" : [
     *      "Erreur générale 1",
     *      "Erreur générale 2"
     *    ],
     *    "children" : {
     *      "token" : {
     *        "errors" : [
     *          "Erreur sur le token 1",
     *          "Erreur sur le token 2"
     *        ]
     *      }
     *      "password" : {
     *        "errors" : [
     *          "Erreur sur le mot de passe 1",
     *          "Erreur sur le mot de passe 2"
     *        ]
     *      }
     *    }
     *  }
     * }
     *
     * @Doc\ApiDoc(
     *  resource=true,
     *  section="Utilisateurs",
     *  statusCodes={
     *    204="Utilisateur activé avec succès",
     *    400="Données manquantes ou invalide",
     *    404="Token invalide ou expiré",
     *  },
     * )
     *
     * @Rest\Post("/users/activate")
     * @Rest\RequestParam(name="activate[token]", description="Token associé au compte utilisateur")
     * @Rest\RequestParam(name="activate[password]", requirements="/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$/", description="Mot de passe avec au moins 8 caractères dont une minuscule, une majuscule, un chiffre et un caractère spécial")
     * @Rest\View
     */
    public function activateAction(Request $request) {
        $form = $this->createForm(new UserActivateType(), null, array(
            'method' => 'POST',
        ));
        $form->handleRequest($request);

        if($form->isValid()) {
            /** @var $userRepo \ARCommonBundle\Entity\UserRepository */
            $userRepo = $this->getDoctrine()->getRepository(Entity::USER);
            $validInterval = new \DateInterval(sprintf('PT%dS', $this->getParameter('account_token_validity_seconds')));
            $limitDate = (new \DateTime())->sub($validInterval);
            $user = $userRepo->findToActivate($form->get('token')->getData(), $limitDate);

            if($user) {
                /** @var $userService \ARCommonBundle\Service\UserService */
                $userService = $this->get('common.user_service');
                $userService->activate($user, $form->get('password')->getData());
                $response = new Response();
                $response->setStatusCode(204);
                return $response;
            }
            else {
                throw new NotFoundHttpException('User not found');
            }
        }

        return View::create($form, 400);
    }

    /**
     * Demande de réinitialisation de mot de passe.
     * 
     * #### Réponse en cas de code 400 ####
     * {
     *  "code" : 400,
     *  "message" : "Validation failed",
     *  "errors" : {
     *    "errors" : [
     *      "Erreur générale 1",
     *      "Erreur générale 2"
     *    ],
     *    "children" : {
     *      "email" : {
     *        "errors" : [
     *          "notFound",
     *          "pattern"
     *        ]
     *      }
     *    }
     *  }
     * }
     * 
     * @Doc\ApiDoc(
     *  resource=true,
     *  section="Utilisateurs",
     *  statusCodes={
     *    400="Email manquant ou invalide"
     *  },
     * )
     *
     * @Rest\Post("/users/ask-password-reset")
     * @Rest\RequestParam(name="passwordResetRequest[email]", description="Email associé au compte utilisateur")
     * @Rest\View
     */
    public function passwordResetRequestAction(Request $request) {
        $form = $this->createForm(new PasswordResetRequestType(), null, array(
            'method' => 'POST',
        ));
        $form->handleRequest($request);

        if($form->isValid()) {
            /** @var \ARCommonBundle\Entity\UserRepository $repo */
            $repo = $this->getDoctrine()->getRepository(Entity::USER);

            /** @var \ARCommonBundle\Entity\User $user */
            $user = $repo->findOneBy(array(
                'email' => $form->get('email')->getData()
            ));

            if(!$user) {
                $form->get('email')->addError(new FormError('notFound'));
                return View::create($form, 400);
            }

            /** @var $userService \ARCommonBundle\Service\UserService */
            $userService = $this->get('common.user_service');
            $userService->passwordResetRequest($user);

            $response = new Response();
            $response->setStatusCode(204);
            return $response;
        }

        return View::create($form, 400);
    }
}