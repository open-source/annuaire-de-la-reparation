<?php

namespace ARApiV1Bundle\Controller;

use ARCommonBundle\Entity\NafCode;
use ARCommonBundle\Enum\Entity;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use Nelmio\ApiDocBundle\Annotation as Doc;

/**
 * La classe NafCodeController définit le contrôleur des requêtes liées aux entités {@link NafCode}.
 *
 * @author vdouillet
 *        
 */
class NafCodeController extends FOSRestController {

    /**
     * Récupère les codes NAF
     * 
     * @Doc\ApiDoc(
     *  resource=true,
     *  section="Codes NAF",
     *  headers={
     *    { "name"="Authorization", "description"="Jeton d'authentification JWT précédé de la mention 'Bearer '", "required"=true }
     *  },
     *  output={ "class"=NafCode::class, "collection"=true, "collectionName"="nafCodes" },
     *  statusCodes={
     *    401="Jeton d'authentification JWT manquant ou invalide",
     *  },
     * )
     * @Rest\Get("/naf-codes")
     * @Rest\View
     */
    public function getAllAction() {
        $repo = $this->getDoctrine()->getRepository(Entity::NAF_CODE);

        return array (
            'nafCodes' => $repo->findAll()
        );
    }

    /**
     * @Rest\Get("/naf-codes/{id}")
     * @Rest\View
     */
    public function getAction(NafCode $nafCode)
    {
        return $nafCode;
    }
}