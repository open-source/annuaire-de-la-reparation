<?php

namespace ARApiV1Bundle\Controller;

use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use Nelmio\ApiDocBundle\Annotation as Doc;

/**
 * La classe ParametersController définit le contrôleur des requêtes permettant de récupérer des paramètres de configuration pour le frontend de l'application.
 *
 * @author vdouillet
 *        
 */
class ParametersController extends FOSRestController {

    /**
     * Récupère les paramètres pour le frontend de l'application
     * 
     * La liste des paramètres est retournée sous forme de tableau associatif avec pour clé le nom du paramètre côté backend, les valeurs des clés sont les suivantes :
     * - google_maps_api_key
     * @Doc\ApiDoc(
     *  resource=true,
     *  section="Paramètres configurables"
     * )
     * @Rest\Get("/parameters")
     * @Rest\View
     */
    public function getAllAction() {
        $googlemapKey = $this->getParameter('google_maps_api_key');

        return array (
            'google_maps_api_key' => $googlemapKey
        );
    }

}