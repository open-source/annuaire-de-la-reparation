<?php

namespace ARApiV1Bundle\Controller;

use ARApiV1Bundle\Exception\RetryWithHttpException;
use ARApiV1Bundle\Form\ContactType;
use ARApiV1Bundle\Form\CraftspersonType;
use ARApiV1Bundle\Form\ErrorReportType;
use ARCommonBundle\Entity\Craftsperson;
use ARCommonBundle\Entity\Department;
use ARCommonBundle\Entity\NafCode;
use ARCommonBundle\Enum\Entity;
use ARCommonBundle\Enum\Role;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Request\ParamFetcherInterface;
use FOS\RestBundle\View\View;
use Nelmio\ApiDocBundle\Annotation as Doc;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\ConflictHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\UnprocessableEntityHttpException;

/**
 * La classe CraftspersonsController définit le contrôleur des requêtes liées aux entités {@link Craftsperson}.
 *
 * @author vdouillet
 *        
 */
class CraftspersonsController extends FOSRestController {

  /**
   * Récupère tous les réparateurs
   * 
   * Les filtres ne fonctionnent qu'en mode authentifié.
   * 
   * #### Réponse sans authentification (mode public) ####
   * {
   *  "total": 150000
   * }
   * 
   * #### Réponse avec authentification
   * {
   *  "craftspersons": tableau de réparateurs,
   *  "total": 156
   * }
   * 
   * @Doc\ApiDoc(
   *  resource=true,
   *  section="Réparateurs",
   *  headers={
   *    { "name"="Authorization", "description"="Jeton d'authentification JWT précédé de la mention 'Bearer '", "required"=false }
   *  },
   *  output={"class"=Craftsperson::class, "collection"=true, "collectionName"="craftspersons"}
   * )
   *
   * @Rest\Get("/craftspersons")
   * @Rest\QueryParam(name="offset", requirements="[\d]+", description="Facultatif. Décalage")
   * @Rest\QueryParam(name="limit", requirements="[\d]+", description="Facultatif. Nombre de résultats")
   * @Rest\QueryParam(name="sort", requirements="[\w]+", description="Facultatif. Colonne de tri")
   * @Rest\QueryParam(name="dir", requirements="ASC|DESC", description="Facultatif. Sens de tri")
   * @Rest\QueryParam(name="siret", requirements="[\d]+", description="Facultatif. Recherche par siret")
   * @Rest\QueryParam(name="name", requirements="[\w ]+", description="Facultatif. Recherche par nom")
   * @Rest\QueryParam(name="zipCode", requirements="[\d]+", description="Facultatif. Recherche par code postal")
   * @Rest\QueryParam(name="address", requirements="[\w ]+", description="Facultatif. Recherche par adresse")
   * @Rest\QueryParam(name="phone", requirements="[\w ]+", description="Facultatif. Recherche par numéro de téléphone")
   * @Rest\QueryParam(name="isError", requirements="true|false", description="Facultatif. Recherche par statut d'erreur")
   * @Rest\QueryParam(name="isReparactor", requirements="true|false", description="Facultatif. Recherche par statut réparacteur")
   * @Rest\QueryParam(name="isEnabled", requirements="true|false", description="Facultatif. Recherche par statut d'activation")
   * @Rest\QueryParam(name="isUpdated", requirements="true|false", description="Facultatif. Recherche par statut de mise à jour manuelle")
   * @Rest\QueryParam(name="updateDate", requirements="\d{2}-\d{2}-\d{4}", description="Facultatif. Recherche par date de mise à jour. Ignoré si la date n'est pas au format dd-mm-yyyy")
   * @Rest\QueryParam(name="cmaCode", requirements="[\dAB]+", description="Facultatif. Recherche par code CMA")
   * @Rest\QueryParam(name="nafCode", requirements="[\w]+", description="Facultatif. Recherche par code NAFA")
   * @Rest\View
   */
  public function getAllAction(ParamFetcherInterface $paramFetcher) {
    /** @var \ARCommonBundle\Entity\CraftspersonRepository $repo */
    $repo = $this->getDoctrine()->getRepository(Entity::CRAFTSPERSON);

    // En mode public, ne retourner que le nombre de réparateurs en base
    $currentToken = $this->get('security.token_storage')->getToken();
    if(!is_object($currentToken->getUser())) {
      $nbInternalCraftpersons = $repo->countAll();
      $nbTotal = $nbInternalCraftpersons + $this->getParameter('repairer_static_count');
      return array(
        'total' => $nbTotal,
      );
    }

    // Valeurs des filtres
    $offset = $paramFetcher->get('offset');
    $limit = $paramFetcher->get('limit');
    $sortColumn = $paramFetcher->get('sort');
    $sortDir = $paramFetcher->get('dir');
    $cmaCode = $paramFetcher->get('cmaCode');
    $nafCode = $paramFetcher->get('nafCode');
    $siret = $paramFetcher->get('siret');
    $name = $paramFetcher->get('name');
    $zipCode = $paramFetcher->get('zipCode');
    $address = $paramFetcher->get('address');
    $phone = $paramFetcher->get('phone');
    $isError = $paramFetcher->get('isError');
    $isReparactor = $paramFetcher->get('isReparactor');
    $isEnabled = $paramFetcher->get('isEnabled');
    $isUpdated = $paramFetcher->get('isUpdated');
    $updateDate = $paramFetcher->get('updateDate');

    // Tri et pagination par défaut
    if(empty($sortColumn)) {
      $sortColumn = 'siret';
    }
    if(empty($sortDir)) {
      $sortDir = 'ASC';
    }
    if(empty($limit)) {
        $limit = 50;
    }

    // Filtre par CMA si l'utilisateur n'est pas ADMIN
    $cma = null;
    if(!$this->isGranted(Role::ADMIN)) {
      $cma = $this->getUser()->getCodeDepartment();
    }
     
    return $repo->getWithFilters($offset, $limit, $sortColumn, $sortDir, $cma, $siret, $name, $zipCode, $address, $phone, $cmaCode, $nafCode, $isError, $isReparactor, $isEnabled, $isUpdated, $updateDate);
  }

  /**
   * Exporte un fichier CSV de réparateurs.
   *
   * @Doc\ApiDoc(
   *  resource=true,
   *  section="Réparateurs",
   *  headers={
   *    { "name"="Authorization", "description"="Jeton d'authentification JWT précédé de la mention 'Bearer '", "required"=false }
   *  },
   *  statusCodes={
   *    200="Fichier généré avec succès",
   *    401="Jeton d'authentification JWT manquant ou invalide",
   *    403="Jeton d'authentification JWT manquant ou invalide"
   *  }
   * )
   *
   * @Rest\QueryParam(name="departments", description="Départements sélectionnés pour export, facultatif")
   * @Rest\Get("/craftspersons/csv")
   * @Rest\View
   */
  public function exportAction(ParamFetcherInterface $paramFetcher) {
    // Opération longue si beaucoup de départements
    set_time_limit(0);
    ini_set('memory_limit', '1024M');

    // Départements sélectionnés pour export
    $currentToken = $this->get('security.token_storage')->getToken();
    if(!is_object($currentToken->getUser())) {
      throw new AccessDeniedHttpException();
    }
    $userDepartments = array();
    foreach ($currentToken->getUser()->getCodeDepartment() as $dpt) {
      $userDepartments[] = $dpt->getCode();
    }

    $departmentsStr = $paramFetcher->get('departments');
    $departmentCodes = array();
    if(!$departmentsStr) {
      $departmentCodes = $userDepartments;
    }
    else {
      $departmentCodes = split(',', $departmentsStr);
      $departmentCodes = array_intersect($userDepartments, $departmentCodes);
    }

    // Fichier en dossier temporaire
    $csvFilePath = $temp_file = tempnam(sys_get_temp_dir(), 'are-craftsperson-export-');
    $csvFile = fopen($csvFilePath, 'w');
    if(!$csvFile) {
      $response = new Response();
      $response->setStatusCode(500);
      return $response;
    }

    $repo = $this->getDoctrine()->getRepository(Entity::CRAFTSPERSON);
    $repo->generateCsvExport($csvFile, $departmentCodes);

    fclose($csvFile);

    return (new BinaryFileResponse($csvFilePath))->deleteFileAfterSend(true);
  }

  /**
   * Vérification d'import de réparateurs depuis un fichier
   * 
   * #### Réponse en cas de succès ####
   * {
   *  "created" : 15,
   *  "updated" : 653,
   *  "deleted" : 6,
   *  "ignored" : 0,
   *  "invalid" : 0
   * }
   *
   * #### Réponse en cas d'erreur ####
   * {
   *  "errors" : [
   *    {"line" : 0, "message" : "Nom de fichier invalide"},
   *    {"line" : 10, "message" : "Code CMA inconnu"},
   *  ]
   * }
   *
   * @Doc\ApiDoc(
   *  resource=true,
   *  section="Réparateurs",
   *  headers={
   *    { "name"="Authorization", "description"="Jeton d'authentification JWT précédé de la mention 'Bearer '", "required"=true }
   *  },
   *  parameters = {
   *    { "name" = "uploadFile", "dataType" = "file", "description" = "Fichier de réparateurs au format CSV", "required"=true }
   *  },
   *  statusCodes={
   *    200="La vérification s'est terminée avec succès",
   *    400="La vérification a généré des erreurs",
   *    401="Jeton d'authentification JWT manquant ou invalide"
   *  }
   * )
   *
   * @Rest\Post("/craftspersons/check-file")
   * @Rest\FileParam(
   *   name="uploadFile",
   *   key=null,
   *   incompatibles={},
   *   default=null,
   *   description="Fichier à importer",
   *   strict=false,
   *   nullable=false,
   *   requirements={},
   *   image=false
   * )
   * @Rest\View
   */
  public function importCheckAction(ParamFetcherInterface $paramFetcher) {
    set_time_limit(240);
    ini_set('memory_limit', '1024M');

    return $this->handleImport($paramFetcher->get('uploadFile'), true);
  }

  /**
   * Import de réparateurs depuis un fichier
   * 
   * #### Réponse en cas de succès ####
   * {
   *  "created" : 15,
   *  "updated" : 653,
   *  "deleted" : 6,
   *  "ignored" : 0,
   *  "invalid" : 0
   * }
   *
   * #### Réponse en cas d'erreur ####
   * {
   *  "errors" : [
   *    {"line" : 0, "message" : "Nom de fichier invalide"},
   *    {"line" : 10, "message" : "Code CMA inconnu"},
   *  ]
   * }
   *
   * @Doc\ApiDoc(
   *  resource=true,
   *  section="Réparateurs",
   *  headers={
   *    { "name"="Authorization", "description"="Jeton d'authentification JWT précédé de la mention 'Bearer '", "required"=true }
   *  },
   *  parameters = {
   *    { "name" = "uploadFile", "dataType" = "file", "description" = "Fichier de réparateurs au format CSV", "required"=true }
   *  },
   *  statusCodes={
   *    200="La vérification s'est terminée avec succès",
   *    400="La vérification a généré des erreurs",
   *    401="Jeton d'authentification JWT manquant ou invalide"
   *  }
   * )
   *
   * @Rest\Post("/craftspersons/import-file")
   * @Rest\FileParam(
   *   name="uploadFile",
   *   key=null,
   *   incompatibles={},
   *   default=null,
   *   description="Fichier à importer",
   *   strict=false,
   *   nullable=false,
   *   requirements={},
   *   image=false
   * )
   * @Rest\View
   */
  public function importAction(ParamFetcherInterface $paramFetcher) {
    set_time_limit(240);
    ini_set('memory_limit', '1024M');

    // TODO valider le fichier : taille...
    return $this->handleImport($paramFetcher->get('uploadFile'), false);
  }

  /**
   * Import du fichier et retour du nombre de fiches créées, mises à jour, ignorées et invalides.
   *
   * @param mixed $uploadFile
   * @param boolean $checkOnly
   * @return array|\FOS\RestBundle\View\View
   */
  private function handleImport($uploadFile = null, $checkOnly) {
    if ($uploadFile instanceof File && ($handle = fopen($uploadFile->getPathname(), 'r')) !== FALSE) {
      // Import du fichier par le service
      /** @var \ARCommonBundle\Service\CraftspersonImportService $importService */
      $importService = $this->get('common.craftsperson_import_service');
      $importResult = $importService->import($handle, $uploadFile->getClientOriginalName(), $checkOnly);
      
      // Conversion d'une erreur d'import en code http 400
      if ($importResult['result']['error']) {
        return View::create(array (
            'errors' => $importResult['result']['logs'] 
        ), 400);
      }
      
      // Suppression des drapeaux d'erreur et retour avec succès
      unset($importResult['result']['error']);
      return $importResult;
    }
    
    return View::create(array (
        'File not found'
    ), 400);
  }

  /**
   * Recherche de réparateurs autour d'une position
   * 
   * La position de recherche doit être spécifiée avec les arguments "lat" et "lon" ou avec une adresse plein texte par l'argument "address".
   * Si une position et une adresse sont spécifiées, la position est utilisée.
   *
   * @Doc\ApiDoc(
   *  resource=true,
   *  section="Réparateurs",
   *  output={"class"=Craftsperson::class, "collection"=true, "collectionName"="craftspersons", "groups"={"list"}},
   *  statusCodes={
   *    422="Paramètres requis manquant(s)",
   *    449="Géocodage de la requête impossible, réessayer avec une requête plus générique"
   *  },
   * )
   *
   * @Rest\Get("/craftspersons/search")
   * @Rest\QueryParam(name="category", requirements="[\d]+", description="Identifiant de catégorie, facultatif")
   * @Rest\QueryParam(name="lat", requirements="[\d,.-]+", description="Latitude, facultatif")
   * @Rest\QueryParam(name="lon", requirements="[\d,.-]+", description="Longitude, facultatif")
   * @Rest\QueryParam(name="address", description="Adresse, facultatif")
   * @Rest\QueryParam(name="radius", requirements="[0-9]*\.?[0-9]+", description="Rayon maximum (km), requis")
   * @Rest\View
   */
  public function searchAction(ParamFetcherInterface $paramFetcher) {
    $categoryId = $paramFetcher->get('category');
    $lat = $paramFetcher->get('lat');
    $lon = $paramFetcher->get('lon');
    $address = $paramFetcher->get('address');
    $radius = $paramFetcher->get('radius');

    $categoryRepo = $this->getDoctrine()->getRepository(Entity::CATEGORY);
    /** @var \ARCommonBundle\Entity\Category $category */
    $category = $categoryRepo->find($categoryId);

    // Présence des paramètres requis
    if((empty($address) && (empty($lat) || empty($lon))) || empty($radius)) {
      throw new UnprocessableEntityHttpException('Missing search parameters');
    }

    // Recherche avec géocodage de la position si besoin
    /** @var \ARCommonBundle\Entity\CraftspersonRepository $repo */
    $repo = $this->getDoctrine()->getRepository(Entity::CRAFTSPERSON);
    if(empty($lat) || empty($lon)) {
      /** @var \ARCommonBundle\Service\GeocodeService $geocodeService */
      $geocodeService = $this->get('common.geocode_service');
      $geocodeService->setApiKey($this->getParameter('google_maps_api_key'));
      $geocoding = $geocodeService->geocodeQuery($address);
      if(!is_array($geocoding)) {
        if(is_string($geocoding)) {
            throw new RetryWithHttpException('Geocoding failed with code ' . $geocoding);
        } else {
            throw new RetryWithHttpException('An unknown error occured on geocoding.');
        }
      }
      else {
        $lat = $geocoding['lat'];
        $lon = $geocoding['lon'];
      }
    }
    $result = $repo->search($lat, $lon, $radius, $category);

    // Déplacement de la distance en tant que propriété du réparateur
    $craftspersons = array();
    $craftpersonsIds = array();

    //Get craftpersons ids
    foreach ($result as $resultEntry) {
      /** @var Craftsperson $craftsperson */
      $craftsperson = $resultEntry[0];
      $craftpersonsIds[] = $craftsperson->getId();
    }

    $codeNafsByCraftPersonIds = $repo->getCodeNafsByCraftpersonId($craftpersonsIds);
    $categoriesByCodeNaf = $repo->getCategoriesByCodeNaf();

    //TODO get categoriesByCodeNaf();
    foreach ($result as $resultEntry) {
      /** @var Craftsperson $craftsperson */
      $craftsperson = $resultEntry[0];
      $craftpersonJson = [];
      $id = $craftsperson->getId();

      $codesNafs = $codeNafsByCraftPersonIds[$id];
      $categories = array();
      foreach($codesNafs as $codeNaf) {
        $categoriesCodeNaf = $categoriesByCodeNaf[$codeNaf];
        foreach($categoriesCodeNaf as $categoryCodeNaf) {
          if(!in_array($categoryCodeNaf, $categories)) {
            $categories[] = $categoryCodeNaf;
          }
        }
      }
      
      $craftpersonJson["categories"] = $categories;
      $craftpersonJson["id"] = $craftsperson->getId();
      $craftpersonJson["name"] = $craftsperson->getName();
      $craftpersonJson["address1"] = $craftsperson->getAddress1();
      $craftpersonJson["address2"] = $craftsperson->getAddress2();
      $craftpersonJson["zipCode"] = $craftsperson->getZipCode();
      $craftpersonJson["zipCodeLabel"] = $craftsperson->getZipCodeLabel();
      $craftpersonJson["phone"] = $craftsperson->getPhone();
      //## Prevent sending the email adress in order to avoid bot spamming
      $craftpersonJson["email"] = $craftsperson->getEmail()?true:false;
      $craftpersonJson["website"] = $craftsperson->getWebsite();
      $craftpersonJson["isReparactor"] = $craftsperson->getIsReparactor();
      $craftpersonJson["latitude"] = $craftsperson->getLatitude();
      $craftpersonJson["longitude"] = $craftsperson->getLongitude();
      $craftpersonJson["likes"] = $craftsperson->getLikes();
      $craftpersonJson["distance"] = sqrt($resultEntry['distance']);
      $craftpersonJson["department"] = $craftsperson->getDepartment();
      $craftspersons[] = $craftpersonJson;
    }

    return array("craftspersons" => $craftspersons);
  }

  /**
   * Recherche de le réparateur le plus proche d'un point indiqué par la latitude lat et longitude lng et la catégorie donnée (optionnelle)'
   *
   * @Doc\ApiDoc(
   *  resource=true,
   *  section="Réparateurs",
   *  output={"class"=Craftsperson::class, "collection"=false, "collectionName"="", "groups"={"list"}},
   *  statusCodes={
   *    422="Paramètres requis manquant(s)"
   *  }
   * )
   *
   * @Rest\Get("/craftspersons/closest")
   * @Rest\QueryParam(name="category", requirements="[\d]+", description="Identifiant de catégorie, facultatif")
   * @Rest\QueryParam(name="lat", requirements="[\d,.-]+", description="Latitude, requis")
   * @Rest\QueryParam(name="lon", requirements="[\d,.-]+", description="Longitude, requis")
   * @Rest\View(serializerGroups={"list"})
   */
  public function closestDistanceAction(ParamFetcherInterface $paramFetcher) {
    $categoryId = $paramFetcher->get('category');
    $lat = $paramFetcher->get('lat');
    $lon = $paramFetcher->get('lon');

    /** @var \ARCommonBundle\Entity\CraftspersonRepository $repo */
    $repo = $this->getDoctrine()->getRepository(Entity::CRAFTSPERSON);

    $categoryRepo = $this->getDoctrine()->getRepository(Entity::CATEGORY);
    /** @var \ARCommonBundle\Entity\Category $category */
    $category = $categoryRepo->find($categoryId);

    // Présence des paramètres requis
    if(empty($lat) || empty($lon)) {
      throw new UnprocessableEntityHttpException('Missing search parameters');
    }

    $result = $repo->searchClosest($lat, $lon, $category);
    foreach ($result as $resultEntry) {
      /** @var Craftsperson $craftsperson */
      $craftsperson = $resultEntry[0];
      // Déplacement de la distance en tant que propriété du réparateur
      $craftsperson->setDistance(sqrt($resultEntry['distance']));
      $craftsperson->setEmail(null);
      return array('craftsperson' => $craftsperson);
    }

    return array();
  }

  /**
   * Récupère un réparateur
   * 
   * Seuls les utilisateurs ayant un role ADMIN peuvent accéder à tous les réparateurs. Les autres utilisateurs n'accèdent qu'aux réparateurs de leurs CMA.
   *
   * @Doc\ApiDoc(
   *  resource=true,
   *  section="Réparateurs",
   *  headers={
   *    { "name"="Authorization", "description"="Jeton d'authentification JWT précédé de la mention 'Bearer '", "required"=false }
   *  },
   *  output={"class"=Craftsperson::class, "collection"=false},
   *  statusCodes={
   *    403="Le réparateur fait partie d'une CMA dont l'accès est refusé",
   *    404="Réparateur introuvable avec l'identifiant fourni"
   *  },
   * )
   *
   * @Rest\Get("/craftspersons/{id}")
   * @Rest\View
   */
  public function getAction(Craftsperson $craftsperson, Request $request)
  {
    $currentToken = $this->get('security.token_storage')->getToken();
    if(is_object($currentToken->getUser())) {
      // Vérification d'accès via CMA si utilisateur connecté
      $this->checkCmaAccess($craftsperson->getCmaCode());
    }
    else {
      // Sinon sérialization en mode public
      $request->attributes->get('_template')->setSerializerGroups(array('public'));
    }

    return $craftsperson;
  }

  /**
   * Vérification d'accès à une CMA pour l'utilisateur courant.
   *
   * @param Department $cma
   */
  private function checkCmaAccess(Department $cma) {
    if(!$this->isGranted(Role::ADMIN)) {
      $currentUser = $this->getUser();
      if(!$cma || !$currentUser || !$currentUser->getCodeDepartment()->contains($cma)) {
        throw new AccessDeniedHttpException('Accès refusé à cette CMA');
      }
    }
  }

  /**
   * Modification d'un réparateur
   *
   * Seuls les utilisateurs ayant un role ADMIN peuvent modifier tous les réparateurs. Les autres utilisateurs ne peuvent modifier que les réparateurs de leurs CMA.
   *
   * #### Réponse en cas de code 400 ####
   * {
   *  "code" : 400,
   *  "message" : "Validation failed",
   *  "errors" : {
   *    "errors" : [
   *      "Erreur générale 1",
   *      "Erreur générale 2"
   *    ],
   *    "children" : {
   *      "siret" : {
   *        "errors" : [
   *          "Erreur sur le SIRET 1",
   *          "Erreur sur le SIRET 2"
   *        ]
   *      }
   *      "phone" : {
   *        "errors" : [
   *          "Erreur sur le téléphone 1",
   *          "Erreur sur le téléphone 2"
   *        ]
   *      }
   *    }
   *  }
   * }
   *
   * @Doc\ApiDoc(
   *  resource=true,
   *  section="Réparateurs",
   *  headers={
   *    { "name"="Authorization", "description"="Jeton d'authentification JWT précédé de la mention 'Bearer '", "required"=true }
   *  },
   *  statusCodes={
   *    204="Réparateur mis à jour avec succès",
   *    400="Données manquantes ou invalide",
   *    403="Le réparateur fait partie d'une CMA dont l'accès est refusé",
   *    404="Réparateur introuvable"
   *  },
   * )
   *
   * @Rest\Put("/craftspersons/{id}")
   * @Rest\RequestParam(name="craftsperson[siret]", description="Numéro de SIRET")
   * @Rest\RequestParam(name="craftsperson[name]", description="Nom commercial")
   * @Rest\RequestParam(name="craftsperson[address1]", description="Adresse (1ère ligne)")
   * @Rest\RequestParam(name="craftsperson[address2]", description="Adresse (2è ligne)")
   * @Rest\RequestParam(name="craftsperson[zipCode]", requirements="/[0-9]+/", description="Code postal")
   * @Rest\RequestParam(name="craftsperson[zipCodeLabel]", description="Libellé postal")
   * @Rest\RequestParam(name="craftsperson[phone]", requirements="/(?:\+?\d{10,11},?)+/", description="Numéro de téléphone")
   * @Rest\RequestParam(name="craftsperson[email]", requirements="email", description="Adresse mail")
   * @Rest\RequestParam(name="craftsperson[website]", description="URL du site internet")
   * @Rest\RequestParam(name="craftsperson[isEnabled]", description="Statut d'affichage du réparateur dans les résultats de recherche")
   * @Rest\RequestParam(name="craftsperson[isReparactor]", description="Statut répar'acteur")
   * @Rest\RequestParam(name="craftsperson[isError]", description="Statut de signalement d'erreur")
   * @Rest\RequestParam(name="craftsperson[reparactorDescription]", description="Présentation du répar'acteur")
   * @Rest\RequestParam(name="craftsperson[reparactorHours]", description="Jours/Horaires d'ouverture du répar'acteur")
   * @Rest\RequestParam(name="craftsperson[reparactorServices]", description="Services proposés par le répar'acteur")
   * @Rest\RequestParam(name="craftsperson[reparactorCertificates]", description="Certificats et agréments du répar'acteur")
   * @Rest\RequestParam(name="craftsperson[cmaCode]", description="Code CMA du réparateur")
   * @Rest\RequestParam(name="craftsperson[nafCodes]", description="Code(s) NAF du réparateur, séparés par une virgule")
   * @Rest\RequestParam(name="craftsperson[otherInfo]", description="Autres informations")
   * @Rest\View
   */
  public function editAction(Craftsperson $craftsperson, Request $request) {
    $this->checkCmaAccess($craftsperson->getCmaCode());
    return $this->processForm($craftsperson, $request, 'PUT');
  }

  /**
   * Ajout d'un réparateur
   *
   * Seuls les utilisateurs ayant un role ADMIN peuvent ajouter un réparateur à n'importe quelle CMA. Les autres utilisateurs ne peuvent ajouter des réparateurs qu'à leur CMA.
   *
   * #### Réponse en cas de code 400 ####
   * {
   *  "code" : 400,
   *  "message" : "Validation failed",
   *  "errors" : {
   *    "errors" : [
   *      "Erreur générale 1",
   *      "Erreur générale 2"
   *    ],
   *    "children" : {
   *      "siret" : {
   *        "errors" : [
   *          "Erreur sur le SIRET 1",
   *          "Erreur sur le SIRET 2"
   *        ]
   *      }
   *      "phone" : {
   *        "errors" : [
   *          "Erreur sur le téléphone 1",
   *          "Erreur sur le téléphone 2"
   *        ]
   *      }
   *    }
   *  }
   * }
   *
   * @Doc\ApiDoc(
   *  resource=true,
   *  section="Réparateurs",
   *  headers={
   *    { "name"="Authorization", "description"="Jeton d'authentification JWT précédé de la mention 'Bearer '", "required"=true }
   *  },
   *  statusCodes={
   *    201="Réparateur créé avec succès",
   *    400="Données manquantes ou invalide",
   *    403="Le réparateur fait partie d'une CMA dont l'accès est refusé"
   *  },
   * )
   *
   * @Rest\Post("/craftspersons")
   * @Rest\RequestParam(name="craftsperson[siret]", description="Numéro de SIRET")
   * @Rest\RequestParam(name="craftsperson[name]", description="Nom commercial")
   * @Rest\RequestParam(name="craftsperson[address1]", description="Adresse (1ère ligne)")
   * @Rest\RequestParam(name="craftsperson[address2]", description="Adresse (2è ligne)")
   * @Rest\RequestParam(name="craftsperson[zipCode]", requirements="/[0-9]+/", description="Code postal")
   * @Rest\RequestParam(name="craftsperson[zipCodeLabel]", description="Libellé postal")
   * @Rest\RequestParam(name="craftsperson[phone]", requirements="/(?:\+?\d{10,11},?)+/", description="Numéro de téléphone")
   * @Rest\RequestParam(name="craftsperson[email]", requirements="email", description="Adresse mail")
   * @Rest\RequestParam(name="craftsperson[website]", description="URL du site internet")
   * @Rest\RequestParam(name="craftsperson[isEnabled]", description="Statut d'affichage du réparateur dans les résultats de recherche")
   * @Rest\RequestParam(name="craftsperson[isReparactor]", description="Statut répar'acteur")
   * @Rest\RequestParam(name="craftsperson[isError]", description="Statut de signalement d'erreur")
   * @Rest\RequestParam(name="craftsperson[reparactorDescription]", description="Présentation du répar'acteur")
   * @Rest\RequestParam(name="craftsperson[reparactorHours]", description="Jours/Horaires d'ouverture du répar'acteur")
   * @Rest\RequestParam(name="craftsperson[reparactorServices]", description="Services proposés par le répar'acteur")
   * @Rest\RequestParam(name="craftsperson[reparactorCertificates]", description="Certificats et agréments du répar'acteur")
   * @Rest\RequestParam(name="craftsperson[cmaCode]", description="Code CMA du réparateur")
   * @Rest\RequestParam(name="craftsperson[nafCodes]", description="Code(s) NAF du réparateur, séparés par une virgule")
   * @Rest\RequestParam(name="craftsperson[otherInfo]", description="Autres informations")
   * @Rest\View
   */
  public function addAction(Request $request) {
    return $this->processForm(new Craftsperson(), $request, 'POST');
  }

  /**
   * Traitement du formulaire d'ajout/mise à jour d'un réparateur
   *
   * @param Craftsperson $formData
   * @param Request $request
   * @param string $method
   * @return Response
   */
  private function processForm(Craftsperson $formData, Request $request, $method) {
    $form = $this->createForm(new CraftspersonType(), $formData, array(
        'method' => $method,
    ));
    $form->handleRequest($request);
    $toPersist = $form->getData();

    // Vérification d'accès CMA
    $this->checkCmaAccess($toPersist->getCmaCode());

    // Gestion des codes NAF
    $toPersist->getNafCode()->clear();
    $nafCodeRepo = $this->getDoctrine()->getRepository(Entity::NAF_CODE);
    if($form->get('nafCodes')->getData()) {
      foreach (split(',', $form->get('nafCodes')->getData()) as $nafCode) {
        $entity = $nafCodeRepo->find($nafCode);
        if($entity) {
          $toPersist->addNafCode($entity);
        }
        else {
          $form->get('nafCodes')->addError(new FormError('unknownNafCode'));
        }
      }
    }
    else {
      $form->get('nafCodes')->addError(new FormError('required'));
    }

    if($form->isValid()) {
      $statusCode = 204;
      $now = new \DateTime();
      if($method == 'POST') {
        // Cas de création d'un réparateur
        $statusCode = 201;
        $toPersist->setCreationDate($now);
      }

      // Un callback preUpdate va vérifier s'il y a vraiment une modification manuelle
      $toPersist->setIsUpdated(true);
      $toPersist->setUpdateDate($now);
      $em = $this->getDoctrine()->getManager();
      $isPersisted = \Doctrine\ORM\UnitOfWork::STATE_MANAGED === $em->getUnitOfWork()->getEntityState($toPersist);
      if(!$isPersisted) {
        $em->persist($toPersist);
      }
      $em->flush();

      $response = new Response();
      $response->setStatusCode($statusCode);

      // Création : header 'Location' dans la réponse
      if($method == 'POST') {
        $response->headers->set('Location',
            $this->generateUrl('arapiv1_craftspersons_get', array('id' => $toPersist->getId()), true)
        );
      }

      return $response;
    }

    return View::create($form, 400);
  }

  /**
   * Supprime un réparateur
   *
   * Seuls les utilisateurs ayant un role ADMIN peuvent supprimer tous les réparateurs. Les autres utilisateurs ne peuvent supprimer que les réparateurs de leurs CMA.
   * 
   * @Doc\ApiDoc(
   *  resource=true,
   *  section="Réparateurs",
   *  headers={
   *    { "name"="Authorization", "description"="Jeton d'authentification JWT précédé de la mention 'Bearer '", "required"=true }
   *  },
   *  statusCodes={
   *    204="Réparateur supprimé avec succès",
   *    403="Le réparateur fait partie d'une CMA dont l'accès est refusé",
   *    404="Réparateur introuvable"
   *  },
   * )
   *
   * @Rest\Delete("/craftspersons/{id}")
   * @Rest\View(statusCode=204)
   */
  public function removeAction(Craftsperson $craftsperson) {
    $this->checkCmaAccess($craftsperson->getCmaCode());
    $em = $this->getDoctrine()->getEntityManager();
    $em->remove($craftsperson);
    $em->flush();
  }

  /**
   * Modification du logo d'un réparateur.
   *
   * @Doc\ApiDoc(
   *  resource=true,
   *  section="Réparateurs",
   *  headers={
   *    { "name"="Authorization", "description"="Jeton d'authentification JWT précédé de la mention 'Bearer '", "required"=true }
   *  },
   *  parameters = {
   *    { "name" = "logo", "dataType" = "file", "description" = "Logo au format JPG ou PNG", "required"=true }
   *  },
   *  statusCodes={
   *    204="Succès",
   *    400="Logo invalide",
   *    401="Jeton d'authentification JWT manquant ou invalide",
   *    404="Réparateur introuvable"
   *  }
   * )
   *
   * @Rest\Post("/craftspersons/{id}/logo")
   * @Rest\FileParam(
   *   name="logo",
   *   key=null,
   *   incompatibles={},
   *   default=null,
   *   description="Fichier image du logo",
   *   strict=false,
   *   nullable=false,
   *   requirements={}
   * )
   * @Rest\View
   */
  public function updateLogoAction(Craftsperson $craftsperson, ParamFetcherInterface $paramFetcher) {
    
    $logoFile = $paramFetcher->get('logo');
    
    if($logoFile instanceof UploadedFile) {
      $extension = $logoFile->guessExtension();
      if(!($extension == 'jpeg' || $extension == 'png')) {
        throw new BadRequestHttpException('png and jpg files only are allowed');
      }
    }

    $logoToSet = $logoFile instanceof UploadedFile ? $logoFile : null;
    $craftsperson->setLogo($logoToSet);
    $this->getDoctrine()->getEntityManager()->flush();

    $response = new Response();
    $response->setStatusCode(204);
    return $response;
  }

  /**
   * Récupère le logo d'un réparateur.
   * 
   * Le logo est retourné sous forme de données binaires (blob).
   * 
   * @Doc\ApiDoc(
   *  resource=true,
   *  section="Réparateurs",
   *  statusCodes={
   *    200="Succès",
   *    404="Logo introuvable"
   *  }
   * )
   * 
   * @Rest\Get("/craftspersons/logo/{id}")
   * @Rest\View
   */
  public function getLogoAction($id) {
    /** @var \ARCommonBundle\Entity\CraftspersonRepository $repo */
    $repo = $this->getDoctrine()->getRepository(Entity::CRAFTSPERSON);
    $craftsperson = $repo->findByLogoId($id);

    if($craftsperson) {
      $response = new BinaryFileResponse($craftsperson->getAbsoluteLogoPath());
      return $response;
    }

    throw new NotFoundHttpException('Logo not found');
  }

  /**
   * Recommandation d'un réparateur.
   * 
   * La recommandation n'est pas prise en compte immédiatement. Un mail de confirmation est envoyé au visiteur pour lui permettre de la confirmer.
   * 
   * @Doc\ApiDoc(
   *  resource=true,
   *  section="Réparateurs",
   *  statusCodes={
   *    201="Demande de recommandation créée avec succès",
   *    404="Réparateur introuvable",
   *    409="Recommandation déja confirmée pour le réparateure et email fourni"
   *  }
   * )
   * 
   * @Rest\Post("/craftspersons/{id}/like/{email}")
   * @Rest\View(serializerGroups={"public"})
   */
  public function likeAction(Craftsperson $craftsperson, $email) {
    $statusCode = 200;
    /** @var \ARCommonBundle\Service\CraftspersonService $service */
    $service = $this->get('common.craftsperson_service');

    if($service->createLikeRequest($craftsperson, $email)) {
      $response = new Response();
      $response->setStatusCode(201);
      return $response;
    }
    else {
      throw new ConflictHttpException('Like is already confirmed');
    }

  }

  /**
   * Confirmation de recommandation d'un réparateur.
   * 
   * #### Réponse vide en cas de code 404 ####
   * 
   * @Doc\ApiDoc(
   *  resource=true,
   *  section="Réparateurs",
   *  output={"class"=Craftsperson::class, "collection"=false, "groups"={"public"}},
   *  statusCodes={
   *    200="Recommandation confirmée avec succès",
   *    404="Demande de recommandation introuvable (token invalide)",
   *    409="Recommandation déja confirmée pour le token fourni"
   *  }
   * )
   * 
   * @Rest\Post("/craftspersons/confirm-like/{token}")
   * @Rest\View(serializerGroups={"public"})
   */
  public function confirmLikeAction($token) {
    $repo = $this->getDoctrine()->getRepository(Entity::CRAFTSPERSON_LIKE);
    $request = $repo->findOneBy(array(
      'confirmationToken' => $token
    ));
    if($request) {
      /** @var \ARCommonBundle\Service\CraftspersonService $service */
      $service = $this->get('common.craftsperson_service');
      if($service->confirmRequest($request)) {
        return $request->getIdCraftsperson();
      }
      else {
        return View::create($request->getIdCraftsperson(), 409);
      }
    }
    else {
      throw new NotFoundHttpException('Token not found');
    }
  }

  /**
   * Signalement d'une erreur pour un réparateur.
   * 
   * #### Réponse en cas de code 400 ####
   * {
   *  "code" : 400,
   *  "message" : "Validation failed",
   *  "errors" : {
   *    "errors" : [
   *      "Erreur générale 1",
   *      "Erreur générale 2"
   *    ],
   *    "children" : {
   *      "description" : {
   *        "errors" : [
   *          "Erreur sur la description 1",
   *          "Erreur sur la description 2"
   *        ]
   *      }
   *    }
   *  }
   * }
   * 
   * #### Réponse en cas de code 200 ####
   * 
   * Objet craftsperson
   * 
   * @Doc\ApiDoc(
   *  resource=true,
   *  section="Réparateurs",
   *  output={"class"=Craftsperson::class, "collection"=false, "groups"={"public"}},
   *  statusCodes={
   *    200="Signalement enregistré avec succès",
   *    400="Erreur de validation des paramètres",
   *    404="Réparateur introuvable"
   *  }
   * )
   * 
   * @Rest\RequestParam(name="description", description="Description de l'erreur, entre 50 et 500 caractères")
   * @Rest\Post("/craftspersons/{id}/error")
   * @Rest\View(serializerGroups={"public"})
   */
  public function errorAction(Craftsperson $craftsperson, ParamFetcherInterface $paramFetcher) {
    $description = $paramFetcher->get('description');

    // Vérification des contraintes de validation
    $form = $this->createForm(new ErrorReportType());
    $form->submit(array(
        'description' => $description
    ));

    if(!$form->isValid()) {
        return View::create($form, 400);
    }
    /** @var \ARCommonBundle\Service\CraftspersonService $service */
    $service = $this->get('common.craftsperson_service');
    $errorMsgs = array_map(function($err){return $err->getMessage();},$form->get('description')->getData()->toArray());
    $service->errorReport($craftsperson, $errorMsgs);

    //## Prevent sending the email adress in order to avoid bot spamming
    $craftsperson->setEmail(null);

    return array(
      'craftsperson' => $craftsperson
    );
  }

/**
* Contact d'un réparateur.
*
* #### Réponse en cas de code 400 ####
* {
*  "code" : 400,
*  "message" : "Validation failed",
*  "errors" : {
*    "errors" : [
*      "Erreur générale 1",
*      "Erreur générale 2"
*    ],
*    "children" : {
*      "description" : {
*        "errors" : [
*          "Erreur sur la description 1",
*          "Erreur sur la description 2"
*        ]
*      }
*    }
*  }
* }
*
* #### Réponse en cas de code 200 ####
*
* Objet craftsperson
*
* @Doc\ApiDoc(
*  resource=true,
*  section="Réparateurs",
*  output={"class"=Craftsperson::class, "collection"=false, "groups"={"public"}},
*  statusCodes={
*    200="Signalement enregistré avec succès",
*    400="Erreur de validation des paramètres",
*    404="Réparateur introuvable"
*  }
* )
*
* @Rest\RequestParam(name="email", description="adresse mail du particulier")
* @Rest\RequestParam(name="message", description="Message du particulier, entre 50 et 1000 caractères")
* @Rest\Post("/craftspersons/{id}/contact")
* @Rest\View(serializerGroups={"public"})
*/
    public function contactAction(Craftsperson $craftsperson, ParamFetcherInterface $paramFetcher)
    {
        $message = $paramFetcher->get('message');
        $email = $paramFetcher->get('email');

        // Vérification des contraintes de validation
        $form = $this->createForm(new ContactType());
        $form->submit(array(
            'message' => $message,
            'email' => $email
        ));

        if (!$form->isValid()) {
            return View::create($form, 400);
        }
        /** @var \ARCommonBundle\Service\CraftspersonService $service */
        $service = $this->get('common.craftsperson_service');
        $sender = $form->get('email')->getData();
        $message = $form->get('message')->getData();
        $service->messageCraftperson($craftsperson, $sender, $message);

        //## Prevent sending the email adress in order to avoid bot spamming
        $craftsperson->setEmail(null);
        return array(
            'craftsperson' => $craftsperson
        );
    }
}