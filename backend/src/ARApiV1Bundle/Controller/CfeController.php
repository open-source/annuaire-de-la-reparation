<?php

namespace ARApiV1Bundle\Controller;

use ARApiV1Bundle\Exception\RetryWithHttpException;
use ARApiV1Bundle\Form\CfeType;
use ARApiV1Bundle\Form\CraftspersonType;
use ARApiV1Bundle\Form\ErrorReportType;
use ARCommonBundle\Entity\Cfe;
use ARCommonBundle\Entity\Craftsperson;
use ARCommonBundle\Entity\Department;
use ARCommonBundle\Entity\NafCode;
use ARCommonBundle\Entity\User;
use ARCommonBundle\Enum\Entity;
use ARCommonBundle\Enum\Role;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Request\ParamFetcherInterface;
use FOS\RestBundle\View\View;
use Nelmio\ApiDocBundle\Annotation as Doc;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\ConflictHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\UnprocessableEntityHttpException;

/**
 * La classe CfeController définit le contrôleur des requêtes liées aux entités {@link Cfe}.
 *
 * @author vdouillet
 *        
 */
class CfeController extends FOSRestController {

  /**
   * Récupère tous les cfes
   * 
   * #### Réponse avec authentification
   * {
   *  "cfe": tableau de cfe,
   *  "total": 156
   * }
   * 
   * @Doc\ApiDoc(
   *  resource=true,
   *  section="Cfe",
   *  headers={
   *    { "name"="Authorization", "description"="Jeton d'authentification JWT précédé de la mention 'Bearer '", "required"=false }
   *  },
   *  output={"class"=Cfe::class, "collection"=true, "collectionName"="cfes"}
   * )
   *
   * @Rest\Get("/cfe")
   * @Rest\View
   */
  public function getAllAction(ParamFetcherInterface $paramFetcher) {
        return $this->allCFEWithPerm();
  }

  public function allCFEWithPerm(){
      /** @var \ARCommonBundle\Entity\CraftspersonRepository $repo */
      $repo = $this->getDoctrine()->getRepository(Entity::CFE);

      $cfes = $repo->findAll();
      $res =array();
      foreach($cfes as $cfe){
          if($this->hasCfeAccess($cfe)){
              $res[] =$cfe;
          }
      }

      return $res;

  }
    /**
     * Modification d'un cfe
     *
     * Seuls les utilisateurs ayant un role ADMIN peuvent modifier tous les réparateurs. Les autres utilisateurs ne peuvent modifier que les réparateurs de leurs CMA.
     *
     *
     * @Doc\ApiDoc(
     *  resource=true,
     *  section="Réparateurs",
     *  headers={
     *    { "name"="Authorization", "description"="Jeton d'authentification JWT précédé de la mention 'Bearer '", "required"=true }
     *  },
     *  statusCodes={
     *    204="Réparateur mis à jour avec succès",
     *    400="Données manquantes ou invalide",
     *    403="Le réparateur fait partie d'une CMA dont l'accès est refusé",
     *    404="Réparateur introuvable"
     *  },
     * )
     *
     * @Rest\Put("/cfe/{id}")
     * @Rest\RequestParam(name="cfe[id]", description="id de la Région")
     * @Rest\RequestParam(name="cfe[url]", description="Url")
     * @Rest\View
     */
    public function editAction(Cfe $cfe, Request $request) {
        if(!$this->hasCfeAccess($cfe)){
            throw new AccessDeniedHttpException('Accès refusé à ce CFE');
        }

        $form = $this->createForm(new CfeType(), $cfe, array(
            'method' => 'PUT',
        ));
        $form->handleRequest($request);
        $toPersist = $form->getData();


        if($form->isValid()) {
            $statusCode = 204;

            $em = $this->getDoctrine()->getManager();
            $isPersisted = \Doctrine\ORM\UnitOfWork::STATE_MANAGED === $em->getUnitOfWork()->getEntityState($toPersist);
            if(!$isPersisted) {
                $em->persist($toPersist);
            }
            $em->flush();

            $response = new Response();
            $response->setStatusCode($statusCode);

            return $this->allCFEWithPerm();
        }

        return View::create($form, 400);
        new \Doctrine\Common\Collections\ArrayCollection();
    }

    private function hasCfeAccess(Cfe $cfe) {
        if($this->isGranted(Role::ADMIN)) {
            return true;
        }
        /** @var User $currentUser */
        $currentUser = $this->getUser();
        return !(!$cma || !$currentUser || $currentUser->getCodeDepartment()->isEmpty() || !$cfe->getDepartments()->contains($currentUser->getCodeDepartment()->get(0)));
    }
}

