<?php

namespace ARApiV1Bundle\Controller;

use ARCommonBundle\Entity\Department;
use ARCommonBundle\Enum\Entity;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use Nelmio\ApiDocBundle\Annotation as Doc;

/**
 * La classe DepartmentController définit le contrôleur des requêtes liées aux entités {@link Department}.
 *
 * @author vdouillet
 *        
 */
class DepartmentController extends FOSRestController {

    /**
     * Récupère les départements
     * 
     * @Doc\ApiDoc(
     *  resource=true,
     *  section="Départements",
     *  headers={
     *    { "name"="Authorization", "description"="Jeton d'authentification JWT précédé de la mention 'Bearer '", "required"=true }
     *  },
     *  output={ "class"=Department::class, "collection"=true, "collectionName"="departments" },
     *  statusCodes={
     *    401="Jeton d'authentification JWT manquant ou invalide",
     *  },
     * )
     * @Rest\Get("/departments")
     * @Rest\View
     */
    public function getAction() {
        $repo = $this->getDoctrine()->getRepository(Entity::DEPARTMENT);

        return array (
            'departments' => $repo->findAll()
        );
    }
}