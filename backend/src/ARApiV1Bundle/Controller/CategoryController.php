<?php

namespace ARApiV1Bundle\Controller;

use ARCommonBundle\Enum\Entity;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use Nelmio\ApiDocBundle\Annotation as Doc;

/**
 * La classe CategoryController définit le contrôleur des requêtes liées aux entités {@link Category}.
 *
 * @author vdouillet    
 */
class CategoryController extends FOSRestController {
  
  /**
   * Récupère toutes les catégories
   * 
   * @Doc\ApiDoc(
   *  resource=true,
   *  section="Catégories de réparateurs",
   *  output={"class"=Category::class, "collection"=true, "collectionName"="categories", "groups"={"public"}},
   * )
   *
   * @Rest\Get("/categories")
   * @Rest\View(serializerGroups={"public"})
   */
  public function getAllAction() {
      $categoryRepo = $this->getDoctrine()->getRepository(Entity::CATEGORY);
      return array(
        'categories' => $categoryRepo->findAll()
      );
  }
}