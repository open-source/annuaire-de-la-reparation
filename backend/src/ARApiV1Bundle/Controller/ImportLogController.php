<?php

namespace ARApiV1Bundle\Controller;

use ARApiV1Bundle\Form\ImportLogType;
use ARCommonBundle\Enum\Entity;
use ARCommonBundle\Enum\Role;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Request\ParamFetcherInterface;
use FOS\RestBundle\View\View;
use Nelmio\ApiDocBundle\Annotation as Doc;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * La classe ImportLogController définit le contrôleur des requêtes liées aux entités {@link ImportLog}.
 *
 * @author vdouillet
 */
class ImportLogController extends FOSRestController {
    /** Format de date accepté pour la recherche */
    const DATETIME_FORMAT = 'd/m/Y H:i:s';

    /**
     * Vérification d'accès aux fonctions protégées du contrôleur.
     */
    private function checkAccess() {
        // Accessible seulement aux administrateurs
        if(!$this->isGranted(Role::ADMIN)) {
            throw new AccessDeniedHttpException('Administrator rights required');
        }
    }

    /**
     * Récupère les journaux d'import
     *
     * #### Réponse en cas de code 400 ####
     * {
     *  "code" : 400,
     *  "message" : "Validation failed",
     *  "errors" : {
     *    "errors" : [
     *      "invalidDateRange"
     *    ],
     *    "children" : {
     *      "startDate" : {
     *        "errors" : [
     *          "pattern"
     *        ]
     *      }
     *      "endDate" : {
     *        "errors" : [
     *          "pattern",
     *        ]
     *      }
     *    }
     *  }
     * }
     * 
     * @Doc\ApiDoc(
     *  resource=true,
     *  section="Journaux d'import",
     *  output={"class"=ImportLog::class, "collection"=true, "collectionName"="logs"},
     *  statusCodes={
     *    400="Régions manquantes",
     *    401="Jeton d'authentification JWT manquant ou invalide",
     *    403="Droits d'accès insuffisants (non administrateur)"
     *  }
     * )
     *
     * @Rest\Get("/import-logs")
     * @Rest\QueryParam(name="startDate", description="Facultatif. Date de départ au format dd/mm/yyyy")
     * @Rest\QueryParam(name="endDate", description="Facultatif. Date de fin au format dd/mm/yyyy. Doit être supérieure ou égale à 'startDate' sinon erreur 400 'invalidDateRange'")
     * @Rest\QueryParam(name="regions", description="Requis. Régions à considérer. Noms de région en minuscules et sans accents, séparés par des virgules")
     * @Rest\View()
     */
    public function getAllAction(ParamFetcherInterface $paramFetcher) {

        // Valeurs des filtres
        $startDate = $paramFetcher->get('startDate');
        $endDate = $paramFetcher->get('endDate');
        $regions = $paramFetcher->get('regions');

        // Vérification de validité des valeurs
        $form = $this->createForm(new ImportLogType());
        $form->submit(array(
            'startDate' => $startDate,
            'endDate' => $endDate,
            'regions' => $regions
        ));

        if(!$form->isValid()) {
            return View::create($form, 400);
        }

        // Lecture des paramètres
        if($startDate) {
            $startDate = \DateTime::createFromFormat($this::DATETIME_FORMAT, $startDate . ' 00:00:00');
        }
        else {
            $startDate = null;
        }
        
        if($endDate) {
            $endDate = \DateTime::createFromFormat($this::DATETIME_FORMAT, $endDate . ' 00:00:00');
        }
        else {
            $endDate = null;
        }

        if($startDate && $endDate && $startDate > $endDate) {
            $form->addError(new FormError('invalidDateRange'));
            return View::create($form, 400);
        }
        
        if($regions) {
            $regions = explode(',', $regions);
        }
        else {
            $regions = null;
        }

        // Requête et résultat
        /** @var \ARCommonBundle\Entity\ImportLogRepository $importLogRepo */
        $importLogRepo = $this->getDoctrine()->getRepository(Entity::IMPORT_LOG);
        return $importLogRepo->getWithFilters($startDate, $endDate, $regions);
    }
}