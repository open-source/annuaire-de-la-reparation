<?php

namespace ARApiV1Bundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use ARCommonBundle\Entity\NafCode;

class NafCodeData implements FixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $nafCode = new NafCode();
        $nafCode->setCode('C1');
        $nafCode->setLabel('DefaultCode');

        $manager->persist($nafCode);
        $manager->flush();
    }
}