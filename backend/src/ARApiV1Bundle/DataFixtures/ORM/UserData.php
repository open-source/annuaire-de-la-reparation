<?php

namespace ARApiV1Bundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use ARCommonBundle\Entity\User;
use ARCommonBundle\Enum\Role;

class UserData implements FixtureInterface, ContainerAwareInterface
{
    const USERNAME = 'test@test.fr';
    const PASSWORD = 'test';

    /** @var ContainerInterface */
    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function load(ObjectManager $manager)
    {
        $user = new User();
        $user->setFirstname('Test');
        $user->setLastname('User');
        $user->setUsername($this::USERNAME);
        $user->setEmail($this::USERNAME);
        $user->setEnabled(true);
        //$user->setLocked(false);
        //$user->setExpired(false);
        //$user->setCredentialsExpired(false);
        $user->setIsMailContact(false);
        $user->setRoles(array(Role::ADMIN));

        $user->setSalt(md5(uniqid()));
        $encoder = $this->container->get('security.password_encoder');
        $password = $encoder->encodePassword($user, $this::PASSWORD);
        $user->setPassword($password);

        $manager->persist($user);
        $manager->flush();
    }
}