<?php

namespace ARApiV1Bundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use ARCommonBundle\Entity\User;
use ARCommonBundle\Enum\Entity;

class UserDepartmentData implements FixtureInterface, ContainerAwareInterface
{
    const USERNAME = 'test@test.fr';
    const CODE_DEPARTMENT = 35;

    /** @var ContainerInterface */
    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function load(ObjectManager $manager)
    {
        $userRepo = $manager->getRepository(Entity::USER);
        $user = $userRepo->findOneBy(array('username' => $this::USERNAME));

        $departmentRepo = $manager->getRepository(Entity::DEPARTMENT);
        $department = $departmentRepo->findOneBy(array('code' => $this::CODE_DEPARTMENT));
        $user->addCodeDepartment($department);

        $manager->flush();
    }
}