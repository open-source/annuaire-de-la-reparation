<?php

namespace ARApiV1Bundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use ARCommonBundle\Entity\Department;

class DepartmentData implements FixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $department = new Department();
        $department->setCode(35);
        $department->setName('Bretagne');

        $department2 = new Department();
        $department2->setCode(56);
        $department2->setName('Morbihan');

        $manager->persist($department);
        $manager->persist($department2);
        $manager->flush();
    }
}