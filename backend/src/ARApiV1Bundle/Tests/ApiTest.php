<?php

namespace ARApiV1Bundle\Tests;

use Symfony\Component\HttpFoundation\Response;
use ARApiV1Bundle\DataFixtures\ORM\UserData;

/**
 * La classe AuthTest définit un test d'un appel à l'API.
 *
 * @author vdouillet
 *        
 */
abstract class ApiTest extends FixtureAwareTest
{
    /**
     * Vérifier le code de réponse et le type de contenu de la réponse.
     * @param Response $response
     * @param integer $statusCode
     */
    protected function assertJsonResponse(Response $response, $statusCode = 200)
    {
        $this->assertEquals($statusCode, $response->getStatusCode(), $response->getContent());
        $this->assertTrue($response->headers->contains('Content-Type', 'application/json'), $response->headers);
    }

    /**
     * Décoder le contenu de la réponse (format json attendu).
     * @param Response $response
     * @return mixed
     */
    protected function getResponseContent(Response $response) {
        return json_decode($response->getContent());
    }

    /**
     * Créer un client authentifié.
     *
     * @param string $username
     * @param string $password
     * @return \Symfony\Bundle\FrameworkBundle\Client
     */
    protected function createAuthenticatedClient($username = UserData::USERNAME, $password = UserData::PASSWORD)
    {
        $client = static::createClient();
        $client->request('POST', '/api/login_check', array(
            'username' => UserData::USERNAME,
            'password' => UserData::PASSWORD
        ));
        $this->assertJsonResponse($client->getResponse(), 200);
        $content = $this->getResponseContent($client->getResponse());

        $client = static::createClient();
        $client->setServerParameter('HTTP_Authorization', sprintf('Bearer %s', $content->token));

        return $client;
    }

    /**
     * Accéder à un service.
     * 
     * @param string $service
     * @return mixed
     */
    protected function get($service) {
        return static::$kernel->getContainer()->get($service);
    }
}