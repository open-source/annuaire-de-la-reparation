<?php

namespace ARApiV1Bundle\Tests\Controller;

use ARApiV1Bundle\DataFixtures\ORM\UserData;
use ARApiV1Bundle\Tests\ApiTest;

/**
 * La classe AuthTest définit les tests de l'api d'authentification.
 *
 * @author vdouillet
 *        
 */
class AuthTest extends ApiTest
{
    /**
     * Ajout de l'utilisateur de test en BDD.
     */
    public function setUp()
    {
        parent::setUp();
        $this->addFixture(new UserData());
        $this->loadFixtures();
    }

    /**
     * Login invalide.
     */
    public function testInvalidLogin()
    {
        $client = static::createClient();
        $client->request('POST', '/api/login_check');
        $this->assertJsonResponse($client->getResponse(), 401);
    }

    /**
     * Login valide.
     */
    public function testValidLogin()
    {
        $client = static::createClient();
        $client->request('POST', '/api/login_check', array(
            'username' => UserData::USERNAME,
            'password' => UserData::PASSWORD
        ));

        $this->assertJsonResponse($client->getResponse(), 200);
        $responseContent = $this->getResponseContent($client->getResponse());
        $this->assertTrue(property_exists($responseContent, 'token'), 'No token in authentication response');
        $this->assertNotEmpty($responseContent->token);
    }
}
