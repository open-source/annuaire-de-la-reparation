<?php

namespace ARApiV1Bundle\Tests\Controller;

use ARApiV1Bundle\DataFixtures\ORM\DepartmentData;
use ARApiV1Bundle\DataFixtures\ORM\NafCodeData;
use ARApiV1Bundle\DataFixtures\ORM\UserData;
use ARApiV1Bundle\DataFixtures\ORM\UserDepartmentData;
use ARApiV1Bundle\Tests\ApiTest;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Response;

/**
 * La classe CraftspersonsControllerTest définit les tests de l'api des entités {@link Craftsperson}.
 *
 * @author vdouillet
 *        
 */
class CraftspersonsControllerTest extends ApiTest {
    const GET_URL = '/api/craftspersons';
    const IMPORT_CHECK_URL = '/api/craftspersons/check-file';
    const IMPORT_URL = '/api/craftspersons/import-file';
    const SEARCH_URL = '/api/craftspersons/search';

    public function setUp()
    {
        parent::setUp();
        $this->addFixture(new UserData());
        $this->addFixture(new DepartmentData());
        $this->addFixture(new NafCodeData());
        $this->addFixture(new UserDepartmentData());
        $this->loadFixtures();
    }

    public function testGetAll()
    {
        $client = $this->createAuthenticatedClient();
        $client->request('GET', $this::GET_URL);
        $this->assertJsonResponse($client->getResponse(), 200);

        $content = $this->getResponseContent($client->getResponse());
        $this->assertTrue(property_exists($content, 'craftspersons'));
    }

    public function testImportNoFile() {
        $client = $this->createAuthenticatedClient();
        $client->request('POST', $this::IMPORT_URL);
        $this->assertJsonResponse($client->getResponse(), 400);

        $client->request('POST', $this::IMPORT_CHECK_URL);
        $this->assertJsonResponse($client->getResponse(), 400);
    }

    public function testImportIncorrectFilename() {
        $this->importFile($this->getResource('data.xls'), 400);
        $this->importFile($this->getResource('REGION_.csv'), 400);
    }

    public function testImportIncorrectColumnFormat() {
        $checkCallback = function($responseContent) {
            $errors = $responseContent->errors;
            $this->assertEquals(1, count($errors), 'Incorrect error count');
            $this->assertEquals(1, $errors[0]->line, 'Error is on incorrect line');
        };

        $this->importFile($this->getResource('DEPARTEMENT_56.csv'), 400, $checkCallback);
    }

    public function testImportIncorrectValue()
    {
        $checkCallback = function($responseContent) {
            $result = $responseContent->result;
            $logs = $result->logs;
            $this->assertEquals(2, count($logs), 'Incorrect number of invalid or ignored lines');
            $this->assertEquals(2, $logs[0]->line, 'Error is on incorrect line ');
            $this->assertEquals('Fiche invalide : 3 erreur(s)', $logs[0]->message, 'Incorrect error count ');
            $this->assertEquals(3, $logs[1]->line, 'Error is on incorrect line');
            $this->assertEquals('Fiche invalide : 1 erreur(s)', $logs[1]->message, 'Incorrect error count');
        };

        $this->importFile($this->getResource('DEPARTEMENT_35.csv'), 200, $checkCallback);
    }

    public function testImport() {
        $checkCallback = function($responseContent) {
            $result = $responseContent->result;
            $this->assertEquals(2, $result->created, 'Incorrect craftsperson creation count ');
        };

        $this->importFile($this->getResource('REGION_BRETAGNE.csv'), 200, $checkCallback);
    }

    public function testSearchError() {
        $client = static::createClient();

        // Rayon et localisation manquants
        $client->request('GET', $this::SEARCH_URL);
        $this->assertJsonResponse($client->getResponse(), 422);

        // Rayon manquant
        $client->request('GET', $this::SEARCH_URL, array(
            'lat' => 48,
            'lon' => 2
        ));
        $this->assertJsonResponse($client->getResponse(), 422);

        // Latitude manquante
        $client->request('GET', $this::SEARCH_URL, array(
            'radius' => 10,
            'lon' => 2
        ));
        $this->assertJsonResponse($client->getResponse(), 422);
        
        // Longitude manquante
        $client->request('GET', $this::SEARCH_URL, array(
            'radius' => 10,
            'lat' => 48
        ));
        $this->assertJsonResponse($client->getResponse(), 422);
        
        // Géolocalisation impossible
        $client->request('GET', $this::SEARCH_URL, array(
            'radius' => 10,
            'address' => 'LE HOUSSARD, 35580 ST SENOUX'
        ));
        $this->assertJsonResponse($client->getResponse(), 449);
    }

    public function testSearchOk()
    {
        $client = static::createClient();

        // Position
        $client->request('GET', $this::SEARCH_URL, array(
            'radius' => 10,
            'lat' => 48,
            'lon' => 2
        ));
        $this->assertSearchResult($client->getResponse());

        // Géolocalisation
        $client->request('GET', $this::SEARCH_URL, array(
            'radius' => 10,
            'address' => 'Paris'
        ));
        $this->assertSearchResult($client->getResponse());

        // Position avec catégorie
        $client->request('GET', $this::SEARCH_URL, array(
            'radius' => 10,
            'lat' => 48,
            'lon' => 2,
            'category' => 'Test'
        ));
        $this->assertSearchResult($client->getResponse());
    }

    private function importFile(UploadedFile $file, $statusCode = 200, $checkResponseCallback = null) {
        $client = $this->createAuthenticatedClient();
        $client->request('POST', $this::IMPORT_CHECK_URL, array(), array(
            'uploadFile' => $file
        ));
        $this->assertJsonResponse($client->getResponse(), $statusCode);
        if($checkResponseCallback) {
            $checkResponseCallback($this->getResponseContent($client->getResponse()));
        }

        $client->request('POST', $this::IMPORT_URL, array(), array(
            'uploadFile' => $file
        ));
        $this->assertJsonResponse($client->getResponse(), $statusCode);
        if($checkResponseCallback) {
            $checkResponseCallback($this->getResponseContent($client->getResponse()));
        }
    }

    private function getResource($name) {
        $path = $this->get('kernel')->getRootDir() . '/../src/ARApiV1Bundle/Tests/files/' . $name;
        $resource = new UploadedFile($path, $name, 'application/xls', filesize($path));

        return $resource;
    }

    private function assertSearchResult(Response $response)
    {
        $this->assertJsonResponse($response, 200);

        $content = $this->getResponseContent($response);
        $this->assertTrue(property_exists($content, 'craftspersons'));
        $this->assertTrue(is_array($content->craftspersons));
    }
}