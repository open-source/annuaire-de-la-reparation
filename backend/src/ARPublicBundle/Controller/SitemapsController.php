<?php

namespace ARPublicBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;

class SitemapsController extends Controller
{
    /**
     * @Route("/sitemap.{_format}", name="sample_sitemaps_sitemap", Requirements={"_format" = "xml"})
     * @Template("ARPublicBundle:Sitemaps:sitemaps.xml.twig")
     */
    public function sitemapAction(Request $request) 
    {
        //set_time_limit(240);
        //ini_set('memory_limit', '1024M');
        $em = $this->getDoctrine()->getManager();
        
        $urls = array();
        $hostname = $request->getHost();

        // add some urls homepage
        $urls[] = array('loc' => $this->get('router')->generate('annuaire_accueil'), 'changefreq' => 'yearly', 'priority' => '0.7');
        $urls[] = array('loc' => $this->get('router')->generate('annuaire_recherche', array('address' => "France")), 'changefreq' => 'always', 'priority' => '1.0');

        // service
        $departements = $em->getRepository('ARCommonBundle:Department')->findAll();
        foreach ($departements as $departement) {
            $adresseName = $departement->getName() . ", France";
            $urls[] = array('loc' => $this->get('router')->generate('annuaire_recherche', 
                     array('address' => $adresseName)), 'changefreq' => 'always', 'priority' => '1.0');
        }

        return array('urls' => $urls, 'hostname' => $hostname);
    }
}
