<?php

namespace ARPublicBundle\Controller;

use ARCommonBundle\Entity\Cfe;
use ARCommonBundle\Entity\Craftsperson;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use ARCommonBundle\Enum\Entity;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="annuaire_accueil")
     */
    public function indexAction()
    {
        return $this->render('ARPublicBundle:Default:index.html.twig');
    }

    /**
     * @Route("/search", name="annuaire_recherche")
     */
    public function searchAction()
    {
        return $this->render('ARPublicBundle:Default:search.html.twig');
    }

    /**
     * @Route("/reparateur/like/{token}")
     */
    public function craftspersonLikeAction($token)
    {
        return $this->render('ARPublicBundle:Default:craftsperson.html.twig', array(
            'token' => $token,
            'id' => ''
        ));
    }

    /**
     * @Route("/reparateur/{id}", name="fiche-artisan-reparateur")
     */
    public function craftspersonAction($id)
    {
        $errRepo = $this->getDoctrine()->getManager()->getRepository(Entity::CRAFTSPERSON_ERR_MSG);
        $errMsgs = $errRepo->findAllArray();

        /** @var Craftsperson $craftsperson */
        $craftsperson = $this->getDoctrine()->getManager()->getRepository(Entity::CRAFTSPERSON)->find($id);
        $cmaCode = $craftsperson? $craftsperson->getCmaCode():null;
        /** @var Cfe $cfe */
        $cfe = $cmaCode? $cmaCode->getCfe():array();

        return $this->render('ARPublicBundle:Default:craftsperson.html.twig', array(
            'cfe' => array("region"=>$cfe->getRegion(), "url"=>$cfe->getUrl()),
            'errMsgs' => $errMsgs,
            'token' => '',
            'id' => $id
        ));
    }

    /**
     * @Route("/mentions-legales", name="annuaire_mentions_legales")
     */
    public function mentionsLegalesAction()
    {
        return $this->render('ARPublicBundle:Default:mentions-legales.html.twig');
    }

    /**
     * @Route("/conditions-utilisation", name="annuaire_conditions_utilisation")
     */
    public function conditionsUtilisationAction()
    {
        return $this->render('ARPublicBundle:Default:conditions-utilisation.html.twig');
    }

    /**
     * @Route("/politique-accessibilite", name="annuaire_politique_accessibilite")
     */
    public function politiqueAccessibiliteAction()
    {
        return $this->render('ARPublicBundle:Default:politique-accessibilite.html.twig');
    }

    /**
     * @Route("/plan-site", name="annuaire_plan_site")
     */
    public function planSiteAction()
    {
        return $this->render('ARPublicBundle:Default:plan-site.html.twig');
    }
}
