<?php

namespace ARPublicBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use GuzzleHttp\Client;
use Symfony\Component\HttpFoundation\Request;
use ARCommonBundle\Enum\Entity;

/**
 * Controlleur permettant d'accéder à la page de test et à la page de version du site
 */
class SiteCheckController extends Controller
{
    /**
     * @Route("/show-version", name="show_version")
     */
    public function versionAction() 
    {
        $version = $this->getParameter('app_version');

        return $this->render('ARPublicBundle:SiteCheck:version.html.twig', array('version' => $version));
    }

    /**
     * @Route("/check-install", name="check_install")
     */
    public function checkInstallAction(Request $request) 
    {
        $errorMessages = array();

        
        $baseurl = $request->getScheme() . '://' . $request->getHttpHost() . $request->getBasePath();

        $client = new Client([
            // Base URI is used with relative requests
            'base_uri' => $baseurl,
            // You can set any number of default request options.
            'timeout'  => 2.0,
        ]);

        //Test accès page d'accueil
        $response = $client->get('/');
        if($response->getStatusCode() != 200) {
            $errorMessages[] = "Impossible d'accéder à la page d'accueil";
        }
        

        //Test accès web service
        $response = $client->get('/api/categories');
        if($response->getStatusCode() != 200) {
            $errorMessages[] = "Impossible d'accéder aux web services";
        } 

        //Test d'accès à la base de données
        $categories = array();
        try {
            $categoryRepo = $this->getDoctrine()->getRepository(Entity::CATEGORY);
            $categories = $categoryRepo->findAll();
        } catch(\Exception $e) {
            $errorMessages[] = "Impossible d'accéder à la base de données";
        }
        
        //Test d'ajout des données de références
        if(count($categories) == 0) {
            $errorMessages[] = "Les données de référence sont manquantes.";
        }

        //Test d'accès google maps
        $response = $client->get('https://maps.google.com');
        if($response->getStatusCode() != 200) {
            $errorMessages[] = "Impossible d'accéder à google maps";
        }

        return $this->render('ARPublicBundle:SiteCheck:check-install.html.twig', array("errorMessages" => $errorMessages));
    }
}
