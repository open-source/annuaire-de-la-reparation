<?php

namespace ARCommonBundle\Entity;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;


/**
 * La classe ImportLogRepository définit la gestion des entités {@link ImportLog}.
 *
 * @author vdouillet
 *        
 */
class ImportLogRepository extends EntityRepository {

  /**
   * Création d'une entrée de log d'import.
   *
   * @param string $filename
   * @param integer $line
   * @param string $message
   * @param string $region
   * @param integer $siret
   * @return \ARCommonBundle\Entity\ImportLog
   */
  public function create($filename, $line, $message, $region = null, $siret) {
    $log = new ImportLog();
    $log->setDate(new \DateTime('NOW'));
    $log->setFilename($filename);
    $log->setLine($line);
    $log->setMessage($message);
    $log->setRegion($region);
    $log->setSiret($siret);
    
    return $log;
  }

  /**
   * Retourne les logs correspondants aux filtres spécifiés.
   *
   * @param \DateTime $startDate
   * @param \DateTime $endDate
   * @param string[] $regions
   * @return array
   */
  public function getWithFilters(\DateTime $startDate = null, \DateTime $endDate = null, $regions) {
    $qb = $this->createQueryBuilder('l');
    $qb->select($qb->expr()->count('l'))
      ->where('1 = 1')
      ->orderBy('l.date', 'DESC');

    if($startDate) {
      $qb->andWhere($qb->expr()->gt('l.date', ':startDate'))
        ->setParameter('startDate', $startDate);
    }
    if($endDate) {
      $qb->andWhere($qb->expr()->lt('l.date', ':endDate'))
        ->setParameter('endDate', $endDate->add(new \DateInterval('P1D')));
    }

    if($regions) {
      $regionFilters = [];
      foreach ($regions as $index => $region) {
          $regionFilters[] = "l.region LIKE :region$index";
          $qb->setParameter("region$index", $region);
      }
      if($regionFilters) {
        $qb->andWhere($qb->expr()->isNotNull('l.region'))
          ->andWhere(new Expr\Orx($regionFilters));
      }
    }

    $count = $qb->getQuery()->getSingleScalarResult();

    $qb->select('l');
    $logs = $qb->getQuery()->getResult();

    return array(
      'logs' => $logs,
      'total' => $count
    );

  }
}