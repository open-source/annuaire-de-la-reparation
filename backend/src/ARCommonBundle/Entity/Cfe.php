<?php

namespace ARCommonBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
/**
 * Cfe
 *
 * @ORM\Table(name="t_cfe")
 * @ORM\Entity(repositoryClass="ARCommonBundle\Entity\CfeRepository")
 */
class Cfe
{

    /**
     * Identifiant unique
     *
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     *
     */
    private $id;

    /**
     * Region du cfe
     *
     * @var string
     *
     * @ORM\Column(name="region", type="string", nullable=false)
     */
    private $region;



    /**
     * Lien vers le site du CFE
     *
     * @var string
     *
     * @ORM\Column(name="url", type="string", length=255, nullable=true)
     */
    private $url;


    /**
     * @var \Doctrine\Common\Collections\Collection
     * @ORM\OneToMany(targetEntity="ARCommonBundle\Entity\Department", mappedBy="cfe")
     */
    private $departments;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->departments = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getRegion()
    {
        return $this->region;
    }

    /**
     * @param string $region
     */
    public function setRegion($region)
    {
        $this->region = $region;
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param string $url
     */
    public function setUrl($url)
    {
        $this->url = $url;
    }

    /**
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDepartments()
    {
        return $this->departments;
    }

    /**
     * @param \Doctrine\Common\Collections\Collection $departments
     */
    public function setDepartments($departments)
    {
        $this->departments = $departments;
    }


}
