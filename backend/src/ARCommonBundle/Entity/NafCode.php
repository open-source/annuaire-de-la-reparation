<?php

namespace ARCommonBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * NafCode
 *
 * @ORM\Table(name="r_naf_code")
 * @ORM\Entity
 */
class NafCode
{
    /**
     * Code
     *
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=50)
     * @ORM\Id
     */
    private $code;

    /**
     * Description
     *
     * @var string
     *
     * @ORM\Column(name="label", type="string", length=255, nullable=false)
     */
    private $label;

    /**
     * Categories
     *
     * @var \Doctrine\Common\Collections\ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="ARCommonBundle\Entity\Category")
     * @ORM\JoinTable(name="r_category_has_naf_code",
     *   joinColumns={
     *     @ORM\JoinColumn(name="code_naf_code", referencedColumnName="code")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="id_category", referencedColumnName="id")
     *   }
     * )
     */
    private $categories;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->categories = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set code
     *
     * @param integer $code
     * @return NafCode
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string 
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set label
     *
     * @param string $label
     * @return NafCode
     */
    public function setLabel($label)
    {
        $this->label = $label;

        return $this;
    }

    /**
     * Get label
     *
     * @return string 
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * Add category
     *
     * @param \ARCommonBundle\Entity\Category $category
     * @return NafCode
     */
    public function addCategory(\ARCommonBundle\Entity\Category $category)
    {
        $this->categories[] = $category;

        return $this;
    }

    /**
     * Remove category
     *
     * @param \ARCommonBundle\Entity\Category $category
     */
    public function removeCategory(\ARCommonBundle\Entity\Category $category)
    {
        $this->categories->removeElement($category);
    }

    /**
     * Get categories
     *
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function getCategories()
    {
        return $this->categories;
    }
}
