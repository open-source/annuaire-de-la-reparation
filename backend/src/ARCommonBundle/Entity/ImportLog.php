<?php

namespace ARCommonBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ImportLog
 *
 * @ORM\Table(name="t_import_log")
 * @ORM\Entity(repositoryClass="ARCommonBundle\Entity\ImportLogRepository")
 */
class ImportLog
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime", nullable=false)
     */
    private $date;

    /**
     * @var string
     *
     * @ORM\Column(name="filename", type="string", length=255, nullable=false)
     */
    private $filename;

    /**
     * @var integer
     *
     * @ORM\Column(name="line", type="integer", nullable=false)
     */
    private $line;

    /**
     * @var string
     *
     * @ORM\Column(name="message", type="text", length=65535, nullable=false)
     */
    private $message;

    /**
     * @var string
     *
     * @ORM\Column(name="region", type="string", length=255,)
     */
    private $region;

    /**
     * @var integer
     *
     * @ORM\Column(name="siret", type="integer")
     */
    private $siret;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     * @return ImportLog
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime 
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set filename
     *
     * @param string $filename
     * @return ImportLog
     */
    public function setFilename($filename)
    {
        $this->filename = $filename;

        return $this;
    }

    /**
     * Get filename
     *
     * @return string 
     */
    public function getFilename()
    {
        return $this->filename;
    }

    /**
     * Set line
     *
     * @param integer $line
     * @return ImportLog
     */
    public function setLine($line)
    {
        $this->line = $line;

        return $this;
    }

    /**
     * Get line
     *
     * @return integer 
     */
    public function getLine()
    {
        return $this->line;
    }

    /**
     * Set message
     *
     * @param string $message
     * @return ImportLog
     */
    public function setMessage($message)
    {
        $this->message = $message;

        return $this;
    }

    /**
     * Get message
     *
     * @return string 
     */
    public function getMessage()
    {
        return $this->message;
    }
    
    /**
     * Set region
     *
     * @param string $region
     * @return ImportLog
     */
    public function setRegion($region)
    {
      $this->region = $region;
      
      return $this;
    }
    
    /**
     * Get region
     *
     * @return string
     */
    public function getRegion()
    {
      return $this->region;
    }
    
    /**
     * Set siret
     *
     * @param integer $siret
     * @return ImportLog
     */
    public function setSiret($siret)
    {
      $this->siret = $siret;
      
      return $this;
    }
    
    /**
     * Get siret
     *
     * @return integer
     */
    public function getSiret()
    {
      return $this->siret;
    }
}
