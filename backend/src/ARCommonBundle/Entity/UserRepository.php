<?php

namespace ARCommonBundle\Entity;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;

/**
 * La classe UserRepository définit la gestion des entités {@link User}.
 *
 * @author vdouillet
 *        
 */
class UserRepository extends EntityRepository {

    /**
     * Récupère les utilisateurs satisfaisant le filtrage demandé.
     *
     * @param integer $offset
     * @param integer $limit
     * @param string $sortColumn
     * @param string $sortDir
     * @param string $lastname
     * @param string $firstname
     * @param string $email
     * @param string $phone
     * @param boolean $isMailContact
     * @param string $departmentCode
     * @return array
     */
    public function getWithFilters($offset, $limit, $sortColumn, $sortDir, $lastname, $firstname, $email, $phone, $isMailContact, $departmentCode) {
        $qb = $this->createQueryBuilder('u');
        $qb->select($qb->expr()->count('u'))
        ->where('1 = 1')
        ->orderBy('u.' . $sortColumn, $sortDir);

        if($lastname) {
            $this->addLikeWhere($qb, 'u.lastname', $lastname);
        }
        if($firstname) {
            $this->addLikeWhere($qb, 'u.firstname', $firstname);
        }
        if($email) {
            $this->addLikeWhere($qb, 'u.email', $email);
        }
        if($phone) {
            $this->addLikeWhere($qb, 'u.phone', $phone);
        }
        if(!empty($isMailContact)) {
            $this->addEqWhere($qb, 'u.isMailContact', $isMailContact === 'true');
        }
        if($departmentCode) {
            $qb->join('u.codeDepartment', 'dpt', Expr\Join::WITH, $qb->expr()->eq('dpt.code', ':dptCode'));
            $qb->setParameter('dptCode', $departmentCode);
        }

        $count = $qb->getQuery()->getSingleScalarResult();
        $qb->select('u')->setFirstResult($offset)->setMaxResults($limit);
        $users = $qb->getQuery()->getResult();

        return array(
            'users' => $users,
            'total' => $count
        );
    }

    /**
     * Ajout d'une clause d'égalité WHERE au constructeur de requête.
     *
     * @param \Doctrine\ORM\QueryBuilder $qb
     * @param string $filter
     * @param mixed $value
     */
    private function addEqWhere($qb, $field, $value)
    {
        $fieldPlaceHolder = ':' . str_replace('.', '', $field);
        $qb->andWhere($qb->expr()->eq($field, $fieldPlaceHolder));
        $qb->setParameter($fieldPlaceHolder, $value);
    }
    
    /**
     * Ajout d'une clause WHERE LIKE au constructeur de requête.
     *
     * @param \Doctrine\ORM\QueryBuilder $qb
     * @param string $filter
     * @param mixed $value
     */
    private function addLikeWhere($qb, $field, $value)
    {
        $fieldPlaceHolder = ':' . str_replace('.', '', $field);
        $qb->andWhere($qb->expr()->like($field, $fieldPlaceHolder));
        $qb->setParameter($fieldPlaceHolder, '%' . $value . '%');
    }

    /**
     * Trouver un utilisateur en attente d'activation avec le token fourni.
     *
     * @param string $token
     * @param \DateTime $limitDate date courante moins délai de validité du token
     */
    public function findToActivate($token, \DateTime $limitDate) {
        $qb = $this->createQueryBuilder('u');
        $qb->select('u');
        $qb->where($qb->expr()->eq('u.enabled', 0));
        $qb->andWhere($qb->expr()->eq('u.confirmationToken', ':token'));
        // Délai d'expiration du token
        $qb->andWhere($qb->expr()->gt('u.passwordRequestedAt', ':limitDate'));
        $qb->setParameter('token', $token);
        $qb->setParameter('limitDate', $limitDate);

        return $qb->getQuery()->getOneOrNullResult();
    }

    /**
     * Retourne les adresses mail des utilisateurs destinataires d'un rapport d'import.
     * 
     * @param Department[] $departments
     * @return array
     */
    public function findMailForImportReport($departments) {
        $qb = $this->createQueryBuilder('u');
        $qb->select('u.email')
        ->join('u.codeDepartment', 'dpt', Expr\Join::WITH, $qb->expr()->in('dpt', ':departments'))
        ->where($qb->expr()->eq('u.isMailContact', 1))
        ->andWhere($qb->expr()->eq('u.enabled', 1))
        ->setParameter('departments', $departments);

        return $qb->getQuery()->getResult();
    }

    /**
     * Retourne les adresses mail des utilisateurs destinataires d'un signalement d'erreur sur une fiche artisan.
     * 
     * @param Department $cma
     * @return array
     */
    public function findMailForErrorReport(Department $cma) {
        $qb = $this->createQueryBuilder('u');
        $qb->select('u.email')
        ->distinct()
        ->join('u.codeDepartment', 'dpt', Expr\Join::WITH, $qb->expr()->eq('dpt', ':dpt'))
        ->where($qb->expr()->eq('u.isMailContact', true))
        ->setParameter(':dpt', $cma);

        return $qb->getQuery()->getResult();
    }
}