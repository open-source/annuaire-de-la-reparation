<?php

namespace ARCommonBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * Départment/CMA
 *
 * @ORM\Table(name="r_department")
 * @ORM\Entity(repositoryClass="ARCommonBundle\Entity\DepartmentRepository")
 */
class Department
{
    /**
     * Numéro du département
     *
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=3, nullable=false)
     * @ORM\Id
     * @JMS\Groups({"default"})
     */
    private $code;

    /**
     * Nom du département
     *
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     * @JMS\Groups({"default"})
     */
    private $name;

    /**
     * Utilisateurs associés à cette CMA
     *
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="ARCommonBundle\Entity\User", mappedBy="codeDepartment")
     * @JMS\Exclude
     */
    private $idUser;

    /**
     * CFE associé à cette CMA
     *
     * @var Cfe
     * @ORM\ManyToOne(targetEntity="ARCommonBundle\Entity\Cfe", inversedBy="departments")
     * @ORM\JoinColumn(name="cfe", nullable=false)
     */
    private $cfe;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->idUser = new \Doctrine\Common\Collections\ArrayCollection();
    }


    /**
     * Set code
     *
     * @param integer $code
     * @return Department
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return integer 
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Department
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Add idUser
     *
     * @param \ARCommonBundle\Entity\User $idUser
     * @return Department
     */
    public function addIdUser(\ARCommonBundle\Entity\User $idUser)
    {
        $this->idUser[] = $idUser;

        return $this;
    }

    /**
     * Remove idUser
     *
     * @param \ARCommonBundle\Entity\User $idUser
     */
    public function removeIdUser(\ARCommonBundle\Entity\User $idUser)
    {
        $this->idUser->removeElement($idUser);
    }

    /**
     * Get idUser
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getIdUser()
    {
        return $this->idUser;
    }

    /**
     * @return Cfe
     */
    public function getCfe()
    {
        return $this->cfe;
    }

    /**
     * @param Cfe $cfe
     */
    public function setCfe($cfe)
    {
        $this->cfe = $cfe;
    }
}
