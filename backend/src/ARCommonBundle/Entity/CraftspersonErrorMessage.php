<?php

namespace ARCommonBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;


/**
 * Class CraftspersonErrorMessage
 * @ORM\Table(name="r_craftperson_err_msg")
 * @ORM\Entity(repositoryClass="ARCommonBundle\Entity\CraftspersonErrorMessageRepository")
 */
class CraftspersonErrorMessage {
    /**
     * Identifiant unique
     *
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue
     */
    private $id;


    /**
     * Message d'erreur
     *
     * @var string
     *
     * @ORM\Column(name="message", type="string", length=100, nullable=true)
     */
    private $message;



    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param string $message
     */
    public function setMessage($message)
    {
        $this->message = $message;
    }
}