<?php

namespace ARCommonBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * CraftspersonLike
 *
 * @ORM\Table(name="t_craftsperson_like")
 * @ORM\Entity
 */
class CraftspersonLike
{
    /**
     * Identifiant unique
     *
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue
     */
    private $id;

    /**
     * Réparateur
     *
     * @var \ARCommonBundle\Entity\Craftsperson
     *
     * @ORM\ManyToOne(targetEntity="ARCommonBundle\Entity\Craftsperson")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_craftsperson", referencedColumnName="id")
     * })
     */
    private $idCraftsperson;

    /**
     * Email de l'utilisateur à l'origine du like
     * 
     * @var string
     * 
     * @ORM\Column(name="email", type="string", length=100, nullable=true)
     */
    private $email;

    /**
     * Token de confirmation du like
     * 
     * @var string
     * 
     * @ORM\Column(name="confirmation_token", type="string", length=255, nullable=true)
     */
    private $confirmationToken;

    /**
     * Statut de confirmation
     *
     * @var boolean
     *
     * @ORM\Column(name="is_confirmed", type="boolean", nullable=false)
     */
    private $isConfirmed = false;

    /**
     * Set id
     *
     * @param integer id
     * @return CraftspersonLike
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idCraftsperson
     *
     * @param \ARCommonBundle\Entity\Craftsperson $craftsperson
     * @return CraftspersonLike
     */
    public function setIdCraftsperson($craftsperson)
    {
        $this->idCraftsperson = $craftsperson;

        return $this;
    }

    /**
     * Get idCraftsperson
     *
     * @return \ARCommonBundle\Entity\Craftsperson
     */
    public function getIdCraftsperson()
    {
        return $this->idCraftsperson;
    }

    /**
     * Set email
     *
     * @param string email
     * @return CraftspersonLike
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set confirmationToken
     *
     * @param string confirmationToken
     * @return CraftspersonLike
     */
    public function setConfirmationToken($confirmationToken)
    {
        $this->confirmationToken = $confirmationToken;

        return $this;
    }

    /**
     * Get confirmationToken
     *
     * @return string 
     */
    public function getConfirmationToken()
    {
        return $this->confirmationToken;
    }

    /**
     * Set isConfirmed
     *
     * @param boolean $isConfirmed
     * @return CraftspersonLike
     */
    public function setIsConfirmed($isConfirmed)
    {
        $this->isConfirmed = $isConfirmed;

        return $this;
    }

    /**
     * Get isConfirmed
     *
     * @return boolean 
     */
    public function getIsConfirmed()
    {
        return $this->isConfirmed;
    }
}
