<?php

namespace ARCommonBundle\Entity;

use ARCommonBundle\Enum\GeocodingStatus;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;
use Doctrine\ORM\Query;

/**
 * La classe CraftspersonRepository définit la gestion des entités {@link Craftsperson}.
 *
 * @author vdouillet
 *        
 */
class CraftspersonRepository extends EntityRepository {

  /**
   * Création d'un réparateur.
   *
   * @param integer $siret
   * @param string $name
   * @param NafCode[] $nafCodes
   * @param string $address1
   * @param string $address2
   * @param string $zipCode
   * @param string $zipCodeLabel
   * @param string $email
   * @param string $phone
   * @param string $website
   * @param Department $cma
   * @param boolean $isReparactor
   * @return \ARCommonBundle\Entity\Craftsperson
   */
  public function create($siret, $name, $nafCodes, $address1, $address2, $zipCode, $zipCodeLabel, $email, $phone, $website, Department $cma = null, $isReparactor) {
    $craftsperson = new Craftsperson();
    $this->setFields($craftsperson, $siret, $name, $nafCodes, $address1, $address2, $zipCode, $zipCodeLabel, $email, 
        $phone, $website, $cma, $isReparactor);
    $craftsperson->setCreationDate(new \DateTime('NOW'));
    return $craftsperson;
  }

  /**
   * Affecter les valeurs de propriétés.
   *
   * @param Craftsperson craftsperson
   * @param integer $siret
   * @param string $name
   * @param mixed $nafCode
   * @param string $address1
   * @param string $address2
   * @param string $zipCode
   * @param string $zipCodeLabel
   * @param string $email
   * @param string $phone
   * @param string $website
   * @param Department $cma
   * @param boolean $isReparactor
   * @return \ARCommonBundle\Entity\Craftsperson
   */
  private function setFields(Craftsperson $craftsperson, $siret, $name, $nafCode, $address1, $address2, $zipCode, $zipCodeLabel, $email, $phone, $website, Department $cma = null, $isReparactor) {
    $craftsperson->setSiret($siret);
    $craftsperson->setName($name);
    $craftsperson->setAddress1($address1);
    $craftsperson->setAddress2($address2);
    $craftsperson->setZipCode($zipCode);
    $craftsperson->setZipCodeLabel($zipCodeLabel);
    $craftsperson->setEmail($email);
    $craftsperson->setPhone($phone);
    $craftsperson->setWebsite($website);
    $craftsperson->setCmaCode($cma);
    $craftsperson->setIsReparactor($isReparactor);
    $craftsperson->setUpdateDate(new \DateTime('NOW'));

    $craftsperson->getNafCode()->clear();
    foreach ($nafCode as $code) {
      if($code) {
        $craftsperson->addNafCode($code);
      }
    }
    
    return $craftsperson;
  }

  /**
   * Mise à jour d'un réparateur existant.
   *
   * @param Craftsperson craftsperson
   * @param integer $siret
   * @param string $name
   * @param NafCode $nafCode
   * @param string $address1
   * @param string $address2
   * @param string $zipCode
   * @param string $zipCodeLabel
   * @param string $email
   * @param string $phone
   * @param string $website
   * @param Department $cma
   * @param boolean $isReparactor
   * @return \ARCommonBundle\Entity\Craftsperson
   */
  public function update(Craftsperson $toUpdate, Craftsperson $ref) {
    return $this->setFields($toUpdate, $ref->getSiret(), $ref->getName(), $ref->getNafCode(), $ref->getAddress1(), 
        $ref->getAddress2(), $ref->getZipCode(), $ref->getZipCodeLabel(), $ref->getEmail(), $ref->getPhone(), 
        $ref->getWebsite(), $ref->getCmaCode(), $ref->getIsReparactor());
  }


    /**
     * Mise à jour d'un réparateur existant, en ne modifiant que les champs nuls.
     *
     * @param Craftsperson craftsperson
     * @param integer $siret
     * @param string $name
     * @param NafCode $nafCode
     * @param string $address1
     * @param string $address2
     * @param string $zipCode
     * @param string $zipCodeLabel
     * @param string $email
     * @param string $phone
     * @param string $website
     * @param Department $cma
     * @param boolean $isReparactor
     * @return \ARCommonBundle\Entity\Craftsperson
     */
    public function complete(Craftsperson $toUpdate, Craftsperson $ref) {
        $nafCodes = $toUpdate->getNafCode();

        return $this->setFields($toUpdate,
            $toUpdate->getSiret(),
            $toUpdate->getName()?$toUpdate->getName():$ref->getName(),
            $nafCodes && !$nafCodes->isEmpty()?$nafCodes->toArray():$ref->getNafCode(),
            $toUpdate->getAddress1()?$toUpdate->getAddress1():$ref->getAddress1(),
            $toUpdate->getAddress1()?$toUpdate->getAddress2():$ref->getAddress2(),
            $toUpdate->getAddress1()?$toUpdate->getZipCode():$ref->getZipCode(),
            $toUpdate->getAddress1()?$toUpdate->getZipCodeLabel():$ref->getZipCodeLabel(),
            $toUpdate->getEmail()?$toUpdate->getEmail():$ref->getEmail(),
            $toUpdate->getPhone()?$toUpdate->getPhone():$ref->getPhone(),
            $toUpdate->getWebsite()?$toUpdate->getWebsite():$ref->getWebsite(),
            $toUpdate->getCmaCode(),
            $toUpdate->getIsReparactor());
    }

  /**
   * Trouver les {@link Craftsperson} autour d'une position.
   *
   * @param double $latitude
   * @param double $longitude
   * @param double $radius
   * @param Category $category
   * @return \ARCommonBundle\Entity\Craftsperson[]
   */
  public function search($latitude, $longitude, $radius, $category = null) {
    // Delta en lat/lon du rayon de recherche autour du point de recherche
    $deltaLat = $radius / 111.3;
    $deltaLon = $radius / (111.3 * \cos(\deg2rad($latitude)));

    $qb = $this->createQueryBuilder('c');
    $qb->select('c', 'POWER(111.3 * (c.latitude - :lat), 2) + POWER(111.3 * (:lon - c.longitude) * COS(c.latitude / 57.3), 2) AS distance')
    ->where($qb->expr()->in('c.geocodingStatus', array(
      GeocodingStatus::OK,
      GeocodingStatus::IMPRECISE
    )))
    ->andWhere($qb->expr()->eq('c.isEnabled', true))
    ->andWhere($qb->expr()->gte('c.latitude', ':minLat'))
    ->andWhere($qb->expr()->lte('c.latitude', ':maxLat'))
    ->andWhere($qb->expr()->gte('c.longitude', ':minLon'))
    ->andWhere($qb->expr()->lte('c.longitude', ':maxLon'))
    ->having($qb->expr()->lt('distance', ':radius'))
    ->orderBy('c.isReparactor', 'DESC')
    ->addOrderBy('distance', 'ASC')
    ->distinct()
    ->setParameter('radius', $radius * $radius)
    ->setParameter('lat', $latitude)
    ->setParameter('lon', $longitude)
    ->setParameter('minLat', $latitude - $deltaLat)
    ->setParameter('maxLat', $latitude + $deltaLat)
    ->setParameter('minLon', $longitude - $deltaLon)
    ->setParameter('maxLon', $longitude + $deltaLon);

    // Recherche par catégorie
    if($category instanceof Category) {
      $qb->join('c.nafCode', 'n')
      ->join('n.categories', 'cat', Expr\Join::WITH, $qb->expr()->eq('cat', ':category'))
      ->setParameter('category', $category);
    }

    return $qb->getQuery()->getResult();
  }

  /**
   * Trouver la distance du réparateur le plus proche d'une position.
   *
   * @param double $latitude
   * @param double $longitude
   * @param Category $category
   * @return Double
   */
  public function searchClosest($latitude, $longitude, $category = null) {

    $qb = $this->createQueryBuilder('c');
    $qb->select('c', 'POWER(111.3 * (c.latitude - :lat), 2) + POWER(111.3 * (:lon - c.longitude) * COS(c.latitude / 57.3), 2) AS distance')
    ->where($qb->expr()->in('c.geocodingStatus', array(
      GeocodingStatus::OK,
      GeocodingStatus::IMPRECISE
    )))
    ->andWhere($qb->expr()->eq('c.isEnabled', true))
    ->orderBy('distance', 'ASC')
    ->distinct()
    ->setParameter('lat', $latitude)
    ->setParameter('lon', $longitude);

    // Recherche par catégorie
    if($category instanceof Category) {
      $qb->join('c.nafCode', 'n')
      ->join('n.categories', 'cat', Expr\Join::WITH, $qb->expr()->eq('cat', ':category'))
      ->setParameter('category', $category);
    }

    //Limitation des résultats à 1 pour avoir l'artisan le plus proche
    $qb->setMaxResults(1);
    return $qb->getQuery()->getResult();
  }

  /**
   * Récupère les réparateurs avec filtrage
   *
   * @param integer $offset
   * @param integer $limit
   * @param string $sortColumn
   * @param string $sortDir
   * @param Department[] $cmaFilter
   * @param string $siret
   * @param string $name
   * @param string $zipCode
   * @param string $zipCode
   * @param string $address
   * @param string $phone
   * @param integer $cmaCode
   * @param string $isError
   * @param string $isReparactor
   * @param string $isEnabled
   * @param string $isUpdated
   * @param string $updateDate
   * @return array
   */
  public function getWithFilters($offset, $limit, $sortColumn, $sortDir, $cmaFilter, $siret, $name, $zipCode, $address, $phone, $cmaCode, $nafCode, $isError, $isReparactor, $isEnabled, $isUpdated, $updateDate)
  {
    $qb = $this->createQueryBuilder('c');
    $qb->select($qb->expr()->count('c'))
      ->where('1 = 1')
      ->orderBy('c.' . $sortColumn, $sortDir);

    if($cmaFilter) {
      $qb->andWhere($qb->expr()->in('c.cmaCode', ':cmaFilter'));
      $qb->setParameter('cmaFilter', $cmaFilter);
    }
    if($cmaCode) {
      $qb->andWhere($qb->expr()->eq('c.cmaCode', ':cmaCode'));
      $qb->setParameter('cmaCode', $cmaCode);
    }
    if($nafCode) {
      $qb->join('c.nafCode', 'naf', Expr\Join::WITH, $qb->expr()->eq('naf.code', ':nafCode'));
      $qb->setParameter('nafCode', $nafCode);
    }
    if($siret) {
      $this->addLikeWhere($qb, 'c.siret', $siret);
    }
    if($name) {
      $this->addLikeWhere($qb, 'c.name', $name);
    }
    if($zipCode) {
      $this->addLikeWhere($qb, 'c.zipCode', $zipCode);
    }
    if($address) {
      $this->addLikeWhere($qb, 'c.address1', $address);
    }
    if($phone) {
      $this->addLikeWhere($qb, 'c.phone', $phone);
    }
    if(!empty($isError)) {
      $this->addEqWhere($qb, 'c.isError', $isError === 'true');
    }
    if(!empty($isReparactor)) {
      $this->addEqWhere($qb, 'c.isReparactor', $isReparactor === 'true');
    }
    if(!empty($isEnabled)) {
      $this->addEqWhere($qb, 'c.isEnabled', $isEnabled === 'true');
    }
    if(!empty($isUpdated)) {
      $this->addEqWhere($qb, 'c.isUpdated', $isUpdated === 'true');
    }
    if($updateDate) {
      $startDate = \DateTime::createFromFormat('d-m-Y H:i:s', $updateDate . ' 00:00:00');
      if($startDate && $startDate->format('d-m-Y') === $updateDate) {
        $endDate = \DateTime::createFromFormat('d-m-Y H:i:s', $updateDate . ' 00:00:00');
        $endDate->add(new \DateInterval('P1D'));
        $qb->andWhere($qb->expr()->gte('c.updateDate', ':startDate'))->andWhere($qb->expr()->lt('c.updateDate', ':endDate'));
        $qb->setParameter('startDate', $startDate)->setParameter('endDate', $endDate);
      }
    }

    $count = $qb->getQuery()->getSingleScalarResult();

    $qb->select('c')->setFirstResult($offset)->setMaxResults($limit);
    $craftspersons = $qb->getQuery()->getResult();

    return array(
      'craftspersons' => $craftspersons,
      'total' => $count
    );
  }

  /**
   * Ajout d'une clause d'égalité WHERE au constructeur de requête.
   *
   * @param \Doctrine\ORM\QueryBuilder $qb
   * @param string $filter
   * @param mixed $value
   */
  private function addEqWhere($qb, $field, $value)
  {
    $fieldPlaceHolder = ':' . str_replace('.', '', $field);
    $qb->andWhere($qb->expr()->eq($field, $fieldPlaceHolder));
    $qb->setParameter($fieldPlaceHolder, $value);
  }
  
  /**
   * Ajout d'une clause WHERE LIKE au constructeur de requête.
   *
   * @param \Doctrine\ORM\QueryBuilder $qb
   * @param string $filter
   * @param mixed $value
   */
  private function addLikeWhere($qb, $field, $value)
  {
    $fieldPlaceHolder = ':' . str_replace('.', '', $field);
    $qb->andWhere($qb->expr()->like($field, $fieldPlaceHolder));
    $qb->setParameter($fieldPlaceHolder, '%' . $value . '%');
  }

  /**
   * Requête permettant l'accès aux réparateurs à supprimer.
   *
   * @param string[] $siretToKeep numéros de siret à ne pas supprimer
   * @param Department[] $departmentsFilter codes de départements concernés par le nettoyage
   * @return \Doctrine\ORM\QueryBuilder
   */
  private function getCleanupBaseQb($siretToKeep, $departmentsFilter) {
    $qb = $this->createQueryBuilder('c');
    $qb->where($qb->expr()->eq('c.isUpdated', 0))
    ->andWhere($qb->expr()->in('c.cmaCode', ':departments'))
    ->andWhere($qb->expr()->notIn('c.siret', ':sirets'))
    ->setParameter('departments', $departmentsFilter)
    ->setParameter('sirets', $siretToKeep);

    return $qb;
  }

  /**
   * Pour les départements fournis, suppression des artisans dont le siret n'est pas fourni et dont les informations
   * n'ont pas été modifiées manuellement.
   *
   * @param string[] $siretToKeep
   * @param Department[] $departmentsFilter
   * @return nombres d'artisans supprimés
   */
  public function cleanup($siretToKeep, $departmentsFilter) {
    $qb = $this->getCleanupBaseQb($siretToKeep, $departmentsFilter);
    $qb->delete();

    return $qb->getQuery()->execute();
  }

  /**
   * Pour les départements fournis, comptage du nombre d'artisans dont le siret n'est pas fourni et dont les
   * informations n'ont pas été modifiées manuellement.
   *
   * @param string[] $siretToKeep
   * @param Department[] $departmentsFilter
   * @return nombres d'artisans supprimés
   */
  public function countNumberToCleanup($siretToKeep, $departmentsFilter) {
    $qb = $this->getCleanupBaseQb($siretToKeep, $departmentsFilter);
    $qb->select('COUNT(c)');

    return $qb->getQuery()->getSingleScalarResult();
  }

  /**
   * Compte le nombre de réparateurs.
   * 
   * @return int
   */
  public function countAll() {
    $qb = $this->createQueryBuilder('c');
    $qb->select('COUNT(c)');

    return $qb->getQuery()->getSingleScalarResult();
  }

  /**
   * Trouver un réparateur par identifiant du logo.
   * 
   * @return Craftsperson
   */
  public function findByLogoId($logoId)
  {
    $qb = $this->createQueryBuilder('c');
    $qb->select('c')
    ->where($qb->expr()->like('c.logoFile', ':id'))
    ->setParameter(':id', '%' . $logoId . '%');

    return $qb->getQuery()->getSingleResult();
  }

  /**
   * Générer l'export CSV.
   *
   * @param resource $csvFile
   * @param string[] $departmentCodes
   */
  public function generateCsvExport($csvFile, $departmentCodes)
  {
    $qb = $this->createQueryBuilder('c');
    $qb->select('c')
    ->where($qb->expr()->in('c.cmaCode', ':dptCodes'))
    ->setParameter('dptCodes', $departmentCodes);

    $iterator = $qb->getQuery()->iterate();

    fputcsv($csvFile, ["ID","SIRET","NOM_COMMERCIAL","CODE_NAFA","ADRESSE_LIGNE_1","ADRESSE_LIGNE_2","CODE_POSTAL","LIBELLE_POSTAL","EMAIL","TELEPHONE","SITE_INTERNET","CMA_RATTACHEMENT","DATE_FERMETURE","LABELLISE_REPARACTEUR","FICHE_MODIFIEE","ERREUR_SIGNALEE","EST_MASQUE"], ";");
    foreach($iterator as $row) {
      $craftsperson = $row[0];
      $this->writeCsvLine($csvFile, $craftsperson);
      //fwrite($csvFile, $this->getCsvLine($craftsperson));
      $this->_em->detach($row[0]);
    }

    return $csvFile;
  }

  /**
   * Créer une ligne du fichier CSV.
   *
   * @param Craftsperson $craftsperson
   * @return string
   */
  private function writeCsvLine($csvFile, Craftsperson $craftsperson) {
    $s = ";";
    $nafCodes = '';
    $firstNafCode = true;
    foreach ($craftsperson->getNafCode() as $naf) {
      if(!$firstNafCode) {
        $nafCodes = $nafCodes . '/';
      }
      else {
        $firstNafCode = false;
      }
      $nafCodes = $nafCodes . $naf->getCode();
    }
    //return sprintf("%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;;%s;%s;%s;%s\n", $craftsperson->getId(), $craftsperson->getSiret(), $craftsperson->getName(), $nafCodes, $craftsperson->getAddress1(), $craftsperson->getAddress2(), $craftsperson->getZipCode(), $craftsperson->getZipCodeLabel(), $craftsperson->getEmail(), $craftsperson->getPhone(), $craftsperson->getWebsite(), $craftsperson->getCmaCode()->getCode(), $this->getBooleanStr($craftsperson->getIsReparactor()), $this->getBooleanStr($craftsperson->getIsUpdated()), $this->getBooleanStr($craftsperson->getIsError()), $this->getBooleanStr(!$craftsperson->getIsEnabled()));
    $lineArray = [$craftsperson->getId(),
        $craftsperson->getSiret(),
        $craftsperson->getName(),
        $nafCodes,
        $craftsperson->getAddress1(),
        $craftsperson->getAddress2(),
        $craftsperson->getZipCode(),
        $craftsperson->getZipCodeLabel(),
        $craftsperson->getEmail(),
        $craftsperson->getPhone(),
        $craftsperson->getWebsite(),
        $craftsperson->getCmaCode()->getCode(),
        "",
        $this->getBooleanStr($craftsperson->getIsReparactor()),
        $this->getBooleanStr($craftsperson->getIsUpdated()),
        $this->getBooleanStr($craftsperson->getIsError()),
        $this->getBooleanStr(!$craftsperson->getIsEnabled())];
    return fputcsv ( $csvFile, $lineArray, ";") ;
  }

  /**
   * Chaine à partir d'un booléen
   *
   * @param boolean $bool
   * @return string
   */
  private function getBooleanStr($bool) {
    return $bool ? '1' : '0';
  }

  /**
   * Get code nafs by craftpersonIds
   * @param mixed $craftpersonIds : tableau des identifiants réparateurs pour lesquels on souhaite avoir le code naf
   * @return array le tableau des codes nafs par identifiant de réparateur
   */
  public function getCodeNafsByCraftpersonId($craftpersonIds)
  {
    $qb = $this->createQueryBuilder('c');
    $qb->select('n.code as nafCode, c.id as craftpersonId')
    ->join('c.nafCode', 'n')
    ->where($qb->expr()->in('c.id', ':ids'))
    ->setParameter(':ids', $craftpersonIds);

    $results = $qb->getQuery()->getResult();

    $nafCodesById = array();
    foreach($results as $res) {
      if(!isset($nafCodesById[$res["craftpersonId"]])) {
        $nafCodesById[$res["craftpersonId"]] = array();
      }
      $nafCodesById[$res["craftpersonId"]][] = $res["nafCode"];
    }
    return $nafCodesById;
  }


  /**
   * Récupère le tableau de catégories par code naf
   * @return array le tableau des catégories par code naf
   */
  public function getCategoriesByCodeNaf()
  {
    $qb = $this->getEntityManager()->createQueryBuilder();
    $qb->select('n.code as nafCode, c.id as categoryId, c.name as categoryName')
    ->from('ARCommonBundle:NafCode', ' n')
    ->join('n.categories', 'c');

    $results = $qb->getQuery()->getResult();

    $categoriesByNafCode = array();
    foreach($results as $res) {
      if(!isset($categoriesByNafCode[$res["nafCode"]])) {
        $categoriesByNafCode[$res["nafCode"]] = array();
      }
      $categoriesByNafCode[$res["nafCode"]][] = array("id" => $res["categoryId"], "name" => $res["categoryName"]);
    }
    return $categoriesByNafCode;
  }
}