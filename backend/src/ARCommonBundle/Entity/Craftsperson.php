<?php

namespace ARCommonBundle\Entity;

use ARCommonBundle\Enum\GeocodingStatus;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Réparateur
 *
 * @ORM\Table(name="t_craftsperson", indexes={@ORM\Index(name="r_naf_code_t_craftsperson_fk", columns={"naf_code"}), @ORM\Index(name="r_department_t_craftsperson_fk", columns={"cma_code"})})
 * @ORM\Entity(repositoryClass="ARCommonBundle\Entity\CraftspersonRepository")
 * @ORM\HasLifecycleCallbacks
 * @UniqueEntity(
 *  fields="siret",
 *  message="unique"
 * )
 */
class Craftsperson
{
	/**
     * Identifiant unique
     *
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="IDENTITY")
     * @JMS\Groups({"Default", "public", "list"})
	 */
	private $id;
	
    /**
     * Numéro de siret
     *
     * @var integer
     *
     * @ORM\Column(name="siret", type="string", length=14, nullable=false)
     * @Assert\NotBlank(message="Champs requis", groups={"Default", "import"})
     * @Assert\Length(
     *    min="14",
     *    max="14",
     *    minMessage="Nombre de caractères minimum : {{ limit }}",
     *    maxMessage="Nombre de caractères maximum : {{ limit }}",
     *    exactMessage="Nombre de caractères requis : {{ limit }}",
     *    groups={"Default", "import"}
     * )
     */
    private $siret;

    /**
     * Raison sociale ou nom commercial
     *
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     * @Assert\NotBlank(message="Champs requis", groups={"Default", "import"})
     * @JMS\Groups({"Default", "public", "list"})
     */
    private $name;

    /**
     * Première ligne d'adresse
     *
     * @var string
     *
     * @ORM\Column(name="address_1", type="string", length=255, nullable=false)
     * @Assert\NotBlank(message="Champs requis", groups={"Default", "import"})
     * @JMS\Groups({"Default", "public", "list"})
     */
    private $address1;

    /**
     * Seconde ligne d'adresse
     *
     * @var string
     *
     * @ORM\Column(name="address_2", type="string", length=255, nullable=true)
     * @JMS\Groups({"Default", "public", "list"})
     */
    private $address2;

    /**
     * Code postal
     *
     * @var string
     *
     * @ORM\Column(name="zip_code", type="string", length=100, nullable=false)
     * @Assert\NotBlank(message="Champs requis", groups={"Default", "import"})
     * @Assert\Length(
     *    min="5",
     *    max="5",
     *    minMessage="Nombre de caractères minimum : {{ limit }}",
     *    maxMessage="Nombre de caractères maximum : {{ limit }}",
     *    exactMessage="Nombre de caractères requis : {{ limit }}",
     *    groups={"Default", "import"}
     * )
     * @JMS\Groups({"Default", "public", "list"})
     */
    private $zipCode;

    /**
     * Commune
     *
     * @var string
     *
     * @ORM\Column(name="zip_code_label", type="string", length=255, nullable=false)
     * @Assert\NotBlank(message="Champs requis", groups={"Default", "import"})
     * @JMS\Groups({"Default", "public", "list"})
     */
    private $zipCodeLabel;

    /**
     * Numéro de téléphone
     *
     * @var string
     *
     * @ORM\Column(name="phone", type="string", length=50, nullable=false)
     * @Assert\Regex(pattern="/([0-9 ,])+/", message="pattern", groups={"Default"})
     * @JMS\Groups({"Default", "public", "list"})
     */
    private $phone;

    /**
     * Email
     *
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=100, nullable=true)
     * @Assert\Email(message="pattern", groups={"Default", "import"})
     * @JMS\Groups({"Default", "public", "list"})
     */
    private $email;

    /**
     * Site internet, avec ou sans le protocole (http://)
     *
     * @var string
     *
     * @ORM\Column(name="website", type="string", length=255, nullable=true)
     * @JMS\Groups({"Default", "public", "list"})
     */
    private $website;

    /**
     * Statut d'activation
     *
     * @var boolean
     *
     * @ORM\Column(name="is_enabled", type="boolean", nullable=false)
     */
    private $isEnabled = true;

    /**
     * Statut réparacteur
     *
     * @var boolean
     *
     * @ORM\Column(name="is_reparactor", type="boolean", nullable=false)
     * @JMS\Groups({"Default", "public", "list"})
     */
    private $isReparactor = false;

    /**
     * Statut d'erreur rapportée sur les données de la fiche
     *
     * @var boolean
     *
     * @ORM\Column(name="is_error", type="boolean", nullable=false)
     */
    private $isError = false;

    /**
     * Statut du géocodage
     *
     * @var string
     *
     * @ORM\Column(name="geocoding_status", type="string", length=100, nullable=false)
     * @JMS\Exclude
     */
    private $geocodingStatus = GeocodingStatus::NC;

    /**
     * Description pour un réparacteur
     *
     * @var string
     *
     * @ORM\Column(name="reparactor_description", type="text", length=65535, nullable=true)
     * @JMS\Groups({"Default", "public"})
     */
    private $reparactorDescription;

    /**
     * Horaires d'ouverture pour un réparacteur
     *
     * @var string
     *
     * @ORM\Column(name="reparactor_hours", type="text", length=65535, nullable=true)
     * @JMS\Groups({"Default", "public"})
     */
    private $reparactorHours;

    /**
     * Description des services pour un réparacteur
     *
     * @var string
     *
     * @ORM\Column(name="reparactor_services", type="text", length=65535, nullable=true)
     * @JMS\Groups({"Default", "public"})
     */
    private $reparactorServices;

    /**
     * Certifications pour un réparacteur
     *
     * @var string
     *
     * @ORM\Column(name="reparactor_certificates", type="text", length=65535, nullable=true)
     * @JMS\Groups({"Default", "public"})
     */
    private $reparactorCertificates;

    /**
     * Date de création
     *
     * @var \DateTime
     *
     * @ORM\Column(name="creation_date", type="datetime", nullable=false)
     */
    private $creationDate;

    /**
     * Statut de mise à jour manuelle
     *
     * @var boolean
     *
     * @ORM\Column(name="is_updated", type="boolean", nullable=false)
     */
    private $isUpdated = false;

    /**
     * Date de dernière mise à jour
     *
     * @var \DateTime
     *
     * @ORM\Column(name="update_date", type="datetime", nullable=true)
     */
    private $updateDate;

    /**
     * Latitude de l'adresse
     * @var double
     *
     * @ORM\Column(name="latitude", type="float", nullable=true)
     * @JMS\Groups({"Default", "public", "list"})
     */
    private $latitude;

    /**
     * Longitude de l'adresse
     *
     * @var double
     *
     * @ORM\Column(name="longitude", type="float", nullable=true)
     * @JMS\Groups({"Default", "public", "list"})
     */
    private $longitude;
    
    /**
     * Nombre de likes.
     *
	 * @var integer
	 *
	 * @ORM\Column(name="likes", type="integer")
     * @JMS\Groups({"Default", "public", "list"})
	 */
	private $likes = 0;

    /**
     * Informations supplémentaires.
     *
	 * @var string
	 *
	 * @ORM\Column(name="other_info", type="text", length=65535, nullable=true)
     * @JMS\Groups({"Default", "public"})
	 */
	private $otherInfo;

    /**
     * Distance. Champ non mappé utilisé uniquement pour les résultats de recherche.
     *
	 * @var double
	 *
     * @JMS\Groups({"list"})
	 */
    private $distance = 0;

    /**
     * Nom du fichier de logo.
     * 
     * @var string
     * 
     * @ORM\Column(type="string", length=255, nullable=true)
     * @JMS\Exclude
     */
    private $logoFile;

    /**
     * Code CMA associé au réparateur
     *
     * @var \ARCommonBundle\Entity\Department
     *
     * @ORM\ManyToOne(targetEntity="ARCommonBundle\Entity\Department")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="cma_code", referencedColumnName="code")
     * })
     * @Assert\NotNull(message="required", groups={"Default", "import"})
     */
    private $cmaCode;

    /**
     * Code(s) NAFA associé(s) au réparateur.
     *
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="ARCommonBundle\Entity\NafCode")
     * @ORM\JoinTable(name="t_craftsperson_has_naf_code",
     *   joinColumns={
     *     @ORM\JoinColumn(name="id_craftsperson", referencedColumnName="id", onDelete="CASCADE")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="naf_code", referencedColumnName="code")
     *   }
     * )
     */
    private $nafCode;


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->nafCode = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Catégories du réparateur.
     * 
     * Champ de sérialisation virtuel.
     * 
     * @JMS\VirtualProperty
     * @JMS\Groups({"Default", "public", "list"})
     * 
     * @return \ARCommonBundle\Entity\Category[]
     */
    public function getCategories() {
        $categories = array();
        $addedCategoryIds = array();
        foreach($this->nafCode as $nafCode) {
            foreach($nafCode->getCategories() as $category) {
                if(!in_array($category->getId(), $addedCategoryIds)) {
                    $addedCategoryIds[] = $category->getId();
                    $categories[] = $category;
                }
            }
        }
        return $categories;
    }

    /**
     * Identifiant du fichier logo.
     * 
     * @JMS\VirtualProperty
     * @JMS\Groups({"Default", "public"})
     * 
     * @return string
     */
    public function getLogo() {
        if($this->logoFile) {
            $matches = array();
            if(preg_match('/logo_([\da-z]+)/', $this->logoFile, $matches) === 1) {
                return $matches[1];
            }
        }
        return null;
    }

    /**
     * Nom du département pour accès public.
     * 
     * @JMS\VirtualProperty
     * @JMS\Groups({"public"})
     * 
     * @return string
     */
    public function getDepartment() {
        return $this->cmaCode->getName();
    }

    /**
     * Calcul du statut de mise à jour manuelle sur modification du réparateur.
     * 
     * @param PreUpdateEventArgs $eventArgs
     * 
     * @ORM\PreUpdate 
     */
    public function computeManualEditStatus(PreUpdateEventArgs $eventArgs)
    {
        if($eventArgs->hasChangedField('isUpdated')) {
            // Champs à suivre pour la modification manuelle (champs modifiés par l'import périodique)
            $trackedFields = array('siret', 'name', 'nafCode', 'address1', 'address2', 'zipCode', 'email', 'phone', 'website', 'cmaCode', 'isReparactor');
            $modified = false;
            foreach($trackedFields as $fieldName) {
                // Si l'un des champs a changé, statut de mise à jour manuelle à vrai
                if($eventArgs->hasChangedField($fieldName)) {
                    // On vérifie que l'ancienne valeur est différente de la nouvelle 
                    // (dans le cas contraire une erreur peut se produire si les champs sont vides mais non nulls)
                    if($eventArgs->getOldValue($fieldName) != $eventArgs->getNewValue($fieldName)) {
                        $modified = true;
                    }
                }
            }
            $eventArgs->setNewValue('isUpdated', $modified);
        }
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
    	return $this->id;
    }
    
    /**
     * Set siret
     *
     * @param integer $siret
     * @return Craftsperson
     */
    public function setSiret($siret)
    {
    	$this->siret = $siret;
    	
    	return $this;
    }
    
    /**
     * Get siret
     *
     * @return integer 
     */
    public function getSiret()
    {
        return $this->siret;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Craftsperson
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set address1
     *
     * @param string $address1
     * @return Craftsperson
     */
    public function setAddress1($address1)
    {
        if($this->address1 != $address1) {
            $this->geocodingStatus = GeocodingStatus::NC;
        }
        $this->address1 = $address1;

        return $this;
    }

    /**
     * Get address1
     *
     * @return string 
     */
    public function getAddress1()
    {
        return $this->address1;
    }

    /**
     * Set address2
     *
     * @param string $address2
     * @return Craftsperson
     */
    public function setAddress2($address2)
    {
        if($this->address2 != $address2) {
            $this->geocodingStatus = GeocodingStatus::NC;
        }
        $this->address2 = $address2;

        return $this;
    }

    /**
     * Get address2
     *
     * @return string 
     */
    public function getAddress2()
    {
        return $this->address2;
    }

    /**
     * Set zipCode
     *
     * @param string $zipCode
     * @return Craftsperson
     */
    public function setZipCode($zipCode)
    {
        if($this->zipCode != $zipCode) {
            $this->geocodingStatus = GeocodingStatus::NC;
        }
        $this->zipCode = $zipCode;

        return $this;
    }

    /**
     * Get zipCode
     *
     * @return string 
     */
    public function getZipCode()
    {
        return $this->zipCode;
    }

    /**
     * Set zipCodeLabel
     *
     * @param string $zipCodeLabel
     * @return Craftsperson
     */
    public function setZipCodeLabel($zipCodeLabel)
    {
        if($this->zipCodeLabel != $zipCodeLabel) {
            $this->geocodingStatus = GeocodingStatus::NC;
        }
        $this->zipCodeLabel = $zipCodeLabel;

        return $this;
    }

    /**
     * Get zipCodeLabel
     *
     * @return string 
     */
    public function getZipCodeLabel()
    {
        return $this->zipCodeLabel;
    }

    /**
     * Set phone
     *
     * @param string $phone
     * @return Craftsperson
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string 
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return Craftsperson
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set website
     *
     * @param string $website
     * @return Craftsperson
     */
    public function setWebsite($website)
    {
        $this->website = $website;

        return $this;
    }

    /**
     * Get website
     *
     * @return string 
     */
    public function getWebsite()
    {
        return $this->website;
    }

    /**
     * Set isEnabled
     *
     * @param boolean $isEnabled
     * @return Craftsperson
     */
    public function setIsEnabled($isEnabled)
    {
        $this->isEnabled = $isEnabled;

        return $this;
    }

    /**
     * Get isEnabled
     *
     * @return boolean 
     */
    public function getIsEnabled()
    {
        return $this->isEnabled;
    }

    /**
     * Set isReparactor
     *
     * @param boolean $isReparactor
     * @return Craftsperson
     */
    public function setIsReparactor($isReparactor)
    {
        $this->isReparactor = $isReparactor;

        return $this;
    }

    /**
     * Get isReparactor
     *
     * @return boolean 
     */
    public function getIsReparactor()
    {
        return $this->isReparactor;
    }

    /**
     * Set isError
     *
     * @param boolean $isError
     * @return Craftsperson
     */
    public function setIsError($isError)
    {
        $this->isError = $isError;

        return $this;
    }

    /**
     * Get isError
     *
     * @return boolean 
     */
    public function getIsError()
    {
        return $this->isError;
    }

    /**
     * Set geocodingStatus
     *
     * @param string $geocodingStatus
     * @return Craftsperson
     */
    public function setGeocodingStatus($geocodingStatus)
    {
        $this->geocodingStatus = $geocodingStatus;

        return $this;
    }

    /**
     * Get geocodingStatus
     *
     * @return string 
     */
    public function getGeocodingStatus()
    {
        return $this->geocodingStatus;
    }

    /**
     * Set reparactorDescription
     *
     * @param string $reparactorDescription
     * @return Craftsperson
     */
    public function setReparactorDescription($reparactorDescription)
    {
        $this->reparactorDescription = $reparactorDescription;

        return $this;
    }

    /**
     * Get reparactorDescription
     *
     * @return string 
     */
    public function getReparactorDescription()
    {
        return $this->reparactorDescription;
    }

    /**
     * Set reparactorHours
     *
     * @param string $reparactorHours
     * @return Craftsperson
     */
    public function setReparactorHours($reparactorHours)
    {
        $this->reparactorHours = $reparactorHours;

        return $this;
    }

    /**
     * Get reparactorHours
     *
     * @return string 
     */
    public function getReparactorHours()
    {
        return $this->reparactorHours;
    }

    /**
     * Set reparactorServices
     *
     * @param string $reparactorServices
     * @return Craftsperson
     */
    public function setReparactorServices($reparactorServices)
    {
        $this->reparactorServices = $reparactorServices;

        return $this;
    }

    /**
     * Get reparactorServices
     *
     * @return string 
     */
    public function getReparactorServices()
    {
        return $this->reparactorServices;
    }

    /**
     * Set reparactorCertificates
     *
     * @param string $reparactorCertificates
     * @return Craftsperson
     */
    public function setReparactorCertificates($reparactorCertificates)
    {
        $this->reparactorCertificates = $reparactorCertificates;

        return $this;
    }

    /**
     * Get reparactorCertificates
     *
     * @return string 
     */
    public function getReparactorCertificates()
    {
        return $this->reparactorCertificates;
    }

    /**
     * Set creationDate
     *
     * @param \DateTime $creationDate
     * @return Craftsperson
     */
    public function setCreationDate($creationDate)
    {
        $this->creationDate = $creationDate;

        return $this;
    }

    /**
     * Get creationDate
     *
     * @return \DateTime 
     */
    public function getCreationDate()
    {
        return $this->creationDate;
    }

    /**
     * Set isUpdated
     *
     * @param boolean $isUpdated
     * @return Craftsperson
     */
    public function setIsUpdated($isUpdated)
    {
        $this->isUpdated = $isUpdated;

        return $this;
    }

    /**
     * Get isUpdated
     *
     * @return boolean 
     */
    public function getIsUpdated()
    {
        return $this->isUpdated;
    }

    /**
     * Set updateDate
     *
     * @param \DateTime $updateDate
     * @return Craftsperson
     */
    public function setUpdateDate($updateDate)
    {
        $this->updateDate = $updateDate;

        return $this;
    }

    /**
     * Get updateDate
     *
     * @return \DateTime 
     */
    public function getUpdateDate()
    {
        return $this->updateDate;
    }

    /**
     * Get latitude
     *
     * @return double
     */
    public function getLatitude()
    {
    	return $this->latitude;
    }
    
    /**
     * Set latitude
     *
     * @param double $latitude
     * @return Craftsperson
     */
    public function setLatitude($latitude)
    {
    	$this->latitude = $latitude;
    	
    	return $this;
    }

    /**
     * Get longitude
     *
     * @return double
     */
    public function getLongitude()
    {
    	return $this->longitude;
    }
    
    /**
     * Set longitude
     *
     * @param double $longitude
     * @return Craftsperson
     */
    public function setLongitude($longitude)
    {
    	$this->longitude = $longitude;
    	
    	return $this;
    }

    /**
     * Get likes
     *
     * @return integer
     */
    public function getLikes()
    {
    	return $this->likes;
    }
    
    /**
     * Set likes
     *
     * @param integer $likes
     * @return Craftsperson
     */
    public function setLikes($likes)
    {
    	$this->likes = $likes;
    	
    	return $this;
    }

    /**
     * Get other information
     *
     * @return string
     */
    public function getOtherInfo()
    {
    	return $this->otherInfo;
    }
    
    /**
     * Set other information
     *
     * @param string $otherInfo
     * @return Craftsperson
     */
    public function setOtherInfo($otherInfo)
    {
    	$this->otherInfo = $otherInfo;
    	
    	return $this;
    }

    /**
     * Get distance
     *
     * @return double
     */
    public function getDistance()
    {
    	return $this->distance;
    }
    
    /**
     * Set distance
     *
     * @param double $distance
     * @return Craftsperson
     */
    public function setDistance($distance)
    {
    	$this->distance = $distance;
    	
    	return $this;
    }

    /**
     * Set cmaCode
     *
     * @param \ARCommonBundle\Entity\Department $cmaCode
     * @return Craftsperson
     */
    public function setCmaCode(\ARCommonBundle\Entity\Department $cmaCode = null)
    {
        $this->cmaCode = $cmaCode;

        return $this;
    }

    /**
     * Get cmaCode
     *
     * @return \ARCommonBundle\Entity\Department 
     */
    public function getCmaCode()
    {
        return $this->cmaCode;
    }

    /**
     * Add nafCode
     *
     * @param \ARCommonBundle\Entity\NafCode $nafCode
     * @return Craftsperson
     */
    public function addNafCode(\ARCommonBundle\Entity\NafCode $nafCode)
    {
        $this->nafCode[] = $nafCode;

        return $this;
    }

    /**
     * Remove nafCode
     *
     * @param \ARCommonBundle\Entity\NafCode $nafCode
     */
    public function removeNafCode(\ARCommonBundle\Entity\NafCode $nafCode)
    {
        $this->nafCode->removeElement($nafCode);
    }

    /**
     * Get nafCode
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getNafCode()
    {
        return $this->nafCode;
    }

    /**
     * Modifier le logo
     * 
     * @param UploadedFile $file
     * @return Craftsperson
     */
    public function setLogo(UploadedFile $file = null)
    {
        if($this->logoFile || !$file) {
            unlink($this->getAbsoluteLogoPath());
            $this->logoFile = null;
        }

        if($file) {
            $name = uniqid('logo_') . '.' . $file->guessExtension();
            $file->move($this->getUploadRootDir(), $name);
            $this->logoFile = $name;
        }

        return $this;
    }

    /**
     * Chemin absolu du fichier logo.
     * 
     * @return string
     */
    public function getAbsoluteLogoPath()
    {
        return null === $this->logoFile
            ? null
            : $this->getUploadRootDir() . '/' . $this->logoFile;
    }

    /**
     * Chemin relatif à la racine du serveur du fichier logo.
     * 
     * @return string
     */
    public function getWebLogoPath()
    {
        return null === $this->logoFile
            ? null
            : $this->getUploadDir() . '/' . $this->logoFile;
    }

    /**
     * Chemin d'upload relativement à ce fichier.
     * 
     * @return string
     */
    protected function getUploadRootDir()
    {
        return __DIR__.'/../../../web/' . $this->getUploadDir();
    }

    /**
     * Chemin d'upload relativement à la racine du serveur.
     * 
     * @return string
     */
    protected function getUploadDir()
    {
        return 'uploads/craftspersons';
    }
}
