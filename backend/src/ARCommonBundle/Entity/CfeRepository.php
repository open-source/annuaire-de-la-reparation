<?php

namespace ARCommonBundle\Entity;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;

/**
 * La classe CfeRepository définit la gestion des entités {@link Cfe}.
 *
 */
class CfeRepository extends EntityRepository {

  /**
   * Retourne les départements filtrés par code et utilisateur associé.
   *
   * @param User $user
   * @param string[] $codes
   * @return Department[]
   */
  /*public function findByUserAndCode(User $user = null, $codes) {
    $qb = $this->createQueryBuilder('d');
    $qb->select('d')
    ->where($qb->expr()->in('d.code', ':codes'))
    ->setParameter('codes', $codes);

    // Ne garder le département que si l'utilisateur y est associé
    if($user) {
      $qb->join('d.idUser', 'u', Expr\Join::WITH, $qb->expr()->eq('u', ':user'))
      ->setParameter('user', $user);
    }

    return $qb->getQuery()->getResult();
  }*/


    function findAllArray(){
        return $this->createQueryBuilder('cfe')->getQuery()->getArrayResult();
    }
}