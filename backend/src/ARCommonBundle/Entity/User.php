<?php

namespace ARCommonBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\User as BaseUser;
use JMS\Serializer\Annotation as JMS;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * User
 *
 * @ORM\Table(name="t_user")
 * @ORM\Entity(repositoryClass="ARCommonBundle\Entity\UserRepository")
 * @UniqueEntity(
 *  fields="email",
 *  message="unique"
 * )
 */
class User extends BaseUser
{
    /**
     * Numéro d'identification unique
     *
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * Prénom
     *
     * @var string
     *
     * @ORM\Column(name="firstname", type="string", length=50, nullable=false)
     * @Assert\NotBlank(message="required")
     */
    private $firstname;

    /**
     * Nom
     *
     * @var string
     *
     * @ORM\Column(name="lastname", type="string", length=50, nullable=false)
     * @Assert\NotBlank(message="required")
     */
    private $lastname;

    /**
     * Numéro de téléphone
     *
     * @var string
     *
     * @ORM\Column(name="phone", type="string", length=50, nullable=true)
     * @Assert\Regex(pattern="/(?:\+?\d{10,11},?)+/", message="pattern")
     */
    private $phone;

    /**
     * Statut de réception des mails périodiques
     *
     * @var boolean
     *
     * @ORM\Column(name="is_mail_contact", type="boolean", nullable=false)
     */
    private $isMailContact;

    /**
     * CMA associée(s) à l'utilisateur
     *
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="ARCommonBundle\Entity\Department", inversedBy="idUser")
     * @ORM\JoinTable(name="t_user_has_department",
     *   joinColumns={
     *     @ORM\JoinColumn(name="id_user", referencedColumnName="id")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="code_department", referencedColumnName="code")
     *   }
     * )
     */
    private $codeDepartment;

    /**
     * Constructor
     */
    public function __construct()
    {
        parent::__construct();
        $this->codeDepartment = new \Doctrine\Common\Collections\ArrayCollection();
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return User
     */
    public function setEmail($email)
    {
        $this->email = $email;
        $this->setUsername($email);

        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set emailCanonical
     *
     * @param string $emailCanonical
     * @return User
     */
    public function setEmailCanonical($emailCanonical)
    {
        $this->emailCanonical = $emailCanonical;

        return $this;
    }

    /**
     * Get emailCanonical
     *
     * @return string 
     */
    public function getEmailCanonical()
    {
        return $this->emailCanonical;
    }

    /**
     * Set enabled
     *
     * @param boolean $enabled
     * @return User
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;

        return $this;
    }

    /**
     * Get enabled
     *
     * @return boolean 
     */
    public function getEnabled()
    {
        return $this->enabled;
    }

    /**
     * Set salt
     *
     * @param string $salt
     * @return User
     */
    public function setSalt($salt)
    {
        $this->salt = $salt;

        return $this;
    }

    /**
     * Get salt
     *
     * @return string 
     */
    public function getSalt()
    {
        return $this->salt;
    }

    /**
     * Set password
     *
     * @param string $password
     * @return User
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Get password
     *
     * @return string 
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set firstname
     *
     * @param string $firstname
     * @return User
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;

        return $this;
    }

    /**
     * Get firstname
     *
     * @return string 
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * Set lastname
     *
     * @param string $lastname
     * @return User
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;

        return $this;
    }

    /**
     * Get lastname
     *
     * @return string 
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * Set phone
     *
     * @param string $phone
     * @return User
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string 
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set isMailContact
     *
     * @param boolean $isMailContact
     * @return User
     */
    public function setIsMailContact($isMailContact)
    {
        $this->isMailContact = $isMailContact;

        return $this;
    }

    /**
     * Get isMailContact
     *
     * @return boolean 
     */
    public function getIsMailContact()
    {
        return $this->isMailContact;
    }

    /**
     * Set isEnabled
     *
     * @param boolean $isEnabled
     * @return User
     */
    public function setIsEnabled($isEnabled)
    {
        $this->isEnabled = $isEnabled;

        return $this;
    }

    /**
     * Get isEnabled
     *
     * @return boolean 
     */
    public function getIsEnabled()
    {
        return $this->isEnabled;
    }

    /**
     * Add codeDepartment
     *
     * @param \ARCommonBundle\Entity\Department $codeDepartment
     * @return User
     */
    public function addCodeDepartment(\ARCommonBundle\Entity\Department $codeDepartment)
    {
        $this->codeDepartment[] = $codeDepartment;

        return $this;
    }

    /**
     * Remove codeDepartment
     *
     * @param \ARCommonBundle\Entity\Department $codeDepartment
     */
    public function removeCodeDepartment(\ARCommonBundle\Entity\Department $codeDepartment)
    {
        $this->codeDepartment->removeElement($codeDepartment);
    }

    /**
     * Get codeDepartment
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCodeDepartment()
    {
        return $this->codeDepartment;
    }
}
