<?php

namespace ARCommonBundle\Entity;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;

/**
 * La classe CraftspersonErrorMessageRepository définit la gestion des entités {@link CraftspersonErrorMessage}.
 *
 */
class CraftspersonErrorMessageRepository extends EntityRepository {
    function findAllArray(){
        return $this->createQueryBuilder('err')->getQuery()->getArrayResult();
    }
}