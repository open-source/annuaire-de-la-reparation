<?php

namespace ARCommonBundle\Command;

use ARCommonBundle\Entity\Craftsperson;
use ARCommonBundle\Enum\Entity;
use ARCommonBundle\Enum\GeocodingStatus;
use ARCommonBundle\Service\GeocodeService;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Psr\Log\LoggerInterface;
use Psr\Log\LogLevel;

/**
 * La classe UpdateGeocodingCommand définit une commande mettant à jour le géocodage des entités {@link Craftsperson}.
 *
 * @author vdouillet
 *        
 */
class UpdateGeocodingCommand extends ContainerAwareCommand
{
    /**
     * Configuration de la commande.
     */
    protected function configure()
    {
        $this->setName('geocoding:update')
        ->setDescription('Mise à jour du géocodage des réparateurs')
        ->setHelp('Cette commande tente de mettre à jour le géocodage des réparateurs pour lesquels il est indiqué comme étant invalide.');
    }

    /**
     * Traitement.
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->log(LogLevel::NOTICE, 'Démarrage de la mise à jour du géocodage des réparateurs...');
        $startTime = new \DateTime();

        /** @var \Doctrine\Bundle\DoctrineBundle\Registry $doctrine */
        $doctrine = $this->getContainer()->get('doctrine');
        /** @var \ARCommonBundle\Entity\CraftspersonRepository $repo */
        $repo = $doctrine->getRepository(Entity::CRAFTSPERSON);
        $needsUpdate = $repo->findBy(array(
            'geocodingStatus' => GeocodingStatus::NC,
            'isEnabled' => true
        ));

        $updateCountMessage = sprintf('%d réparateurs sélectionnés pour mise à jour', count($needsUpdate));
        $this->log(LogLevel::NOTICE, $updateCountMessage);

        $successCount = 0;
        /** @var \ARCommonBundle\Service\GeocodeService $craftsperson */
        $geocoder = $this->getContainer()->get('common.geocode_service');
        /** @var \ARCommonBundle\Entity\Craftsperson $craftsperson */

        try {
            foreach ($needsUpdate as $craftsperson) {
                if($this->updateGeocoding($craftsperson, $geocoder)) {
                    $successCount++;
                }
            }
        } catch(\Exception $e) {
             $this->log(LogLevel::ERROR, "An exception occured : " . $e->getMessage());
             return;
        } finally {
            // Commit des modifications des entités
            $doctrine->getEntityManager()->flush();
            $endTime = new \DateTime();
            $processTime = date_diff($endTime, $startTime);

            $summary = sprintf('Géocodage terminé avec %d erreurs et %d succès. Temps de traitement : %s', count($needsUpdate) - $successCount, $successCount, $processTime->format('%hh %im %ss'));
            $this->log(LogLevel::NOTICE, $summary);
        }
    }

    /**
     * Met à jour le géocodage pour une entité {@link Craftsperson}.
     *
     * @param Craftsperson $craftsperson
     * @param GeocodeService $geocoder
     * @return boolean
     */
    private function updateGeocoding(Craftsperson $craftsperson, GeocodeService $geocoder) {
        if($craftsperson->getGeocodingStatus() == GeocodingStatus::NC) {
            $geocoding = $geocoder->geocodeAddress($craftsperson->getAddress1(), $craftsperson->getZipCode(), $craftsperson->getZipCodeLabel());

            if(is_array($geocoding)) {
                $craftsperson->setLatitude($geocoding['lat']);
                $craftsperson->setLongitude($geocoding['lon']);
                $craftsperson->setGeocodingStatus($geocoding['status']);
            }
            else {
                $craftsperson->setGeocodingStatus(GeocodingStatus::ERROR);
            }
        }

        $status = $craftsperson->getGeocodingStatus();
        return $status == GeocodingStatus::IMPRECISE || $status == GeocodingStatus::OK;
    }

    /**
     * Logger un message.
     *
     * @param string $logLevel
     * @param string $message
     * @param OutputInterface $output
     */
    private function log($logLevel, $message) {
        /** @var $logger LoggerInterface */
        $logger = $this->getContainer()->get('logger');
        $logger->log($logLevel, $message);
    }
}
