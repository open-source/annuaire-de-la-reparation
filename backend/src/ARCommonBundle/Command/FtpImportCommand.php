<?php

namespace ARCommonBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Psr\Log\LoggerInterface;
use Psr\Log\LogLevel;

/**
 * La classe FtpImportCommand définit une commande important des fichiers de {@link Craftsperson} depuis un serveur ftp.
 *
 * @author vdouillet
 */
class FtpImportCommand extends ContainerAwareCommand {
    /** Retour chariot */
    const LINE_RETURN = "\n";
    /** Format des dates dans les noms de fichier */
    const DATE_FORMAT = 'd-m-Y-H-i-s';

    /**
     * Configuration de la commande.
     */
    protected function configure()
    {
        $this->setName('import:ftp')
        ->setDescription('Import FTP des réparateurs')
        ->setHelp('Cette commande importe les réparateurs depuis le serveur FTP configuré dans l\'application.');
    }

    /**
     * Traitement.
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function execute(InputInterface $input, OutputInterface $output) {
        // Fichier de log
        $logFile = tmpfile();

        // Connexion au serveur FTP
        $ftp = $this->connect($logFile);

        if($ftp) {
            // Traitement des fichiers
            $now = new \DateTime();
            $this->importFiles($ftp, $logFile, $now);

            // Upload du fichier de log
            $logDir = $this->getContainer()->getParameter('ftp_log_dir');
            $logFilePath = $logDir . '/import-log-' . $now->format($this::DATE_FORMAT) . '.txt';
            $this->uploadFile($ftp, $logFilePath, $logFile, $logFile);

            ftp_close($ftp);
        }

        fclose($logFile);
    }

    /**
     * Connexion au serveur ftp
     * 
     * @param resource $logFile
     * @return resource|false
     */
    private function connect($logFile) {
        $server = $this->getContainer()->getParameter('ftp_server');
        $port = $this->getContainer()->getParameter('ftp_port');
        $username = $this->getContainer()->getParameter('ftp_username');
        $ftp = ftp_connect($server, $port);
        $loginResult = $ftp && ftp_login($ftp, $username, $this->getContainer()->getParameter('ftp_password'));

        if (!$ftp || !$loginResult) {
            $error = sprintf('Erreur de connexion au serveur %s pour utilisateur %s', $server, $username);
            $this->log(LogLevel::ERROR, $error, $logFile);
            return false;
        }
        else {
            ftp_pasv($ftp, true);
            $success = sprintf('Connexion avec succès au serveur %s pour utilisateur %s', $server, $username);
            $this->log(LogLevel::NOTICE, $success, $logFile);
            return $ftp;
        }
    }

    /**
     * Import des fichiers
     * 
     * @param resource $ftp
     * @param resource $logFile
     * @param DateTime $importDate
     */
    private function importFiles($ftp, $logFile, $importDate) {
        // Listage des fichiers à traiter
        $importDir = $this->getContainer()->getParameter('ftp_import_dir');
        $fileList = ftp_nlist($ftp, $importDir);
        if ($fileList === FALSE) {
            $error = sprintf('Échec de listage des fichiers du dossier %s', $importDir);
            $this->log(LogLevel::ERROR, $error, $logFile);
        }
        else {
            $success = sprintf('Traitement des %d fichier(s) du dossier %s ...', count($fileList), $importDir);
            $this->log(LogLevel::NOTICE, $success, $logFile);
        }

        /** @var \ARCommonBundle\Service\CraftspersonImportService $importService */
        $importService = $this->getContainer()->get('common.craftsperson_import_service');
        $errorFolder = $this->getContainer()->getParameter('ftp_error_dir');
        $archiveFolder = $this->getContainer()->getParameter('ftp_success_dir');
        foreach($fileList as $filePath) {
            // Téléchargement
            $tmpFile = tmpfile();
            $filename = basename($filePath);
            if(!ftp_fget($ftp, $tmpFile , $filePath, FTP_ASCII)) {
                $error = sprintf('Échec d\'import du fichier %s : impossible de télécharger le fichier', $filename);
                $this->log(LogLevel::ERROR, $error, $logFile);
                break;
            }

            // Import
            fseek($tmpFile, 0);
            $importResult = $importService->import($tmpFile, $filename, false);
            $importResult = $importResult['result'];
            if($importResult['error'] !== FALSE) {
                $error = sprintf('%s : échec d\'import avec %d erreurs', $filename, count($importResult['logs']));
                $this->log(LogLevel::ERROR, $error, $logFile);
                // Enregistrer les erreurs dans le log
                foreach($importResult['logs'] as $log) {
                    /** @var \ARCommonBundle\Entity\ImportLog $log */
                    $logLine = sprintf('%s ligne %d, siret %s, region %s : %s', $log->getFilename(), $log->getLine(), $log->getSiret(), $log->getRegion(), $log->getMessage());
                    $this->log(LogLevel::ERROR, $logLine, $logFile);
                }
                // Déplacement du fichier dans le dossier d'erreur
                $this->rename($ftp, $filePath, $errorFolder . '/' . $this->getDatedFileName($filename, $importDate), $logFile);
            }
            else {
                $success = sprintf('%s : %d supprimés, %d mis à jour, %d créés, %d invalides, %d ignorés', $filename, $importResult['deleted'], $importResult['updated'], $importResult['created'], $importResult['invalid'], $importResult['ignored']);
                $this->log(LogLevel::NOTICE, $success, $logFile);
                // Déplacement du fichier dans le dossier d'archive
                $this->rename($ftp, $filePath, $archiveFolder . '/' . $this->getDatedFileName($filename, $importDate), $logFile);
            }

            fclose($tmpFile);
        }
    }

    /**
     * Retourne le nom du fichier suffixé avec la date
     * 
     * @param string $fileName
     * @param DateTime $date
     * @return string
     */
    private function getDatedFileName($fileName, \DateTime $date) {
        $info = pathinfo($fileName);
        $newName =  basename($fileName, '.' . $info['extension']) . '_' . $date->format($this::DATE_FORMAT) . '.' . $info['extension'];
        return $newName;
    }

    /**
     * Renommer et/ou déplacer un fichier/dossier sur le FTP
     * 
     * @param resource $ftp
     * @param string $sourcePath
     * @param string $destPath
     * @return true en cas de succès, false sinon
     */
    private function rename($ftp, $sourcePath, $destPath, $logFile) {
        $result = ftp_rename($ftp, $sourcePath, $destPath);
        if(!$result) {
            $error = sprintf('Échec de renommage de %s en %s', $sourcePath, $destPath);
            $this->log(LogLevel::ERROR, $error, $logFile);
        }
        else {
            $success = sprintf('%s -> %s (renommage)', $sourcePath, $destPath);
            $this->log(LogLevel::NOTICE, $success, $logFile);
        }
        return $result;
    }

    /**
     * Uploader un fichier sur le FTP.
     * 
     * @param resource $ftp
     * @param string $filePath
     * @param resource $toUpload
     * @param resource $logFile
     * @return true en cas de succès, false sinon
     */
    private function uploadFile($ftp, $filePath, $toUpload, $logFile) {
        $currentPos = ftell($toUpload);
        fseek($toUpload, 0);
        $result = ftp_fput($ftp, $filePath, $toUpload, FTP_ASCII);
        if(!$result) {
            $error = sprintf('Échec d\'upload du fichier %s', $filePath);
            $this->log(LogLevel::ERROR, $error, $logFile);
        }
        else {
            $success = sprintf('Fichier %s uploadé avec succès', $filePath);
            $this->log(LogLevel::NOTICE, $success, $logFile);
        }
        fseek($toUpload, $currentPos);
        return $result;
    }

    /**
     * Changement de dossier sur le FTP.
     * 
     * @param resource $ftp
     * @param string $folder
     * @param resource $logFile
     * @return true en cas de succès, false sinon
     */
    private function chdir($ftp, $folder, $logFile) {
        $chdir = ftp_chdir($ftp, $folder);
        if (!$chdir) {
            $error = sprintf('Échec d\'ouverture du dossier %s', $folder);
            $this->log(LogLevel::ERROR, $error, $logFile);
        }
        else {
            $success = sprintf('Ouverture du dossier %s', $importDir);
            $this->log(LogLevel::NOTICE, $success, $logFile);
        }
        return $chdir;
    }

    /**
     * Logger un message.
     *
     * @param string $logLevel
     * @param string $message
     * @param resource $logFile
     */
    private function log($logLevel, $message, $logFile) {
        /** @var $logger LoggerInterface */
        $logger = $this->getContainer()->get('logger');
        $logger->log($logLevel, $message);
        // Écriture dans le fichier de log
        $fileLogLine = strtoupper($logLevel) . ': ' . $message . $this::LINE_RETURN;
        fwrite($logFile, $fileLogLine);
    }
}