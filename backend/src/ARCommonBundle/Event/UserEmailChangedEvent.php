<?php

namespace ARCommonBundle\Event;

use ARCommonBundle\Entity\User;
use Symfony\Component\EventDispatcher\Event;

/**
 * La classe UserEmailChangedEvent définit un évenement de modification de l'adresse mail d'une entité {@link User}.
 *
 * @author vdouillet
 */
class UserEmailChangedEvent extends Event {
    /** Identifiant de l'évenement */
    const NAME = 'user.email.changed';
    /** @var \ARCommonBundle\Entity\User $user */
    protected $user;

    /**
     * Constructeur.
     *
     * @param User $user
     */
    public function __construct(User $user) {
        $this->user = $user;
    }

    /**
     * Retourne l'utilisateur concerné par l'évenement.
     *
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }
}