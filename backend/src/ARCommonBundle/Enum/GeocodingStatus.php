<?php

namespace ARCommonBundle\Enum;

/**
 * La classe GeocodingStatus définit les constantes de statut de géocodage.
 * 
 * @author vdouillet
 *
 */
abstract class GeocodingStatus {
    const OK = 'ok';
    const IMPRECISE = 'imprecise';
    const ERROR = 'error';
    const NC = 'nc';
}