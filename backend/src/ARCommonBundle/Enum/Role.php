<?php

namespace ARCommonBundle\Enum;

/**
 * La classe Role définit les constantes des différents roles.
 *
 * @author vdouillet
 *
 */
abstract class Role {
	const ADMIN = 'ROLE_ADMIN';
	const CMA = 'ROLE_CMA';
}