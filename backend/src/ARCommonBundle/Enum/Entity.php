<?php

namespace ARCommonBundle\Enum;

/**
 * La classe Entity définit les constantes d'accès aux différentes classes d'entités.
 * 
 * @author vdouillet
 *
 */
abstract class Entity {
	const CFE = 'ARCommonBundle:Cfe';
	const CRAFTSPERSON = 'ARCommonBundle:Craftsperson';
	const DEPARTMENT = 'ARCommonBundle:Department';
	const IMPORT_LOG = 'ARCommonBundle:ImportLog';
	const NAF_CODE = 'ARCommonBundle:NafCode';
	const USER = 'ARCommonBundle:User';
	const CATEGORY = 'ARCommonBundle:Category';
	const CRAFTSPERSON_LIKE = 'ARCommonBundle:CraftspersonLike';
	const CRAFTSPERSON_ERR_MSG = 'ARCommonBundle:CraftspersonErrorMessage';
}