<?php

namespace ARCommonBundle\Listener;

use ARCommonBundle\Entity\User;
use ARCommonBundle\Event\UserEmailChangedEvent;
use ARCommonBundle\Service\UserService;

/**
 * La classe UserEmailChangedListener définit les traitements liés au changement d'email d'une entité {@link User}.
 *
 * @author vdouillet
 */
class UserEmailChangedListener {
    /** @var \ARCommonBundle\Service\UserService $userService */
    protected $userService;

    /**
     * Constructeur.
     *
     * @param UserService $userService
     */
    public function __construct(UserService $userService) {
        $this->userService = $userService;
    }

    /**
     * Changement d'email.
     *
     * @param UserEmailChangedEvent $event
     */
    public function onUserEmailChanged(UserEmailChangedEvent $event) {
        $this->userService->emailChanged($event->getUser());
    }
}