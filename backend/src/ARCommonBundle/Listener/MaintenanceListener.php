<?php

namespace ARCommonBundle\Listener;

use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\DependencyInjection\ContainerInterface;

class MaintenanceListener
{
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function onKernelRequest(GetResponseEvent $event)
    {
        $maintenanceParameterName = 'public_maintenance';
        $maintenanceEndDateParameterName = 'public_maintenanceEndDate';

        $pathInfo = $event->getRequest()->getPathInfo();

        //Check wich bundle is called Admin or Public to activate maintenance on right bundle
        if (preg_match('/\/admin/',$pathInfo)) { //Admin
            $maintenanceParameterName = 'admin_maintenance';
            $maintenanceEndDateParameterName = 'admin_maintenanceEndDate';
        }

        if($maintenanceParameterName) {
            $maintenanceEndDate = $this->container->hasParameter($maintenanceEndDateParameterName) ? $this->container->getParameter($maintenanceEndDateParameterName) : false;
            $maintenance = $this->container->hasParameter($maintenanceParameterName) ? $this->container->getParameter($maintenanceParameterName) : false;

            $debug = in_array($this->container->get('kernel')->getEnvironment(), array('test', 'dev'));

            if ($maintenance && !$debug) {
                $engine = $this->container->get('templating');
                $content = $engine->render('ARPublicBundle::maintenance.html.twig', array('maintenanceEndDate'=>$maintenanceEndDate));
                $event->setResponse(new Response($content, 503));
                $event->stopPropagation();
            }
        }
    }
}