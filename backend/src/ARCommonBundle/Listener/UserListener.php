<?php

namespace ARCommonBundle\Listener;

use ARCommonBundle\Entity\User;
use ARCommonBundle\Event\UserEmailChangedEvent;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use FOS\UserBundle\Util\TokenGeneratorInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * La classe UserListener définit les traitements liés aux événements des entités {@link User}.
 *
 * @author vdouillet
 */
class UserListener implements EventSubscriber {
    /** @var \FOS\UserBundle\Util\TokenGeneratorInterface $tokenGenerator */
    private $tokenGenerator;
    /** @var \Symfony\Component\EventDispatcher\EventDispatcherInterface $eventDispatcher */
    private $eventDispatcher;
    /** @var boolean $emailChanged */
    private $emailChanged = false;

    /**
     * Constructeur.
     *
     * @param EventDispatcherInterface $eventDispatcher
     * @param TokenGeneratorInterface $tokenGenerator
     */
    public function __construct(EventDispatcherInterface $eventDispatcher, TokenGeneratorInterface $tokenGenerator) {
        $this->eventDispatcher = $eventDispatcher;
        $this->tokenGenerator = $tokenGenerator;
    }

    /**
     * Évenements écoutés.
     */
    public function getSubscribedEvents()
    {
        return array(
            'preUpdate',
            'postUpdate',
        );
    }

    /**
     * Vérification du statut de modification de l'adresse mail.
     *
     * @param LifecycleEventArgs $eventArgs
     */
    public function preUpdate(LifecycleEventArgs $eventArgs)
    {
        if ($eventArgs->getEntity() instanceof User) {
            $this->emailChanged = $eventArgs->hasChangedField('email');
        }
    }

    /**
     * Lancement d'un évenement de changement de mail si le mail a changé.
     *
     * @param LifecycleEventArgs $eventArgs
     */
    public function postUpdate(LifecycleEventArgs $eventArgs)
    {
        if($this->emailChanged) {
            $event = new UserEmailChangedEvent($eventArgs->getEntity());
            $this->eventDispatcher->dispatch(UserEmailChangedEvent::NAME, $event);
        }
    }
}