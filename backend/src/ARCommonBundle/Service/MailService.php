<?php

namespace ARCommonBundle\Service;

/**
 * La classe MailService définit les méthodes de gestion des envois d'email.
 * 
 * @author vdouillet
 */
class MailService {
    /** Service d'envoi de mails */
    private $mailerService;
    /** Adresse de l'expéditeur */
    private $areSenderMail;

    /**
     * Constructeur.
     *
     * @param $mailer
     * @param $areSenderMail
     */
    public function __construct($mailer, $areSenderMail) {
        $this->mailerService = $mailer;
        $this->areSenderMail = $areSenderMail;
    }

    /**
     * Envoi d'un email
     *
     * @param string|array $to
     * @param string $subject
     * @param string $body
     * @param string $contentType
     */
    public function sendMail($to, $subject, $body, $contentType = 'text/plain') {
        $message = \Swift_Message::newInstance()
        ->setSubject($subject)
        ->setSender($this->areSenderMail)
        ->setFrom(array($this->areSenderMail => $this->areSenderMail))
        ->setTo($to)
        ->setBody($body, $contentType);

        $this->mailerService->send($message);
    }
}