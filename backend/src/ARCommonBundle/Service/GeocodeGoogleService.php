<?php

namespace ARCommonBundle\Service;

use Unirest;

/**
 * La classe GeocodeGoogleService définit un service permettant le géocodage via l'api Google.
 *
 * @author vdouillet
 *        
 */
class GeocodeGoogleService implements GeocoderInterface {
    const GEOCODE_URL = 'https://maps.googleapis.com/maps/api/geocode/json';
    const FULL_ADDRESS_FORMAT = '%s, %s %s';
    const PARTIAL_ADDRESS_FORMAT = '%s %s';

    public $apiKey;

    /**
     * Constructeur.
     *
     * @param string $apiKey
     */
    public function __construct($apiKey) {
        $this->apiKey = $apiKey;
    }

    /**
     * Conversion d'une adresse en coordonnées lat/lon
     *
     * @param string $address
     * @param string $zipCode
     * @param string $city
     * @return array|null
     */
    public function geocodeAddress($address, $zipCode, $city) {
        return $this->geocodeQuery(sprintf($this::FULL_ADDRESS_FORMAT, $address, $zipCode, $city));
    }

    /**
     * Conversion d'une adresse partielle en coordonnées lat/lon.
     *
     * @param string $zipCode
     * @param string $city
     * @return array|null
     */
    public function geocodePartialAddress($zipCode, $city) {
        return $this->geocodeQuery(sprintf($this::PARTIAL_ADDRESS_FORMAT, $zipCode, $city));
    }

    /**
     * Conversion d'une requête quelconque en coordonnées lat/lon.
     *
     * @param string $query
     * @return array|null
     */
    public function geocodeQuery($query) {
        // Headers par défaut
        $headers = array('Accept' => 'application/json');

        // Paramètres par défaut
        $parameters = array(
            'key' => $this->apiKey,
            'region' => 'fr',
            'address' => $query
        );

        $response = Unirest\Request::get($this::GEOCODE_URL, $headers, $parameters);
        
        // Retour de la réponse avec gestion des erreurs
        $content = $response->body;
        if($response->code != 200) {
            return $response->code;
        }
        elseif($content->status !== 'OK') {
            return $content->status;
        }
        else {
            $res = $content->results[0]->geometry->location;
            return array(
                'lat' => $res->lat,
                'lon' => $res->lng
            );
        }
    }

    public function setApiKey($apiKey) {
        $this->apiKey = $apiKey;
    }
}