<?php

namespace ARCommonBundle\Service;

use ARCommonBundle\Enum\GeocodingStatus;
use Psr\Log\LoggerInterface;

/**
 * La classe GeocodeService définit un service permettant la conversion d'adresses en coordonnées lat/lon et inversement.
 *
 * @author vdouillet
 *        
 */
class GeocodeService {
    /** @var GeocoderInterface $googleGeocoder */
    private $googleGeocoder;
    /** @var LoggerInterface $logger */
    private $logger;

    /**
     * Constructeur.
     *
     * @param GeocoderInterface $googleGeocoder
     */
    public function __construct(LoggerInterface $logger, GeocoderInterface $googleGeocoder) {
        $this->logger = $logger;
        $this->googleGeocoder = $googleGeocoder;
    }

    public function setApiKey($apiKey) {
        $this->googleGeocoder->setApiKey($apiKey);
    }

    /**
     * Conversion d'une adresse en coordonnées lat/lon
     *
     * @param string $address
     * @param string $zipCode
     * @param string $city
     * @return array|null
     */
    public function geocodeAddress($address, $zipCode, $city) {
        // Adresse complète
        $result = $this->googleGeocoder->geocodeAddress($address, $zipCode, $city);
        $status = "";
        //S'il y a un tableau de résultat alors le géocodage a réussi, on renvoie le résultat
        if(is_array($result)) {
            $result['status'] = GeocodingStatus::OK;
            return $result;
        } 

        //Si $result n'est pas un tableau de résultat alors il s'agit du status de la requête
        $status = $result;

        switch($status) {
            CASE "ZERO_RESULTS": //Si aucun résultat trouvé
                $result = $this->googleGeocoder->geocodePartialAddress($zipCode, $city);

                //On effectue le géocodage sur la ville et le code postal et on renvoie le résultat
                if(is_array($result)) {
                    $result['status'] = GeocodingStatus::IMPRECISE;
                    return $result;
                } 

                //Si ce géocodage ne rend aucuns résultats, on affiche l'erreur.
                $errorMessage = sprintf("Aucun résultat trouvé pour l'adresse '%s, %s %s", $address, $zipCode, $city);
                $this->logger->error($errorMessage);
            break;
            CASE "OVER_QUERY_LIMIT":
                throw new \Exception("Le quota de requêtes journalier a été atteint.");
            break;
            CASE "REQUEST_DENIED":
                $errorMessage = sprintf("Votre requête a été rejetée pour l'adresse '%s, %s %s", $address, $zipCode, $city);
                $this->logger->error($errorMessage);
            break;
            CASE "INVALID_REQUEST":
                $errorMessage = sprintf("Votre requête est invalide pour l'adresse '%s, %s %s", $address, $zipCode, $city);
                $this->logger->error($errorMessage);
            break;
            CASE "UNKNOWN_ERROR":
                $errorMessage = sprintf("La requête n'a pas pu aboutir en raison d'une erreur serveur pour l'adresse '%s, %s %s", $address, $zipCode, $city);
                $this->logger->error($errorMessage);
            break;
            DEFAULT:
                $errorMessage = sprintf("Erreur inconnue pour l'adresse '%s, %s %s", $address, $zipCode, $city);
                $this->logger->error($errorMessage);
        }

        return GeocodingStatus::NC;
    }

    /**
     * Conversion d'une requête quelconque en coordonnées lat/lon
     *
     * @param string $query
     * @return array|null
     */
    public function geocodeQuery($query)
    {
        $result = $this->googleGeocoder->geocodeQuery($query);

        return $result;
    }

}