<?php

namespace ARCommonBundle\Service;

use ARCommonBundle\Entity\User;
use ARCommonBundle\Enum\Role;
use Doctrine\ORM\EntityManagerInterface;
use FOS\UserBundle\Model\UserManagerInterface;
use FOS\UserBundle\Util\TokenGeneratorInterface;

/**
 * La classe UserService définit les méthodes de gestion des utilisateurs.
 * 
 * @author vdouillet
 */
class UserService {
    /** @var \Doctrine\ORM\EntityManager $em */
    private $em;
    /** @var UserManagerInterface $userManager */
    private $userManager;
    /** @var TokenGeneratorInterface */
    private $tokenGenerator;
    private $twig;
    /** @var MailService $mailService */
    private $mailService;
    private $activateAddress;
    private $passwordResetAddress;

    /**
     * Constructeur.
     *
     * @param EntityManagerInterface $entityManager
     * @param UserManagerInterface $userManager
     * @param TokenGeneratorInterface $tokenGenerator
     * @param $twig
     * @param MailService $mailer
     * @param $areSenderMail
     */
    public function __construct(EntityManagerInterface $entityManager, UserManagerInterface $userManager, TokenGeneratorInterface $tokenGenerator, MailService $mail, $twig, $activateAddress, $passwordResestAddress)
    {
        $this->em = $entityManager;
        $this->userManager = $userManager;
        $this->tokenGenerator = $tokenGenerator;
        $this->twig = $twig;
        $this->mailService = $mail;
        $this->activateAddress = $activateAddress;
        $this->passwordResetAddress = $passwordResestAddress;
    }

    /**
     * Création d'un utilisateur.
     *
     * @param User $user
     */
    public function create(User $user, $activateAddress) {

        // Mot de passe autogénéré (8 caractères)
        $password = substr($this->tokenGenerator->generateToken(), 0, 8);
        $user->setPlainPassword($password);
        $this->userManager->updatePassword($user);

        // Token de création de mot de passe
        $activateToken = $this->setNewActivationToken($user);

        // Mail d'activation du compte
        $activateAddress = str_replace('{:activationToken}', $activateToken, $this->activateAddress);
        $subject = 'Activation de votre compte Annuaire des réparateurs';
        $body = $this->twig->render('ARCommonBundle:Emails:registration.txt.twig', array(
            'user' => $user,
            'activateAddress' => $activateAddress
        ));

        $this->em->persist($user);
        $this->em->flush();

        $this->mailService->sendMail($user->getEmail(), $subject, $body);
    }

    /**
     * Activation d'un utilisateur.
     *
     * @param User $user
     * @param string $password
     */
    public function activate(User $user, $password) {
        $user->setPlainPassword($password);
        $user->setConfirmationToken(null);
        $user->setEnabled(true);
        $this->userManager->updateUser($user);

        $this->em->persist($user);
        $this->em->flush();
    }

    /**
     * Désactivation du compte et envoi d'un mail de confirmation au nouvel email.
     *
     * @param User $user
     */
    public function emailChanged(User $user) {
        // Désactivation du compte
        $user->setEnabled(false);

        // Token de création de mot de passe
        $activateToken = $this->tokenGenerator->generateToken();
        $user->setConfirmationToken($activateToken);
        $user->setPasswordRequestedAt(new \DateTime());

        $this->em->flush($user);

        // Mail de confirmation d'email
        $activateAddress = str_replace('{:activationToken}', $activateToken, $this->activateAddress);
        $subject = 'Changement d\'adresse mail de votre compte Annuaire des réparateurs';
        $body = $this->twig->render('ARCommonBundle:Emails:email-changed.txt.twig', array(
            'activateAddress' => $activateAddress
        ));
        $this->mailService->sendMail($user->getEmail(), $subject, $body);
    }

    /**
     * Désactivation du compte et envoi d'un mail de réinitialisation de mot de passe.
     * 
     * @param User $user
     */
    public function passwordResetRequest(User $user) {
        // Token de réinitialisation
        $token = $this->setNewActivationToken($user);

        // Mail de réinitialisation
        $passwordResetAddress = str_replace('{:token}', $token, $this->passwordResetAddress);
        $subject = 'Réinitialisation de votre mot de passe Annuaire des réparateurs';
        $body = $this->twig->render('ARCommonBundle:Emails:reset-password.txt.twig', array(
            'user' => $user,
            'passwordResetAddress' => $passwordResetAddress
        ));

        $this->em->persist($user);
        $this->em->flush();

        $this->mailService->sendMail($user->getEmail(), $subject, $body);
    }
    
    /**
     * Nouveau token d'activation pour un utilisateur.
     * 
     * @param User $user
     * @return string le nouveau token
     */
    private function setNewActivationToken(User $user) {
        // Désactivation du compte
        $user->setEnabled(false);

        // Token de création de mot de passe
        $token = $this->tokenGenerator->generateToken();
        $user->setConfirmationToken($token);
        $user->setPasswordRequestedAt(new \DateTime());

        return $token;
    }
}