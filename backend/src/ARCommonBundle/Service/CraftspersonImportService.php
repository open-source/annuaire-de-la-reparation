<?php

namespace ARCommonBundle\Service;

use ARCommonBundle\Entity\Craftsperson;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\EntityManager;
use ARCommonBundle\Entity\Department;
use ARCommonBundle\Entity\User;
use ARCommonBundle\Enum\Role;
use ARCommonBundle\Entity\CraftspersonRepository;
use ARCommonBundle\Entity\UserRepository;
use ARCommonBundle\Entity\ImportLogRepository;
use ARCommonBundle\Entity\ImportLog;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * La classe Region définit une région de France.
 *
 * @author vdouillet
 */
class Region {
  /** Correspondance région vers départements */
  const REGION_TO_DPT = array (
    'auvergne-rhone-alpes' => array('01', '03', '07', '15', '26', '38', '42', '43', '63', '69', '73', '74'),
    'bourgogne-franche-comte' => array('21', '25', '39', '58', '70', '71', '89', '90'),
    'bretagne' => array('22', '29', '35', '56'),
    'centre-val de loire' => array('18', '28', '36', '37', '41', '45'),
    'corse' => array('2A', '2B'),
    'grand est' => array('08', '10', '51', '52', '54', '55', '57', '67', '68', '88'),
    'hauts-de-france' => array('02', '59', '60', '62', '80'),
    'ile-de-france' => array('75', '77', '78', '91', '92', '93', '94', '95'),
    'normandie' => array('14', '27', '50', '61', '76'),
    'nouvelle-aquitaine' => array('16', '17', '19', '23', '24', '33', '40', '47', '64', '79', '86', '87'),
    'occitanie' => array('09', '11', '12', '30', '31', '32', '34', '46', '48', '65', '66', '81', '82'),
    'pays de la loire' => array('44', '49', '53', '72', '85'),
    'provence-alpes-cote d\'azur' => array('04', '05', '06', '13', '83', '84'),
    'guadeloupe' => array('971'),
    'martinique' => array('972'),
    'guyane' => array('973'),
    'la reunion' => array('974'),
    'mayotte' => array('976'),
  );

  /** Correspondance ancienne région (<2016) vers départements */
  const OLD_REGION_TO_DPT = array(
    'alsace' => array('67', '68'),
    'aquitaine' => array('24', '33', '40', '47', '64'),
    'auvergne' => array('03', '15', '43', '63'),
    'bourgogne' => array('21', '58', '71', '89'),
    'bretagne' => array('22', '29', '35', '56'),
    'centre' => array('18', '28', '36', '37', '41', '45'),
    'champagne-ardenne' => array('08', '10', '51', '52'),
    'corse' => array('2A', '2B'),
    'franche-comte' => array('25', '39', '70', '90'),
    'ile-de-france' => array('75', '77', '78', '91', '92', '93', '94', '95'),
    'languedoc-roussillon' => array('11', '30', '34', '48', '66'),
    'limousin' => array('19', '23', '87'),
    'lorraine' => array('54', '55', '88'),
    'midi-pyrenees' => array('09', '12', '31', '32', '46', '65', '81', '82'),
    'nord-pas-de-calais' => array('59', '62'),
    'basse-normandie' => array('14', '50', '61'),
    'haute-normandie' => array('27', '76'),
    'pays de la loire' => array('44', '49', '53', '72', '85'),
    'picardie' => array('02', '60', '80'),
    'poitou-charentes' => array('16', '17', '79', '86'),
    'provence-alpes-cote d\'azur' => array('04', '05', '06', '13', '83', '84'),
    'rhone-alpes' => array('01', '07', '26', '38', '42', '69', '73', '74'),
    'guadeloupe' => array('971'),
    'martinique' => array('972'),
    'guyane' => array('973'),
    'la reunion' => array('974'),
    'mayotte' => array('976'),
  );

  /** Nom textuel */
  private $name;
  /** Région avant 2016 */
  private $isOld;

  /**
   * Constructeur.
   *
   * @param string $name
   * @param boolean $isOld
   */
  public function __construct($name, $isOld) {
    $this->name = $name;
    $this->isOld = $isOld;
  }

  /**
   * Nom textuel.
   *
   * @return string
   */
  public function getName() {
    return $this->name;
  }

  /**
   * Région d'avant 2016
   *
   * @return boolean
   */
  public function getIsOld() {
    return $this->isOld;
  }

  /**
   * Départements associés à la région ou tableau vide si le nom de région est introuvable.
   * 
   * @param Region $region
   * @return Department[]
   */
  public function getDepartments($currentUser, $departmentRepo) {
    $departmentCodes = array();
    $lowerCaseName = strtolower($this->getName());
    if(!$this->getIsOld() && array_key_exists($lowerCaseName, $this::REGION_TO_DPT)) {
      $departmentCodes = $this::REGION_TO_DPT[$lowerCaseName];
    }
    else if ($this->getIsOld() && array_key_exists($lowerCaseName, $this::OLD_REGION_TO_DPT)) {
      // Ancienne région
      $departmentCodes = $this::OLD_REGION_TO_DPT[$lowerCaseName];
    }

    // Filtrage des CMA non accessibles à l'utilisateur courant
    return $departmentRepo->findByUserAndCode($currentUser, $departmentCodes);
  }

  /**
   * Fabrique de {@link Region} correspondant à un département.
   *
   * @param Department $dpt
   * @return Region|null
   */
  public static function buildFromDepartment(Department $dpt) {
    $name = null;
    foreach(Region::REGION_TO_DPT as $regionName => $dptCodes) {
      if(in_array($dpt->getCode(), $dptCodes)) {
        $name = $regionName;
        break;
      }
    }

    if($name) {
      return new Region($name, false);
    }
    return null;
  }
}

/**
 * La classe CraftspersonImportService définit un service permettant l'import d'un fichier de réparateurs.
 *
 * @author vdouillet
 */
class CraftspersonImportService {
  // Délimiteur de colonnes
  const CSV_DELIMITER = ';';

  // Header du fichier
  const EXPECTED_HEADER = array (
      'SIRET',
      'NOM_COMMERCIAL',
      'CODE_NAFA',
      'ADRESSE_LIGNE_1',
      'ADRESSE_LIGNE_2',
      'CODE_POSTAL',
      'LIBELLE_POSTAL',
      'EMAIL',
      'TELEPHONE',
      'SITE_INTERNET',
      'CMA_RATTACHEMENT',
      'DATE_FERMETURE',
      'LABELLISE_REPARACTEUR' 
  );

    // Nom des champs correspondants aux colonnes
    const COLUMN_FIELD = array (
        'siret'         => 0,
        'name'          => 1,
        'nafCode'       => 2,
        'address1'      => 3,
        'address2'      => 4,
        'zipCode'       => 5,
        'zipCodeLabel'  => 6,
        'email'         => 7,
        'phone'         => 8,
        'website'       => 9,
        'cmaCode'       => 10,
        'DATE_FERMETURE'=> 11,
        'isReparactor'  => 12
    );

  // Index des colonnes
  const INDEX_SIRET = 0;
  const INDEX_NAME = 1;
  const INDEX_NAF_CODES = 2;
  const INDEX_ADDRESS_1 = 3;
  const INDEX_ADDRESS_2 = 4;
  const INDEX_ZIPCODE = 5;
  const INDEX_ZIPCODE_LABEL = 6;
  const INDEX_EMAIL = 7;
  const INDEX_PHONE = 8;
  const INDEX_WEBSITE = 9;
  const INDEX_CMA = 10;
  const INDEX_CLOSE_DATE = 11;
  const INDEX_REPARACTOR = 12;

  // Statuts d'import
  const IMPORT_STATUS_CREATED = 0;
  const IMPORT_STATUS_UPDATED = 1;
  const IMPORT_STATUS_DELETED = 2;
  const IMPORT_STATUS_IGNORED = 3;
  const IMPORT_STATUS_INVALID = 4;

  const VALID_PHONE_SIZE = 10;
  
  const LOG_MESSAGE = array (
      self::IMPORT_STATUS_IGNORED => 'Fiche non mise à jour pour cause de droits d\'accès insuffisants',
      self::IMPORT_STATUS_INVALID => 'Fiche invalide' 
  );

  // Nommage des fichiers
  const DEPARTMENT_FILE_PREFIX = 'DEPARTEMENT';
  const OLD_REGION_FILE_PREFIX = 'ANCIENNE_REGION';

  // Dates de fermeture
  const CLOSE_DATE_FORMAT = 'd/m/Y';

  const AIN_OTHER_INFO = 'Plus d\'informations sur http://reparateurs.ain.fr';

  private $craftspersonRepo;
  private $departmentRepo;
  private $nafCodeRepo;
  private $importLogRepo;
  private $userRepo;
  private $entityManager;
  private $tokenStorage;
  private $validator;
  private $mailService;
  private $twig;

  /**
   * Constructeur.
   *
   * @param EntityRepository $craftspersonRepo
   * @param AuthorizationCheckerInterface $authorizationChecker
   */
  public function __construct(CraftspersonRepository $craftspersonRepo, EntityRepository $departmentRepo, EntityRepository $nafCodeRepo, ImportLogRepository $importLogRepo, UserRepository $userRepo, EntityManager $em, TokenStorageInterface $tokenStorage, ValidatorInterface $validator, MailService $mail, $twig) {
    $this->craftspersonRepo = $craftspersonRepo;
    $this->departmentRepo = $departmentRepo;
    $this->nafCodeRepo = $nafCodeRepo;
    $this->importLogRepo = $importLogRepo;
    $this->userRepo = $userRepo;
    $this->entityManager = $em;
    $this->tokenStorage = $tokenStorage;
    $this->validator = $validator;
    $this->mailService = $mail;
    $this->twig = $twig;
  }

  /**
   * Import d'un fichier de réparateurs avec mode de simulation optionnel.
   *
   * @param string $fileHandle
   * @param string $filename
   * @param boolean $checkOnly
   * @return array
   */
  public function import($fileHandle, $filename, $checkOnly) {
    // Valeurs de retour
    $importError = false;
    $logs = array ();
    $deleted = 0;
    $updated = 0;
    $created = 0;
    $ignored = 0;
    $invalid = 0;
    
    // Validation du nom de fichier et lecture des départements concernés par l'import.
    $region = $this->getRegionFromFilename($filename);
    $currentUser = $this->getUser();
    $department = $this->getDepartmentFromFilename($filename);
    $importDepartments = null;
    $importName = '';
    if($region) {
      $importDepartments = $region->getDepartments($currentUser, $this->departmentRepo);
      $importName = $region->getName();
    }
    else if($department) {
      if($this->cmaAccess($currentUser, $department)) {
        $importDepartments = array($department);
      }
      $tmpRegion = Region::buildFromDepartment($department);
      if($tmpRegion) {
        $importName = $tmpRegion->getName();
      }
      else {
        $importName = $department->getName();
      }
    }

    // Gestion des erreurs
    if($region == null && $department == null) {
      $this->addLog($logs, $filename, 0, null, 0, "Le nom de votre fichier est invalide. Veuillez vous réferer au livret d'accueil pour plus d'informations");
      $importError = true;
    }
    else if (empty($importDepartments)) {
      $username = $currentUser ? $currentUser->getUsername() : '';
      $this->addLog($logs, $filename, 0, $importName, 0, "Vous n'avez pas les droits pour importer des données pour cette zone géographique. Veuillez vous rapprocher de l'administrateur du site.");
      $importError = true;
    }
    
    // Lecture du fichier
    if (!$importError) {
      $isFirstRow = true;
      $line = 0;
      $readSiret = array();
      $callback = function($value) {
          return iconv( "Windows-1252", "UTF-8", $value );
      };
     
      while (($row = fgetcsv($fileHandle, 0, $this::CSV_DELIMITER)) !== FALSE) {

        $row = array_map($callback, $row );
        
        $line++;
        // La première ligne contient les descriptions de colonnes
        if ($isFirstRow) {
          if ($this->validateHeader($logs, $filename, $row)) {
            $isFirstRow = false;
            continue;
          }
          $importError = true;
          break;
        }

        // Import d'une ligne
        $importResult = $this->importRow($currentUser, $row, $checkOnly, $readSiret);
        $importStatus = $importResult['status'];
        switch ($importStatus) {
          case $this::IMPORT_STATUS_CREATED :
            $created++;
            break;
          case $this::IMPORT_STATUS_UPDATED :
            $updated++;
            break;
          case $this::IMPORT_STATUS_IGNORED :
            $this->addLog($logs, $filename, $line, $importName, $importResult['craftsperson']->getSiret(), $importStatus);
            $ignored++;
            break;
          case $this::IMPORT_STATUS_INVALID :
          default :
            $this->addLog($logs, $filename, $line, $importName, $importResult['craftsperson']->getSiret(), $importResult['errorMessage']);
            $errorMessages = $importResult['errorMessages'];
            foreach($errorMessages as $field => $errorMessage){
                $this->addLog($logs, $filename, $line, $importName, $importResult['craftsperson']->getSiret(), 'Colonne '.$this::EXPECTED_HEADER[$this::COLUMN_FIELD[$field]].' : '.$errorMessage);
            }

            $invalid++;
        }
        $readSiret[] = $importResult['craftsperson']->getSiret();
      }

      // Suppression des artisans qui ne sont pas dans le fichier
      if($checkOnly || $importError) {
        $deleted = $this->craftspersonRepo->countNumberToCleanup($readSiret, $importDepartments);
      }
      else if(!$importError) {
        $deleted = $this->craftspersonRepo->cleanup($readSiret, $importDepartments);
      }
      
      // Commit les modifications en bdd
      if (!$checkOnly && !$importError) {
        $this->entityManager->flush();
      }
    }

    $result = array (
        'result' => array (
            'created' => $created,
            'updated' => $updated,
            'deleted' => intval($deleted),
            'ignored' => $ignored,
            'invalid' => $invalid,
            'error' => $importError,
            'logs' => $logs 
        ) 
    );

    // Rapport d'import
    if(!$checkOnly) {
      $this->sendReport($result, $importDepartments);
    }
    
    return $result;
  }

  /**
   * Envoi d'un rapport d'import
   *
   * @param array $reportData
   * @param Departments[] $importDepartments
   */
  private function sendReport($reportData, $importDepartments) {
    // Destinataires basés sur les départements concernés par l'import
    $requestResult = $this->userRepo->findMailForImportReport($importDepartments);

    // Applatissement du résultat de la requête
    $mailRecipients = array();
    foreach($requestResult as $res) {
      $mailRecipients[] = $res['email'];
    }

    // Envoi du mail
    if(!empty($mailRecipients)) {
      $subject = 'Annuaire des réparateurs - Rapport d\'import de réparateurs';
      $body = $this->twig->render('ARCommonBundle:Emails:import-report.txt.twig', $reportData);
      $this->mailService->sendMail($mailRecipients, $subject, $body);
    }
  }

  /**
   * Lecture de la région depuis le nom du fichier.
   *
   * @param string filename
   * @return Region
   */
  private function getRegionFromFilename($filename) {
    $parsedName = array ();
    if (!preg_match("/(\w+)_([^_]+).csv$/i", $filename, $parsedName)) {
      return null;
    }
    else {
      $name = strtolower($parsedName[2]);
      $isOld = $parsedName[1] == $this::OLD_REGION_FILE_PREFIX;
      if(($isOld && array_key_exists($name, Region::OLD_REGION_TO_DPT)) || (!$isOld && array_key_exists($name, Region::REGION_TO_DPT))) {
        return new Region($name, $isOld);
      }
      return null;
    }
  }

  /**
   * Lecture du département depuis le nom du fichier.
   *
   * @param string filename
   * @return Department
   */
  private function getDepartmentFromFilename($filename) {
    $result = null;
    $parsedName = array();
    if (preg_match("/(\w+)_([^_]+).csv$/i", $filename, $parsedName) && $parsedName[1] == $this::DEPARTMENT_FILE_PREFIX) {
      $result = $this->departmentRepo->find($parsedName[2]);
    }

    return $result;
  }

  /**
   * Import d'une ligne du fichier.
   *
   * @param User $currentUser
   * @param array $row
   * @param boolean $checkOnly
   * @param array $readSiret : tableau contenant tous les numéros de sirets précédemment importés
   * @return array
   */
  private function importRow(User $currentUser = null, $row, $checkOnly, $readSiret) {
    $status = $this::IMPORT_STATUS_INVALID;
    $errorMessage = 'Fiche invalide';
    //listOfErrors associe le numéro de la colonne avec la description de l'erreur
    $listOfErrors = array();
    $errorMessages = array();

    // Validation de la fiche avec le groupe de validation spécifique à l'import
    $craftsperson = $this->createEntityFromRow($row);
    $validationErrors = $this->validator->validate($craftsperson, null, array('import'));
    if(count($validationErrors)){
        /** @var ConstraintViolation $validationError */
        foreach($validationErrors as $validationError){
            $field = $validationError->getPropertyPath();
            $message = $validationError->getMessage();
            $errorMessages[$field].= $errorMessages[$field]? ", ".$message:$message;
        }
    }
    $validationErrorCount = count($validationErrors);

    //Vérification des codes NAFA
    $nbNafCodesRow = count(explode('/', $row[$this::INDEX_NAF_CODES]));
    $nbNafCodesCraftPerson = count($craftsperson->getNafCode());

    //Si les nafs codes n'ont pas été ajoutés aux craftperson pour chaque nafCode saisi, 
    //l'entité nafCode correspondante n'a pas été trouvée (ou a été déjà ajoutée) donc le naf code n'était pas valide
    if($nbNafCodesRow !== $nbNafCodesCraftPerson) {
      // Erreur sur le naf code
        $errorMessages['nafCode']="Code NAFA Invalide";
      $status = $this::IMPORT_STATUS_INVALID;
      $validationErrorCount++;
    }

    //Vérification de la date de fermeture
    $now = new \DateTime();
    $closeDate = $this->getCloseDate($row);
    if($closeDate !== false && is_string($closeDate)) {
          // Erreur de format de date
        $errorMessages['DATE_FERMETURE']="Erreur de format de date";
          $status = $this::IMPORT_STATUS_INVALID;
          $validationErrorCount++;
    }

    //Vérification que ce numéro de siret n'existe pas déjà dans ce même fichier
    if(in_array($row[$this::INDEX_SIRET], $readSiret)) {
      $status = $this::IMPORT_STATUS_INVALID;
      $validationErrorCount++;
      $errorMessage = 'Fiche invalide : ' . $validationErrorCount . ' erreur(s)';
      $errorMessages['siret'] = 'Une fiche trouvée avec un numéro de SIRET déjà existant dans le fichier';
    } else {
      $errorMessage = 'Fiche invalide : ' . $validationErrorCount . ' erreur(s)';
    }

    if ($validationErrorCount === 0) {
      if ($this->cmaAccess($currentUser, $craftsperson->getCmaCode())) {
        /** @var \ARCommonBundle\Entity\Craftsperson $craftsperson */
        $existingCraftsperson = $this->craftspersonRepo->findOneBy(
            array (
                'siret' => $craftsperson->getSiret(),
                'cmaCode' => $craftsperson->getCmaCode()
            ));
        if ($existingCraftsperson) {
          // Pas de modification si la fiche a été mise à jour manuellement
          if (!$existingCraftsperson->getIsUpdated()) {
            if (!$checkOnly) {
              $this->craftspersonRepo->update($existingCraftsperson, $craftsperson);
            }
            $status = $this::IMPORT_STATUS_UPDATED;
          }
          else {
              if (!$checkOnly) {
                  $this->craftspersonRepo->complete($existingCraftsperson, $craftsperson);
              }
              $status = $this::IMPORT_STATUS_UPDATED;
          }
        }
        else {
          if (!$checkOnly) {
            $this->entityManager->persist($craftsperson);
          }
          $existingCraftsperson = $craftsperson;
          $status = $this::IMPORT_STATUS_CREATED;
        }
        
        // Vérification du besoin de masquage
        if($closeDate && $existingCraftsperson->getIsEnabled() && $closeDate < $now) {
          if(!$checkOnly) {
            $existingCraftsperson->setIsEnabled(false);
          }
          if($status !== $this::IMPORT_STATUS_CREATED) {
            $status = $this::IMPORT_STATUS_UPDATED;
          }
        }
      }
      else {
        $status = $this::IMPORT_STATUS_IGNORED;
      }
    }
    
    return array (
        'status' => $status,
        'craftsperson' => $craftsperson,
        'errorMessage' => $errorMessage,
        'errorMessages' => $errorMessages
    );
  }

  /**
   * Création d'un réparateur à partir d'une ligne du fichier.
   *
   * @param array $row
   * @return \ARCommonBundle\Entity\Craftsperson
   */
  private function createEntityFromRow($row) {
    // Récupération du département
    $departmentCode = $row[$this::INDEX_CMA];
    $formattedDepartmentCode = sprintf("%02d", $departmentCode);
    $department = $this->departmentRepo->find($formattedDepartmentCode);
    
    // Récupération des codes NAFA
    $nafCodes = explode('/', $row[$this::INDEX_NAF_CODES]);
    $nafCodeArray = array();
    $addedNafCodes = array();

    foreach($nafCodes as $nafCodeCode) {
      $cleanCode = preg_replace('/\s/', '', $nafCodeCode);
      $entity = $this->nafCodeRepo->find($cleanCode);
      //Si le naf code est valide et qu'il n'a pas déjà été ajouté
      if($entity && !in_array($entity->getCode(), $addedNafCodes)) {
        $nafCodeArray[] = $entity;
        $addedNafCodes[] = $cleanCode;
      }
    }

    // Nettoyage du siret
    $siret = preg_replace('/\s/', '', $row[$this::INDEX_SIRET]);

    // Nettoyage du téléphone (on ne conserve que les chiffres que l'on sépare ensuite par une virgule s'il y en a plusieurs)
    $phone = preg_replace('~\D~', '', $row[$this::INDEX_PHONE]);
    $phoneSize = strlen($phone);
    $moreThanOnePhoneCondition = $phoneSize > $this::VALID_PHONE_SIZE && fmod($phoneSize, $this::VALID_PHONE_SIZE) == 0;
    
    // Si plusieurs téléphones, on les séparent par une virgule
    if($moreThanOnePhoneCondition) {
      $phone = wordwrap($phone, $this::VALID_PHONE_SIZE, ",", true);
    }
    
    $craftsperson = $this->craftspersonRepo->create($siret, $row[$this::INDEX_NAME], $nafCodeArray, $row[$this::INDEX_ADDRESS_1], $row[$this::INDEX_ADDRESS_2], $row[$this::INDEX_ZIPCODE], $row[$this::INDEX_ZIPCODE_LABEL], $row[$this::INDEX_EMAIL], 
      $phone, $row[$this::INDEX_WEBSITE], $department, strtoupper(trim($row[$this::INDEX_REPARACTOR])) == 'OUI');

    // Cas particulier Ain
    if($department && $department->getCode() == '01') {
      $craftsperson->setOtherInfo($this::AIN_OTHER_INFO);
    }
    return $craftsperson;
  }

  /**
   * Vérifier l'accès au CMA.
   * Les utilisateurs administrateurs ont accès à tous les CMA, les utilisateurs CMA n'accèdent qu'à leur CMA.
   *
   * @param User $user
   * @param Department $cma
   * @return boolean
   */
  private function cmaAccess(User $user = null, Department $cma) {
    if ($user) {
      return $user->hasRole(Role::ADMIN) || $user->getCodeDepartment()->contains($cma);
    }
    // Pas d'utilisateur = mode CLI
    return true;
  }

  /**
   * Utilisateur courant.
   *
   * @return User
   */
  private function getUser() {
    $user = null;
    $token = $this->tokenStorage->getToken();
    if ($token) {
      $user = $token->getUser();
    }
    return $user;
  }

  /**
   * Valider l'en-tête du fichier.
   *
   * @param array $logs
   * @param string $filename
   * @param array $header
   * @return boolean
   */
  private function validateHeader(&$logs, $filename, $header) {
    $expectedColumnCount = count($this::EXPECTED_HEADER);
    $isValid = count($header) >= $expectedColumnCount;
    if ($isValid) {
      for($i = 0; $i < $expectedColumnCount && $isValid; $i++) {
        $isValid = $isValid && $header[$i] === $this::EXPECTED_HEADER[$i];
      }
    }
    
    if (!$isValid) {
      $this->addLog($logs, $filename, 1, '', 0, 'Les colonnes ne correspondent pas aux colonnes attendues');
    }
    
    return $isValid;
  }

  /**
   * Ajout d'un log d'import.
   *
   * @param array $logs
   * @param string $filename
   * @param integer $line
   * @param string $region
   * @param integer $siret
   * @param mixed $importStatus
   */
  private function addLog(&$logs, $filename, $line, $region = null, $siret, $importStatus) {
    if (array_key_exists($importStatus, $this::LOG_MESSAGE)) {
      $message = $this::LOG_MESSAGE[$importStatus];
    }
    else {
      $message = $importStatus;
    }
    
    if (!empty($message)) {
      $log = $this->importLogRepo->create($filename, $line, $message, $region, $siret);
      if ($log) {
        $this->entityManager->persist($log);
        $this->entityManager->flush($log);
        $logs[] = $log;
      }
    }
  }

  /**
   * Retourne la date de fermeture pour une ligne du fichier à importer.
   *
   * Retourne faux si la date est vide ou un message d'erreur si elle est invalide.
   *
   * @param array $row
   * @return \DateTime|boolean|string
   */
  private function getCloseDate($row)
  {
    $dateString = $row[$this::INDEX_CLOSE_DATE];
    if($dateString) {
      $dateTime = \DateTime::createFromFormat($this::CLOSE_DATE_FORMAT, $dateString);
      $errors = \DateTime::getLastErrors();
      if (!empty($errors['warning_count'])) {
        return array_values($errors['warnings'])[0];;
      }
      return $dateTime;
    }
    return false;
  }
}