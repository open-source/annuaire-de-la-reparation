<?php

namespace ARCommonBundle\Service;

/**
 * L'interface GeocoderInterface définit les services de géoréférencement.
 *
 * @author vdouillet
 *        
 */
interface GeocoderInterface {
    /**
     * Conversion d'une adresse complète en coordonnées lat/lon.
     *
     * @param string $address
     * @param string $zipCode
     * @param string $city
     * @return array|null
     */
    public function geocodeAddress($address, $zipCode, $city);

    /**
     * Conversion d'une adresse partielle en coordonnées lat/lon.
     *
     * @param string $zipCode
     * @param string $city
     * @return array|null
     */
    public function geocodePartialAddress($zipCode, $city);

    /**
     * Conversion d'une requête quelconque en coordonnées lat/lon.
     *
     * @param string $query
     * @return array|null
     */
    public function geocodeQuery($query);
}