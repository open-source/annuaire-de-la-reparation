<?php

namespace ARCommonBundle\Service;

use ARCommonBundle\Entity\Craftsperson;
use ARCommonBundle\Entity\CraftspersonLike;
use ARCommonBundle\Enum\Entity;
use Doctrine\ORM\EntityManagerInterface;

/**
 * La classe CraftspersonService définit les méthodes de gestion des entités {@link Craftsperson}.
 * 
 * @author vdouillet
 */
class CraftspersonService {
    /** @var \Doctrine\ORM\EntityManagerInterface $em */
    private $em;
    /** @var string $confirmAddress */
    private $confirmAddress;
    /** @var MailService $mailService */
    private $mailService;
    private $twig;

    /**
     * Constructeur.
     * 
     * @param EntityManagerInterface $em
     * @param MailService $mailService
     * @param object $twig
     * @param string $confirmAddress
     */
    public function __construct(EntityManagerInterface $em, MailService $mailService, $twig, $confirmAddress) {
        $this->em = $em;
        $this->mailService = $mailService;
        $this->twig = $twig;
        $this->confirmAddress = $confirmAddress;
    }

    /**
     * Création d'une demande de like non confirmée pour le réparateur et email fournis.
     * 
     * @param Craftsperson $craftsperson
     * @param string $email
     * @return true en cas de succès, false sinon
     */
    public function createLikeRequest(Craftsperson $craftsperson, $email) {
        $repo = $this->em->getRepository(Entity::CRAFTSPERSON_LIKE);

        /** @var \ARCommonBundle\Entity\CraftspersonLike $existingRequest */
        $existingRequest = $repo->findOneBy(array(
            'idCraftsperson' => $craftsperson,
            'email' => $email
        ));

        // Si pas de requête existante, on en créé une nouvelle
        $request = $existingRequest ? $existingRequest : $this->buildNewRequest($craftsperson, $email);

        // Requête déjà confirmée -> erreur
        if($request->getIsConfirmed()) {
            return false;
        }

        // Enregistrement de la requête et envoi du mail de confirmation
        $this->em->persist($request);
        $this->em->flush();
        $this->sendConfirmEmail($craftsperson, $request->getEmail(), $request->getConfirmationToken());

        return true;
    }

    /**
     * Confirmation d'une demande de like.
     * 
     * @param CraftspersonLike $existingRequest
     * @return true en cas de succès, false sinon
     */
    public function confirmRequest(CraftspersonLike $existingRequest) {
        if($existingRequest->getIsConfirmed()) {
            return false;
        }

        $existingRequest->setIsConfirmed(true);
        $craftsperson = $existingRequest->getIdCraftsperson();
        $craftsperson->setLikes($craftsperson->getLikes() + 1);
        $this->em->flush($craftsperson);
        $this->em->flush($existingRequest);

        return true;
    }

    /**
     * Construction d'une nouvelle demande de like.
     * 
     * @param Craftsperson $craftsperson
     * @param string $email
     * @return CraftspersonLike
     */
    private function buildNewRequest(Craftsperson $craftsperson, $email) {
        $request = new CraftspersonLike();
        $request->setIdCraftsperson($craftsperson);
        $request->setEmail($email);
        $token = rtrim(strtr(base64_encode(random_bytes(32)), '+/', '-_'), '=');
        $request->setConfirmationToken($token);

        return $request;
    }

    /**
     * Envoi d'un email de confirmation de like.
     * 
     * @param Craftsperson $craftsperson
     * @param string $email
     * @param string $token
     */
    private function sendConfirmEmail(Craftsperson $craftsperson, $email, $token) {
        $confirmAddress = str_replace('{:token}', $token, $this->confirmAddress);
        $subject = sprintf("Recommandation du réparateur %s sur l'Annuaire des réparateurs", $craftsperson->getName());
        $body = $this->twig->render('ARCommonBundle:Emails:like.txt.twig', array(
            'craftsperson' => $craftsperson,
            'confirmAddress' => $confirmAddress
        ));

        $this->mailService->sendMail($email, $subject, $body);
    }

    /**
     * Signaler une erreur sur le réparateur.
     * 
     * @param Craftsperson $craftsperson
     * @param array $errors
     */
    public function errorReport(Craftsperson $craftsperson, $errors) {
        /** @var \ARCommonBundle\Entity\UserRepository $userRepo */
        $userRepo = $this->em->getRepository(Entity::USER);
        $destMails = $userRepo->findMailForErrorReport($craftsperson->getCmaCode());
        // Applatissement du résultat de la requête des emails
        $mailRecipients = array();
        foreach($destMails as $res) {
            $mailRecipients[] = $res['email'];
        }

        $craftsperson->setIsError(true);
        $this->em->flush($craftsperson);
        $subject = sprintf("Annuaire des réparateurs : signalement d'erreur pour le réparateur %s", $craftsperson->getName());
        // ATTENTION la description et les données du réparateur devraient être nettoyée pour ne pas accepter de HTML/JS si le template est HTML.
        // C'est inutile ici car le mail est envoyé au format texte
        $body = $this->twig->render('ARCommonBundle:Emails:error-report.txt.twig', array(
            'craftsperson' => $craftsperson,
            'errorDescription' => $errors
        ));

        $this->mailService->sendMail($mailRecipients, $subject, $body);
    }

    /**
     * Envoi d'un message au réparateur.
     *
     * @param Craftsperson $craftsperson
     * @param array $errors
     */
    public function messageCraftperson(Craftsperson $craftsperson, $sender, $message) {
        $subject = sprintf("Annuaire des réparateurs : %s, vous avez reçu un message", $craftsperson->getName());
        // ATTENTION la description et les données du réparateur devraient être nettoyée pour ne pas accepter de HTML/JS si le template est HTML.
        // C'est inutile ici car le mail est envoyé au format texte
        $body = $this->twig->render('ARCommonBundle:Emails:contact.txt.twig', array(
            'craftsperson' => $craftsperson,
            'sender' => $sender,
            'message' => $message
        ));

        $this->mailService->sendMail(array(0=>$craftsperson->getEmail()), $subject, $body);
    }
}