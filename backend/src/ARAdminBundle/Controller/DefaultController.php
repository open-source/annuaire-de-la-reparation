<?php

namespace ARAdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class DefaultController extends Controller
{
    /**
     * @Route("/")
     * @Route("/{ngroute}")
     * @Route("/{ngroute1}/{ngroute2}")
     * @Route("/{ngroute1}/{ngroute2}/{ngroute3}")
     * @Route("/{ngroute1}/{ngroute2}/{ngroute3}/{ngroute4}")
     */
    public function indexAction()
    {
        return $this->render('ARAdminBundle:Default:index.html.twig');
    }
}
