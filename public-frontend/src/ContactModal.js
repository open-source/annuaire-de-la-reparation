import React from 'react';
import ReactDOM from "react-dom";
import Modal from 'react-modal';
import $ from "jquery";

const config = require('Config');

// Length of the message
const MESSAGE_MIN_LENGTH = 50;
const MESSAGE_MAX_LENGTH = 1000;

// Initial state
const INITIAL_STATE = {
    isFormSent: false,
    isMessageValid: true, // Do not display validation error until input is modified
    isEmailValid: true, // Do not display validation error until input is modified
    isRequestValid: true
};

function isMessageValid(message) {
    return message.length >= MESSAGE_MIN_LENGTH && message.length <= MESSAGE_MAX_LENGTH;
}

function isEmailValid(email) {
    /** Expression régulière pour la validation d'adresses mail */
    const EMAIL_REGEXP = /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)$/;
    return EMAIL_REGEXP.test(String(email).toLowerCase());
}
/**
 * Component that displays a modal dialog to report an error on a crafstperson.
 */
export default class ContactModal extends React.Component {

    /**
     * Constructor.
     * 
     * @param {*} props properties
     */
    constructor(props) {
        super(props);
        this.state = INITIAL_STATE;
        this.message = null;
        this.email = null;

        this.onMessageChange = this.onMessageChange.bind(this);
        this.onEmailChange = this.onEmailChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
        this.onClose = this.onClose.bind(this);
    }

    /**
     * Handle a change of the error description text.
     * 
     * @param {*} event 
     */
    onMessageChange(event) {
        const value = event.target.value;

        const isValid = isMessageValid(value);
        this.message = value;

        this.setState({
            isMessageValid: isValid
        });
    }


    /**
     * Handle a change of the error description text.
     *
     * @param {*} event
     */
    onEmailChange(event) {
        const value = event.target.value;

        const isValid = isEmailValid(value);
        this.email = value;

        this.setState({
            isEmailValid: isValid
        });
    }

    /**
     * User submit error report.
     * 
     * @param {*} event 
     */
    onSubmit(event) {
        const that = this;
        event.preventDefault();

        $.post(config.apiBaseUrl + "craftspersons/" + this.props.craftsperson.id + "/contact", {
            email: this.email,
            message: this.message
        }).fail(function (data) {
            // On error 400, display validation errors, otherwise display general server error
            that.setState({
                isFormSent: data.status != 400,
                isRequestValid: data.status == 400
            });
            if(data.status == 400) {
                that.setState({
                    isMessageValid: isMessageValid(this.message),
                    isEmailValid: isEmailValid(this.email)
                });
            }
        })
        .done(function () {
            that.setState({
                isFormSent: true,
                isRequestValid: true,
                isMessageValid: true,
                isEmailValid: true,
            });
        });
    }

    /**
     * Close the modal.
     */
    onClose() {
        this.message = null;
        this.email = null;
        this.setState(INITIAL_STATE);
        this.props.onClose();
    }

    /**
     * Render component.
     */
    render() {
        const c = this.props.craftsperson;
        let modalContent;
        if(this.state.isFormSent && this.state.isRequestValid && this.state.isMessageValid && this.state.isEmailValid) {
            modalContent = 
                <div className="modal-content" id="contact-modal">
                    <div className="modal-header">
                            <h3>Message envoyé</h3>
                    </div>
                    <div className="modal-body">
                        <p>Votre message a été envoyé à l'artisan.</p>
                    </div>
                    <div className="modal-footer">
                        <div className="btn-toolbar pull-right">
                            <button className="btn btn-primary" type="button" title="Fermer" onClick={this.onClose}>Fermer</button>
                        </div>
                    </div>
                </div>;
        }
        else if (this.state.isFormSent && !this.state.isRequestValid) {
            modalContent = 
                <div className="modal-content" id="contact-modal">
                    <div className="modal-header">
                            <h3>Erreur lors de l'envoie du message</h3>
                    </div>
                    <div className="modal-body">
                        <p>Une erreur est survenue lors de la soumission de votre message. Merci de réessayer.</p>
                    </div>
                    <div className="modal-footer">
                        <div className="btn-toolbar pull-right">
                            <button className="btn btn-primary" type="button" title="Fermer" onClick={this.onClose}>Fermer</button>
                        </div>
                    </div>
                </div>;
        }
        else {

            modalContent =
                <div className="modal-content" id="contact-modal">
                    <form className="form-horizontal" onSubmit={this.onSubmit}>
                        <div className="modal-header">
                            <h3>Envoyer un message</h3>
                        </div>
                        <div className="modal-body">
                            <p>Merci de bien vouloir saisir votre message dans la zone de texte ci-dessous.</p>
                            <div className="form-group">
                                <div className="col-sm-4">
                                    <label className="control-label" htmlFor="name">Nom de l'artisan :&nbsp;</label>
                                </div>
                                <div className="col-sm-8">
                                    <input type="text" className="width100" name="name" title="Nom de l'artisan" value={c!=undefined? c.name:''} readOnly disabled/>
                                </div>
                            </div>
                            <div className="form-group">
                                <div className="col-sm-4">
                                    <label className="control-label" htmlFor="cma">Département :&nbsp;</label>
                                </div>
                                <div className="col-sm-8">
                                    <input type="text" className="width100" name="cma" title="Département" value={c!=undefined? c.department:''} readOnly disabled/>
                                </div>
                            </div>
                            <div className="form-group">
                                <div className="col-sm-4">
                                    <label className="control-label" htmlFor="email">Votre adresse email<sup className="infoRGPD">*</sup> :&nbsp;</label>
                                </div>
                                <div className="col-sm-8">
                                    <input type="text" className="width100" name="email" onChange={this.onEmailChange}/>
                                    {!this.state.isEmailValid &&
                                    <div className="alert alert-danger">
                                        Adresse email non valide.
                                    </div>
                                    }
                                </div>

                            </div>
                            <div className="form-group">
                                <div className="col-sm-12">
                                    <label className="control-label" htmlFor="message">Contenu du message :&nbsp;</label>
                                    <textarea className="error-textarea" rows="4" name="message" onChange={this.onMessageChange}></textarea>
                                    {!this.state.isMessageValid &&
                                    <div className="alert alert-danger">
                                        Le message doit contenir entre {MESSAGE_MIN_LENGTH} et {MESSAGE_MAX_LENGTH} caractères.
                                    </div>
                                    }
                                </div>
                            </div>

                            <div className="infoRGPD">
                                * L’adresse électronique fournie dans ce formulaire sera transmise à l'artisan sélectionné et supprimée de l'application. <em>Aucune donnée personnelle collectée n’est stockée dans les bases de données de .</em>
                            </div>
                        </div>
                        <div className="modal-footer">
                            <div className="btn-toolbar pull-right">
                                <button className="btn btn-primary" type="submit" title="Valider" disabled={this.message == null || !this.state.isMessageValid || this.email == null || !this.state.isEmailValid}>Envoyer</button>
                                <button className="btn btn-default" type="button" title="Fermer" onClick={this.onClose}>Annuler</button>
                            </div>
                        </div>
                    </form>
                </div>;
        }

        return (
            <Modal isOpen={this.props.isOpen} contentLabel="ContactModal" className={{ base: 'react-modal' }} overlayClassName={{ base: 'react-modal-overlay' }}>
                {modalContent}
            </Modal>
        );
    }
}