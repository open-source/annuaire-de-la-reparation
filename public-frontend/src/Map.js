import React from "react";
import $ from "jquery";

const config = require('Config');
const GoogleMapsLoader = require('google-maps'); // only for common js environments
GoogleMapsLoader.LIBRARIES = ['places'];
GoogleMapsLoader.LANGUAGE = 'fr';

let buildUrlParamString = require('./Utils.js').buildUrlParamString;
let getBoundsZoomLevel = require('./Utils.js').getBoundsZoomLevel;

export default class Map extends React.Component {

    constructor(props) {
        super(props);

        //Properties
        this.map = null;
        this.bounds = null;
        this.isInfoWindowOpen = false;
        this.markerCluster = null;
        this.infoWindow = null;
        this.markerClusterOptions = {
            styles: [{
                height: 53,
                url: config.imgRelativePath + "marker-group-1.png",
                width: 53
            },
            {
                height: 53,
                url: config.imgRelativePath + "marker-group-1-selected.png",
                width: 53
            },
            {
                height: 56,
                url: config.imgRelativePath + "marker-group-2.png",
                width: 56
            },
            {
                height: 56,
                url: config.imgRelativePath + "marker-group-2-selected.png",
                width: 56
            },
            {
                height: 66,
                url: config.imgRelativePath + "marker-group-3.png",
                width: 66
            },
            {
                height: 66,
                url: config.imgRelativePath + "marker-group-3-selected.png",
                width: 66
            },
            {
                height: 78,
                url: config.imgRelativePath + "marker-group-4.png",
                width: 78
            },
            {
                height: 78,
                url: config.imgRelativePath + "marker-group-4-selected.png",
                width: 78
            },
            {
                height: 90,
                url: config.imgRelativePath + "marker-group-5.png",
                width: 90
            },
            {
                height: 90,
                url: config.imgRelativePath + "marker-group-5-selected.png",
                width: 90
            }]
        };

        this.createRepairerMarker = this.createRepairerMarker.bind(this);
        this.createUserMarker = this.createUserMarker.bind(this);
        this.updateInfoWindow = this.updateInfoWindow.bind(this);
        this.updateVisibleMarkers = this.updateVisibleMarkers.bind(this);
        this.onInfoWindowClose = this.onInfoWindowClose.bind(this);
    }

    componentDidMount() {
        let that = this;
        $.getJSON(config.apiBaseUrl + "parameters").done(function (data) {
            GoogleMapsLoader.KEY = data.google_maps_api_key;

            //load map
            GoogleMapsLoader.load(function (google) {
                //Init google map
                let map = new google.maps.Map(that.refs.map, {
                    center: { lat: that.props.lat, lng: that.props.lng },
                    zoom: that.props.zoom
                });
                that.map = map;
                if(that.props.isSearchClosest) {
                    that.bounds = new google.maps.LatLngBounds();
                }

                //Notify parent component on zoom change
                map.addListener('zoom_changed', function () {
                    that.props.onZoomChanged(map.getZoom());
                });

                //Create only one info windows
                that.infoWindow = new google.maps.InfoWindow({
                    content: ''
                });

                that.infoWindow.addListener('closeclick', () => that.onInfoWindowClose());

                //Close info window on map click and zoom
                let closeInfoWindow = function () {
                    that.onInfoWindowClose();
                    that.infoWindow.close();
                };
                map.addListener('click', closeInfoWindow);
                map.addListener('zoom_changed', closeInfoWindow);

                //Create a marker for user
                that.createUserMarker(map);

                //Add a marker clusterer and update visible markers.
                that.markerCluster = new MarkerClusterer(map, [], that.markerClusterOptions);

                //Dans les styles figurent un aspect selected et un aspect non selected pour chaque style,
                //On modifie la fonction calculant l'index du style du cluster afin de le prendre en compte
                that.markerCluster.setCalculator(function(markers, numStyles) {
                    var index = 0;
                    var count = markers.length;
                    var dv = count;
                    while (dv !== 0) {
                        dv = parseInt(dv / 10, 10);
                        index++;
                    }

                    index = Math.min(index, numStyles / 2);
                    var containsHovered = false;

                    var hoveredRepairer = that.props.hoveredRepairerResult;
                    if (hoveredRepairer !== null) {
                        markers.forEach(marker => {
                            if (marker.repairer.id == hoveredRepairer.id) {
                                containsHovered = true;
                            }
                        });
                    }

                    index = containsHovered ? index*2 : index*2-1;
                    return {
                        text: count,
                        index: index
                    };
                });
                that.updateVisibleMarkers();

                /*function setRightZoom(){
                    let dimensions = null;
                    dimensions = {width: $("#map-area").width(), height: $("#map-area").height()};

                    var markers = that.markerCluster.getMarkers();

                    var bnds = new google.maps.LatLngBounds();
                    for (var i = 0; i < markers.length; i++) {
                        bnds.extend(markers[i].getPosition());
                    }

                    if(dimensions != null) {
                        let zoom = getBoundsZoomLevel(bnds, dimensions);
                        that.map.setZoom(zoom);
                        console.log(zoom);
                        google.maps.event.trigger(map, 'resize');
                    }
                }
                window.onresize = function(){
                    setRightZoom();
                };*/


                /*var markers = that.markerCluster.getMarkers();

                var bnds = new google.maps.LatLngBounds();
                for (var i = 0; i < markers.length; i++) {
                    bnds.extend(markers[i].getPosition());
                };

                that.map.fitBounds(bnds);*/


                if(that.bounds && that.props.isSearchClosest) {
                    let dimensions = null;
                    dimensions = {width: $("#map-area").width(), height: $("#map-area").height()};
                    
                    if(dimensions != null) {
                        let zoom = getBoundsZoomLevel(that.bounds, dimensions);
                        that.map.setZoom(zoom - 1);
                        google.maps.event.trigger(map, 'resize');
                    }
                }
            });
        });
    }

    componentDidUpdate() {
        this.updateVisibleMarkers();
        this.updateSelectedMarker();
    }

    static getRadiusFromZoom(latitude, zoom) {
        let meterPerPixel = 156543.03392 * Math.cos(latitude * Math.PI / 180) / Math.pow(2, zoom);
        let pixelSize = Math.max(document.getElementById('map-area').clientWidth, document.getElementById('map-area').clientWidth);
        return meterPerPixel * pixelSize / 2;
    }

    onInfoWindowClose() {
        this.isInfoWindowOpen = false;
        this.props.onRepairerHover(null);
    }

    createUserMarker(map) {
        let marker = new google.maps.Marker({
            map: map,
            position: new google.maps.LatLng(this.props.posLat, this.props.posLng),
            title: 'Ma position',
            label: '',
            icon: config.imgRelativePath + 'marker-user.png'
        });
        if(this.bounds && this.props.isSearchClosest) {
            this.bounds.extend(marker.getPosition());
        }
        return marker;
    }

    createRepairerMarker(that, repairer) {
        let iconName = repairer.isReparactor === true ? 'marker-reparacteur.png' : 'marker-artisan.png';
        let title = repairer.isReparactor === true ? "Un artisan qui s’engage à tout mettre en œuvre pour réparer votre objet" : "Artisan";
        let marker = new google.maps.Marker({
            map: that.map,
            position: new google.maps.LatLng(repairer.latitude, repairer.longitude),
            label: '',
            title: title,
            icon: config.imgRelativePath + iconName,
            repairer: repairer
        });

        //Open info window on marker click
        marker.addListener('click', function () {
            that.isInfoWindowOpen = true;
            that.props.onRepairerHover(marker.repairer);

            //Update info window
            that.updateInfoWindow(that.infoWindow, repairer);
            that.infoWindow.open(that.map, marker);
        });

        //Inform parent component on mouseover/mouseout on a marker
        marker.addListener('mouseover', function () {
            if (!that.isInfoWindowOpen) {
                that.props.onRepairerHover(marker.repairer);
            }
        });
        marker.addListener('mouseout', function () {
            if (!that.isInfoWindowOpen) {
                that.props.onRepairerHover(null);
            }
        });

        if(that.bounds && that.props.isSearchClosest) {
            that.bounds.extend(marker.getPosition());
            this.props.key = Math.random();
        }

        return marker;
    }

    updateInfoWindow(infoWindow, repairer) {
        let config = require('Config');
        let urlParams = [];
        if (this.props.category) {
            urlParams.push({
                name: 'category',
                value: this.props.category
            });
        }
        if (this.props.address) {
            urlParams.push({
                name: 'address',
                value: this.props.address
            });
        }
        let repairerLink = '/reparateur/' + repairer.id + buildUrlParamString(urlParams);

        let infoWindowContent =
            '<div class="infowindow">' +
            '<div class="libelle-block dark-red">' + repairer.name +
            '<span class="category-icons">';
        infoWindowContent = infoWindowContent +
            '</span></div>' +
            '<div class="clear" />' +
            '<div>' +
            repairer.address1 +
            '<br/>' +
            repairer.zipCode + ' ' + repairer.zipCodeLabel.toUpperCase() +
            '</div><div class="clear" /><div>' +
            '<span><i class="glyphicon glyphicon-earphone"></i>&nbsp;&nbsp;' + repairer.phone +
            '</span>' +
            (repairer.email ? ' - <span><i class="glyphicon glyphicon-envelope"></i></span>' : '') +
            '</div>' +
            '<span class="pull-right"><a href="' + repairerLink + '" class="dark-green">Voir la fiche</a></span>' +
            '</div>';

        infoWindow.setContent(infoWindowContent);
    }

    updateVisibleMarkers() {
        let that = this;
        if (this.map != null && this.markerCluster != null) {
            let toRemove = [];
            let toAdd = [];

            //Compute markers to add
            this.props.repairers.forEach(function (repairer) {
                if (!that.markerCluster.getMarkers().some(function (marker) {
                    return repairer.id == marker.repairer.id;
                })) {
                    GoogleMapsLoader.load(function (google) {
                        toAdd.push(that.createRepairerMarker(that, repairer));
                    });
                }
            });

            // Compute markers to remove
            this.markerCluster.getMarkers().forEach(marker => {
                if (!that.props.repairers.some(repairer => repairer.id == marker.repairer.id)) {
                    toRemove.push(marker);
                }
            });

            //Update marker clusterer
            toRemove.forEach(marker => {
                that.markerCluster.removeMarker(marker);
                marker.setMap(null);
            });

            var bounds = new google.maps.LatLngBounds();
            toAdd.forEach(marker => {
                that.markerCluster.addMarker(marker);
            });
        }
    }

    updateSelectedMarker() {
        let hoveredRepairer = this.props.hoveredRepairerResult;
        if (this.markerCluster != null) {
            if (hoveredRepairer !== null) {
                this.markerCluster.getMarkers().forEach(marker => {
                    if (marker.repairer.id == hoveredRepairer.id) {
                        let icon = this.getMarkerIcon(marker, true);
                        marker.setIcon(icon);
                    }
                });
            }
            else {
                this.markerCluster.getMarkers().forEach(marker => {
                    let icon = this.getMarkerIcon(marker, false);
                    marker.setIcon(icon);
                });
            }
            this.markerCluster.clusters_.forEach((cluster) =>{
                cluster.updateIcon();
            })
        }
    }

    getMarkerIcon(marker, isHover) {
        let markerReparacteurIcon = isHover === true ? 'marker-reparacteur-selected.png' : 'marker-reparacteur.png';
        let markerArtisanIcon = isHover === true ? 'marker-artisan-selected.png' : 'marker-artisan.png';
        let imgWitdth = isHover === true ? 37 : 32;
        let imgHeight = isHover === true ? 56 : 48;
        let iconName = marker.repairer.isReparactor === true ? markerReparacteurIcon : markerArtisanIcon;
        var icon = {
            url: config.imgRelativePath + iconName,
            scaledSize: new google.maps.Size(imgWitdth, imgHeight),
            origin: new google.maps.Point(0, 0),
            anchor: new google.maps.Point(16, imgHeight)
        };
        return icon;
    }

    render() {
        if (this.map != null) {
            let that = this;
            //Retrieve google maps key from backend
            GoogleMapsLoader.load(function (google) {
                google.maps.event.trigger(that.map, 'resize');
            });
        }
        return (
            <div id="map-area" className="map-area" ref="map" />
        );
    }
}