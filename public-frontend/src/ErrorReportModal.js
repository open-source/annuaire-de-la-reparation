import React from 'react';
import ReactDOM from "react-dom";
import Modal from 'react-modal';
import $ from "jquery";

const config = require('Config');

// Length of the error description
const ERROR_DESCRIPTION_MIN_LENGTH = 50;
const ERROR_DESCRIPTION_MAX_LENGTH = 500;

// Initial state
const INITIAL_STATE = {
    isFormSent: false,
    isErrorDescriptionValid: true, // Do not display validation error until input is modified
    isRequestValid: true
};

/**
 * Component that displays a modal dialog to report an error on a crafstperson.
 */
export default class ErrorReportModal extends React.Component {

    /**
     * Constructor.
     * 
     * @param {*} props properties
     */
    constructor(props) {
        super(props);
        this.state = INITIAL_STATE;
        this.errorDescription = null;

        this.onErrorDescriptionChange = this.onErrorDescriptionChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
        this.onClose = this.onClose.bind(this);
    }

    /**
     * Handle a change of the error description text.
     * 
     * @param {*} event 
     */
    onErrorDescriptionChange(event) {
        var errors = [];

        var errorDescriptors = document.getElementsByName("errorDescription");
        var isValid = false;//value.length >= ERROR_DESCRIPTION_MIN_LENGTH && value.length <= ERROR_DESCRIPTION_MAX_LENGTH;
        for(var i=0;i<errorDescriptors.length;i++){
            if(errorDescriptors[i].checked) {
                isValid = true;
                errors.push(errorDescriptors[i].value);
            }
        }
        this.errorDescription = errors;

        this.setState({
            isErrorDescriptionValid: isValid
        });
    }

    /**
     * User submit error report.
     * 
     * @param {*} event 
     */
    onSubmit(event) {
        const that = this;
        event.preventDefault();

        $.post(config.apiBaseUrl + "craftspersons/" + this.props.craftsperson.id + "/error", {
            description: this.errorDescription
        }).fail(function (data) {
            // On error 400, display validation errors, otherwise display general server error
            that.setState({
                isFormSent: data.status != 400,
                isRequestValid: data.status == 400
            })
            if(data.status == 400) {
                that.setState({
                    isErrorDescriptionValid: false
                });
            }
        })
        .done(function () {
            that.setState({
                isFormSent: true,
                isRequestValid: true,
                isErrorDescriptionValid: true,
            });
        });
    }

    /**
     * Close the modal.
     */
    onClose() {
        this.errorDescription = null;
        this.setState(INITIAL_STATE);
        this.props.onClose();
    }

    /**
     * Render component.
     */
    render() {
        const c = this.props.craftsperson;
        const errMsgs = this.props.craftpersonErrorMsgs;
        let modalContent;
        if(this.state.isFormSent && this.state.isRequestValid && this.state.isErrorDescriptionValid) {
            modalContent = 
                <div className="modal-content" id="error-report-modal">
                    <div className="modal-header">
                            <h3>Signalement pris en compte</h3>
                    </div>
                    <div className="modal-body">
                        <p>Votre signalement a été enregistré.</p>
                    </div>
                    <div className="modal-footer">
                        <div className="btn-toolbar pull-right">
                            <button className="btn btn-primary" type="button" title="Fermer" onClick={this.onClose}>Fermer</button>
                        </div>
                    </div>
                </div>;
        }
        else if (this.state.isFormSent && !this.state.isRequestValid) {
            modalContent = 
                <div className="modal-content" id="error-report-modal">
                    <div className="modal-header">
                            <h3>Erreur lors du signalement d'erreur</h3>
                    </div>
                    <div className="modal-body">
                        <p>Une erreur est survenue lors de la soumission de votre signalement. Merci de réessayer.</p>
                    </div>
                    <div className="modal-footer">
                        <div className="btn-toolbar pull-right">
                            <button className="btn btn-primary" type="button" title="Fermer" onClick={this.onClose}>Fermer</button>
                        </div>
                    </div>
                </div>;
        }
        else {
            let errCheckbox = [];
            for(var idErr in errMsgs){
                errCheckbox.push(<br/>);
                errCheckbox.push(<label><input type="checkbox" name="errorDescription" value={errMsgs[idErr]['id']} onChange={this.onErrorDescriptionChange} /> {errMsgs[idErr]['message']}</label>);
            };

            modalContent =
                <div className="modal-content" id="error-report-modal">
                    <form className="form-horizontal" onSubmit={this.onSubmit}>
                        <div className="modal-header">
                            <h3>Signaler une erreur</h3>
                        </div>
                        <div className="modal-body">
                            <p>Merci de bien vouloir détailler l'erreur à signaler.</p>
                            <div className="form-group">
                                <div className="col-sm-4">
                                    <label className="control-label" htmlFor="name">Nom de l'artisan :&nbsp;</label>
                                </div>
                                <div className="col-sm-8">
                                    <input type="text" className="width100" name="name" title="Nom de l'artisan" value={c.name} readOnly disabled/>
                                </div>
                            </div>
                            <div className="form-group">
                                <div className="col-sm-4">
                                    <label className="control-label" htmlFor="cma">Département :&nbsp;</label>
                                </div>
                                <div className="col-sm-8">
                                    <input type="text" className="width100" name="cma" title="Département" value={c.department} readOnly disabled/>
                                </div>
                            </div>
                            <div className="form-group">
                                <div className="col-sm-12">
                                    <label className="control-label" htmlFor="errorDescription">Détail de l'erreur :&nbsp;</label>

                                    {errCheckbox}

                                    {!this.state.isErrorDescriptionValid &&
                                        <div className="alert alert-danger">
                                            Vous devez sélectionner au moins une anomalie.
                                        </div>
                                    }

                                </div>
                            </div>
                        </div>
                        <div className="modal-footer">
                            <div className="btn-toolbar pull-right">
                                <button className="btn btn-primary" type="submit" title="Valider" disabled={this.errorDescription == null || !this.state.isErrorDescriptionValid}>Signaler l'erreur</button>
                                <button className="btn btn-default" type="button" title="Fermer" onClick={this.onClose}>Annuler</button>
                            </div>
                        </div>
                    </form>
                </div>;
        }

        return (
            <Modal isOpen={this.props.isOpen} contentLabel="ErrorReportModal" className={{ base: 'react-modal' }} overlayClassName={{ base: 'react-modal-overlay' }}>
                {modalContent}
            </Modal>
        );
    }
}