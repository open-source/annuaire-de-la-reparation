import React from 'react';
import ReactDOM from "react-dom";
import Modal from 'react-modal';
import $ from "jquery";

const config = require('Config');


/**
 * Component that displays a modal dialog to report an error on a crafstperson.
 */
export default class CraftspersonErrorReportModal extends React.Component {

    /**
     * Constructor.
     * 
     * @param {*} props properties
     */
    constructor(props) {
        super(props);

        this.onClose = this.onClose.bind(this);
    }



    /**
     * Close the modal.
     */
    onClose() {
        this.props.onClose();
    }

    /**
     * Render component.
     */
    render() {
        const c = this.props.craftsperson;
        const errMsgs = this.props.craftpersonErrorMsgs;
        const cfe = this.props.cfe;
        let modalContent;

        modalContent =
                <div className="modal-content" id="error-report-modal">
                    <div className="modal-header">
                            <h3>Signaler une erreur</h3>
                    </div>
                    <div className="modal-body">
                        <p>Pour signaler une erreur dans votre fiche d'artisan, merci de vous reporter au site du CFE de la région {cfe.region} : <a href={cfe.url} target="_blank">{cfe.url}</a></p>
                    </div>
                    <div className="modal-footer">
                        <div className="btn-toolbar pull-right">
                            <button className="btn btn-primary" type="button" title="Fermer" onClick={this.onClose}>Fermer</button>
                        </div>
                    </div>
                </div>;

        return (
            <Modal isOpen={this.props.isOpen} contentLabel="CraftspersonErrorReportModal" className={{ base: 'react-modal' }} overlayClassName={{ base: 'react-modal-overlay' }}>
                {modalContent}
            </Modal>
        );
    }
}