import React from "react";
import $ from "jquery";
import IconsElement from './IconsElement';

import ContactModal from './ContactModal';

let buildUrlParamString = require('./Utils.js').buildUrlParamString;

export class ListElement extends React.Component {

	constructor(props) {
		super(props);

		this.onMouseHover = this.onMouseHover.bind(this);
		this.onMouseOut = this.onMouseOut.bind(this);
		this.onMouseClick = this.onMouseClick.bind(this);
	}

    componentDidUpdate(){
		if(this.props.isHovered) {
            document.getElementsByClassName("hovered")[0].scrollIntoView({
                behavior: 'smooth',
                block: 'center',
                inline: 'center',
            });
        }
	}

	onMouseHover() {
		this.props.onListElementMouseOver(this.props.repairer);
	}

	onMouseOut() {
		this.props.onListElementMouseOver(null);
	}

	onMouseClick(event){
        event.preventDefault();
        event.stopPropagation();
        this.props.openContactModal(this.props.repairer);
    }

	render() {
		let config = require('Config');
		let dist = Number.parseFloat(this.props.repairer.distance);
		let distanceFormatted = dist.toFixed(1).replace('.', ',');
		let repairerLink = "/reparateur/" + this.props.repairer.id;
		let urlParams = [];
		if(this.props.category) {
			urlParams.push({
				name: 'category',
				value: this.props.category
			})
		}
		if(this.props.address) {
			urlParams.push({
				name: 'address',
				value: this.props.address
			})
		}
		repairerLink = repairerLink + buildUrlParamString(urlParams);

		return (
			<a href={repairerLink}>
				<div className={"pro-element clickable height-auto " + (this.props.isHovered ? "hovered" : "")} onMouseOver={this.onMouseHover} onMouseOut={this.onMouseOut}>
					<div className="row row-element">
						<div className="title-block col-xs-12">
							<span className="reparactor-icon pull-right">
                                {
                                    this.props.repairer.isReparactor &&
                                    <img className="pull-right" src={config.imgRelativePath + "/icon-reparactor.png"} alt="Répar'acteur" title="Un artisan qui s’engage à tout mettre en œuvre pour réparer votre objet" height="28" width="28"/>
                                }
                            </span>
                            <span className="dark-red">{this.props.repairer.name}</span>
						</div>
					</div>

                    <div className="row row-element">
                        <div className="title-block col-xs-4">
                            <span>{distanceFormatted} km</span>
                        </div>
                        <div className="title-block col-xs-8">
                            <span className="pull-right">
                                <IconsElement categories={this.props.repairer.categories} size="28" />
                            </span>
                        </div>
                    </div>
					<div className="clear" />
					<div className="row row-element">
						<div className="col-xs-10">
							<div>{this.props.repairer.address1}</div>
							<div>{this.props.repairer.zipCode} {this.props.repairer.zipCodeLabel.toUpperCase()}</div>
							<div>
                                {this.props.repairer.phone !== null && this.props.repairer.phone !== "" &&
                                <div>
                                    <span className="info-label"><strong>Téléphone :</strong></span> <a className="underlined" href={"tel:" + this.props.repairer.phone}>{this.props.repairer.phone}</a>
                                </div>
                                }
                                {this.props.repairer.email !== null && this.props.repairer.email !== "" && this.props.repairer.email !== false &&
                                <div>
                                    <span className="info-label"><strong>Contact :</strong></span> <a className="underlined" href="#" title="Envoyer un email" onClick={this.onMouseClick}><span className="glyphicon glyphicon-envelope" title="Contact" ></span></a>
                                </div>
                                }
							</div>
						</div>
					</div>
                    <div className="row row-element">
                        <div className="col-xs-9"></div>
                        <div className="col-xs-3 like-block">
                            <span className="pull-right">{this.props.repairer.likes} &nbsp;<span className="glyphicon glyphicon-thumbs-up dark-green glyphicon-font-18" title="Recommandations" /></span>
                        </div>
                    </div>
					<div className="clear" />
				</div>
			</a>
		);
	}
}

export class Filter extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			filterVisibility: true
		};

		this.handleFilterVisibility = this.handleFilterVisibility.bind(this);
		this.handleReorderResults = this.handleReorderResults.bind(this);
		this.handleFilterReparacteur = this.handleFilterReparacteur.bind(this);
	}

	handleFilterVisibility(event) {
		this.setState({
			filterVisibility: !this.state.filterVisibility
		});
	}

	handleReorderResults(event) {
		var value = this.refs.orderField.value;
		this.props.updateOrder(value);
	}

	handleFilterReparacteur(event) {
		var value = this.refs.reparActeurInput.checked;
		this.props.updateFilter(value);
	}

	render() {
		return (
			<form action="" className=" form">
				<div className="panel panel-default">
					<div className="result-heading panel-heading" onClick={this.handleFilterVisibility}>
						<p className="sort-title">Trier les artisans à proximité par&nbsp;:&nbsp;</p>
						<select ref="orderField" name="order-field" className="form-control input-sm sort-input" onChange={this.handleReorderResults} onClick={(e) => e.stopPropagation()}>
							<option value="isReparactor">Artisans Répar'acteurs</option>
							<option value="distance">Distance</option>
							<option value="likes">Popularité</option>
						</select>
					</div>
					{this.state.filterVisibility &&
						<div className="panel-body" id="searchBody" ref="toggleInput">
							<div className="form-group">
								<div className="checkbox">
									<label>
										<input type="checkbox" ref="reparActeurInput" name="label-field" id="label-field" onChange={this.handleFilterReparacteur} />
											Afficher uniquement les artisans labellisés
											<span className="custom-tooltip" title="Un artisan qui s’engage à tout mettre en œuvre pour réparer votre objet">Répar'Acteurs</span>
									</label>
								</div>
							</div>
						</div>
					}
				</div>
			</form>
		);
	}
}

export default class Results extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			order: "isReparactor",
			filterReparacteur: false,
			isAjaxRequestRunning : true,
			isContactModalOpen: false,
            contactedRepairer: null
		};

		this.handleReorder = this.handleReorder.bind(this);
		this.handleListElementMouseOver = this.handleListElementMouseOver.bind(this);
		this.openContactModal = this.openContactModal.bind(this);
		this.onContactModalClose = this.onContactModalClose.bind(this);
	}

	componentDidMount() {
		let that = this;
		$( document ).ajaxStart(function() {
			that.setState({
				isAjaxRequestRunning : true
			});
		});

		$( document ).ajaxStop(function() {
			that.setState({
				isAjaxRequestRunning : false
			});
		});
	  }

	handleReorder(orderValue) {
		this.setState({
			order: orderValue
		});
	}

	sort(preorder, order) {
		let sortOrder = -1;
		if (order === "distance") {
			sortOrder = 1;
		}
		return function (a, b) {
			let result = (a[order] < b[order]) ? -1 : (a[order] > b[order]) ? 1 : 
				(a[preorder] < b[preorder]) ? 1 : (a[preorder] > b[preorder]) ? -1 : 0;
			return result * sortOrder;
		}
	}

	handleListElementMouseOver(repairer) {
		this.props.onRepairerResultHover(repairer);
	}

    openContactModal(repairer) {
	    //console.log("open");
	    //console.log(repairer);
	    this.setState({
            contactedRepairer: repairer,
            isContactModalOpen: true
        });
    }

    onContactModalClose(){
        this.setState({
            contactedRepairer: null,
            isContactModalOpen: false
        });
    }

	render() {
		let listRepairers;	
		if(this.props.repairers.length > 0) {
			listRepairers = this.props.repairers.sort(this.sort("distance", this.state.order)).map((repairer) => {
					let hovered = this.props.hoveredRepairer && repairer.id == this.props.hoveredRepairer.id;
					return <ListElement repairer={repairer} key={repairer.id} category={this.props.category} address={this.props.address} isHovered={hovered}
										onListElementMouseOver={this.handleListElementMouseOver} openContactModal={this.openContactModal}/>
				}
			);
		}
		else {
			if(this.state.isAjaxRequestRunning) {
				listRepairers = 
				<div className="pro-element vert-center">
					<div className="row row-element text-center">
						<b>Recherche en cours...</b>
					</div>
				</div>;
			} else {
				listRepairers = 
				<div className="pro-element vert-center">
					<div className="row row-element text-center">
						<b>Aucun réparateur trouvé.</b>
					</div>
				</div>;
			}
			
		}

		return (
			<div>
				<div className="filter">
					<Filter updateOrder={this.handleReorder} updateFilter={this.props.onFilterReparactor} />
				</div>
				<div className="scroll-list">
					<div className="row row-element">
						<div>{listRepairers}</div>
					</div>
				</div>
                <ContactModal craftsperson={this.state.contactedRepairer} isOpen={this.state.isContactModalOpen} onClose={this.onContactModalClose} />
			</div>
		);
	}
}