import React from 'react';
import ReactDOM from "react-dom";
import $ from "jquery";
import Modal from 'react-modal';

const config = require('Config');
const GoogleMapsLoader = require('google-maps'); // only for common js environments
GoogleMapsLoader.LANGUAGE = 'fr';

let getFranceResultFirst = require('./Utils.js').getFranceResultFirst;
let handleSpecificRedirections = require('./Utils.js').handleSpecificRedirections;

export default class SearchForm extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      category: this.props.category ? this.props.category : '',
      address: this.props.address ? this.props.address : '',
      categories: this.props.categories ? this.props.categories : [],
      isCategoriesLoading: false,
      menuVisible: false,
      repairerCount: null,
      initAutocomplete: false,
      isModalOpen: false,
      browserKey: ''
    };

    this.countOverQueryLimit = 0;

    this.onInputChange = this.onInputChange.bind(this);
    this.onInputFocus = this.onInputFocus.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
    this.onGeolocalize = this.onGeolocalize.bind(this);
    this.onToggleMenu = this.onToggleMenu.bind(this);
    this.onModalClose = this.onModalClose.bind(this);
  }
        
  componentDidMount() {
    let that = this;

     $.getJSON(config.apiBaseUrl + "parameters").done(function (data) {
            GoogleMapsLoader.KEY = data.google_maps_api_key;
            that.setState({
                browserKey: data.google_maps_api_key
            });
     });

    //Load search categories from server if parent component did not provide them.
    if (this.state.categories.length == 0) {
      that.setState({
        isCategoriesLoading: true
      });
      $.getJSON(config.apiBaseUrl + "categories").done(function (data) {
        that.setState({
          categories: data.categories,
          isCategoriesLoading: false
        });
      });
    }

    // Load repairer count
    if (this.state.repairerCount == null) {
      $.getJSON(config.apiBaseUrl + "craftspersons").done(function (data) {
        that.setState({
          repairerCount: parseInt(data.total)
        });
      });
    }
  }

  onInputChange(event) {
    const target = event.target;
    const value = target.value;
    const name = target.name;

    this.setState({
      [name]: value
    });
  }

  onInputFocus(event) {

    let that = this;

    //Add autocomplete on address field (only at first focus on input)
    if (!this.state.initAutocomplete) {

      //Retrieve google maps key from backend
        GoogleMapsLoader.load(function (google) {
          var options = {
            types: ['geocode'],
            componentRestrictions: { country: 'fr' }
          };
          var input = /** @type {!HTMLInputElement} */(document.getElementById('address'));
          var autocomplete = new google.maps.places.Autocomplete(input, options);

          //Update input after address selection
          google.maps.event.addListener(autocomplete, 'place_changed', function (event) {
            that.setState({
              "address": input.value
            });
            that.onSubmit(event);
          });
        });

        this.setState({
          initAutocomplete: true
        });
    }

  }

  onSubmit(event) {

    let that = this;
    event.preventDefault();

    //Find address location and precision
    let addressString = this.state.address ? this.state.address : config.defaultAddress;
    $.getJSON(config.googleMapGeocodeApiUrl + "&address=" + encodeURI(addressString) + "&region=fr&key="+this.state.browserKey).done(function (data) {
      if (data.status === "OK") {
        //By default, if several results are found, first one is taken
        let results = getFranceResultFirst(data.results);

        let lat = results.geometry.location.lat;
        let lng = results.geometry.location.lng;

        //For searches in following areas : Bretagne, Limousin, Aquitaine, Occitanie, redirect to external sites
        const redirected = handleSpecificRedirections(that.state.category, addressString, results.address_components);
        let regionN = that.getRegion(results.address_components);
        ga('send', 'event', "Search", "Region", regionN);
        console.log("sent region : "+regionN);
        // If no redirection, search is done
        if(!redirected) {
          // Inform other components that search location has changed
          // Last parameter is precision (lower is better)
          const regionName = that.getRegion(results.address_components);
          that.props.onSubmit(that.state.category, addressString, lat, lng, results.address_components.length, regionName);
        }
        
      } else {
        if(data.status === "ZERO_RESULTS" || data.status === "INVALID_REQUEST") {
         // Display warning message : Adresse unknown
          that.setState({
            isModalOpen: true
          });
          console.log("L'adresse entrée est inconnue. Veuillez vérifier que l'adresse saisie est valide et réessayer. " + data.status); 
        } else {
          that.setState({
            isModalOpen: false
          });
        }
        if(data.status === "OVER_QUERY_LIMIT" || data.status === "REQUEST_DENIED" || data.status === "UNKNOWN_ERROR") {
          console.log("Une erreur inattendue est survenue. Veuillez réessayer ultérieurement. " + data.status);

          // Handle over query limit per seconds
          if(data.status === "OVER_QUERY_LIMIT") {
            // If less than 5 "OVER_QUERY_LIMIT" message, wait and rerun request to google api
            if(that.countOverQueryLimit <= 10) {
              setTimeout(function(){
                if(data.error_message){
                    console.log(data.error_message);
                }else{
                    console.log("Dépassement du nombre de requêtes autorisées par secondes. Renvoi de la requête après 2 secondes d'attente");
                }

                that.countOverQueryLimit ++;
                that.onSubmit(event);
              }, 2000);
            } else {
                if(data.error_message){
                  console.log(data.error_message);
                }else {
                    console.log("Le nombre de requêtes autorisées par jour a été dépassé.");
                }
            }
          }
        }
      }

    });
  }

  /**
   * Return region from addressComponents
   * 
   * @param {*} addressComponents 
   * @returns regionName
   */
  getRegion(addressComponents) {
    // Find region name from address
    let regionName = null;
    for (let addrComponent of addressComponents) {
      if (addrComponent.types.includes("country") && (addrComponent.long_name=="Martinique"||addrComponent.long_name=="Réunion")) {
          regionName = addrComponent.long_name;
          break;
      }else if (addrComponent.types.includes("administrative_area_level_1")) {
        regionName = addrComponent.long_name;
      }
    }

    // Check region is supported
    return regionName; 
  }

  onModalClose() {
    this.setState({
      isModalOpen: false
    });
  }

  onGeolocalize() {
    let that = this;
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(function (position) {

        let lat = position.coords.latitude;
        let lng = position.coords.longitude;

        //Retrieve address from lat and lng
        GoogleMapsLoader.load(function (google) {
          var geocoder = new google.maps.Geocoder();
          var latLng = new google.maps.LatLng(lat, lng);
          geocoder.geocode({ 'latLng': latLng }, function (results, status) {

            if (status == google.maps.GeocoderStatus.OK && results.length > 0) {

              let formattedAddress = results[0].formatted_address; //we geolocalise at a street scale, use results[1] for city scale, etc.
              if (formattedAddress) {
                //Update address input with current address
                that.setState({
                  address: formattedAddress,
                  key: Math.random()
                });
              }
            } else {
              alert("Aucune adresse trouvée");
            }
          });
        });

      }, function () {
        alert('Erreur: Le service de géolocalisation a échoué.');
      });
    } else {
      // Browser doesn't support Geolocation
      alert('Erreur: Votre navigateur ne supporte pas la géolocalisation.');
    }
  }

  onToggleMenu() {
    this.setState({
      menuVisible: !this.state.menuVisible
    })
  }

  render() {
    const categoryOptions = this.state.categories.map((cat) =>
      <option key={cat.id} value={cat.id} defaultValue={cat.id == this.state.category ? cat.id : ''}>{String.fromCharCode(9567+cat.id)} {cat.name}</option>
    );

    let bannerClasses = 'banner';
    //Wait for categories to render in order to avoid flicker...
    return (
      !this.state.isCategoriesLoading && this.state.repairerCount != null &&
      <div className="banner-container">
        <div className={bannerClasses}>
          <div className="overlay">
            <div className="container-fluid">

              <div className="title-form-container">
                <div className="row main-title"><span>Trouver un réparateur</span><span>proche de chez vous</span></div>
                <div className="row">
                  <div id="form-container">
                    <form id="search-form" onSubmit={this.onSubmit} className="form-inline">
                        <select name="category" className="form-control search-form-category category-font" aria-label="Catégorie" onChange={this.onInputChange} value={this.state.category}  >
                          <option value="">Que voulez-vous réparer ?</option>
                          {categoryOptions}
                        </select>
                        <div className="input-group margin-right-5">
                          <input name="address" id="address" type="text" className="form-control" placeholder="Où ? (adresse)" aria-label="Adresse"
                            onChange={this.onInputChange} onFocus={this.onInputFocus} value={this.state.address} />
                          <span className="input-group-btn">
                            <button className="btn btn-default" title="Me géolocaliser" type="button" onClick={this.onGeolocalize}><span className="glyphicon glyphicon-screenshot dark-red glyphicon-font-18"></span></button>
                          </span>
                        </div>
                      <div id="submit-container" className="align-right">
                        <input type="submit" className="btn" aria-label="Rechercher" value="Rechercher" />
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <Modal isOpen={ this.state.isModalOpen } contentLabel="Modal" className={{ base: 'react-modal' }} overlayClassName={{ base: 'react-modal-overlay' }}>
          <div className="modal-content">
            <div className="modal-header">
                <h3>Adresse inconnue</h3>
            </div>
            <div className="modal-body">
                <p>L'adresse entrée est inconnue. Veuillez vérifier que l'adresse saisie est valide et réessayer.</p>
            </div>
            <div className="modal-footer">
                <div className="btn-toolbar pull-right">
                    <button className="btn btn-default" type="button" title="Fermer" onClick={this.onModalClose}>Fermer</button>
                </div>
            </div>
          </div>
        </Modal>
      </div>
    );
  }
}
