import React from 'react'
import IconCategory from './IconCategory'

export default class IconsElement extends React.Component {
	constructor(props) {
		super(props);
	}

	render() {

		const iconeCategories = this.props.categories.map((categorie) =>
			<IconCategory categorie={categorie} key={categorie.id} size={this.props.size} />
		);

		return (
			<span className="inline-icons">
				{iconeCategories}
			</span>
		);
	}
}