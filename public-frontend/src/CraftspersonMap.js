import React from "react";
import $ from "jquery";

const config = require('Config');
const GoogleMapsLoader = require('google-maps'); // only for common js environments
GoogleMapsLoader.LANGUAGE = 'fr';

/**
 * Map with route for craftsperson details.
 */
export default class CraftspersonMap extends React.Component {

    /**
     * Initialize map.
     */
    componentDidMount() {
        let that = this;
        //Retrieve google maps key from backend
        $.getJSON(config.apiBaseUrl + "parameters").done(function (data) {
            GoogleMapsLoader.KEY = data.google_maps_api_key;
            GoogleMapsLoader.load(function (google) {
                //Position
                let position = { lat: that.props.lat, lng: that.props.lng };

                //Init google map
                let map = new google.maps.Map(that.refs.map056, {
                    center: position,
                    zoom: config.cityScaleZoom
                });

                //Create a marker for craftsperson
                let iconName = that.props.isReparactor === true ? 'marker-reparacteur.png' : 'marker-artisan.png';
                new google.maps.Marker({
                    map: map,
                    position: position,
                    icon: config.imgRelativePath + iconName
                });
            });
        });
    }

    /** 
     * Render map.
     */
    render() {
        return (
            <div className="map-area" ref="map056" />
        );
    }
}