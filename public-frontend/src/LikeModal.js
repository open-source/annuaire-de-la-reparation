import React from 'react';
import ReactDOM from "react-dom";
import Modal from 'react-modal';
import $ from "jquery";

const config = require('Config');

/**
 * Component that displays a modal dialog to "like" a craftsperson.
 */
export default class LikeModal extends React.Component {

    /**
     * Constructor.
     * 
     * @param {*} props properties
     */
    constructor(props) {
        super(props);
        this.state = {
            isLikeSent: false,
            isEmailValid: true, // Do not display validation error until input is modified,
            isRequestValid: true
        };
        this.email = null;
        this.EMAIL_REGEX = /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)$/;

        this.onEmailChange = this.onEmailChange.bind(this);
        this.onLike = this.onLike.bind(this);
    }

    /**
     * Handle a change of the email for like validation.
     * 
     * @param {*} event 
     */
    onEmailChange(event) {
        const value = event.target.value;
        
        this.email = value;
        const isValid = value && value.match(this.EMAIL_REGEX) != null;

        this.setState({
            isEmailValid: isValid
        })
    }

    /**
     * User confirms like.
     * 
     * @param {*} event 
     */
    onLike(event) {
        const that = this;
        event.preventDefault();

        $.post(config.apiBaseUrl + "craftspersons/" + this.props.craftsperson.id + "/like/" + this.email).always(function (data) {
            that.setState({
                isLikeSent: true,
                isRequestValid: data.status == 201
            });
        });
    }

    /**
     * Render component.
     */
    render() {
        const c = this.props.craftsperson;

        let modalContent;
        if(this.state.isLikeSent && this.state.isRequestValid) {
            modalContent = 
                <div className="modal-content">
                    <div className="modal-header">
                            <h3>Demande prise en compte</h3>
                    </div>
                    <div className="modal-body">
                        <p>Votre demande a été prise en compte. Veuillez vérifier vos emails pour confirmer votre demande.</p>
                    </div>
                    <div className="modal-footer">
                        <div className="btn-toolbar pull-right">
                            <button className="btn btn-primary" type="button" title="Fermer" onClick={this.props.onClose}>Fermer</button>
                        </div>
                    </div>
                </div>;
        }
        else if (this.state.isLikeSent && !this.state.isRequestValid) {
            modalContent = 
                <div className="modal-content">
                    <div className="modal-header">
                            <h3>Demande déja soumise</h3>
                    </div>
                    <div className="modal-body">
                        <p>Une recommandation existe déjà pour ce réparateur et cette adresse email.</p>
                    </div>
                    <div className="modal-footer">
                        <div className="btn-toolbar pull-right">
                            <button className="btn btn-primary" type="button" title="Fermer" onClick={this.props.onClose}>Fermer</button>
                        </div>
                    </div>
                </div>;
        }
        else {
            modalContent =
                <div className="modal-content">
                    <form className="form-horizontal" onSubmit={this.onLike}>
                        <div className="modal-header">
                            <h3>Recommander {c.name}</h3>
                        </div>
                        <div className="modal-body">
                            <p>Pour valider votre recommandation, merci de renseigner votre adresse email ci-dessous. Un email de confirmation vous sera envoyé pour valider votre recommandation.
                                <br/>En cas de non réception de cet email, pensez à vérifier dans votre anti-spam.</p>
                            <div className="form-group">
                                <div className="col-sm-2">
                                    <label className="control-label" htmlFor="email">Email :&nbsp;</label>
                                </div>
                                <div className="col-sm-10">
                                    <input type="email" name="email" title="email" onChange={this.onEmailChange} />
                                    {!this.state.isEmailValid &&
                                        <div className="alert alert-danger">
                                            Adresse invalide
                                        </div>
                                    }
                                </div>
                            </div>
                        </div>
                        <div className="modal-footer">
                            <div className="btn-toolbar pull-right">
                                <button className="btn btn-primary" type="submit" title="Valider" disabled={this.email == null || !this.state.isEmailValid}>Recommander ce réparateur</button>
                                <button className="btn btn-default" type="button" title="Fermer" onClick={this.props.onClose}>Annuler</button>
                            </div>
                        </div>
                    </form>
                </div>;
        }

        return (
            <Modal isOpen={this.props.isOpen} contentLabel="Modal" className={{ base: 'react-modal' }} overlayClassName={{ base: 'react-modal-overlay' }}>
                {modalContent}
            </Modal>
        );
    }
}