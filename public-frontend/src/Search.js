import $ from 'jquery';
import { addUrlProps, UrlQueryParamTypes, urlAction } from 'react-url-query';
import createHistory from 'history/createBrowserHistory';
import React from 'react';
import ReactDOM from 'react-dom';

import Map from './Map';
import Results from './Results';
import SearchForm from './SearchForm';
import { buildUrlParamString } from './Utils';
import Modal from 'react-modal';

const config = require('Config');
const history = createHistory();

//Accepted url params
const urlPropsQueryConfig = {
  address: { type: UrlQueryParamTypes.string },
  category: { type: UrlQueryParamTypes.number },
  lat: { type: UrlQueryParamTypes.number },
  lng: { type: UrlQueryParamTypes.number },
  p: { type: UrlQueryParamTypes.number }, // Address precision
  region: { type: UrlQueryParamTypes.string } 
};
const SUPPORTED_REGIONS = ["Auvergne-Rhône-Alpes", "Auvergne Rhône-Alpes", "Bourgogne-Franche-Comté", "Bourgogne Franche Comté", "Bretagne", "Centre-Val de Loire", "Grand Est", "Haut-de-France", "Hauts-de-France", "Ile de France", "Martinique", "Normandie", "Nouvelle-Aquitaine", "Occitanie", "Pays de la Loire", "Provence-Alpes-Côte d'Azur", "Réunion"];

class Search extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      repairers: [],
      radius: config.mapRadius,
      categories: [],
      category: null,
      mapCenterLat: config.mapCenterLat,
      mapCenterLng: config.mapCenterLng,
      addressLat:config.mapCenterLat,
      addressLng:config.mapCenterLng,
      zoom: config.defaultZoom,
      reparactorOnly: false,
      hoveredRepairer: null,
      hoveredRepairerResult: null,
      outRepairerResult: null,
      isModalOpen: false,
      modalBody:"",
      modalHeader:"",
      mapKey: 0 //used to force map reload (unmount/remount),
    }

    this.maxRadius = 0;
    //Mis à vrai si aucuns résultats trouvés, alors la recherche s'effectue sur le réparateur le plus proche
    this.isSearchClosest = false;
    this.closestDistance = 0;

    this.handleSubmit = this.handleSubmit.bind(this);
    this.getApiData = this.getApiData.bind(this);
    this.getDataAndZoom = this.getDataAndZoom.bind(this);
    this.handleFilterReparactor = this.handleFilterReparactor.bind(this);
    this.filterReparactor = this.filterReparactor.bind(this);
    this.onRepairerHover = this.onRepairerHover.bind(this);
    this.onRepairerResultHover = this.onRepairerResultHover.bind(this);
    this.onMapZoomChanged = this.onMapZoomChanged.bind(this);
    this.isSupportedRegion = this.isSupportedRegion.bind(this);
    this.onModalClose = this.onModalClose.bind(this);
  }

  componentWillMount() {
    this.setState({
      category: this.props.category,
      address: this.props.address
    });
  }

  componentDidMount() {
    //Load search categories from server
    let that = this;
    $.getJSON(config.apiBaseUrl + "categories").done(function (data) {
      that.setState({
        categories: data.categories
      });
    });

    //Update data
    if (this.state.address && this.props.lat && this.props.lng && this.props.p) {
      this.handleSubmit(this.state.category, this.state.address, this.props.lat, this.props.lng, this.props.p, this.props.region);
    }
  }

  handleSubmit(category, address, lat, lng, precision, region) {
    this.state.address = address;

    let that = this;

    //Update state
    this.maxRadius = 0;

    //Send new search url to google analytics
    const location = history.location;
    const unlisten = history.listen(location => {
      let locationString = location.pathname;
      locationString = location.search ? locationString + location.search : locationString;
      ga('set', 'location', locationString);
      ga('send', 'pageview');
    });

    //Update URL
    this.props.onChangeUrlQueryParams({
      address: address,
      category: category,
      lat: lat,
      lng: lng,
      p: precision,
      region: region
    });
    const urlParams = [
      { name: "category", value: category },
      { name: "address", value: address },
      { name: "lat", value: lat },
      { name: "lng", value: lng },
      { name: "p", value: precision },
      { name: "region", value: region }
    ];
    const requestString = encodeURI('/search' + buildUrlParamString(urlParams));
    history.replace(requestString);
    unlisten();

    this.getDataAndZoom(category, lat, lng, precision, region);
  }

  getDataAndZoom(category, lat, lng, addressPrecision, region) {
    let that = this;

    //Find map zoom from address precision
    let mapZoom = config.defaultZoom;
    if (addressPrecision > 0 && addressPrecision < 2) {
      mapZoom = config.countryScaleZoom;
    }
    else if (addressPrecision > 0 && addressPrecision < 4) {
      mapZoom = config.regionScaleZoom;
    }
    else if (addressPrecision > 0 && addressPrecision < 6) {
      mapZoom = config.cityScaleZoom;
    }
    else {
      mapZoom = config.streetScaleZoom;
    }

    //Load repairer data for the computed area
    let radius = that.getApiSearchRadius(lat, mapZoom);
    this.maxRadius = Math.max(this.maxRadius, radius);
    const preventUpdateNotSupportedRegion = that.getApiData(category, lat, lng, radius, true, region);
      
    // Update state
    if(!preventUpdateNotSupportedRegion) {
      that.setState({
        mapCenterLat: lat,
        mapCenterLng: lng,
        addressLat: lat,
        addressLng: lng,
        zoom: mapZoom,
        category: category
      });
    }
    
  }

  getApiData(category, lat, lng, radius, forceMapReload, region) {
    let that = this;

    //get craftpersons for current category, address and radius
    let categoryString = category ? ('&category=' + category) : '';
    const requestString = encodeURI("craftspersons/search?radius=" + radius + categoryString + "&lat=" + lat + "&lon=" + lng);
    $.getJSON(config.apiBaseUrl + requestString).fail(function (data) {
      console.log("Impossible d'afficher les réparateurs (erreur " + data.status + ")");
    })
      .done(function (data) {
        let newState = {};
        //if no results found and region not in supported regions
        if(!data.craftspersons.length && lat && lng && !that.isSupportedRegion(region)) {
          that.setState({
            isModalOpen: true,
              modalHeader: "Malheureusement, il n'y a pas de résultat trouvé dans un rayon de 40 km",
              modalBody: "Aucun résultat trouvé"
          });
          return true;
        } /*else if (!data.craftspersons.length && lat && lng){
          that.setState({
            isModalOpen: true,
            modalHeader: "Malheureusement, il n'y a pas de résultat trouvé dans un rayon de 40 km",
            modalBody: "Nous vous invitons à réitérer votre recherche à partir d'une autre adresse."
          });
          return true;
        }*/ else {
          that.setState({
            isModalOpen: false
          });

          function getRightZoom(repairerList){
              var filteredRepairers = repairerList.filter(that.filterReparactor());

              let dimensions = null;
//              console.log(document.width()*58.33333333/100);
              dimensions = {width: 840, height: 622};

              var ne = null;
              var sw = null;
              for (var i = 0; i < filteredRepairers.length; i++) {
                  let repairer = filteredRepairers[i];

                  if(ne == null){


                      //Il faut penser le zoom par rapport au point central,
                      let latDiff = Math.abs(lat - repairer.latitude);
                      let lngDiff = Math.abs(lng - repairer.longitude);

                      ne = {
                          lat: repairer.latitude,//lat + latDiff,
                          lng: repairer.longitude//lng + lngDiff
                      };
                      sw = {
                          lat: repairer.latitude,//lat + latDiff,
                          lng: repairer.longitude//lng + lngDiff
                      };
                  }else{

                      //Il faut penser le zoom par rapport au point central,
                      let latDiff = Math.abs(lat - repairer.latitude);
                      let lngDiff = Math.abs(lng - repairer.longitude);

                      ne.lat = Math.max(ne.lat, repairer.latitude);//lat + latDiff);
                      ne.lng = Math.max(ne.lng, repairer.longitude);//lng + lngDiff);

                      sw.lat = Math.min(sw.lat, repairer.latitude);//lat - latDiff);
                      sw.lng = Math.min(sw.lng, repairer.longitude);//lng - lngDiff);
                  }
              }

              //Intégrer l'adresse dans le calcul du périmètre, au cas où elle ne serait pas au centre des résultats
              if(ne != null){
                  ne.lat = Math.max(ne.lat, lat);
                  ne.lng = Math.max(ne.lng, lng);

                  sw.lat = Math.min(sw.lat, lat);
                  sw.lng = Math.min(sw.lng, lng);
              }


              if(ne!=null && dimensions != null) {
                  var mapDim = dimensions;
                  var WORLD_DIM = { height: 256, width: 256 };
                  var ZOOM_MAX = 21;

                  function latRad(lat) {
                      var sin = Math.sin(lat * Math.PI / 180);
                      var radX2 = Math.log((1 + sin) / (1 - sin)) / 2;
                      return Math.max(Math.min(radX2, Math.PI), -Math.PI) / 2;
                  }

                  function zoom(mapPx, worldPx, fraction) {
                      //console.log("map : "+mapPx+","+worldPx+","+fraction);
                      return Math.floor(Math.log(mapPx / worldPx / fraction) / Math.LN2);
                  }

                  var latFraction = (latRad(ne.lat) - latRad(sw.lat)) / Math.PI;

                  var lngDiff = ne.lng - sw.lng;
                  var lngFraction = ((lngDiff < 0) ? (lngDiff + 360) : lngDiff) / 360;

                  var latZoom = zoom(mapDim.height, WORLD_DIM.height, latFraction);
                  var lngZoom = zoom(mapDim.width, WORLD_DIM.width, lngFraction);
                  let zoomMap = Math.min(latZoom, lngZoom, ZOOM_MAX);
                  let latCenter = (ne.lat+0.004+sw.lat)/2;
                  let lngCenter = (ne.lng+sw.lng)/2;

                  if(!isNaN(zoomMap)){
                      return {
                          zoom : zoomMap,
                          latCenter : latCenter,
                          lngCenter : lngCenter
                      };
                  }
              }
              return false;
          }

          //if no results found and region is in supported regions, we adjust radius to closest result
          if (!data.craftspersons.length && lat && lng && that.isSupportedRegion(region)) {
            $.getJSON(config.apiBaseUrl + "craftspersons/closest?lat=" + lat + "&lon=" + lng + categoryString).done(function (closestData) {
              if (Object.keys(closestData).length !== 0 && closestData.craftsperson.distance < config.maxSearchRadius) {
                  let craftsperson = closestData.craftsperson;
                //Mise à jour du radius pour voir le réparateur le plus proche
                that.isSearchClosest = true;
                that.closestDistance = craftsperson.distance;
                //Ajout du réparateur le plus proche dans la liste d'artisans
                newState.repairers = [craftsperson];
              } else {
                  //no closest results found
                newState.repairers = [];
                  that.setState({
                      isModalOpen: true,
                      modalHeader: "Malheureusement, il n'y a pas de résultat trouvé dans un rayon de 40 km",
                      modalBody: "Nous vous invitons à réitérer votre recherche à partir d'une autre adresse."
                  });
              }

              if(forceMapReload && that.props.home !== "1") {
                newState.mapKey = Math.random();
              }

              let zoomMap = newState.repairers.length>0? getRightZoom(newState.repairers) : null;

              if(zoomMap != null){
                newState.zoom = zoomMap.zoom;
                newState.mapCenterLat = zoomMap.latCenter;
                newState.mapCenterLng = zoomMap.lngCenter;
                newState.addressLat = lat;
                newState.addressLng = lng;
              }

              that.setState(newState);
            });
          } else {
            that.isSearchClosest = false;

            newState.repairers = data.craftspersons

            if(forceMapReload && that.props.home !== "1") {
              newState.mapKey = Math.random();
            }

            let zoomMap = getRightZoom(newState.repairers);

            if(zoomMap != null){
              newState.zoom = zoomMap.zoom;
              newState.mapCenterLat = zoomMap.latCenter;
              newState.mapCenterLng = zoomMap.lngCenter;
              newState.addressLat = lat;
              newState.addressLng = lng;
            }

            that.setState(newState);
          }
        }
      });
  }

  getApiSearchRadius(latitude, mapZoomLevel) {
    let radius = Map.getRadiusFromZoom(latitude, mapZoomLevel);
    //Limit value between configured min and max
    let apiRadius = Math.min(config.maxSearchRadius, Math.ceil(radius / 1000.0));
    apiRadius = Math.max(config.minSearchRadius, apiRadius);
    return apiRadius;
  }

  handleFilterReparactor(filterValue) {
    this.setState({
      reparactorOnly: filterValue
    });
  }

  filterReparactor() {
    var filterValue = this.state.reparactorOnly;
    return function (a) {
      if (filterValue === true) {
        return a["isReparactor"] === true;
      }
      return true;
    }
  }

  isSupportedRegion(region) {
    return !region || SUPPORTED_REGIONS.includes(region);
  }

  onRepairerHover(repairer) {
    this.setState({
      hoveredRepairer: repairer
    });
  }

  onRepairerResultHover(repairer) {
    this.setState({
      hoveredRepairerResult: repairer
    });
  }

  onMapZoomChanged(newZoom) {

    //zoom update is enabled only if results have been found or if closest craftperson is still in maximum authorized radius
    if (!this.isSearchClosest || (this.isSearchClosest && this.closestDistance < config.maxSearchRadius)) {

      let radius = this.getApiSearchRadius(this.state.addressLat, newZoom);

      //Update repairer data if needed
      this.setState({
        zoom: newZoom
      });

      if (radius > this.maxRadius) {
        this.maxRadius = radius;
        this.getApiData(this.state.category, this.state.addressLat, this.state.addressLng, radius, false, null);
      }
    }
  }

  onModalClose() {
    this.setState({
      isModalOpen: false
    });
  }

  render() {
    const filteredRepairers = this.state.repairers.filter(this.filterReparactor());

    return (
      <div>
        <SearchForm key={this.state.key} onSubmit={this.handleSubmit} categories={this.state.categories} category={this.state.category} address={this.state.address}/>
        <div id="search" className="page">
          <div className="container search-container">
            <div className="row">

              <div className="col-lg-5 col-md-5 col-sm-12 recommandation-banner">
                  <div>
                      <span className="glyphicon glyphicon-thumbs-up" title="Recommandations"></span> Pensez à recommander vos réparateurs.
                  </div>
              </div>
              <div className="col-lg-7 col-md-7 col-sm-12 pull-right legend">
                <span>
                    <img src={config.imgRelativePath + 'marker-user.png'} alt="Position recherchée" title="Ma position" height="48" width="32" />
                    <span className="legend-text">Vous êtes ici</span>
                </span>
                <span>
                    <img src={config.imgRelativePath + 'marker-reparacteur.png'} alt="Artisan répar'acteur" title="Un artisan qui s’engage à tout mettre en œuvre pour réparer votre objet" height="48" width="32" />
                    <span className="custom-tooltip legend-text" title="Un artisan qui s’engage à tout mettre en œuvre pour réparer votre objet">Répar'Acteurs</span>
                </span>
                <span>
                    <img src={config.imgRelativePath + 'marker-artisan.png'} alt="Artisan non répar'acteur" title="Artisan non répar'acteur" height="48" width="32" />
                    <span className="legend-text">Réparateurs</span>
                </span>
              </div>
            </div>
            <div className="row">
              <div className="col-lg-7 col-md-7 col-sm-12 col-xs-12 pull-right">
                <Map key={this.state.mapKey} repairers={filteredRepairers} onRepairerHover={this.onRepairerHover} hoveredRepairerResult={this.state.hoveredRepairerResult} onZoomChanged={this.onMapZoomChanged} lat={this.state.mapCenterLat} lng={this.state.mapCenterLng} posLat={this.state.addressLat} posLng={this.state.addressLng} zoom={this.state.zoom} reparactorOnly={this.state.reparactorOnly} category={this.state.category} address={this.state.address} isSearchClosest={this.isSearchClosest} />
              </div>

              <div className="col-lg-5 col-md-5 col-sm-12 col-xs-12 pull-left" id="result-list">
                <Results onFilterReparactor={this.handleFilterReparactor} repairers={filteredRepairers} onRepairerResultHover={this.onRepairerResultHover} hoveredRepairer={this.state.hoveredRepairer} category={this.state.category} address={this.state.address} />
              </div>
            </div>
          </div>
        </div>

        <Modal isOpen={ this.state.isModalOpen } contentLabel="Modal" className={{ base: 'react-modal' }} overlayClassName={{ base: 'react-modal-overlay' }}>
          <div className="modal-content">
            <div className="modal-header">
                <h3>{this.state.modalHeader}</h3>
            </div>
            <div className="modal-body">
                <p>{this.state.modalBody}</p>
            </div>
            <div className="modal-footer">
                <div className="btn-toolbar pull-right">
                    <button className="btn btn-default" type="button" title="Fermer" onClick={this.onModalClose}>Fermer</button>
                </div>
            </div>
          </div>
        </Modal>
      </div>
    )
  }
};

//Map URL queries to props
export default addUrlProps({ urlPropsQueryConfig })(Search);