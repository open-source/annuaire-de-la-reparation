const config = require('Config');

var buildUrlParamString = function(urlParams) {
    if(urlParams.length > 0) {
        let paramsString = urlParams.map(function (param) {
            return param.name + '=' + param.value; 
        }).join('&');

        return '?' + paramsString;
    }

    return '';
}

var getAddressComponentNames = function(addressComponents) {
    let regionName = "";
    let departmentName = "";
    let countryName = "";
    let localityName = "";
    let routeName = "";
    let streetNumber = "";

    //Retrieve region and department name from addressComponents
    for (let addrComponent of addressComponents) {
      if (addrComponent.types.includes("administrative_area_level_1")) {
        regionName = addrComponent.long_name;
      }

      if (addrComponent.types.includes("administrative_area_level_2")) {
        departmentName = addrComponent.long_name;
      }

      if (addrComponent.types.includes("country")) {
        countryName = addrComponent.long_name;
      }

      if (addrComponent.types.includes("locality")) {
        localityName = addrComponent.long_name;
      }

      if (addrComponent.types.includes("route")) {
        routeName = addrComponent.long_name;
      }

      if (addrComponent.types.includes("street_number")) {
        streetNumber = addrComponent.long_name;
      }
    }

    return {"region" : regionName, "department": departmentName, "country": countryName, "locality": localityName, "route": routeName, "streetNumber": streetNumber};
}

var getFranceResultFirst = function(results) {
    if(results.length == 1) {
      return results[0];
    }

    for (let res of results) {
      for (let addrComponent of res.addressComponents) {
        if (addrComponent.types.includes("country") && addrComponent.long_name === "France") {
          return res;
        }
      }
    }

    return results[0];
}

var getRegionName = function(result){
    for(var i=0;i<result.address_components.length;i++){
        var addr=result.address_components[i];
        if(addr.types.includes("administrative_area_level_1")){
            return addr.long_name;
        }
    }
    return null;
}

function getUrlFromSpecificRedirection(category, address, addressComponents) {

    let addressObj = getAddressComponentNames(addressComponents);
    let regionName = addressObj.region;
    let departmentName = addressObj.department;
    let countryName = addressObj.country;
    let localityName = addressObj.locality;
    let routeName = addressObj.route;
    let streetNumber = addressObj.streetNumber;

    var url = "";

    if (regionName === "Bretagne") {
      url = config.annuaire_bretagne_url;
    } else if (["Haute-Vienne", "Correze", "Creuse"].includes(departmentName)) { // Ancienne région Limousin
      url = config.annuaire_nouvelle_aquitaine_url;
    } else if (["Gironde", "Dordogne", "Lot-et-Garonne", "Landes", "Pyrénées-Atlantiques"].includes(departmentName)) { // Ancienne région Aquitaine
      url = config.annuaire_nouvelle_aquitaine_url;
    } else if (["Charente", "Charente-Maritime", "Deux-Sèvres", "Vienne"].includes(departmentName)) { // Ancienne région Poitou-Charente
      url = config.annuaire_nouvelle_aquitaine_url;
    } else if (regionName === "Occitanie") { //redirection with params
      url = config.annuaire_occitanie_url;
    } else if (countryName === "Réunion") { //redirection with params
        url = config.annuaire_reunion_url;
    }

    return url;
}

  /**
   * Handle specific redirections for searches in following areas : Bretagne, Nouvelle-Aquitaine, Occitanie, Réunion
   */
  var handleSpecificRedirections = function(category, address, addressComponents) {

    //Handle redirections for region Bretagne and Nouvelle-Aquitaine and department Occitanie
    let url = getUrlFromSpecificRedirection(category, address, addressComponents);
    let redirected = false;
    if (url !== "") {
      redirected = true;
      window.location = url;
    }
    return redirected;
  }

var getBoundsZoomLevel = function(bounds, mapDim) {
    var WORLD_DIM = { height: 256, width: 256 };
    var ZOOM_MAX = 21;

    function latRad(lat) {
        var sin = Math.sin(lat * Math.PI / 180);
        var radX2 = Math.log((1 + sin) / (1 - sin)) / 2;
        return Math.max(Math.min(radX2, Math.PI), -Math.PI) / 2;
    }

    function zoom(mapPx, worldPx, fraction) {
        return Math.floor(Math.log(mapPx / worldPx / fraction) / Math.LN2);
    }

    var ne = bounds.getNorthEast();
    var sw = bounds.getSouthWest();

    var latFraction = (latRad(ne.lat()) - latRad(sw.lat())) / Math.PI;

    var lngDiff = ne.lng() - sw.lng();
    var lngFraction = ((lngDiff < 0) ? (lngDiff + 360) : lngDiff) / 360;

    var latZoom = zoom(mapDim.height, WORLD_DIM.height, latFraction);
    var lngZoom = zoom(mapDim.width, WORLD_DIM.width, lngFraction);

    return Math.min(latZoom, lngZoom, ZOOM_MAX);
}

exports.buildUrlParamString = buildUrlParamString;
exports.getBoundsZoomLevel = getBoundsZoomLevel;
exports.getFranceResultFirst = getFranceResultFirst;
exports.handleSpecificRedirections = handleSpecificRedirections;
exports.getRegionName = getRegionName;