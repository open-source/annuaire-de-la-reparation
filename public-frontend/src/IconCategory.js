import React from 'react'

export default class IconeCategory extends React.Component {

	constructor(props) {
		super(props);
	}
	
	render() {
		let config = require('Config');
		let size = this.props.size ? this.props.size : 32;
		return (
			<img src={config.imgRelativePath + 'category/' + this.props.categorie.id + '.png'} title={this.props.categorie.name} alt={this.props.categorie.name} height={size} width={size} />
		);
	}
}