import Linkify from 'react-linkify';
import React from 'react';
import ReactDOM from "react-dom";
import { addUrlProps, UrlQueryParamTypes } from 'react-url-query';
import { ShareButtons, generateShareIcon } from 'react-share';
import Modal from 'react-modal';
import $ from "jquery";
import createHistory from 'history/createBrowserHistory'

import HomeSearchForm from './HomeSearchForm';
import CraftspersonMap from './CraftspersonMap';
import IconsElement from './IconsElement';
import LikeModal from './LikeModal';
import ErrorReportModal from './ErrorReportModal';
import CraftspersonErrorReportModal from './CraftspersonErrorReportModal'
import ContactModal from './ContactModal';

const config = require('Config');

// Share buttons
const {
  FacebookShareButton,
    GooglePlusShareButton,
    TwitterShareButton,
} = ShareButtons;
const FacebookIcon = generateShareIcon('facebook');
const TwitterIcon = generateShareIcon('twitter');
const GooglePlusIcon = generateShareIcon('google');

// Accepted url params
const urlPropsQueryConfig = {
    address: { type: UrlQueryParamTypes.string },
    category: { type: UrlQueryParamTypes.number },
};

// Modals
const MODAL_NONE = 0;
const MODAL_LIKE = 1;
const MODAL_ERROR_REPORT = 2;
const MODAL_CONTACT = 3;
const MODAL_CRAFTSPERSON_ERROR_REPORT = 4;

// Like confirm status
const LIKE_CONFIRM_NONE = 0;
const LIKE_CONFIRM_NOT_FOUND = 1;
const LIKE_CONFIRM_ERROR = 2;
const LIKE_CONFIRM_SUCCESS = 3;

/**
 * Component that displays a Craftsperson's details.
 */
class Craftsperson extends React.Component {

    /**
     * Constructor.
     * 
     * @param {*} props properties
     */
    constructor(props) {
        super(props);
        this.state = {
            craftsperson: null,
            craftspersonLogoUrl: null,
            openModal: MODAL_NONE,
            likeConfirmStatus: LIKE_CONFIRM_NONE
        };

        this.onOpenModal = this.onOpenModal.bind(this);
        this.onCloseModal = this.onCloseModal.bind(this);
        this.confirmLikeCraftsperson = this.confirmLikeCraftsperson.bind(this);
        this.loadCraftsperson = this.loadCraftsperson.bind(this);
        this.loadCraftspersonLogo = this.loadCraftspersonLogo.bind(this);
    }

    /**
     * Load craftsperson data from server.
     */
    componentDidMount() {
        // Get id or token from url
        let parentNode = ReactDOM.findDOMNode(this).parentNode;
        let craftspersonId = null;
        let idAttr = parentNode.attributes.getNamedItem('data-id');
        let tokenAttr = parentNode.attributes.getNamedItem('data-token');
        let choicesAttr = parentNode.attributes.getNamedItem('data-choices');
        let cfeAttr = parentNode.attributes.getNamedItem('data-cfe');
        if (idAttr.value !== '') {
            // Load craftsperson details from API
            this.loadCraftsperson(idAttr.value);
        }
        else if (tokenAttr.value !== '') {
            // Confirm like from token
            this.confirmLikeCraftsperson(tokenAttr.value);
        }
        else {
            // Missing data
            this.notFoundCraftsperson();
        }

        if (choicesAttr.value !== ''){
            this.setState({
                craftpersonErrorMsgs : JSON.parse(choicesAttr.value)
            });
        }

        if(cfeAttr.value !== ''){
            this.setState({
                cfe : JSON.parse(cfeAttr.value)
            });
        }

    }

    /**
     * Confirm a like request.
     * 
     * @param {*} token 
     */
    confirmLikeCraftsperson(token) {
        const that = this;
        $.post(config.apiBaseUrl + 'craftspersons/confirm-like/' + token)
            .done(function (data) {
                that.setState({
                    craftsperson: data,
                    likeConfirmStatus: LIKE_CONFIRM_SUCCESS
                });
                that.switchToCraftspersonUrl(data);
                that.loadCraftspersonLogo(data);
            })
            .fail(function (data) {
                const craftsperson = data.status == 409 ? data.responseJSON : null;
                const confirmStatus = craftsperson ? LIKE_CONFIRM_ERROR : LIKE_CONFIRM_NOT_FOUND;
                that.setState({
                    craftsperson: craftsperson,
                    likeConfirmStatus: confirmStatus
                });
                if (craftsperson) {
                    that.switchToCraftspersonUrl(craftsperson);
                    that.loadCraftspersonLogo(craftsperson);
                }
            });
    }

    /**
     * Change the url to that of a craftsperson with the corresponding id.
     * 
     * @param {*} craftsperson 
     */
    switchToCraftspersonUrl(craftsperson) {
        history.pushState(craftsperson, "Réparateur", "/reparateur/" + craftsperson.id);
    }

    /**
     * Load a craftsperson's details from the API by id.
     * 
     * @param {*} id 
     */
    loadCraftsperson(id) {
        let that = this;
        $.getJSON(config.apiBaseUrl + 'craftspersons/' + id)
            .done(function (data) {
                that.setState({
                    craftsperson: data
                });
                that.loadCraftspersonLogo(data);
            })
            .fail(function () {
                that.notFoundCraftsperson();
            });
    }

    /**
     * Load craftsperson logo.
     * 
     * @param {*} craftsperson 
     */
    loadCraftspersonLogo(craftsperson) {
        const that = this;

        if (craftsperson.isReparactor && craftsperson.logo) {
            var xhr = new XMLHttpRequest();
            xhr.onreadystatechange = function () {
                if (this.readyState == 4 && this.status == 200) {
                    const logoSrc = window.URL.createObjectURL(this.response);
                    that.setState({
                        craftspersonLogoUrl: logoSrc
                    });
                }
            };
            xhr.open('GET', config.apiBaseUrl + 'craftspersons/logo/' + craftsperson.logo);
            xhr.responseType = 'blob';
            xhr.send();
        }
    }

    /**
     * Craftsperson not found.
     */
    notFoundCraftsperson() {
        alert('Erreur : réparateur introuvable');
        const history = createHistory();
        history.push("/");
        history.go(0);
    }

    /**
     * Open a modal.
     */
    onOpenModal(modalId) {
        this.setState({
            openModal: modalId
        })
    }

    /**
     * Close any modal.
     */
    onCloseModal() {
        this.setState({
            openModal: MODAL_NONE
        })
    }

    /**
     * Create url for social media share feature.
     * 
     * @return string
     */
    getShareUrl() {
        const currentLocation = window.location;

        let host = currentLocation.hostname;
        if (currentLocation.port && currentLocation.port != 80) {
            host += ':' + currentLocation.port;
        }

        return window.location.protocol + '//' + host + window.location.pathname;
    }

    /**
     * Render component.
     */
    render() {

        // Wait for data before rendering
        if (!this.state.craftsperson) {
            return (
                <div>
                    <HomeSearchForm address={this.props.address} category={this.props.category} />
                    <div id="craftsperson" className="page">
                        {this.state.likeConfirmStatus == LIKE_CONFIRM_NONE &&
                            <p className='loading-indicator'>Chargement...</p>
                        }
                        {this.state.likeConfirmStatus == LIKE_CONFIRM_NOT_FOUND &&
                            <div className="container">
                                <div className="alert alert-danger alert-like">
                                    <strong>Erreur : </strong>Demande de recommandation introuvable.<br />
                                    <a href="/" title="Accueil" className="alert-link">Retour à l'accueil</a>
                                </div>
                            </div>
                        }
                    </div>
                </div>
            );
        }

        const c = this.state.craftsperson;
        const craftspersonAddress = c.address1 + ' ' + c.zipCode + ' ' + c.zipCodeLabel;
        const routeUrl = 'https://www.google.fr/maps/dir/' + this.props.address + '/' + craftspersonAddress;
        const mailto = c.email ? 'mailto:' + c.email : '';
        const recommendButtonTitle = "Je recommande ce réparateur (" + c.likes + " recommandations au total)";
        let websiteAbsoluteUrl = c.website;
        if (c.website && !c.website.startsWith("http://") && !c.website.startsWith("https://")) {
            // Ensure the website url is absolute
            websiteAbsoluteUrl = "http://" + c.website;
        }
        return (
            <div>
                <HomeSearchForm responsive={true} address={this.props.address} category={this.props.category} showSlogan={false} />
                <div id="craftsperson" className="page">
                    <div className="container">
                        {this.state.likeConfirmStatus == LIKE_CONFIRM_ERROR &&
                            <div className="alert alert-danger alert-like">
                                <strong>Erreur : </strong>Demande de recommandation déjà validée.
                            </div>
                        }
                        {this.state.likeConfirmStatus == LIKE_CONFIRM_SUCCESS &&
                            <div className="alert alert-info alert-like">
                                Recommandation prise en compte avec succès.
                            </div>
                        }
                        <h1><IconsElement categories={c.categories} />&nbsp;{c.name}</h1>
                        <div className="row icon-bar">
                            <div className="col-xs-12">
                                <div className="pull-right" id="btn-social">
                                    <button className="btn btn-default btn-like" title={recommendButtonTitle} onClick={() => this.onOpenModal(MODAL_LIKE)}><span className="glyphicon glyphicon-thumbs-up glyphicon-font-18" title="Recommandations"></span> Recommander ({c.likes})</button>

                                    <FacebookShareButton url={this.getShareUrl()} title={config.socialMediaShareText}>
                                        <FacebookIcon size={32} round />
                                    </FacebookShareButton>
                                    <TwitterShareButton url={this.getShareUrl()} title={config.socialMediaShareText}>
                                        <TwitterIcon size={32} round />
                                    </TwitterShareButton>
                                    <GooglePlusShareButton url={this.getShareUrl()} title={config.socialMediaShareText}>
                                        <GooglePlusIcon size={32} round />
                                    </GooglePlusShareButton>
                                </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-md-5 col-sm-6">
                                {this.state.craftspersonLogoUrl &&
                                    <div className="logo">
                                        <img src={this.state.craftspersonLogoUrl} alt="Logo du réparateur" title="Logo" />
                                    </div>
                                }
                                <div className="panel panel-default panel-general-info">
                                    <div className="panel-body">
                                        <div style={{ marginBottom: '10px' }}>
                                            <span className="info-label"><strong>Adresse :</strong></span><br />
                                            {c.address1}<br />
                                            {c.address2 &&
                                                <span>
                                                    {c.address2}<br />
                                                </span>
                                            }
                                            {c.zipCode} {c.zipCodeLabel}<br />
                                            {this.props.address &&
                                                <a href={routeUrl} target="_blank"><span className="glyphicon glyphicon-map-marker"></span> Voir l'itinéraire</a>
                                            }
                                        </div>
                                        {c.phone &&
                                            <div>
                                                <span className="info-label"><strong>Téléphone :</strong></span> <a href={"tel:" + c.phone}>{c.phone}</a>
                                            </div>
                                        }
                                        {c.email &&
                                            <div>
                                                <span className="info-label"><strong>Contact :</strong></span> <a href="#" title="Envoyer un email" onClick={() => this.onOpenModal(MODAL_CONTACT)}><span className="glyphicon glyphicon-envelope" title="Contact" ></span></a>
                                            </div>
                                        }
                                        {c.website &&
                                            <div>
                                                <span className="info-label"><strong>Site internet :</strong></span> <a href={websiteAbsoluteUrl} target="_blank" title="Visiter le site internet">{c.website}</a>
                                            </div>
                                        }
                                    </div>
                                </div>
                                {c.isReparactor && (c.reparactorDescription || c.reparactorHours || c.reparactorServices || c.reparactorCertificates) &&
                                    <div className="panel panel-default">
                                        <div className="panel-body">
                                            {c.reparactorDescription &&
                                                <div>
                                                    <h4 className="info-label">Description</h4>
                                                    <span className="textarea-display">{c.reparactorDescription}</span>
                                                </div>
                                            }
                                            {c.reparactorHours &&
                                                <div>
                                                    <h4 className="info-label">Horaires d'ouverture</h4>
                                                    <span className="textarea-display">{c.reparactorHours}</span>
                                                </div>
                                            }
                                            {c.reparactorServices &&
                                                <div>
                                                    <h4 className="info-label">Services proposés</h4>
                                                    <span className="textarea-display">{c.reparactorServices}</span>
                                                </div>
                                            }
                                            {c.reparactorCertificates &&
                                                <div>
                                                    <h4 className="info-label">Certifications</h4>
                                                    <span className="textarea-display">{c.reparactorCertificates}</span>
                                                </div>
                                            }
                                        </div>
                                    </div>
                                }
                                {c.otherInfo &&
                                    <div className="panel panel-default">
                                        <div className="panel-body">
                                            <div>
                                                <h4 className="info-label">Autres informations</h4>
                                                <Linkify>
                                                    <span className="textarea-display">{c.otherInfo}</span>
                                                </Linkify>
                                            </div>
                                        </div>
                                    </div>
                                }
                            </div>
                            <div className="col-md-7 col-sm-6">
                                <CraftspersonMap lat={c.latitude} lng={c.longitude} isReparactor={c.isReparactor} />
                            </div>
                        </div>
                        <div className="row error-btn-bar">
                            <div className="col-xs-12">
                                <div className="pull-left back-button">
                                    <a href="#" onClick={() => history.go(-1)}><span className="glyphicon glyphicon-chevron-left"></span>Revenir à la recherche</a>
                                </div>
                                <div className="pull-right unprobleme">
                                    <h3>Un problème?</h3>
                                    <button className="btn btn-default btn-responsive" onClick={() => this.onOpenModal(MODAL_ERROR_REPORT)}>Je suis un client et il y a un problème pour joindre ce réparateur</button>
                                    <br/><button className="btn btn-default btn-responsive" onClick={() => this.onOpenModal(MODAL_CRAFTSPERSON_ERROR_REPORT)}>Je suis l’artisan et je constate une erreur sur ma fiche</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <LikeModal craftsperson={c} isOpen={this.state.openModal === MODAL_LIKE} onClose={this.onCloseModal} />
                <ErrorReportModal craftsperson={c} craftpersonErrorMsgs={this.state.craftpersonErrorMsgs} isOpen={this.state.openModal === MODAL_ERROR_REPORT} onClose={this.onCloseModal} />
                <CraftspersonErrorReportModal craftsperson={c} cfe={this.state.cfe} isOpen={this.state.openModal === MODAL_CRAFTSPERSON_ERROR_REPORT} onClose={this.onCloseModal} />
                <ContactModal craftsperson={c} craftpersonErrorMsgs={this.state.craftpersonErrorMsgs} isOpen={this.state.openModal === MODAL_CONTACT} onClose={this.onCloseModal} />
            </div>
        );
    }
}

//Map URL queries to props
export default addUrlProps({ urlPropsQueryConfig })(Craftsperson);