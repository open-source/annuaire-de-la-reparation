import React from 'react'
import ReactDOM from "react-dom"
import { addUrlProps, UrlQueryParamTypes } from 'react-url-query';
import SearchForm from './SearchForm'
import createHistory from 'history/createBrowserHistory';
import $ from "jquery"
import { buildUrlParamString } from './Utils';

const config = require('Config');

const urlPropsQueryConfig = {
  address: { type: UrlQueryParamTypes.string },
  category: { type: UrlQueryParamTypes.number },
};

const history = createHistory();

let handleSpecificRedirections = require('./Utils.js').handleSpecificRedirections;
let getFranceResultFirst = require('./Utils.js').getFranceResultFirst;
let getRegionName = require('./Utils.js').getRegionName;

class HomeSearchForm extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      category: '',
      address: 'France',
      repairerCount: null,
      browserKey: ''
    };

    this.handleSubmit = this.handleSubmit.bind(this);
  }

  componentDidMount() {
    let that = this;


    $.getJSON(config.apiBaseUrl + "parameters").done(function (data) {
        that.setState({
            browserKey: data.google_maps_api_key
        });
    });

    // Load repairer count
    if (this.state.repairerCount == null) {
      $.getJSON(config.apiBaseUrl + "craftspersons").done(function (data) {
        that.setState({
          repairerCount: parseInt(data.total)
        })
      });
    }
  }

  componentWillMount() {
    let category = this.props.category;
    let address = this.props.address;

    this.setState({
      category: category,
      address: address
    })
  }

  handleSubmit(category, address, lat, lng, precision, region) {

    $.getJSON(config.googleMapGeocodeApiUrl + "&address=" + encodeURI(address) + "&region=fr&key="+this.state.browserKey).done(function (data) {
      if (data.status === "OK") {
        //By default, if several results are found, first one is taken
        let results = getFranceResultFirst(data.results);

        //console.table(results.address_components.types);
        console.log(getRegionName(results));
        let lat = results.geometry.location.lat;
        let lng = results.geometry.location.lng;

        //For searches in following areas : Bretagne, Nouvelle-Aquitaine, Occitanie, redirect to external sites
        const redirected = handleSpecificRedirections(category, address, results.address_components);
        
        // If no redirection, go to search page
        if(!redirected) {
          const urlParams = [
            { name: "category", value: category },
            { name: "address", value: address },
            { name: "lat", value: lat },
            { name: "lng", value: lng },
            { name: "p", value: precision },
            { name: "region", value: region}
          ];
          const requestString = encodeURI('/search' + buildUrlParamString(urlParams));
          window.location = requestString;
        }

      }
    });
  }

  render() {

    let nbArtisans = this.state.repairerCount ? this.state.repairerCount : 0;
    nbArtisans = nbArtisans.toLocaleString('fr');

    return (
      <div>
        <SearchForm onSubmit={this.handleSubmit} category={this.props.category} address={this.props.address}/>

        {this.props.showSlogan &&
          <div className="container">
            <div className="row slogan-container">
              <strong>{nbArtisans} artisans répertoriés !</strong>
              <br/>
              <span>Trouver facilement votre professionnel de la réparation et du dépannage partout en France*.
              </span>
            </div>
          </div>
        }
      </div>
    )
  }
};

/* Propriétés par défaut */
HomeSearchForm.defaultProps = {
  showSlogan: true
};

//Map URL queries to props
export default addUrlProps({ urlPropsQueryConfig })(HomeSearchForm);