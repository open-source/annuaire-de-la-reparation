
#remove ARPublicBundle previous assets
rm -Rf ../backend/src/ARPublicBundle/Resources/public/*

#create assets subfolders in ARPublicBundle asset folder
mkdir ../backend/src/ARPublicBundle/Resources/public/js
npm run build-prod

#copy other assets (css, images, font, etc.) to ARPublicBundle asset folder
cp -r ./public/* ../backend/src/ARPublicBundle/Resources/public/