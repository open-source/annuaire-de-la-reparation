# Se placer dans le répertoire public-frontend  

> cd public-frontend

## Installer des dépendances (répertoire node_modules)  

> npm install

## Développer et tester 

Démarrer le serveur avec webpack (pour les développements)  

> webpack-dev-server --port=3000 --hot --inline --progress --colors

## Déployer l'application en production  

Depuis le répertoire public frontend, lancer le script "install.sh" pour générer les ressources côté production  

> ./install.sh  

## Accéder à l'application

Accès à la partie frontend publique en mode développement :

http://localhost:1080/app_dev.php/ (page d'accueil)  
http://localhost:1080/app_dev.php/search (page de recherche)  

Accéder à la partie frontend public en mode production :  

http://localhost:1080/ (page d'accueil)  
http://localhost:1080/search (page de recherche)  


