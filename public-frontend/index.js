import 'babel-polyfill';
import { configureUrlQuery } from 'react-url-query';
import React from 'react'
import ReactDOM from "react-dom";
import Search from './src/Search';
import HomeSearchForm from './src/HomeSearchForm';
import Craftsperson from './src/Craftsperson';

import createHistory from 'history/createBrowserHistory';

const history = createHistory();

//Link the history to url-queries.
configureUrlQuery({ history });

//Map components to elements ids. Check first that id exist in DOM to avoid errors (otherwise all components must be in one page)
document.getElementById("search-component") &&
ReactDOM.render( <Search category='' address='France' /> ,
    document.getElementById("search-component")
);

document.getElementById("search-banner-component") &&
ReactDOM.render( <HomeSearchForm /> ,
    document.getElementById("search-banner-component")
);

document.getElementById("craftsperson-component") &&
ReactDOM.render( <Craftsperson /> ,
    document.getElementById("craftsperson-component")
);
