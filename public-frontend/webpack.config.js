module.exports = {
  entry: [
    'babel-polyfill',
    './index.js'
  ],
  
  module: {
    loaders: [
      { test: /\.js$/, exclude: /node_modules/, loader: 'babel-loader?presets[]=es2015&presets[]=react' }
    ]
  },

  externals: {
    'Config': JSON.stringify({
      apiBaseUrl: "/api/",
      googleMapGeocodeApiUrl: "https://maps.google.com/maps/api/geocode/json?language=fr",
      imgRelativePath: "/bundles/arpublic/images/",
      countryScaleZoom: 7, 
      regionScaleZoom: 10, 
      cityScaleZoom: 15, 
      streetScaleZoom: 16,
      defaultZoom: 6,
      mapCenterLat: 46.5,
      mapCenterLng: 1.92,
      defaultAddress: "France", 
      minSearchRadius: 5, // Km
      maxSearchRadius: 40, // Km
      socialMediaShareText: "J'aimerais partager ce réparateur avec vous",
      annuaire_bretagne_url: "http://www.crma.bzh/annuaire-reparacteurs",
      annuaire_limousin_url: "http://www.reparacteurs-limousin.fr/trouver-un-reparacteur",
      annuaire_aquitaine_url: "http://www.reparacteurs-aquitaine.fr",
      annuaire_occitanie_url: "http://www.reparacteurs-occitanie.fr",
      annuaire_nouvelle_aquitaine_url: "https://www.dechets-nouvelle-aquitaine.fr/reparacteurs/",
      annuaire_reunion_url: "http://www.reparer.re"
    })
  },

  // Proxy to api in development mode
  devServer: {
		proxy: {
			"/api": "http://localhost:10180/app_dev.php",
    }
  },
}

