--Ajout des cfe gérés par l'application
INSERT INTO `t_cfe` (`id`, `url`, `region`) VALUES (8, '', 'Bourgogne-Franche-Comté'), (9, '', 'Hauts-de-France'), (10, '', 'Martinique'), (11, '', 'Réunion');

-- Liaison Département-CFE
UPDATE `r_department` SET `cfe` = '8' WHERE `r_department`.`code` IN ('21', '25', '39', '58', '70', '71', '89', '90');
UPDATE `r_department` SET `cfe` = '9' WHERE `r_department`.`code` IN ('02', '59', '60', '62', '80');
UPDATE `r_department` SET `cfe` = '10' WHERE `r_department`.`code` IN ('972');
UPDATE `r_department` SET `cfe` = '11' WHERE `r_department`.`code` IN ('974');
