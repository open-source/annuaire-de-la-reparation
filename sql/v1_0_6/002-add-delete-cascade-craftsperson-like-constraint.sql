-- Drop old constraint on craftsperson_like
ALTER TABLE `t_craftsperson_like`
  DROP FOREIGN KEY `t_craftsperson_t_craftsperson_like_request_fk`;

-- Add new on delete cascade constraint
ALTER TABLE `t_craftsperson_like`
  ADD CONSTRAINT `t_craftsperson_t_craftsperson_like_request_fk` FOREIGN KEY (`id_craftsperson`) REFERENCES `t_craftsperson` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;