-- Remove '.', '-', ' '
update t_craftsperson set phone = REPLACE(phone, '.', '');
update t_craftsperson set phone = REPLACE(phone, '-', '');
update t_craftsperson set phone = REPLACE(phone, ' ', '');
update t_craftsperson set phone = REPLACE(phone, ',', '');

-- Add ',' between two phones
update t_craftsperson set phone = concat(substring(phone,1,10), ',',substring(phone,11,10)) where length(phone) = 20;