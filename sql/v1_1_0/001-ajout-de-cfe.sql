CREATE TABLE `t_cfe` (
  `id` int(11) NOT NULL,
  `url` varchar(300) NOT NULL,
  `region` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for table `t_cfe`
--
ALTER TABLE `t_cfe`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for table `t_cfe`
--
ALTER TABLE `t_cfe`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;


--Ajout des cfe gérés par l'application
INSERT INTO `t_cfe` (`id`, `url`, `region`) VALUES (1, '', 'Auvergne Rhône-Alpes'), (2, '', 'Bretagne'), (3, '', 'Centre-Val de Loire'), (4, '', 'Nouvelle-Aquitaine'), (5, '', 'Occitanie'), (6, '', 'Pays de la Loire'), (7, '', 'Provence-Alpes-Côte-d’Azur');

ALTER TABLE `r_department` ADD `cfe` INT NOT NULL AFTER `name`;
ALTER TABLE `r_department` ADD INDEX(`cfe`);


-- Liaison Département-CFE
UPDATE `r_department` SET `cfe` = '1';
UPDATE `r_department` SET `cfe` = '1' WHERE `r_department`.`code` IN ('01', '03', '07', '15', '26', '38', '42', '43', '63', '69', '73', '74');
UPDATE `r_department` SET `cfe` = '2' WHERE `r_department`.`code` IN ('22', '29', '35', '56');
UPDATE `r_department` SET `cfe` = '3' WHERE `r_department`.`code` IN ('18', '28', '36', '37', '41', '45');
UPDATE `r_department` SET `cfe` = '4' WHERE `r_department`.`code` IN ('16', '17', '19', '23', '24', '33', '40', '47', '64', '79', '86', '87');
UPDATE `r_department` SET `cfe` = '5' WHERE `r_department`.`code` IN ('09', '11', '12', '30', '31', '32', '34', '46', '48', '65', '66', '81', '82');
UPDATE `r_department` SET `cfe` = '6' WHERE `r_department`.`code` IN ('44', '49', '53', '72', '85');
UPDATE `r_department` SET `cfe` = '7' WHERE `r_department`.`code` IN ('04', '05', '06', '13', '83', '84');


ALTER TABLE `r_department` ADD CONSTRAINT `t_cfe_r_department_fk` FOREIGN KEY (`cfe`) REFERENCES `are`.`t_cfe`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;


