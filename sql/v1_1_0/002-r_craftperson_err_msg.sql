
--
-- Table structure for table `r_craftperson_err_msg`
--

CREATE TABLE `r_craftperson_err_msg` (
  `id` int(11) NOT NULL,
  `message` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `r_craftperson_err_msg`
--

INSERT INTO `r_craftperson_err_msg` (`id`, `message`) VALUES
(1, 'Pas d''entreprise à l''adresse'),
(2, 'Ne correspond pas à l''activité de réparation'),
(3, 'Erreur téléphone'),
(5, 'Erreur Mail');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `r_craftperson_err_msg`
--
ALTER TABLE `r_craftperson_err_msg`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for table `r_craftperson_err_msg`
--
ALTER TABLE `r_craftperson_err_msg`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
