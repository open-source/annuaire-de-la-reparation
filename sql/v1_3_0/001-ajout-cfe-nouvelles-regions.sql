--Ajout des cfe gérés par l'application
INSERT INTO `t_cfe` (`id`, `url`, `region`) VALUES (12, '', 'Grand Est'), (13, '', 'Ile de France'), (14, '', 'Normandie');

-- Liaison Département-CFE
UPDATE `r_department` SET `cfe` = '12' WHERE `r_department`.`code` IN ('08', '10', '51', '52', '54', '55', '57', '67', '68', '88');
UPDATE `r_department` SET `cfe` = '13' WHERE `r_department`.`code` IN ('75', '77', '78', '91', '92', '93', '94', '95');
UPDATE `r_department` SET `cfe` = '14' WHERE `r_department`.`code` IN ('14', '27', '50', '61', '76');
