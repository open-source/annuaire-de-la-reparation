create table if not exists r_category
(
    id   int          not null
        primary key,
    name varchar(255) not null
)
    charset = utf8;

create table if not exists r_craftperson_err_msg
(
    id      int auto_increment
        primary key,
    message varchar(100) not null
)
    charset = utf8;

create table if not exists r_naf_code
(
    code  varchar(6)   not null
        primary key,
    label varchar(255) not null
)
    charset = utf8;

create table if not exists r_category_has_naf_code
(
    id_category   int        not null,
    code_naf_code varchar(6) not null,
    primary key (id_category, code_naf_code),
    constraint r_category_r_category_has_naf_code_fk
        foreign key (id_category) references r_category (id),
    constraint r_naf_code_r_category_has_naf_code_fk
        foreign key (code_naf_code) references r_naf_code (code)
)
    charset = utf8;

create table if not exists t_cfe
(
    id     int auto_increment
        primary key,
    url    varchar(300) not null,
    region varchar(40)  not null
)
    charset = utf8;

create table if not exists r_department
(
    code varchar(3)   not null
        primary key,
    name varchar(255) not null,
    cfe  int          not null,
    constraint t_cfe_r_department_fk
        foreign key (cfe) references t_cfe (id)
)
    charset = utf8;

create index cfe
    on r_department (cfe);

create table if not exists t_craftsperson
(
    id                      int auto_increment
        primary key,
    siret                   varchar(14)                            not null,
    name                    varchar(255)                           not null,
    address_1               varchar(255)                           not null,
    address_2               varchar(255)                           null,
    zip_code                varchar(100)                           not null,
    zip_code_label          varchar(255)                           not null,
    phone                   varchar(50)                            null,
    email                   varchar(100)                           null,
    website                 varchar(255)                           null,
    is_enabled              tinyint(1)   default 1                 not null,
    cma_code                varchar(3)                             not null,
    is_reparactor           tinyint(1)   default 0                 not null,
    is_error                tinyint(1)   default 0                 not null,
    reparactor_description  text                                   null,
    reparactor_hours        text                                   null,
    reparactor_services     text                                   null,
    reparactor_certificates text                                   null,
    creation_date           datetime     default CURRENT_TIMESTAMP not null,
    is_updated              tinyint(1)   default 0                 not null,
    update_date             datetime                               null,
    latitude                double                                 null,
    longitude               double                                 null,
    geocoding_status        varchar(100) default 'nc'              not null,
    logo_file               varchar(255)                           null,
    likes                   int                                    not null,
    other_info              text                                   null,
    constraint r_department_t_craftsperson_fk
        foreign key (cma_code) references r_department (code)
)
    charset = utf8;

create table if not exists t_craftsperson_has_naf_code
(
    id_craftsperson int        not null,
    naf_code        varchar(6) not null,
    primary key (id_craftsperson, naf_code),
    constraint r_naf_code_t_craftsperson_has_naf_code_fk
        foreign key (naf_code) references r_naf_code (code),
    constraint t_craftsperson_t_craftsperson_has_naf_code_fk
        foreign key (id_craftsperson) references t_craftsperson (id)
            on delete cascade
)
    charset = utf8;

create table if not exists t_craftsperson_like
(
    id                 int auto_increment
        primary key,
    id_craftsperson    int                  not null,
    email              varchar(100)         not null,
    confirmation_token varchar(255)         not null,
    is_confirmed       tinyint(1) default 0 not null,
    constraint t_craftsperson_t_craftsperson_like_request_fk
        foreign key (id_craftsperson) references t_craftsperson (id)
            on delete cascade
)
    charset = utf8;

create table if not exists t_import_log
(
    id       int auto_increment
        primary key,
    date     datetime     not null,
    region   varchar(255) null,
    siret    varchar(14)  null,
    filename varchar(255) not null,
    line     int          not null,
    message  text         not null
)
    charset = utf8;

create table if not exists t_user
(
    id                    int auto_increment
        primary key,
    username              varchar(180)         not null,
    username_canonical    varchar(180)         not null,
    email                 varchar(180)         not null,
    email_canonical       varchar(180)         not null,
    enabled               tinyint(1)           not null,
    salt                  varchar(255)         null,
    password              varchar(255)         not null,
    last_login            datetime             null,
    confirmation_token    varchar(255)         null,
    password_requested_at datetime             null,
    roles                 text                 not null,
    firstname             varchar(50)          not null,
    lastname              varchar(50)          not null,
    phone                 varchar(50)          null,
    is_mail_contact       tinyint(1) default 1 not null
)
    charset = utf8;

create table if not exists t_user_has_department
(
    id_user         int        not null,
    code_department varchar(3) not null,
    primary key (id_user, code_department),
    constraint r_department_t_user_has_department_fk
        foreign key (code_department) references r_department (code),
    constraint t_user_t_user_has_department_fk
        foreign key (id_user) references t_user (id)
)
    charset = utf8;


