SET storage_engine=INNODB;

DROP DATABASE IF EXISTS are_test;
CREATE DATABASE are_test;

CREATE USER 'are_test' IDENTIFIED BY 'are_test';
GRANT USAGE ON *.* TO 'are_test' IDENTIFIED BY 'are_test';
GRANT SELECT, INSERT, UPDATE, DELETE ON `are_test`.* TO 'are_test';

FLUSH PRIVILEGES;