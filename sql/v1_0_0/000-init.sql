
CREATE TABLE r_category (
                id INT NOT NULL,
                name VARCHAR(255) NOT NULL,
                PRIMARY KEY (id)
);


CREATE TABLE t_import_log (
                id INT AUTO_INCREMENT NOT NULL,
                date DATETIME NOT NULL,
                region VARCHAR(255),
                siret INT,
                filename VARCHAR(255) NOT NULL,
                line INT NOT NULL,
                message TEXT NOT NULL,
                PRIMARY KEY (id)
);


CREATE TABLE r_naf_code (
                code VARCHAR(6) NOT NULL,
                label VARCHAR(255) NOT NULL,
                PRIMARY KEY (code)
);


CREATE TABLE r_category_has_naf_code (
                id_category INT NOT NULL,
                code_naf_code VARCHAR(6) NOT NULL,
                PRIMARY KEY (id_category, code_naf_code)
);


CREATE TABLE r_department (
                code VARCHAR(3) NOT NULL,
                name VARCHAR(255) NOT NULL,
                PRIMARY KEY (code)
);


CREATE TABLE t_craftsperson (
                id INT AUTO_INCREMENT NOT NULL,
                siret VARCHAR(14) NOT NULL,
                name VARCHAR(255) NOT NULL,
                address_1 VARCHAR(255) NOT NULL,
                address_2 VARCHAR(255),
                zip_code VARCHAR(100) NOT NULL,
                zip_code_label VARCHAR(255) NOT NULL,
                phone VARCHAR(50),
                email VARCHAR(100),
                website VARCHAR(255),
                is_enabled BOOLEAN DEFAULT 1 NOT NULL,
                cma_code VARCHAR(3) NOT NULL,
                is_reparactor BOOLEAN DEFAULT 0 NOT NULL,
                is_error BOOLEAN DEFAULT 0 NOT NULL,
                reparactor_description TEXT,
                reparactor_hours TEXT,
                reparactor_services TEXT,
                reparactor_certificates TEXT,
                creation_date DATETIME DEFAULT NOW() NOT NULL,
                is_updated BOOLEAN DEFAULT 0 NOT NULL,
                update_date DATETIME,
                latitude DOUBLE PRECISION,
                longitude DOUBLE PRECISION,
                geocoding_status VARCHAR(100) DEFAULT "nc" NOT NULL,
                logo_file VARCHAR(255),
                likes INT NOT NULL,
                other_info TEXT,
                PRIMARY KEY (id)
);


CREATE TABLE t_craftsperson_like (
                id INT AUTO_INCREMENT NOT NULL,
                id_craftsperson INT NOT NULL,
                email VARCHAR(100) NOT NULL,
                confirmation_token VARCHAR(255) NOT NULL,
                is_confirmed BOOLEAN DEFAULT 0 NOT NULL,
                PRIMARY KEY (id)
);


CREATE TABLE t_craftsperson_has_naf_code (
                id_craftsperson INT NOT NULL,
                naf_code VARCHAR(6) NOT NULL,
                PRIMARY KEY (id_craftsperson, naf_code)
);


CREATE TABLE t_user (
                id INT AUTO_INCREMENT NOT NULL,
                username VARCHAR(180) NOT NULL,
                username_canonical VARCHAR(180) NOT NULL,
                email VARCHAR(180) NOT NULL,
                email_canonical VARCHAR(180) NOT NULL,
                enabled BOOLEAN NOT NULL,
                salt VARCHAR(255),
                password VARCHAR(255) NOT NULL,
                last_login DATETIME,
                confirmation_token VARCHAR(255),
                password_requested_at DATETIME,
                roles TEXT NOT NULL,
                firstname VARCHAR(50) NOT NULL,
                lastname VARCHAR(50) NOT NULL,
                phone VARCHAR(50),
                is_mail_contact BOOLEAN DEFAULT 1 NOT NULL,
                PRIMARY KEY (id)
);


CREATE TABLE t_user_has_department (
                id_user INT NOT NULL,
                code_department VARCHAR(3) NOT NULL,
                PRIMARY KEY (id_user, code_department)
);


ALTER TABLE r_category_has_naf_code ADD CONSTRAINT r_category_r_category_has_naf_code_fk
FOREIGN KEY (id_category)
REFERENCES r_category (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE t_craftsperson_has_naf_code ADD CONSTRAINT r_naf_code_t_craftsperson_has_naf_code_fk
FOREIGN KEY (naf_code)
REFERENCES r_naf_code (code)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE r_category_has_naf_code ADD CONSTRAINT r_naf_code_r_category_has_naf_code_fk
FOREIGN KEY (code_naf_code)
REFERENCES r_naf_code (code)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE t_craftsperson ADD CONSTRAINT r_department_t_craftsperson_fk
FOREIGN KEY (cma_code)
REFERENCES r_department (code)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE t_user_has_department ADD CONSTRAINT r_department_t_user_has_department_fk
FOREIGN KEY (code_department)
REFERENCES r_department (code)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE t_craftsperson_has_naf_code ADD CONSTRAINT t_craftsperson_t_craftsperson_has_naf_code_fk
FOREIGN KEY (id_craftsperson)
REFERENCES t_craftsperson (id)
ON DELETE CASCADE
ON UPDATE NO ACTION;

ALTER TABLE t_craftsperson_like ADD CONSTRAINT t_craftsperson_t_craftsperson_like_request_fk
FOREIGN KEY (id_craftsperson)
REFERENCES t_craftsperson (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE t_user_has_department ADD CONSTRAINT t_user_t_user_has_department_fk
FOREIGN KEY (id_user)
REFERENCES t_user (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION;